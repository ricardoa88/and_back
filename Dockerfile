FROM ubuntu:18.04
WORKDIR /packages

ENV ACCEPT_EULA=Y
RUN apt-get update \
 && apt-get install -y wget

RUN wget https://packages.microsoft.com/config/ubuntu/18.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb

RUN apt-get install -y software-properties-common \
    && add-apt-repository universe \
    && apt-get update \
    && apt-get install -y apt-transport-https dotnet-sdk-3.0 aspnetcore-runtime-3.0

ENV DOTNET_CLI_TELEMETRY_OPTOUT=false

WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .
COPY src/*.csproj ./src/
RUN dotnet restore

COPY src/. ./src/
WORKDIR /app/src
RUN dotnet build  -c Release -o /app/build

RUN dotnet publish -c Release -o /app/publish

ENTRYPOINT ["dotnet", "/app/publish/tramites-servicios-webapi.dll"]

