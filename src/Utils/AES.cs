using System;
using System.IO;
using System.Security.Cryptography;

namespace src.Utils
{
    public class AES
    {
        public static string cifrar(string mensaje, byte[] llave, byte[] IV) {
            byte[] encrypted;

            using (Aes aesAlg = Aes.Create()) {
                aesAlg.Key = llave;
                aesAlg.IV = IV;
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                using (MemoryStream msEncrypt = new MemoryStream()) {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write)) {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt)) {
                            swEncrypt.Write(mensaje);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(encrypted);
        }
        public static string descifrar(string mensaje, byte[] llave, byte[] IV) {
            string plaintext = null;
            byte[] encrypted = Convert.FromBase64String(mensaje);

            using (Aes aesAlg = Aes.Create()) {
                aesAlg.Key = llave;
                aesAlg.IV = IV;
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
                using (MemoryStream msDecrypt = new MemoryStream(encrypted)) {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)) {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt)) {
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }
    }
}