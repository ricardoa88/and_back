﻿using JsonApiDotNetCore.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using tramites_servicios_webapi.Areas.ServiciosNoSuit.Models;
using tramites_servicios_webapi.Areas.ServiciosSuit.Models;
using tramites_servicios_webapi.Models;


namespace tramites_servicios_webapi {
    public class Startup {
        public Startup (IConfiguration configuration, IWebHostEnvironment env) {
            _env = env;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment _env { get; set; }

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            string connectionString = Configuration.GetConnectionString ("DefaultConnection");
            string suitString = Configuration.GetConnectionString ("SuitConnection");

            services.AddDbContext<TramiteContext> (opt => opt.UseSqlServer (connectionString));
            services.AddScoped<TramiteContext> ();
            services.AddJsonApi<TramiteContext> (opt => {
                opt.Namespace = "api";
                opt.DefaultPageSize = 10;
            });

            services.AddCors(options =>
            {
                options.AddPolicy("AllowOrigin",
                builder =>
                {
                    builder.AllowAnyOrigin()
                     .AllowAnyMethod()
                     .AllowAnyHeader();
                });
            });

            /*services.AddCors(c =>  
            {  
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());  
            });*/

            services.AddDbContext<SuitContext> (opt => opt.UseSqlServer (suitString));
            services.AddScoped<SuitContext> ();

            services.AddDbContext<ServiciosSuitContext> (opt => opt.UseSqlServer (suitString));
            services.AddScoped<ServiciosSuitContext> ();

            services.AddDbContext<ServiciosNoSuitContext> (opt => opt.UseSqlServer (connectionString));
            services.AddScoped<ServiciosNoSuitContext> ();

            services.AddControllers ();



            if (_env.IsDevelopment () || _env.IsStaging ()) {
                // Register the Swagger generator, defining 1 or more Swagger documents
                services.AddSwaggerGen (c => {
                    c.SwaggerDoc ("v1", new OpenApiInfo { Title = "Tramites y Servicios - API", Version = "v1" });
                });
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app) {
            if (_env.IsDevelopment () || _env.IsStaging ()) {
                // Enable middleware to serve generated Swagger as a JSON endpoint.
                app.UseSwagger ();

                // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
                // specifying the Swagger JSON endpoint.
                app.UseSwaggerUI (c => {
                    c.SwaggerEndpoint ("/swagger/v1/swagger.json", "Tramites y Servicios - V1");
                });

                app.UseDeveloperExceptionPage ();
            } else {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts ();
            }

            /*app.UseCors (builder => {
                builder.WithOrigins ("*");
            });*/

            //

            app.UseHttpsRedirection ();

            app.UseRouting ();

             app.UseCors(builder => builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());   
 

            //app.UseCors(MyAllowSpecificOrigins);
            //app.UseCors(options => options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());  

            app.UseAuthorization ();

            app.UseEndpoints (endpoints => {
                endpoints.MapControllers ();
            });
        }
    }
}