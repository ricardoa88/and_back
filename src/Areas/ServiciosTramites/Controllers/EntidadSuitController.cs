﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JsonApiDotNetCore.Controllers;
using JsonApiDotNetCore.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using tramites_servicios_webapi.Models;


namespace tramites_servicios_webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EntidadSuitController : ControllerBase
    //ControllerBase
    {
        private SuitContext contexto;
        public EntidadSuitController(SuitContext contextoBD)
        {
            //var optionsBuilder = new DbContextOptionsBuilder<SuitContext>();
            //contexto = new SuitContext(optionsBuilder.Options);

            contexto = contextoBD;
        }

        //[HttpGet]
        //public override async Task<IActionResult> GetAsync()
        //=> await base.GetAsync();


        [HttpGet("GetUrlEntidadByItem/{idTramite}", Name = "GetUrlEntidadByItem")]
        public ActionResult<EntidadUrl> GetUrlEntidadByItem(string idTramite)
        {
            var rt = contexto.VM_SGP_INSTITUCION;
            var re = contexto.VM_CT_TRAMITE;
            EntidadUrl model = new EntidadUrl();
            model = re.Where(res => res.NUMERO == idTramite)
                                        .Join(rt,
                                              res => res.INSTITUCION_ID,
                                              rts => rts.ID,
                            (rts, res) => new EntidadUrl
                            {
                                IdTramite = rts.NUMERO,
                                UrlEntidad = res.PAGINA_WEB
                            }).FirstOrDefault();

            return model;
                //Json(recursostramite);
        }

    }
}
