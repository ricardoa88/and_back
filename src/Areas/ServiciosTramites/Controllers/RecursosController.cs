﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JsonApiDotNetCore.Controllers;
using JsonApiDotNetCore.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using tramites_servicios_webapi.Models;

namespace tramites_servicios_webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecursosController : JsonApiController<TBL_RECURSOS>
    //ControllerBase
    {

        private TramiteContext _context;
        public RecursosController(TramiteContext context,
                    IJsonApiContext jsonApiContext,
                   IResourceService<TBL_RECURSOS> resourceService,
                   ILoggerFactory loggerFactory) : base(jsonApiContext, resourceService, loggerFactory)
        {
            _context = context;
        }

        //To-Do JsonAPi
        [HttpGet]
        public override async Task<IActionResult> GetAsync()
        => await base.GetAsync();


        [HttpGet("TramiteGet/{idTramite}", Name = "GetRecursosTramite")]
        public ActionResult<List<RecursosList>> TramiteGet(string idTramite)
        {
            List<RecursosList> recursosList = new List<RecursosList>();
            recursosList = (from rt in _context.TBL_RECURSO_TRAMITES
                            join re in _context.TBL_RECURSOSS on rt.RECURSO_ID equals re.RECURSO_ID
                            join ti in _context.TBL_TIPO_RECURSOSS on re.TIPO_RECURSO_ID equals ti.TIPO_RECURSO_ID
                            where rt.TRAMITE_NUMERO == idTramite
                            orderby re.ORDEN
                            select new RecursosList
                            {
                                IdRecurso = rt.RECURSO_TRAMITE_ID,
                                Nombre = re.NOMBRE,
                                UrlRecurso = re.URL_RECURSO,
                                TipoRecurso = re.TIPO_RECURSO_ID
                            }).ToList();

            if (recursosList.Count < 1)
            {

                recursosList = (from rt in _context.TblRecursoOtrosServicios
                                join re in _context.TBL_RECURSOSS on rt.RecursosId equals re.RECURSO_ID
                                where rt.OtrosServiciosId == idTramite
                                orderby re.ORDEN
                                select new RecursosList
                                {
                                    Nombre = re.NOMBRE,
                                    UrlRecurso = re.URL_RECURSO
                                }).ToList();

            }


            return Json(recursosList);
        }

        [HttpGet("GetRecursosAreaTramite/{idTramite}", Name = "GetRecursosAreaTramite")]
        public ActionResult<List<RecursosList>> GetRecursosAreaTramite(string idTramite)
        {
            List<RecursosList> recursosList = new List<RecursosList>();
            recursosList = (from rt in _context.TBL_RECURSO_TRAMITES
                            join re in _context.TBL_RECURSOSS on rt.RECURSO_ID equals re.RECURSO_ID
                            join ti in _context.TBL_TIPO_RECURSOSS on re.TIPO_RECURSO_ID equals ti.TIPO_RECURSO_ID
                            where rt.TRAMITE_NUMERO == idTramite
                            orderby re.ORDEN
                            select new RecursosList
                            {
                                IdRecurso = rt.RECURSO_TRAMITE_ID,
                                Nombre = re.NOMBRE,
                                UrlRecurso = re.URL_RECURSO,
                                TipoRecurso = re.TIPO_RECURSO_ID
                            }).ToList();

            return Json(recursosList);
        }

        // POST: api/TramitesRecursos
        [HttpPost("CreateRecursosTramite/{idTramite}", Name = "CreateRecursosTramite")]
        public ActionResult CreateRecursosTramite(string idTramite, List<RecursosList> recursos)
        {
            try
            {
                int RECURSO_ID;
                var registrosBD = (from rt in _context.TBL_RECURSO_TRAMITES
                                      where rt.TRAMITE_NUMERO == idTramite && rt.CODIGO_ESTADO == 1
                                      select rt).ToList();

                if (registrosBD.Count == 0)
                {
                    foreach (var recurso in recursos)
                    {
                        TBL_RECURSOS recursoinsert = new TBL_RECURSOS();
                        recursoinsert.NOMBRE = recurso.Nombre;

                        recursoinsert.CODIGO_ESTADO = 1;
                        recursoinsert.URL_RECURSO = recurso.UrlRecurso;
                        recursoinsert.TIPO_RECURSO_ID = recurso.TipoRecurso;
                        recursoinsert.FECHA_CREACION = DateTime.Now;

                        _context.TBL_RECURSOSS.Add(recursoinsert);
                        _context.SaveChanges();
                        RECURSO_ID = recursoinsert.RECURSO_ID;

                        TBL_RECURSO_TRAMITE recursoTramite = new TBL_RECURSO_TRAMITE();
                        recursoTramite.RECURSO_ID = RECURSO_ID;
                        recursoTramite.TRAMITE_NUMERO = idTramite;
                        recursoTramite.CODIGO_ESTADO = 1;
                        recursoTramite.FECHA_CREACION = DateTime.Now;

                        _context.TBL_RECURSO_TRAMITES.Add(recursoTramite);
                        _context.SaveChanges();
                    }
                    //CreatedAtAction("GetTblMasUsados", new { id = tblMasUsados.MasUsadosId }, tblMasUsados);
                }
                else
                {
                    foreach (var recurso in recursos)
                    {
                        var existeRecurso = registrosBD.SingleOrDefault(x => x.RECURSO_TRAMITE_ID == recurso.IdRecurso);

                        if (existeRecurso != null)
                        {
                            var guardar = (from rt in _context.TBL_RECURSO_TRAMITES
                                           join re in _context.TBL_RECURSOSS on rt.RECURSO_ID equals re.RECURSO_ID
                                           where rt.RECURSO_TRAMITE_ID == existeRecurso.RECURSO_TRAMITE_ID
                                           select re).First();

                            guardar.NOMBRE = recurso.Nombre;
                            guardar.URL_RECURSO = recurso.UrlRecurso;
                            guardar.TIPO_RECURSO_ID = recurso.TipoRecurso;

                            _context.TBL_RECURSOSS.Update(guardar);
                            _context.SaveChanges();
                        }
                        else
                        {
                            TBL_RECURSOS recursoinsert = new TBL_RECURSOS();
                            recursoinsert.NOMBRE = recurso.Nombre;

                            recursoinsert.CODIGO_ESTADO = 1;
                            recursoinsert.URL_RECURSO = recurso.UrlRecurso;
                            recursoinsert.TIPO_RECURSO_ID = recurso.TipoRecurso;
                            recursoinsert.FECHA_CREACION = DateTime.Now;

                            _context.TBL_RECURSOSS.Add(recursoinsert);
                            _context.SaveChanges();
                            RECURSO_ID = recursoinsert.RECURSO_ID;

                            TBL_RECURSO_TRAMITE recursoTramite = new TBL_RECURSO_TRAMITE();
                            recursoTramite.RECURSO_ID = RECURSO_ID;
                            recursoTramite.TRAMITE_NUMERO = idTramite;
                            recursoTramite.CODIGO_ESTADO = 1;
                            recursoTramite.FECHA_CREACION = DateTime.Now;

                            _context.TBL_RECURSO_TRAMITES.Add(recursoTramite);
                            _context.SaveChanges();
                        } 
                    }

                    foreach (var registro in registrosBD)
                    {
                        var existeRecurso = recursos.SingleOrDefault(x => x.IdRecurso == registro.RECURSO_TRAMITE_ID);
                        if(existeRecurso == null)
                        {
                            _context.TBL_RECURSO_TRAMITES.Remove(registro);

                            var eliminar = (from rt in _context.TBL_RECURSO_TRAMITES
                                           join re in _context.TBL_RECURSOSS on rt.RECURSO_ID equals re.RECURSO_ID
                                           where rt.RECURSO_TRAMITE_ID == registro.RECURSO_TRAMITE_ID
                                           select re).First();

                            _context.TBL_RECURSOSS.Remove(eliminar);
                            _context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception x)
            {              
                string messageNotFound = "No se logro realizar el registro "+ x.Message.ToString();
                var result = new { message = messageNotFound, StatusCode = 404 };
                return NotFound(result);
            }

            return Json(Ok());
        }

       


    }
}
