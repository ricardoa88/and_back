﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using JsonApiDotNetCore.Controllers;
using JsonApiDotNetCore.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using tramites_servicios_webapi.Models;

namespace tramites_servicios_webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TramiteController : JsonApiController<TBL_TRAMITES>
    {

        private TramiteContext _context;
        private SuitContext _contextSuit;

        public TramiteController(TramiteContext context, SuitContext contextSuit,
            IJsonApiContext jsonApiContext,
            IResourceService<TBL_TRAMITES> resourceService,
            ILoggerFactory loggerFactory)
        : base(jsonApiContext, resourceService, loggerFactory)
        {
            _context = context;
            _contextSuit = contextSuit;
        }

        [HttpGet]
        public override async Task<IActionResult> GetAsync()
        => await base.GetAsync();

        [HttpGet("GetTramitesEmbebidos/{numPage}", Name = "GetTramitesEmbebidos")]
        public ActionResult<List<TramitesEmbebidos>> GetTramitesEmbebidos(int numPage)
        {
            List<TramitesEmbebidos> entryPoint = new List<TramitesEmbebidos>();
            List<VM_SGP_INSTITUCION> instituciones = new List<VM_SGP_INSTITUCION>();
            instituciones = (from sg in _contextSuit.VM_SGP_INSTITUCION
                             select new VM_SGP_INSTITUCION { ID = sg.ID, NOMBRE = sg.NOMBRE }).ToList();
            entryPoint = (from tram in _context.TBL_TRAMITES
                          join tit in _context.TBL_INTEGRACION_TRAMITESS on tram.NUMERO equals tit.NUMERO
                          where tit.EMBEBIDO == "Si"
                          select new TramitesEmbebidos
                          {
                              IntegradorId = tit.INTEGRADOR_ID,
                              Numero = tit.NUMERO,
                              NombreTramite = tram.NOMBRE,
                              NombreEntidad = tram.INSTITUCION_NOMBRE,
                              Embebido = tit.EMBEBIDO,
                              UrlTramite = tit.URL_TRAMITE,
                              FechaCreacion = tit.FECHA_CREACION,
                              FechaModificacion = tit.FECHA_MODIFICACION,
                              EstadoId = tit.ESTADO_ID,
                              UsuarioCreador = tit.USUARIO_CREADOR_ENTIDAD,
                              UsuarioModificacion = tit.USUARIO_MODIFICACION_ENTIDAD,
                              Municipio = tram.MUNICIPIO_NOMBRE,
                              Departamento = tram.DEPARTAMENTO_NOMBRE
                          })
                              .Union
                              (from tos in _context.TBL_OTROS_SERVICIOSS
                               join tit in _context.TBL_INTEGRACION_TRAMITESS on tos.OTROS_SERVICIOS_ID equals tit.NUMERO
                               join mun in _context.Municipios on tos.MUNICIPIO_CODIGO_DANE equals mun.MunCodigo
                               join dep in _context.Departamentos on mun.DepCodigo equals dep.DepCodigo
                               join sgpint in instituciones on tos.ENTIDAD_ID equals sgpint.ID
                               where tit.EMBEBIDO == "Si"
                               select new TramitesEmbebidos
                               {
                                   IntegradorId = tit.INTEGRADOR_ID,
                                   Numero = tit.NUMERO,
                                   NombreTramite = tos.NOMBRE,
                                   NombreEntidad = sgpint.NOMBRE,
                                   Embebido = tit.EMBEBIDO,
                                   UrlTramite = tit.URL_TRAMITE,
                                   FechaCreacion = tit.FECHA_CREACION,
                                   FechaModificacion = tit.FECHA_MODIFICACION,
                                   EstadoId = tit.ESTADO_ID,
                                   UsuarioCreador = tit.USUARIO_CREADOR_ENTIDAD,
                                   UsuarioModificacion = tit.USUARIO_MODIFICACION_ENTIDAD,
                                   Municipio = mun.MunNombre,
                                   Departamento = dep.DepNombre
                               }
                                ).Skip((numPage - 1)*10).Take(10)
                              .ToList();

            return Json(entryPoint.OrderBy(p => p.NombreEntidad));
        }


        [HttpGet("GetInfoTramitesById/{idTramite}", Name = "GetInfoTramitesById")]
        public IActionResult GetInfoTramitesById(string idTramite)
        {
            TBL_TRAMITES validaUnicidad = new TBL_TRAMITES();
            validaUnicidad = (from tit in _context.TBL_INTEGRACION_TRAMITESS
                              where tit.NUMERO == idTramite
                              select new TBL_TRAMITES
                              { NUMERO = tit.NUMERO }).FirstOrDefault();

            if (validaUnicidad != null)
            {
                string messageNotFound = "El id del tramite "+ idTramite + " ya se encuentra asociado a un tramite embebido";
                var result = new { message = messageNotFound, StatusCode = 404 };
                return Json(result);
            }
            TramitesEmbebidos validateTramiteSuit = new TramitesEmbebidos();
            TramitesEmbebidos validateTramiteNoSuit = new TramitesEmbebidos();
            validateTramiteSuit = (from tram in _context.TBL_TRAMITES
                                       where tram.NUMERO == idTramite
                                       select new TramitesEmbebidos
                                       {
                                           Numero = tram.NUMERO,
                                           NombreTramite = tram.NOMBRE,
                                           NombreEntidad = tram.INSTITUCION_NOMBRE,
                                           Municipio = tram.MUNICIPIO_NOMBRE,
                                           Departamento = tram.DEPARTAMENTO_NOMBRE
                                       }).FirstOrDefault();


            if (validateTramiteSuit != null )
            {
                return Json(validateTramiteSuit);
            }
            List<VM_SGP_INSTITUCION> instituciones = new List<VM_SGP_INSTITUCION>();
            instituciones = (from sg in _contextSuit.VM_SGP_INSTITUCION
                             select new VM_SGP_INSTITUCION { ID = sg.ID, NOMBRE = sg.NOMBRE }).ToList();
            validateTramiteNoSuit = (from tos in _context.TBL_OTROS_SERVICIOSS
                                        join mun in _context.Municipios on tos.MUNICIPIO_CODIGO_DANE equals mun.MunCodigo
                                        join dep in _context.Departamentos on mun.DepCodigo equals dep.DepCodigo
                                        join sgpint in instituciones on tos.ENTIDAD_ID equals sgpint.ID
                                        where tos.OTROS_SERVICIOS_ID == idTramite
                                       select new TramitesEmbebidos
                                       {
                                           Numero = tos.OTROS_SERVICIOS_ID,
                                           NombreTramite = tos.NOMBRE,
                                           NombreEntidad = sgpint.NOMBRE,
                                           Municipio = mun.MunNombre,
                                           Departamento = dep.DepNombre
                                       }).FirstOrDefault();

            if (validateTramiteNoSuit != null)
            {
                return Json(validateTramiteNoSuit);
            }

            string messageNotFound2 = "El id del tramite "+ idTramite + " no se encuentra registrado.";
            var result2 = new { message = messageNotFound2, StatusCode = 400 };
            return Json(result2);
            
        }


    }
}
