﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JsonApiDotNetCore.Controllers;
using JsonApiDotNetCore.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using tramites_servicios_webapi.Models;


namespace tramites_servicios_webapi.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EtapasBarraController : JsonApiController<TBL_ETAPAS>

    {
        private TramiteContext _context;

        public EtapasBarraController(TramiteContext context,
                    IJsonApiContext jsonApiContext,
                   IResourceService<TBL_ETAPAS> resourceService,
                   ILoggerFactory loggerFactory) : base(jsonApiContext, resourceService, loggerFactory)
        {
            _context = context;
        }


        [HttpGet]
        public override async Task<IActionResult> GetAsync()
            => await base.GetAsync();


        [HttpGet("GetDataBarraTramite/{idTramite}", Name = "GetDataBarraTramite")]
        public ActionResult<List<EtapasBarrasList>> GetDataBarraTramite(string idTramite)
        {

            var entryPoint = (from et in _context.TBL_ETAPASS
                              join em in _context.TBL_ETAPA_MOMENTOS on et.CODIGO_ETAPA equals em.CODIGO_ETAPA into EtapaMomento
                              from etm in EtapaMomento.DefaultIfEmpty()
                              where etm.TRAMITE_NUMERO == idTramite
                              select new
                              {
                                  ItemId = idTramite,
                                  Paso = et.ORDEN,
                                  Titulo = et.DESCRIPCION_ETAPA,
                                  Etapa = etm.DESCRIPCION_ETAPA
                              }).Union(from et2 in _context.TBL_ETAPASS
                                       where !(from et3 in _context.TBL_ETAPASS
                                               join em in _context.TBL_ETAPA_MOMENTOS on
                                               et3.CODIGO_ETAPA equals em.CODIGO_ETAPA into TablaEtapasMomentos2
                                               from TEM in TablaEtapasMomentos2.DefaultIfEmpty()
                                               where TEM.TRAMITE_NUMERO == idTramite
                                               select et3.DESCRIPCION_ETAPA).Distinct().Contains(et2.DESCRIPCION_ETAPA) select new
                                               {
                                                   ItemId = idTramite,
                                                   Paso = et2.ORDEN,
                                                   Titulo = et2.DESCRIPCION_ETAPA,
                                                   Etapa = ""
                                       }).Take(10).ToList();

            



            var results2 = entryPoint.GroupBy(p => p.Titulo, (k, c) => 
                new EtapasBarrasList() { Titulo = k, Etapas = c.Select(cs => cs.Etapa).ToArray(), ItemId = c.Select(cs=>cs.ItemId).FirstOrDefault().ToString(), Paso = c.Select(cs => cs.Paso).FirstOrDefault().ToString() }).ToList();


            return Json(results2.OrderBy(p=>p.Paso));
        }


        [HttpGet("GetUrlTramiteLinea/{idTramite}", Name = "GetUrlTramiteLinea")]
        public ActionResult<UrlTramiteDesarrollado> GetUrlTramiteLinea(string idTramite)
        {

            var entryPoint = (from tit in _context.TBL_INTEGRACION_TRAMITESS
                              join tra in _context.TBL_TRAMITES on tit.NUMERO equals tra.NUMERO into TablaTramites
                              from ta in TablaTramites.DefaultIfEmpty()
                              join tos in _context.TBL_OTROS_SERVICIOSS on tit.NUMERO equals tos.OTROS_SERVICIOS_ID into TablaOtrosServicios
                              from toss in TablaOtrosServicios.DefaultIfEmpty()
                              where tit.NUMERO == idTramite
                              select new UrlTramiteDesarrollado
                              {
                                  IdTramite = tit.NUMERO,
                                  UrlTramite = tit.URL_TRAMITE
                              }).FirstOrDefault();

            return Json(entryPoint);
        }



    }
}