﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using tramites_servicios_webapi.Models;

namespace tramites_servicios_webapi.Areas.ServiciosTramites.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TramitesEmbebidosController : ControllerBase
    {
        private readonly TramiteContext _context;
        private SuitContext _contextSuit;

        public TramitesEmbebidosController(TramiteContext context, SuitContext contextSuit)
        {
            _context = context;
            _contextSuit = contextSuit;
        }

        // GET: api/TramitesEmbebidos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TBL_INTEGRACION_TRAMITES>>> Get()
        {
            return await _context.TBL_INTEGRACION_TRAMITESS.ToListAsync();
        }

        // GET: api/TramitesEmbebidos/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TBL_INTEGRACION_TRAMITES>> GetEmbebidosById(int id)
        {
            var tBL_INTEGRACION_TRAMITES = await _context.TBL_INTEGRACION_TRAMITESS.FindAsync(id);

            if (tBL_INTEGRACION_TRAMITES == null)
            {
                return NotFound();
            }

            return tBL_INTEGRACION_TRAMITES;
        }

        // GET: api/TramitesEmbebidos/5
        [HttpGet("GetEmbebidosByIdTramite/{idTramite}")]
        public async Task<ActionResult<TBL_INTEGRACION_TRAMITES>> GetEmbebidosByIdTramite(string idTramite)
        {
            var tBL_INTEGRACION_TRAMITES = await _context.TBL_INTEGRACION_TRAMITESS.SingleOrDefaultAsync( p =>p.NUMERO == idTramite && p.EMBEBIDO == "Si");

            if (tBL_INTEGRACION_TRAMITES == null)
            {
                return NotFound();
            }

            return tBL_INTEGRACION_TRAMITES;
        }

        // PUT: api/TramitesEmbebidos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTramiteEmbebido(int id, TBL_INTEGRACION_TRAMITES tBL_INTEGRACION_TRAMITES)
        {
            if (id != tBL_INTEGRACION_TRAMITES.INTEGRADOR_ID)
            {
                return BadRequest();
            }

            TBL_INTEGRACION_TRAMITES TramiteEmbebido = new TBL_INTEGRACION_TRAMITES();
            TramiteEmbebido = (from tit in _context.TBL_INTEGRACION_TRAMITESS
                               where tit.URL_TRAMITE == tBL_INTEGRACION_TRAMITES.URL_TRAMITE
                               select new TBL_INTEGRACION_TRAMITES { INTEGRADOR_ID = tit.INTEGRADOR_ID }).FirstOrDefault();

            if (TramiteEmbebido != null && TramiteEmbebido.INTEGRADOR_ID != tBL_INTEGRACION_TRAMITES.INTEGRADOR_ID)
            {
                string messageNotFound = "El url del tramite ya se encuentra asociado a un tramite embebido";
                var result = new { message = messageNotFound, StatusCode = 404 };
                return NotFound(result);
            }

            
            try
            {
                HttpWebRequest request = WebRequest.Create(tBL_INTEGRACION_TRAMITES.URL_TRAMITE) as HttpWebRequest;
                request.Method = "HEAD";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                response.Close();
            }
            catch
            {
                string messageNotFound = "El url del tramite no es valida";
                var result = new { message = messageNotFound, StatusCode = 404 };
                return NotFound(result);
            }

            _context.Entry<TBL_INTEGRACION_TRAMITES>(tBL_INTEGRACION_TRAMITES).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TBL_INTEGRACION_TRAMITESExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TramitesEmbebidos
        [HttpPost]
        public async Task<ActionResult<TBL_INTEGRACION_TRAMITES>> Create(TBL_INTEGRACION_TRAMITES tBL_INTEGRACION_TRAMITES)
        {
            var TramiteEmbebido = await _context.TBL_INTEGRACION_TRAMITESS.SingleOrDefaultAsync(p => p.NUMERO == tBL_INTEGRACION_TRAMITES.NUMERO);

            if (TramiteEmbebido != null)
            {
                string messageNotFound = "El id del tramite " + tBL_INTEGRACION_TRAMITES.NUMERO + " ya se encuentra asociado a un tramite embebido";
                var result = new { message = messageNotFound, StatusCode = 404 };
                return NotFound(result);
            }

            TramiteEmbebido = await _context.TBL_INTEGRACION_TRAMITESS.SingleOrDefaultAsync(p => p.URL_TRAMITE == tBL_INTEGRACION_TRAMITES.URL_TRAMITE);

            if (TramiteEmbebido != null)
            {
                string messageNotFound = "El url del tramite ya se encuentra asociado a un tramite embebido";
                var result = new { message = messageNotFound, StatusCode = 404 };
                return NotFound(result);
            }

            try
            {
                HttpWebRequest request = WebRequest.Create(tBL_INTEGRACION_TRAMITES.URL_TRAMITE) as HttpWebRequest;
                request.Method = "HEAD";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                response.Close();
            }
            catch
            {
                string messageNotFound = "El url del tramite no es valida";
                var result = new { message = messageNotFound, StatusCode = 404 };
                return NotFound(result);
            }


            _context.TBL_INTEGRACION_TRAMITESS.Add(tBL_INTEGRACION_TRAMITES);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEmbebidosById", new { id = tBL_INTEGRACION_TRAMITES.INTEGRADOR_ID }, tBL_INTEGRACION_TRAMITES);
        }

        
        private bool TBL_INTEGRACION_TRAMITESExists(int id)
        {
            return _context.TBL_INTEGRACION_TRAMITESS.Any(e => e.INTEGRADOR_ID == id);
        }


        // Post: api/TramitesEmbebidos/GetTramiteEmbebidoFilters/5
        [HttpPost("GetTramiteEmbebidoFilters/{numPage}")]
        public ActionResult<List<TramitesEmbebidos>> GetTramiteEmbebidoFilters(int numPage, FiltrosTramitesEmbebidos filters)
        {
            List<TramitesEmbebidos> entryPoint = new List<TramitesEmbebidos>();
            List<VM_SGP_INSTITUCION> instituciones = new List<VM_SGP_INSTITUCION>();
            instituciones = (from sg in _contextSuit.VM_SGP_INSTITUCION
                             select new VM_SGP_INSTITUCION { ID = sg.ID, NOMBRE = sg.NOMBRE }).ToList();
            entryPoint = (from tram in _context.TBL_TRAMITES
                          join tit in _context.TBL_INTEGRACION_TRAMITESS on tram.NUMERO equals tit.NUMERO
                          where tit.EMBEBIDO == "Si"
                          select new TramitesEmbebidos
                          {
                              IntegradorId = tit.INTEGRADOR_ID,
                              Numero = tit.NUMERO,
                              NombreTramite = tram.NOMBRE,
                              NombreEntidad = tram.INSTITUCION_NOMBRE,
                              Embebido = tit.EMBEBIDO,
                              UrlTramite = tit.URL_TRAMITE,
                              FechaCreacion = tit.FECHA_CREACION,
                              FechaModificacion = tit.FECHA_MODIFICACION,
                              EstadoId = tit.ESTADO_ID,
                              UsuarioCreador = tit.USUARIO_CREADOR_ENTIDAD,
                              UsuarioModificacion = tit.USUARIO_MODIFICACION_ENTIDAD,
                              Municipio = tram.MUNICIPIO_NOMBRE,
                              Departamento = tram.DEPARTAMENTO_NOMBRE
                          })
                              .Union
                              (from tos in _context.TBL_OTROS_SERVICIOSS
                               join tit in _context.TBL_INTEGRACION_TRAMITESS on tos.OTROS_SERVICIOS_ID equals tit.NUMERO
                               join mun in _context.Municipios on tos.MUNICIPIO_CODIGO_DANE equals mun.MunCodigo
                               join dep in _context.Departamentos on mun.DepCodigo equals dep.DepCodigo
                               join sgpint in instituciones on tos.ENTIDAD_ID equals sgpint.ID
                               where tit.EMBEBIDO == "Si"
                               select new TramitesEmbebidos
                               {
                                   IntegradorId = tit.INTEGRADOR_ID,
                                   Numero = tit.NUMERO,
                                   NombreTramite = tos.NOMBRE,
                                   NombreEntidad = sgpint.NOMBRE,
                                   Embebido = tit.EMBEBIDO,
                                   UrlTramite = tit.URL_TRAMITE,
                                   FechaCreacion = tit.FECHA_CREACION,
                                   FechaModificacion = tit.FECHA_MODIFICACION,
                                   EstadoId = tit.ESTADO_ID,
                                   UsuarioCreador = tit.USUARIO_CREADOR_ENTIDAD,
                                   UsuarioModificacion = tit.USUARIO_MODIFICACION_ENTIDAD,
                                   Municipio = mun.MunNombre,
                                   Departamento = dep.DepNombre
                               }
                                ).Skip((numPage - 1) * 10).Take(10)
                              .ToList();

            if (filters.IdTramite != null)
            {
                entryPoint = entryPoint.Where(c => c.Numero == filters.IdTramite).ToList();
            }

            if (filters.Entidad != null)
            {
                entryPoint = entryPoint.Where(c => c.NombreEntidad.Contains(filters.Entidad) || c.NombreEntidad.Contains((filters.Entidad).ToLower()) || c.NombreEntidad.Contains((filters.Entidad).ToUpper())).ToList();
            }

            if (filters.Estado != 0)
            {
                entryPoint = entryPoint.Where(c => c.EstadoId == filters.Estado).ToList();
            }

            if (filters.OrdenColumna != null && filters.Orden != null)
            {
                
                var param = filters.OrdenColumna;
                var propertyInfo = typeof(TramitesEmbebidos).GetProperty(param);
                //var result = entryPoint.OrderBy(x => propertyInfo.GetValue(x, null));

                if (filters.Orden == "Descendente") {
                    return entryPoint.OrderBy(x => propertyInfo.GetValue(x, null)).Reverse().ToList();

                }

                return entryPoint.OrderBy(x => propertyInfo.GetValue(x, null)).ToList();
            }


            return entryPoint;

        }
    }
}