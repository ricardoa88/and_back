﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tramites_servicios_webapi.Areas.ServiciosTramites.Models;
using tramites_servicios_webapi.Areas.ServiciosSuit.Models;
using tramites_servicios_webapi.Models;

namespace tramites_servicios_webapi.Areas.ServiciosTramites.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MasUsadosController : ControllerBase
    {
        private readonly TramiteContext _context;
        private SuitContext _contextSuit;
        private ServiciosSuitContext _contextServiciosSuit;

        public MasUsadosController(TramiteContext context, SuitContext contextSuit, ServiciosSuitContext contextServiciosSuit)
        {
            _context = context;
            _contextSuit = contextSuit;
            _contextServiciosSuit = contextServiciosSuit;
        }

        // GET: api/MasUsados
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TblMasUsados>>> GetTblMasUsadoss()
        {
            var retorno = await _context.TblMasUsadoss.ToListAsync();
            return retorno;
        }

        // GET: api/MasUsados/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TblMasUsados>> GetTblMasUsados(int id)
        {
            var tblMasUsados = await _context.TblMasUsadoss.FindAsync(id);

            if (tblMasUsados == null)
            {
                return NotFound();
            }

            return tblMasUsados;
        }

        // PUT: api/MasUsados/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTblMasUsados(int id, TblMasUsados tblMasUsados)
        {
            if (id != tblMasUsados.MasUsadosId)
            {
                return BadRequest();
            }

            _context.Entry(tblMasUsados).State = EntityState.Modified;

            var valida = (from mas in _context.TblMasUsadoss
                          where mas.Orden == tblMasUsados.Orden && mas.TramiteNumero != tblMasUsados.TramiteNumero
                          && mas.CodigoEstado == 1
                          select new { TramiteNumero = mas.TramiteNumero,
                                       Orden = mas.Orden}).FirstOrDefault();

             if (valida != null) {
                string messageNotFound = "El tramite "+ valida.TramiteNumero + " ya tiene asignado el orden " +valida.Orden;
                var result = new { message = messageNotFound, StatusCode = 500 };
                return Ok(result);
                
            }
             tblMasUsados.FechaModificacion = DateTime.Now;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblMasUsadosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            var resultTrue = new { message = "Registro actualizado correctamente", StatusCode = 200 };
            return Ok(resultTrue);
        }

        // POST: api/MasUsados
        [HttpPost]
        public async Task<ActionResult<TblMasUsados>> PostTblMasUsados(TblMasUsados tblMasUsados)
        {
            _context.TblMasUsadoss.Add(tblMasUsados);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTblMasUsados", new { id = tblMasUsados.MasUsadosId }, tblMasUsados);
        }

        // DELETE: api/MasUsados/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TblMasUsados>> DeleteTblMasUsados(int id)
        {
            var tblMasUsados = await _context.TblMasUsadoss.FindAsync(id);
            if (tblMasUsados == null)
            {
                return NotFound();
            }

            _context.TblMasUsadoss.Remove(tblMasUsados);
            await _context.SaveChangesAsync();

            return tblMasUsados;
        }

        private bool TblMasUsadosExists(int id)
        {
            return _context.TblMasUsadoss.Any(e => e.MasUsadosId == id);
        }

        // GET: api/MasUsados/GetInfoHomeMasUsados
        [HttpGet("GetInfoHomeMasUsados/", Name = "GetInfoHomeMasUsados")]
        public ActionResult<List<MasUsadosHome>> GetInfoHomeMasUsados()
        {

            List<MasUsadosHome> listMasUsadosHome = new List<MasUsadosHome>();

            List<TblMasUsados> listMasUsados = new List<TblMasUsados>();
            listMasUsados = (from mau in _context.TblMasUsadoss
                             orderby mau.Orden 
                             where mau.CodigoEstado == 1
                             select new TblMasUsados { MasUsadosId = mau.MasUsadosId,
                                 TramiteNumero = mau.TramiteNumero,
                                 Orden = mau.Orden,
                                 CodigoEstado = mau.CodigoEstado,
                                 FechaCreacion = mau.FechaCreacion,
                                 FechaModificacion = mau.FechaModificacion
                             }).Take(5).ToList();

            int index = 0;
            foreach (var usados in listMasUsados) {

                MasUsadosHome masUsadosHome = new MasUsadosHome();


                masUsadosHome.MasUsadosId = usados.MasUsadosId;
                masUsadosHome.Orden = Convert.ToInt32(usados.Orden);
                masUsadosHome.TramiteNumero = usados.TramiteNumero;

                var entidadInfo = (from tra in _contextSuit.VM_CT_TRAMITE
                                   where tra.NUMERO == usados.TramiteNumero
                                   select new { NombreEntidad = tra.INSTITUCION_NOMBRE,
                                       Linea = tra.REALIZADO_MEDIOS_ELECTRONICOS,
                                       NombreTramite = tra.NOMBRE,
                                       TiempoObtencion = tra.TIEMPO_OBTENCION,
                                   }).FirstOrDefault();

                if (entidadInfo == null)
                {
                    var idEntidad = (from ots in _context.TBL_OTROS_SERVICIOSS
                                     where ots.OTROS_SERVICIOS_ID == usados.TramiteNumero
                                     select new { IdEntidad = ots.ENTIDAD_ID,
                                         NombreTramite = ots.NOMBRE }).FirstOrDefault();

                    if (idEntidad != null)
                    {
                        var entidadInfoNoSuit = (from sgp in _contextSuit.VM_SGP_INSTITUCION
                                                 where sgp.ID == idEntidad.IdEntidad
                                                 select new { NombreEntidad = sgp.NOMBRE }).FirstOrDefault();
                        masUsadosHome.NombreTramite = idEntidad.NombreTramite;
                        masUsadosHome.Entidad = entidadInfoNoSuit.NombreEntidad;
                        masUsadosHome.Costo = false;
                        masUsadosHome.Linea = true;
                    }
                    else
                        return NotFound();

                }
                else {
                    masUsadosHome.Entidad = entidadInfo.NombreEntidad;
                    masUsadosHome.NombreTramite = entidadInfo.NombreTramite;
                    masUsadosHome.Linea = entidadInfo.Linea.Equals("TOTALMENTE") ? true : false;
                    masUsadosHome.TiempoObtencion = entidadInfo.TiempoObtencion;

                    if(!masUsadosHome.Linea)
                    {
                        var validateLinea = (from inte in _context.TBL_INTEGRACION_TRAMITESS
                                             where inte.NUMERO == usados.TramiteNumero
                                             select inte).Any();
                        masUsadosHome.Linea = validateLinea;

                    }

                    int costo = (from vp in _contextServiciosSuit.VmCtValorPagos
                                 join ac in _contextServiciosSuit.VmCtAccionCondicions on vp.AccionCondicionId equals ac.AccionCondicionId
                                 where ac.TramiteNumero == usados.TramiteNumero && vp.Valor != null
                                 select vp).Count();
                    masUsadosHome.Costo = costo >= 1 ? true : false;

                }
                listMasUsadosHome.Add(masUsadosHome);

                Console.WriteLine(listMasUsadosHome[index]);

            }

            return listMasUsadosHome;
        }
    }
}
