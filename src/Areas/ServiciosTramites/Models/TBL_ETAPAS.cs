﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Models
{
    public partial class TBL_ETAPAS : Identifiable
    {
        
        [NotMapped]
        public override int Id { get; set; }

        [Key]
        [Attr("CodigoEtapa")]
        public int CODIGO_ETAPA { get; set; }

        [Attr("DescripcionEtapa")]
        public string DESCRIPCION_ETAPA { get; set; }

        [Attr("CodigoEstado")]
        public int? CODIGO_ESTADO { get; set; }

        [Attr("Orden")]
        public byte? ORDEN { get; set; }

        [Attr("UsuarioCreadorEntidad")]
        public string USUARIO_CREADOR_ENTIDAD { get; set; }

        [Attr("FechaCreacion")]
        public DateTime FECHA_CREACION { get; set; }

        [Attr("CodigoEstadoNavigation")]
        public virtual TBL_ESTADOS CODIGO_ESTADONavigation { get; set; }

        [Attr("TblEtapaMomento")]
        public virtual ICollection<TBL_ETAPA_MOMENTO> TBL_ETAPA_MOMENTO { get; set; }

        public TBL_ETAPAS()
        {
            TBL_ETAPA_MOMENTO = new HashSet<TBL_ETAPA_MOMENTO>();
        }

    }
}