﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Models
{
    public partial class TBL_TIPO_RECURSOS
    {
        public TBL_TIPO_RECURSOS()
        {
            TBL_RECURSOS = new HashSet<TBL_RECURSOS>();
        }

        public int TIPO_RECURSO_ID { get; set; }
        public string NOMBRE_TIPO { get; set; }
        public string DESCRIPCION { get; set; }
        public byte[] ICONO_TIPO_RECURSOS { get; set; }
        public int? CODIGO_ESTADO { get; set; }
        public DateTime FECHA_CREACION { get; set; }
        public DateTime? FECHA_MODIFICACION { get; set; }

        public virtual ICollection<TBL_RECURSOS> TBL_RECURSOS { get; set; }
    }
}