﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Models
{
    public partial class TBL_ETAPA_MOMENTO : Identifiable
    {
        [NotMapped]
        public override int Id { get; set; }

        [Key]
        [Attr("CodigoEtapaMomento")]
        public int CODIGO_ETAPA_MOMENTO { get; set; }

        [Attr("DescripcionEtapa")]
        public string DESCRIPCION_ETAPA { get; set; }

        [Attr("CodigoEstado")]
        public int? CODIGO_ESTADO { get; set; }

        [Attr("TramiteNumero")]
        public string TRAMITE_NUMERO { get; set; }

        [Attr("Orden")]
        public int ORDEN { get; set; }

        [Attr("Antecesor")]
        public byte? ANTECESOR { get; set; }

        [Attr("UsuarioCreador")]
        public string USUARIO_CREADOR { get; set; }

        [Attr("FechaCreacion")]
        public DateTime FECHA_CREACION { get; set; }

        [Attr("CodigoEtapa")]
        public int? CODIGO_ETAPA { get; set; }

        [Attr("CodigoEstadoNavigation")]
        public virtual TBL_ESTADOS CODIGO_ESTADONavigation { get; set; }

        [Attr("CodigoEtapaNavigation")]
        public virtual TBL_ETAPAS CODIGO_ETAPANavigation { get; set; }
    }
}