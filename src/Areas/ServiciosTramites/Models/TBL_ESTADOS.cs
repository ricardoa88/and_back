﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Models
{
    public partial class TBL_ESTADOS : Identifiable
    {
        
        [NotMapped]
        public override int Id { get; set; }

        [Key]
        [Attr("CodigoEstado")]
        public int CODIGO_ESTADO { get; set; }

        [Attr("DescripcionEstado")]
        public string DESCRIPCION_ESTADO { get; set; }

        public virtual ICollection<TBL_ETAPAS> TBL_ETAPAS { get; set; }
        public virtual ICollection<TBL_ETAPA_MOMENTO> TBL_ETAPA_MOMENTO { get; set; }

        public TBL_ESTADOS()
        {
            TBL_ETAPAS = new HashSet<TBL_ETAPAS>();
            TBL_ETAPA_MOMENTO = new HashSet<TBL_ETAPA_MOMENTO>();
        }

    }
}