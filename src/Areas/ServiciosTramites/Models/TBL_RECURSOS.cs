﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Models
{
    public partial class TBL_RECURSOS: Identifiable
    {
        [NotMapped]
        public override int Id { get; set; }

        [Key]
        [Attr("Recurso_Id")]
        public int RECURSO_ID { get; set; }

        [Attr("Nombre")]
        public string NOMBRE { get; set; }

        [Attr("Descripcion")]
        public string DESCRIPCION { get; set; }

        [Attr("Codigo_Estado")]
        public int? CODIGO_ESTADO { get; set; }

        [Attr("Orden")]
        public byte? ORDEN { get; set; }

        [Attr("Tipo_Recurso_Id")]
        public int TIPO_RECURSO_ID { get; set; }

        [Attr("Url_Recurso")]
        public string URL_RECURSO { get; set; }

        [Attr("Id_Entidad")]
        public string ID_ENTIDAD { get; set; }

        [Attr("Fecha_Creacion")]
        public DateTime FECHA_CREACION { get; set; }

        [Attr("Fecha_Modificacion")]
        public DateTime? FECHA_MODIFICACION { get; set; }

        public TBL_RECURSOS()
        {
            TBL_RECURSO_TRAMITE = new HashSet<TBL_RECURSO_TRAMITE>();
        }

        //[NotMapped]
        [Attr("Tipo_Recurso")]
        public virtual TBL_TIPO_RECURSOS TIPO_RECURSO_ { get; set; }

        //[NotMapped]
        [Attr("Tbl_Recurso_Tramite_")]
        public virtual ICollection<TBL_RECURSO_TRAMITE> TBL_RECURSO_TRAMITE { get; set; }
    }

   

}