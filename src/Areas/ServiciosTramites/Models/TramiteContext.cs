﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using tramites_servicios_webapi.Areas.ServiciosTramites.Models;

namespace tramites_servicios_webapi.Models
{
    public partial class TramiteContext : DbContext
    {
        //public TramiteContext()
        //{
        //}

        public TramiteContext(DbContextOptions<TramiteContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TBL_TRAMITES> TBL_TRAMITES { get; set; }

        public virtual DbSet<TBL_RECURSOS> TBL_RECURSOSS { get; set; }
        public virtual DbSet<TBL_RECURSO_TRAMITE> TBL_RECURSO_TRAMITES { get; set; }
        public virtual DbSet<TBL_TIPO_RECURSOS> TBL_TIPO_RECURSOSS { get; set; }
        public virtual DbSet<TBL_ESTADOS> TBL_ESTADOSS { get; set; }
        public virtual DbSet<TBL_ETAPAS> TBL_ETAPASS { get; set; }
        public virtual DbSet<TBL_ETAPA_MOMENTO> TBL_ETAPA_MOMENTOS { get; set; }
        public virtual DbSet<TBL_INTEGRACION_TRAMITES> TBL_INTEGRACION_TRAMITESS { get; set; }
        public virtual DbSet<Departamento> Departamentos { get; set; }
        public virtual DbSet<Municipio> Municipios { get; set; }
        public virtual DbSet<TBL_OTROS_SERVICIOS> TBL_OTROS_SERVICIOSS { get; set; }
        public virtual DbSet<TblMasUsados> TblMasUsadoss { get; set; }

        public virtual DbSet<TblRecursoOtrosServicio> TblRecursoOtrosServicios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<TblRecursoOtrosServicio>(entity =>
            {
                entity.HasKey(e => e.RecursoOtroServicioId)
                    .HasName("PK__TBL_RECU__442B0222FE71D823");

                entity.ToTable("TBL_RECURSO_OTROS_SERVICIOS", "tramites_y_servicios");

                entity.Property(e => e.RecursoOtroServicioId).HasColumnName("RECURSO_OTRO_SERVICIO_ID");

                entity.Property(e => e.CodigoEstado)
                    .HasColumnName("CODIGO_ESTADO")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("FECHA_CREACION")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("FECHA_MODIFICACION")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OtrosServiciosId)
                    .IsRequired()
                    .HasColumnName("OTROS_SERVICIOS_ID")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.RecursosId).HasColumnName("RECURSOS_ID");
            });

            modelBuilder.Entity<TBL_TRAMITES>(entity =>
            {

                entity.Property(e => e.NUMERO)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.NOMBRE)
                    .IsRequired()
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.DEPARTAMENTO_NOMBRE)
                    .HasMaxLength(62)
                    .IsUnicode(false);

                entity.Property(e => e.INSTITUCION_NOMBRE)
                    .HasColumnName("INSTITUCION_NOMBRE")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.MUNICIPIO_NOMBRE)
                   .HasColumnName("MUNICIPIO_NOMBRE")
                   .HasMaxLength(120)
                   .IsUnicode(false);

            });

            modelBuilder.Entity<TBL_RECURSOS>(entity =>
            {
                entity.HasKey(e => e.RECURSO_ID)
                    .HasName("PK__TBL_RECU__3BEF173F94C5F76C");

                entity.ToTable("TBL_RECURSOS", "tramites_y_servicios");

                entity.Property(e => e.CODIGO_ESTADO).HasDefaultValueSql("((1))");

                entity.Property(e => e.DESCRIPCION)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.FECHA_CREACION)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FECHA_MODIFICACION).HasColumnType("smalldatetime");

                entity.Property(e => e.ID_ENTIDAD)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.NOMBRE)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.URL_RECURSO)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.ORDEN).HasColumnName("ORDEN");

                entity.HasOne(d => d.TIPO_RECURSO_)
                    .WithMany(p => p.TBL_RECURSOS)
                    .HasForeignKey(d => d.TIPO_RECURSO_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_RECUR__TIPO___7A3223E8");
            });

            modelBuilder.Entity<TBL_RECURSO_TRAMITE>(entity =>
            {
                entity.HasKey(e => e.RECURSO_TRAMITE_ID)
                    .HasName("PK__TBL_RECU__0D3B002B83DF10AC");

                entity.ToTable("TBL_RECURSO_TRAMITE", "tramites_y_servicios");

                entity.Property(e => e.CODIGO_ESTADO).HasDefaultValueSql("((1))");

                entity.Property(e => e.FECHA_CREACION)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FECHA_MODIFICACION).HasColumnType("smalldatetime");

                entity.Property(e => e.TRAMITE_NUMERO)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.RECURSO_)
                    .WithMany(p => p.TBL_RECURSO_TRAMITE)
                    .HasForeignKey(d => d.RECURSO_ID)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_RECUR__RECUR__1A9EF37A");
            });

            modelBuilder.Entity<TBL_TIPO_RECURSOS>(entity =>
            {
                entity.HasKey(e => e.TIPO_RECURSO_ID)
                    .HasName("PK__TBL_TIPO__7410FCAFA9670936");

                entity.ToTable("TBL_TIPO_RECURSOS", "tramites_y_servicios");

                entity.Property(e => e.CODIGO_ESTADO).HasDefaultValueSql("((1))");

                entity.Property(e => e.DESCRIPCION)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.FECHA_CREACION)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FECHA_MODIFICACION).HasColumnType("smalldatetime");

                entity.Property(e => e.NOMBRE_TIPO)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });


            modelBuilder.Entity<TBL_ESTADOS>(entity =>
            {
                entity.HasKey(e => e.CODIGO_ESTADO)
                    .HasName("PK__TBL_ESTA__EB32A215FFD0FD74");

                entity.ToTable("TBL_ESTADOS", "tramites_y_servicios");

                entity.Property(e => e.DESCRIPCION_ESTADO)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<TBL_ETAPAS>(entity =>
            {
                entity.HasKey(e => e.CODIGO_ETAPA)
                    .HasName("PK__TBL_ETAP__34FCFE5DD5ED8F66");

                entity.ToTable("TBL_ETAPAS", "tramites_y_servicios");

                entity.Property(e => e.CODIGO_ESTADO).HasDefaultValueSql("((1))");

                entity.Property(e => e.DESCRIPCION_ETAPA)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.FECHA_CREACION)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ORDEN).HasDefaultValueSql("((0))");

                entity.Property(e => e.USUARIO_CREADOR_ENTIDAD)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.HasOne(d => d.CODIGO_ESTADONavigation)
                    .WithMany(p => p.TBL_ETAPAS)
                    .HasForeignKey(d => d.CODIGO_ESTADO)
                    .HasConstraintName("FK__TBL_ETAPA__CODIG__531856C7");
            });

            modelBuilder.Entity<TBL_ETAPA_MOMENTO>(entity =>
            {
                entity.HasKey(e => e.CODIGO_ETAPA_MOMENTO)
                    .HasName("PK__TBL_ETAP__34FCFE5DAA7F4E0D");

                entity.ToTable("TBL_ETAPA_MOMENTO", "tramites_y_servicios");

                entity.Property(e => e.CODIGO_ESTADO).HasDefaultValueSql("((1))");

                entity.Property(e => e.DESCRIPCION_ETAPA)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FECHA_CREACION)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.ORDEN).HasDefaultValueSql("((0))");

                entity.Property(e => e.TRAMITE_NUMERO)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.USUARIO_CREADOR)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.HasOne(d => d.CODIGO_ESTADONavigation)
                    .WithMany(p => p.TBL_ETAPA_MOMENTO)
                    .HasForeignKey(d => d.CODIGO_ESTADO)
                    .HasConstraintName("FK__TBL_ETAPA__CODIG__58D1301D");

                entity.HasOne(d => d.CODIGO_ETAPANavigation)
                    .WithMany(p => p.TBL_ETAPA_MOMENTO)
                    .HasForeignKey(d => d.CODIGO_ETAPA)
                    .HasConstraintName("FK__TBL_ETAPA__CODIG__54CB950F");
            });

            modelBuilder.Entity<Departamento>(entity =>
            {
                entity.HasKey(e => e.DepCodigo)
                    .HasName("PK_departamento_id");

                entity.ToTable("DEPARTAMENTO");

                entity.Property(e => e.DepCodigo)
                    .HasColumnName("DEP_CODIGO")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DepNombre)
                    .IsRequired()
                    .HasColumnName("DEP_NOMBRE")
                    .HasMaxLength(75)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Municipio>(entity =>
            {
                entity.HasKey(e => e.MunCodigo)
                    .HasName("PK_municipio_id");

                entity.ToTable("MUNICIPIO");

                entity.Property(e => e.MunCodigo)
                    .HasColumnName("MUN_CODIGO")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DepCodigo)
                    .IsRequired()
                    .HasColumnName("DEP_CODIGO")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MunNombre)
                    .IsRequired()
                    .HasColumnName("MUN_NOMBRE")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TBL_INTEGRACION_TRAMITES>(entity =>
            {
                entity.HasKey(e => e.INTEGRADOR_ID)
                    .HasName("PK__TBL_INTE__122311D5C35C7513");

                entity.ToTable("TBL_INTEGRACION_TRAMITES", "tramites_y_servicios");

                entity.Property(e => e.EMBEBIDO)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('NO')");

                entity.Property(e => e.FECHA_CREACION)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FECHA_MODIFICACION).HasColumnType("smalldatetime");

                entity.Property(e => e.NUMERO)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.URL_TRAMITE)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.USUARIO_CREADOR_ENTIDAD)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.USUARIO_MODIFICACION_ENTIDAD)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TBL_OTROS_SERVICIOS>(entity =>
            {
                entity.HasKey(e => e.OTROS_SERVICIOS_ID)
                    .HasName("PK__TBL_OTRO__FC01C3DF27EDE027");

                entity.ToTable("TBL_OTROS_SERVICIOS", "tramites_y_servicios");

                entity.Property(e => e.OTROS_SERVICIOS_ID)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CODIGO_ESTADO).HasDefaultValueSql("((1))");

                entity.Property(e => e.CONSIDERACIONES)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.DESCRIPCION)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.ENTIDAD_ID)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.FECHA_CREACION)
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FECHA_MODIFICACION).HasColumnType("smalldatetime");

                entity.Property(e => e.MUNICIPIO_CODIGO_DANE)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.NOMBRE)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PALABRAS_CLAVE).IsUnicode(false);

                entity.Property(e => e.PROPOSITO).IsUnicode(false);

                entity.Property(e => e.RESULTADO_OTRO_SERVICIO)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.URL_SERVICIO_EN_LINEA)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.USUARIO_AUTORIZO_GOVCO)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.USUARIO_CREADOR_ENTIDAD)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblMasUsados>(entity =>
            {
                entity.HasKey(e => e.MasUsadosId)
                    .HasName("PK__TBL_MAS___D1ACF497EE806B80");

                entity.ToTable("TBL_MAS_USADOS", "tramites_y_servicios");

                entity.Property(e => e.MasUsadosId).HasColumnName("MAS_USADOS_ID");

                entity.Property(e => e.CodigoEstado)
                    .HasColumnName("CODIGO_ESTADO")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("FECHA_CREACION")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("FECHA_MODIFICACION")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.Orden).HasColumnName("ORDEN");

                entity.Property(e => e.TramiteNumero)
                    .HasColumnName("TRAMITE_NUMERO")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}