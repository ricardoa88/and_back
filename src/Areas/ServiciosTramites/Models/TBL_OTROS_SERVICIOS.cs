﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Models
{
    public partial class TBL_OTROS_SERVICIOS : Identifiable
    {
        [NotMapped]
        public override int Id { get; set; }

        [Key]
        [Attr("OtrosServiciosId")]
        public string OTROS_SERVICIOS_ID { get; set; }

        [Attr("Nombre")]
        public string NOMBRE { get; set; }

        [Attr("Descripcion")]
        public string DESCRIPCION { get; set; }

        [Attr("Proposito")]
        public string PROPOSITO { get; set; }

        [Attr("PalabrasClave")]
        public string PALABRAS_CLAVE { get; set; }

        [Attr("Consideraciones")]
        public string CONSIDERACIONES { get; set; }

        [Attr("ResultadoOtroServicio")]
        public string RESULTADO_OTRO_SERVICIO { get; set; }

        [Attr("CodigoEstado")]
        public int? CODIGO_ESTADO { get; set; }

        [Attr("EntidadId")]
        public string ENTIDAD_ID { get; set; }

        [Attr("MunicipioCodigoDane")]
        public string MUNICIPIO_CODIGO_DANE { get; set; }

        [Attr("UrlServicioLinea")]
        public string URL_SERVICIO_EN_LINEA { get; set; }

        [Attr("UsuarioCreadorEntidad")]
        public string USUARIO_CREADOR_ENTIDAD { get; set; }

        [Attr("UsuarioAutorizoGovco")]
        public string USUARIO_AUTORIZO_GOVCO { get; set; }

        [Attr("FechaCreacion")]
        public DateTime FECHA_CREACION { get; set; }

        [Attr("FechaModifcacion")]
        public DateTime? FECHA_MODIFICACION { get; set; }
    }
}