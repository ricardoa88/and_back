﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Models
{
    public partial class TBL_TRAMITES : Identifiable
    {
        [NotMapped]
        public override int Id { get; set; }
        [Key]
        [Attr("Numero")]
        public string NUMERO { get; set; }

        [Attr("Nombre")]
        public string NOMBRE { get; set; }

        [NotMapped]

        public string MUNICIPIO_NOMBRE { get; set; }

        [NotMapped]

        public string DEPARTAMENTO_NOMBRE { get; set; }

        [NotMapped]
        public string INSTITUCION_NOMBRE { get; set; }

    }
}