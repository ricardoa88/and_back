﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Models
{
    public partial class TBL_RECURSO_TRAMITE : Identifiable
    {
        [NotMapped]
        public override int Id { get; set; }
        [Key]
        [Attr("Recurso_Tramite_Id")]
        public int RECURSO_TRAMITE_ID { get; set; }

        [Attr("Recurso_Id")]
        public int RECURSO_ID { get; set; }

        [Attr("Tramite_Numero")]
        public string TRAMITE_NUMERO { get; set; }

        [Attr("Codigo_Estado")]
        public int? CODIGO_ESTADO { get; set; }

        [Attr("Fecha_Creacion")]
        public DateTime FECHA_CREACION { get; set; }

        [Attr("Fecha_Modificacion")]
        public DateTime? FECHA_MODIFICACION { get; set; }

        [Attr("Recurso")]

        public virtual TBL_RECURSOS RECURSO_ { get; set; }
    }
}