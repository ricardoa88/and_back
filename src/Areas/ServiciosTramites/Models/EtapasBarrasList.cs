﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Models
{
    public partial class EtapasBarrasList
    {

        [Attr("Paso")]
        public string Paso { get; set; }

        [Attr("Titulo")]
        public string Titulo { get; set; }

        [Attr("Etapa")]
        public string[] Etapas { get; set; }

        [Attr("ItemId")]
        public string ItemId { get; set; }

        [Attr("Etapa")]
        public string Etapa { get; set; }

    }

    public partial class UrlTramiteDesarrollado
    {

        [Attr("IdTramites")]
        public string IdTramite { get; set; }

        [Attr("UrlTramite")]
        public string UrlTramite { get; set; }
    }



    }