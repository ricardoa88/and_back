﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Models
{
    public partial class VM_CT_TRAMITE : Identifiable
    {
        [NotMapped]
        public override int Id { get; set; }

        [Key]
        [Attr("Numero")]
        public string NUMERO { get; set; }

        [Attr("NombreEstandarizado")]
        public string NOMBRE_ESTANDARIZADO { get; set; }

        [Attr("Nombre")]
        public string NOMBRE { get; set; }

        [Attr("Prposito")]
        public string PROPOSITO { get; set; }

        [Attr("NombreResultado")]
        public string NOMBRE_RESULTADO { get; set; }

        [Attr("Clase")]
        public string CLASE { get; set; }

        [Attr("Obtencioninmediata")]
        public string OBTENCION_INMEDIATA { get; set; }

        [Attr("TiempoObtencion")]
        public string TIEMPO_OBTENCION { get; set; }

        [Attr("ObservacionTiempoObtencion")]
        public string OBSERVACION_TIEMPO_OBTENCION { get; set; }

        [Attr("PalabrasClave")]
        public string PALABRAS_CLAVE { get; set; }

        [Attr("RealizadoMediosElectronicos")]
        public string REALIZADO_MEDIOS_ELECTRONICOS { get; set; }

        [Attr("SeguimientoSedePrincipal")]
        public string SEGUIMIENTO_SEDE_PRINCIPAL { get; set; }

        [Attr("UrlPuntosAtencion")]
        public string URL_PUNTOS_ATENCION { get; set; }

        [Attr("TipoAtencionPresencial")]
        public string TIPO_ATENCION_PRESENCIAL { get; set; }

        [Attr("UrlTramiteLinea")]
        public string URL_TRAMITE_EN_LINEA { get; set; }

        [Attr("UrlResultadoWeb")]
        public string URL_RESULTADO_WEB { get; set; }

        [Attr("TipoFechaEjecucion")]
        public string TIPO_FECHA_EJECUCION { get; set; }

        [Attr("UrlCalendarioEjecucion")]
        public string URL_CALENDARIO_EJECUCION { get; set; }

        [Attr("EstadoCodigo")]
        public string ESTADO_CODIGO { get; set; }

        [Attr("EstadoNombre")]
        public string ESTADO_NOMBRE { get; set; }

        [Attr("ObservaiconFechaGeneral")]
        public string OBSERVACION_FECHA_GENERAL { get; set; }

        [Attr("SituacionVidaId")]
        public decimal? SITUACION_VIDA_ID { get; set; }

        [Attr("SituacionVidaNombre")]
        public string SITUACION_VIDA_NOMBRE { get; set; }

        [Attr("ObjetoNombre")]
        public string OBJETO_NOMBRE { get; set; }

        [Attr("VerboNombre")]
        public string VERBO_NOMBRE { get; set; }

        [Attr("InstitucionId")]
        public string INSTITUCION_ID { get; set; }

        [Attr("InstitucionNombre")]
        public string INSTITUCION_NOMBRE { get; set; }

        [Attr("MunicipioCodigoDane")]
        public string MUNICIPIO_CODIGO_DANE { get; set; }

        [Attr("MunicipioNombre")]
        public string MUNICIPIO_NOMBRE { get; set; }

        [Attr("DepartamentoCodigoDane")]
        public string DEPARTAMENTO_CODIGO_DANE { get; set; }

        [Attr("DepartamentoNombre")]
        public string DEPARTAMENTO_NOMBRE { get; set; }

        [Attr("UrlPaginaDetalle")]
        public string URL_PAGINA_DETALLE { get; set; }

        [Attr("FechaActualizacion")]
        public DateTime? FECHA_ACTUALIZACION { get; set; }

        [Attr("UrlManualLinea")]
        public string URL_MANUAL_EN_LINEA { get; set; }

        [Attr("AnotaBasicaLinea")]
        public string ANOTA_BASICA_EN_LINEA { get; set; }

        [Attr("Subcategoria")]
        public string SUBCATEGORIA { get; set; }

        [Attr("Barrio")]
        public string BARRIO { get; set; }

        [Attr("FInstitucion")]
        public virtual VM_SGP_INSTITUCION INSTITUCION_ { get; set; }
    }
}