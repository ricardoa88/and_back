﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace tramites_servicios_webapi.Models
{
    public partial class SuitContext : DbContext
    {
        public SuitContext()
        {
        }

        
        public SuitContext(DbContextOptions<SuitContext> options)
            : base(options)
        {
        }

        public virtual DbSet<VM_CT_TRAMITE> VM_CT_TRAMITE { get; set; }
        public virtual DbSet<VM_SGP_INSTITUCION> VM_SGP_INSTITUCION { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<VM_CT_TRAMITE>(entity =>
            {
                entity.HasKey(e => e.NUMERO);

                entity.Property(e => e.NUMERO)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ANOTA_BASICA_EN_LINEA)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.BARRIO).IsUnicode(false);

                entity.Property(e => e.CLASE)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DEPARTAMENTO_CODIGO_DANE)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.DEPARTAMENTO_NOMBRE)
                    .HasMaxLength(62)
                    .IsUnicode(false);

                entity.Property(e => e.ESTADO_CODIGO)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ESTADO_NOMBRE)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FECHA_ACTUALIZACION)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.INSTITUCION_ID)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.INSTITUCION_NOMBRE)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.MUNICIPIO_CODIGO_DANE)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.MUNICIPIO_NOMBRE)
                    .HasMaxLength(62)
                    .IsUnicode(false);

                entity.Property(e => e.NOMBRE)
                    .IsRequired()
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.NOMBRE_ESTANDARIZADO)
                    .HasMaxLength(252)
                    .IsUnicode(false);

                entity.Property(e => e.NOMBRE_RESULTADO)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.OBJETO_NOMBRE)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OBSERVACION_FECHA_GENERAL)
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.OBSERVACION_TIEMPO_OBTENCION).IsUnicode(false);

                entity.Property(e => e.OBTENCION_INMEDIATA)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PALABRAS_CLAVE)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.PROPOSITO).IsUnicode(false);

                entity.Property(e => e.REALIZADO_MEDIOS_ELECTRONICOS)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SEGUIMIENTO_SEDE_PRINCIPAL)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SITUACION_VIDA_ID).HasColumnType("numeric(4, 0)");

                entity.Property(e => e.SITUACION_VIDA_NOMBRE)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SUBCATEGORIA).IsUnicode(false);

                entity.Property(e => e.TIEMPO_OBTENCION)
                    .HasMaxLength(58)
                    .IsUnicode(false);

                entity.Property(e => e.TIPO_ATENCION_PRESENCIAL)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TIPO_FECHA_EJECUCION)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.URL_CALENDARIO_EJECUCION)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.URL_MANUAL_EN_LINEA)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.URL_PAGINA_DETALLE)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.URL_PUNTOS_ATENCION)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.URL_RESULTADO_WEB)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.URL_TRAMITE_EN_LINEA)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.VERBO_NOMBRE)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.INSTITUCION_)
                    .WithMany(p => p.VM_CT_TRAMITE)
                    .HasForeignKey(d => d.INSTITUCION_ID)
                    .HasConstraintName("FK_VM_CT_TRAMITE_VM_SGP_INSTITUCION");
            });

            modelBuilder.Entity<VM_SGP_INSTITUCION>(entity =>
            {
                entity.Property(e => e.ID)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CLOR_ID)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CLOR_NOMBRE)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DEPA_CODIGO_DANE)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.DEPA_NOMBRE)
                    .HasMaxLength(62)
                    .IsUnicode(false);

                entity.Property(e => e.DIRECCION)
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.EMAIL)
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.ES_DEPENDEN_ESPECIAL)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.FAX)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MUNI_CODIGO_DANE)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.MUNI_NOMBRE)
                    .HasMaxLength(62)
                    .IsUnicode(false);

                entity.Property(e => e.NAJU_ID)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.NAJU_NOMBRE)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NIVE_ID)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.NIVE_NOMBRE)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NOMBRE)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.ORDR_ID)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ORDR_NOMBRE)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PADRE_ID)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.PADRE_NOMBRE)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.PAGINA_WEB)
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.SECT_ID)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SECT_NOMBRE)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.SIGLA)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.SUOR_ID)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SUOR_NOMBRE)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TELEFONO)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TIPO_DEPENDENCIA)
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}