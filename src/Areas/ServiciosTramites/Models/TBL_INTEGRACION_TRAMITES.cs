﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Models
{
    public partial class TBL_INTEGRACION_TRAMITES : Identifiable
    {
        [NotMapped]
        public override int Id { get; set; }

        [Key]
        [Attr("IntegradorId")]
        public int INTEGRADOR_ID { get; set; }

        [Attr("Numero")]
        public string NUMERO { get; set; }

        [Attr("Embebido")]
        public string EMBEBIDO { get; set; }

        [Attr("UrlTramite")]
        public string URL_TRAMITE { get; set; }

        [Attr("FechaCreacion")]
        public DateTime FECHA_CREACION { get; set; }

        [Attr("FechaModificacion")]
        public DateTime? FECHA_MODIFICACION { get; set; }

        [Attr("EstadoId")]
        public int? ESTADO_ID { get; set; }

        [Attr("UsuarioCreador")]
        public string USUARIO_CREADOR_ENTIDAD { get; set; }

        [Attr("UsuarioModificacion")]
        public string USUARIO_MODIFICACION_ENTIDAD { get; set; }
    }

    public partial class TramitesEmbebidos
    {

        [Attr("IntegradorId")]
        public int IntegradorId { get; set; }

        [Attr("Numero")]
        public string Numero { get; set; }


        [Attr("NombreTramite")]
        public string NombreTramite { get; set; }


        [Attr("NombreEntidad")]
        public string NombreEntidad { get; set; }

        [Attr("Embebido")]
        public string Embebido { get; set; }

        [Attr("UrlTramite")]
        public string UrlTramite { get; set; }

        [Attr("FechaCreacion")]
        public DateTime FechaCreacion { get; set; }

        [Attr("FechaModificacion")]
        public DateTime? FechaModificacion { get; set; }

        [Attr("EstadoId")]
        public int? EstadoId { get; set; }

        [Attr("UsuarioCreador")]
        public string UsuarioCreador { get; set; }

        [Attr("UsuarioModificacion")]
        public string UsuarioModificacion { get; set; }

        [Attr("Municipio")]
        public string Municipio { get; set; }

        [Attr("Departamento")]
        public string Departamento { get; set; }
    }

    public partial class FiltrosTramitesEmbebidos
    {

        [Attr("IdTramite")]
        public string IdTramite { get; set; }

        [Attr("Entidad")]
        public string Entidad { get; set; }

        [Attr("Estado")]
        public int Estado { get; set; }

       
        public string OrdenColumna { get; set; }

       
        public string Orden { get; set; }


    }


}