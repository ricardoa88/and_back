﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Models
{
    public partial class VM_SGP_INSTITUCION : Identifiable
    {
        public VM_SGP_INSTITUCION()
        {
            VM_CT_TRAMITE = new HashSet<VM_CT_TRAMITE>();
        }


        
        [Key]
        [Attr("Id")]
        public string ID { get; set; }

        public string NOMBRE { get; set; }
        public string SIGLA { get; set; }
        public string ES_DEPENDEN_ESPECIAL { get; set; }
        public string ORDR_ID { get; set; }
        public string ORDR_NOMBRE { get; set; }
        public string SUOR_ID { get; set; }
        public string SUOR_NOMBRE { get; set; }
        public string MUNI_CODIGO_DANE { get; set; }
        public string MUNI_NOMBRE { get; set; }
        public string SECT_ID { get; set; }
        public string SECT_NOMBRE { get; set; }
        public string NAJU_ID { get; set; }
        public string NAJU_NOMBRE { get; set; }
        public string CLOR_ID { get; set; }
        public string CLOR_NOMBRE { get; set; }

        [Attr("PaginaWeb")]
        public string PAGINA_WEB { get; set; }
        public string EMAIL { get; set; }
        public string DIRECCION { get; set; }
        public string TELEFONO { get; set; }
        public string FAX { get; set; }
        public string NIVE_ID { get; set; }
        public string NIVE_NOMBRE { get; set; }
        public string PADRE_ID { get; set; }
        public string PADRE_NOMBRE { get; set; }
        public string TIPO_DEPENDENCIA { get; set; }
        public string DEPA_CODIGO_DANE { get; set; }
        public string DEPA_NOMBRE { get; set; }

        public virtual ICollection<VM_CT_TRAMITE> VM_CT_TRAMITE { get; set; }
    }
}