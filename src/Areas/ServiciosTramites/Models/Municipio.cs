﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosTramites.Models
{
    public partial class Municipio
    {
        public string MunCodigo { get; set; }
        public string DepCodigo { get; set; }
        public string MunNombre { get; set; }
    }
}