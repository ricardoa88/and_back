﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Areas.ServiciosTramites.Models
{
    public partial class TblMasUsados : Identifiable
    {

        [NotMapped]
        public override int Id { get; set; }
        public int MasUsadosId { get; set; }
        public string TramiteNumero { get; set; }
        public byte? Orden { get; set; }
        public int? CodigoEstado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
    }


    public partial class MasUsadosHome
    {

        public MasUsadosHome()
        {
        }

        public MasUsadosHome(MasUsadosHome masUsadosHome)
        {
        }

        public int MasUsadosId { get; set; }
        public string TramiteNumero { get; set; }
        public int Orden { get; set; }
        public string NombreTramite { get; set; }
        public string TiempoObtencion { get; set; }
        public string Entidad { get; set; }
        public Boolean Linea { get; set; }
        public Boolean Costo{ get; set; }
    }
}
