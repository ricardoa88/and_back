﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tramites_servicios_webapi.Models
{
    public partial class RecursosList
    {
        [Attr("Id_Recurso")]
        public int IdRecurso { get; set; }

        [Attr("Nombre")]
        public string Nombre { get; set; }

        [Attr("Url_Recurso")]
        public string UrlRecurso { get; set; }

        [Attr("Tipo_Recurso")]
        public int TipoRecurso { get; set; }

        

    }

    public partial class RecursosEmbebidos
    {       

        [Key]
        [Attr("Recurso_Id")]
        public int Recurso_Id { get; set; }

        [Attr("Nombre")]
        public string Nombre { get; set; }

        [Attr("Descripcion")]
        public string Descripcion { get; set; }

        [Attr("Codigo_Estado")]
        public int? Codigo_Estado { get; set; }

        [Attr("Orden")]
        public int? Orden { get; set; }

        [Attr("Tipo_Recurso_Id")]
        public int Tipo_Recurso_Id { get; set; }

        [Attr("Url_Recurso")]
        public string Url_Recurso { get; set; }

        [Attr("Fecha_Creacion")]
        public DateTime Fecha_Creacion { get; set; }    
        
    }


    public partial class EntidadUrl
    {

        [Attr("IdTramite")]
        public string IdTramite { get; set; }

        [Attr("Url_Recurso")]
        public string UrlEntidad { get; set; }

    }

}