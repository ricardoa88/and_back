﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosTramites.Models
{
    public partial class Departamento
    {
        public string DepCodigo { get; set; }
        public string DepNombre { get; set; }
    }
}