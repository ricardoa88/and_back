﻿using JsonApiDotNetCore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace tramites_servicios_webapi.Models
{
    public partial class TblRecursoOtrosServicio
    {
        public int RecursoOtroServicioId { get; set; }
        public int RecursosId { get; set; }
        public string OtrosServiciosId { get; set; }
        public int? CodigoEstado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
    }
}