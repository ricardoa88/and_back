using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using tramites_servicios_webapi.Sena.CertificadoConstancia.Models;

namespace tramites_servicios_webapi.Sena.CertificadoConstancia.Services
{
    /// <summary>
    /// Servicios Tramite certificados y constancias del SENA
    /// </summary>
    public class CertificadoConstanciaService
    {
        private static readonly HttpClient _client;
        private const String _urlBase = "https://190.60.233.147:8243/";
        private const String _urlConstancias = "certificaciones-constancias-sena/v1";
        private const String _urlToken = "token";

        static CertificadoConstanciaService()
        {
            //Omitit Certificado SSL
            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual;
            handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {
                    return true;
                };

            _client = new HttpClient(handler);
            _client.BaseAddress = new Uri(_urlBase);
        }

        /// <summary>
        /// Consulta certificados y constancias
        /// </summary>
        /// <param name="data">Modelo</param>
        /// <returns>Listado certificados y constancias</returns>
        public static async Task<JsonResult> Consultar(DatosSolicitanteModel data)
        {
            // Inicializaciones
            JsonResult resultado = null;
            var infoResult= "";
            var soapAction= "";

            //Token
            TokenModel tokenModel = await CertificadoConstanciaService.ObtenerToken();

            //Body XML
            XNamespace ns = "http://www.w3.org/2003/05/soap-envelope";
            XNamespace myns = "http://certificados.sena.edu.co";

            XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
            XNamespace xsd = "http://www.w3.org/2001/XMLSchema";

            XElement rawElement = null;

            //Busqueda por registro
            if(data.Registro != null && data.Registro != "")
            {
                infoResult = "getInfoIdCertResult";
                soapAction = "getInfoIdCert";
                rawElement = new XElement(myns + "getInfoIdCert",
                                            new XElement(myns + "idcert", data.Registro),
                                            new XElement(myns + "CodVer", "C3rt1f1c4.s3n4")
                                );
            } //Busqueda por  documento
            else{ 
                 infoResult = "getInfoCertDocResult";
                 soapAction = "getInfoCertDoc";
                 rawElement = new XElement(myns + "getInfoCertDoc",
                                            new XElement(myns + "numident", data.Documento),
                                            new XElement(myns + "tipoident", data.TipoDocumento),
                                            new XElement(myns + "CodVer", "C3rt1f1c4.s3n4")
                                );

            }
           
            //Soap Request
            XDocument soapRequest = new XDocument(
                new XDeclaration("1.0", "UTF-8", "no"),
                new XElement(ns + "Envelope",
                    new XAttribute(XNamespace.Xmlns + "xsi", xsi),
                    new XAttribute(XNamespace.Xmlns + "xsd", xsd),
                    new XAttribute(XNamespace.Xmlns + "soap12", ns),
                    new XElement(ns + "Body",
                       rawElement 
                    )
                ));

            //Default Request Headers
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("SOAPAction", "http://certificados.sena.edu.co/"+soapAction);
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);
            StringContent stringContent = new StringContent(soapRequest.ToString(), Encoding.UTF8, "text/xml");

            //Petición API Gateway
            try
            {
                HttpResponseMessage resp = await _client.PostAsync(_urlConstancias, stringContent);
                if (resp.IsSuccessStatusCode)
                {
                    Task<Stream> streamTask = resp.Content.ReadAsStreamAsync();
                    Stream stream = streamTask.Result;
                    var sr = new StreamReader(stream);
                    var soapResponse = XDocument.Load(sr);
                    var xml = soapResponse.Descendants(myns + infoResult).FirstOrDefault().ToString();
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xml);
                    var json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);
                    //Salida XML->JSON
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);
                    var getInfoCerResult = jsonObj[infoResult].ToString();
                    jsonObj = JsonConvert.DeserializeObject(getInfoCerResult);
                    var rgetRegDoc = jsonObj["rgetRegDoc"].ToString();
                    var certificadosConstancias = new List<CertificadoConstanciaModel>();
                    //Por registro
                    if(data.Registro != null && data.Registro != ""){
                         var certificadoConstancia = JsonConvert.DeserializeObject<CertificadoConstanciaModel>(rgetRegDoc);
                         //Si encuentra resultados
                         if(certificadoConstancia.IdCertificado != null){
                            certificadosConstancias.Add(certificadoConstancia);
                            resultado = new JsonResult(certificadosConstancias);
                         }             
                    }//Por documento
                    else{
                        certificadosConstancias = JsonConvert.DeserializeObject<IEnumerable<CertificadoConstanciaModel>>(rgetRegDoc);
                        resultado = new JsonResult(certificadosConstancias);
                    }        
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            //Resultado
            return resultado;
        }

        /// <summary>
        /// Obtener token tramites sena
        /// </summary>
        /// <returns>Token</returns>
        public static async Task<TokenModel> ObtenerToken()
        {

            // Inicializaciones
            String resultado = null;
            TokenModel tokenModel = null;

            //Content
            HttpContent content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials")
            });
            _client.DefaultRequestHeaders.Accept.Clear();
            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "UVJrYWpSTGZka2MzbERWalVGUm1nWEIwUmxvYTpkYXl4ZVdkQ1kydk5lenRWeUxxWG95OEpMeVVh");

            //Petición API Gateway
            try
            {
                HttpResponseMessage resp = await _client.PostAsync(_urlToken, content);
                if (resp.IsSuccessStatusCode)
                {
                    resultado = JsonConvert.SerializeObject(resultado);
                    resultado = resp.Content.ReadAsStringAsync().Result;
                    tokenModel = JsonConvert.DeserializeObject<TokenModel>(resultado);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            //Salida
            return tokenModel;
        }
    }
}