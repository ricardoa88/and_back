using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Sena.CertificadoConstancia.Models;
using tramites_servicios_webapi.Sena.CertificadoConstancia.Services;

namespace tramites_servicios_webapi.Sena.CertificadoConstancia.Controllers
{
    /// <summary>
    /// Certificados y constancias académicas del SENA
    /// </summary>
    [Route ("api/sena/[controller]")]
    [ApiController]
    public class CertificadoConstanciaController : ControllerBase 
    {

        /// <summary>
        /// Consulta los certificados y constancias académicas del sena
        /// </summary>
        /// <param name="data">Objeto con tipo y numero de documento</param>
        /// <returns>Listado de certificados y constancias académicas</returns>
        [HttpPost ("consultar")]
        [EnableCors("PermitirApiRequest")]
        public async Task<IActionResult> Consultar ([FromBody] DatosSolicitanteModel data) 
        {
            var respuesta = await CertificadoConstanciaService.Consultar (data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }
        
    }
}