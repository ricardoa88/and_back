using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Sena.CertificadoConstancia.Models
{
    /// <summary>
    /// Modelo con la información de los certificados y constancias académicas
    /// </summary>
    public class CertificadoConstanciaModel
    {
        [JsonProperty("NOMBRE_APRENDIZ")]
        public string NombreAprendiz { get; set; }

        [JsonProperty("NRO_IDENT")]
        public int Documeto { get; set; }

        [JsonProperty("TIPO_IDENT")]
        public string TipoDocumento { get; set; }

        [JsonProperty("TIPO_ARCHIVO")]
        public string TipoArchivo { get; set; }

        [JsonProperty("PROGRAMA")]
        public string Programa { get; set; }

        [JsonProperty("NO_ORDEN")]
        public string NumeroOrden { get; set; }

        [JsonProperty("IDCERT")]
        public string IdCertificado { get; set; }

       [JsonProperty("FECHA_CERTIFICACION")]
        public string FechaCertificacion { get; set; }

        [JsonProperty("FECHA_FIRMA")]
        public string FechaFirma { get; set; }

        [JsonProperty("URLCERT")]
        public string Url { get; set; }
    }
}