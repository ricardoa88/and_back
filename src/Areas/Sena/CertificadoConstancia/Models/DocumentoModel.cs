using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Sena.CertificadoConstancia.Models
{
    /// <summary>
    /// Modelo con la información de consulta de certificados y constancias académicas
    /// por documento
    /// </summary>
    public class DocumentoModel
    {
        /// <summary>
        /// Tipo de documento
        /// </summary>
        public string TipoDocumento { get; set; }

        /// <summary>
        /// Numero de documento de identificación
        /// </summary>
        public int Documento { get; set; }

        /// <summary>
        /// Numero de documento de identificación
        /// </summary>
        public string Registro { get; set; }
    }
}