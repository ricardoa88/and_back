
using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Cancilleria.Models
{

    public class OficinasModel : CancilleriaBodyModel
    {
        [JsonProperty("tns:Oficina")]
        public List<ItemList> OficinaList { get; set; }
    }

    public class TiposDocumentoModel : CancilleriaBodyModel
    {
        [JsonProperty("tns:TipoDocumento")]
        public ItemList TiposDocumento { get; set; }
    }

    public class TiposPasaporteModel : CancilleriaBodyModel
    {
        [JsonProperty("tns:TipoPasaporte")]
        public List<ItemList> TipoPasaporte { get; set; }
    }

    public class MotivosSolicitudModel : CancilleriaBodyModel
    {
        [JsonProperty("tns:MotivoSolicitud")]
        public List<ItemList> MotivosSolicitud { get; set; }
    }

    public class ItemList
    {
        [JsonProperty("tns:id")]
        public string Id { get; set; }

        [JsonProperty("tns:nombre")]
        public string Nombre { get; set; }
    }

    #region Modelos generales para el cuerpo de la respuesta del trámite: Tramitar pasaporte
    public class CancilleriaBodyModel
    {
        [JsonProperty("tns:DetalleRespuesta")]
        public CancilleriaBodyResponseModel Response { get; set; }
    }

    public class CancilleriaBodyResponseModel
    {
        [JsonProperty("com:codigo")]
        public ResponseDetailModel Codigo { get; set; }

        [JsonProperty("com:detalle")]
        public ResponseDetailModel Detalle { get; set; }

        [JsonProperty("com:mensaje")]
        public ResponseDetailModel Mensaje { get; set; }
    }

    public class ResponseDetailModel
    {
        [JsonProperty("#text")]
        public string Texto { get; set; }
    }
    #endregion

    #region Modelo del cuerpo de respuesta de la autrización para el tratamiento de datos
    public class TratamientoDatosBodyModel
    {
        [JsonProperty("return")]
        public TratamientoDatosBodyResponseModel Response { get; set; }
    }

    public class TratamientoDatosBodyResponseModel
    {
        [JsonProperty("codigoRespuesta")]
        public long CodigoRespuesta { get; set; }

        [JsonProperty("mensajeAviso")]
        public string MensajeAviso { get; set; }

        [JsonProperty("mensajeConsentimiento")]
        public string MensajeConsentimiento { get; set; }

        [JsonProperty("mensajeTratamiento")]
        public string MensajeTratamiento { get; set; }

        [JsonProperty("tratamientoDatosPersonalesCuerpo")]
        public string TratamientoDatosCuerpo { get; set; }

        [JsonProperty("tratamientoDatosPersonalesTitulo")]
        public string TratamientoDatosTitulo { get; set; }
    }
    #endregion

    #region Modelo para la información de la solicitud del pasaporte.
    public class DataSolicitudPasaporteModel
    {
        public string motivoSolicitud { get; set; }

        public string oficina { get; set; }

        public string tipoPasaporte { get; set; }

        public string digitoVerificacionOCR { get; set; }

        public string fechaExpedicionPasaporte { get; set; }

        public string numeroPasaporte { get; set; }

        public string codigoDocumento { get; set; }

        public string correoElectronico { get; set; }

        public string fechaExpedicionDocIdentidad { get; set; }

        public string numeroPersonal { get; set; }
    }
    #endregion

    #region Modelos para construir el body de la solicitud
    public class InfoNuevaSolicitudModel
    {
        public string motivoSolicitud { get; set; }

        public string oficina { get; set; }

        public string tipoPasaporte { get; set; }
    }

    public class InfoPasaporteActualModel
    {
        public string digitoVerificacionOCR { get; set; }

        public string fechaExpedicionPasaporte { get; set; }

        public string numeroPasaporte { get; set; }
    }

    public class InfoSolicitanteModel
    {
        public string codigoDocumento { get; set; }

        public string correoElectronico { get; set; }

        public string fechaExpedicionDocIdentidad { get; set; }

        public string numeroPersonal { get; set; }
    }

    public class ArgSolicitudModel
    {
        public InfoNuevaSolicitudModel infoNuevaSolicitud { get; set; }

        public InfoPasaporteActualModel infoPasaporteActual { get; set; }

        public InfoSolicitanteModel infoSolicitante { get; set; }

        public string ipCiudadano { get; set; }
    }

    public class bodySolicitudModel
    {
        public ArgSolicitudModel arg0 { get; set; }
    
    }
    #endregion

    #region Modelos para la consulta de comprobante de pago
    public class BodyComprobanteModel
    {
        public string numeroReferencia { get; set; }
    }

    public class DataComprobanteModel
    {
        public string obtenerReciboResult { get; set; }
    }
    #endregion

    #region Modelo para la consulta de la solicitud
    public class DataConsultaSolicitudModel
    {
        public string numeroDocumentoIdentidad { get; set; }

        public string numeroSolicitud { get; set; }

        public string nacionalidad { get; set; }

        public string tipoDocumentoIdentidad { get; set; }

        public string usuario { get; set; }

        public string contrasena { get; set; }

        public string fuente { get; set; }

    }
    #endregion

    #region Modelos para la construir el body de la consulta de la solicitud
    public class BodyConsultaSolicitudModel
    {
        public DataConsultaSolicitudModel arg0 { get; set; }
    }
    #endregion

    #region Modelo del cuerpo de respuesta de la autrización para el tratamiento de datos
    public class DataResultConsultaModel
    {
        [JsonProperty("return")]
        public DataConsultaResponseModel Response { get; set; }
    }

    public class DataConsultaResponseModel
    {
        [JsonProperty("nombres")]
        public string Nombres { get; set; }

        [JsonProperty("numeroDocumento")]
        public string NumeroDocumento { get; set; }

        [JsonProperty("numeroSolicitud")]
        public string NumeroSolicitud { get; set; }

        [JsonProperty("tipoTramite")]
        public string TipoTramite { get; set; }

        [JsonProperty("codigo")]
        public string Codigo { get; set; }

        [JsonProperty("detalle")]
        public string Detalle { get; set; }

        [JsonProperty("oficinaExpedidora")]
        public string OficinaExpedidora { get; set; }

        [JsonProperty("estadoActual")]
        public string EstadoActual { get; set; }

        [JsonProperty("textoEstadoActual")]
        public string TextoEstadoActual { get; set; }

        [JsonProperty("estadoGovco")]
        public string EstadoGovco { get; set; }

        [JsonProperty("estadosTipoTramite")]
        public List<string> EstadosTipoTramite { get; set; }
    }
    #endregion
}