using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using tramites_servicios_webapi.Cancilleria.Models;
using tramites_servicios_webapi.common.Models;
using System.Dynamic;

namespace tramites_servicios_webapi.Cancilleria.Services
{

    public class TramitarPasaporteService
    {

        #region declaración de URLs 
        private static readonly HttpClient client;

        private const string UrlBase = "https://192.168.1.46:8243/";

        private const string AuthenticationToken = "ZlJBcmhDY0JUa1V3YWZBNDdjOGV4UDFSR05jYToxWTdIc3JxZjdxdWY3UUZNYkhxTlNvbmZsV2th";

        private const string UrlToken = "token";

        private const string UrlListarOficinas = "lista-oficina-cancilleria/v1/";

        private const string UrlListarTiposPasapote = "lista-tipos-pasaporte/v1/";

        private const string UrlTiposDocumento = "tipos-documento-cancilleria/v1/";

        private const string UrlMotivos = "/lista-motivos-cancilleria/v1/";

        private const string UrlTratamientoDatos = "tratamiento-datos-cancilleria/v1";

        private const string UrlSolicitarPasaporte = "solicitar-pasaporte/v1/";

        private const string UrlObtenerDataComprobante = "consultar-recibo-pago-cancilleria/v1";

        private const string UrlConsultarSolicitud = "consulta-estado-pasaporte-cancilleria/v1";    

        #endregion

        static TramitarPasaporteService()
        {
            //TODO: se usa para consultar la URL expuesta de la agencia, cambiar cuando cambie
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };

            client = new HttpClient(clientHandler);
            client.BaseAddress = new Uri(UrlBase);
        }

        public static async Task<TokenModel> GetToken()
        {
            String resultado = null;
            TokenModel tokenModel = new TokenModel();

            //Content
            HttpContent content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials")
            });
            client.DefaultRequestHeaders.Accept.Clear();
            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", AuthenticationToken);

            try
            {
                HttpResponseMessage resp = await client.PostAsync(UrlToken, content);
                if (resp.IsSuccessStatusCode)
                {
                    resultado = resp.Content.ReadAsStringAsync().Result;
                    tokenModel = JsonConvert.DeserializeObject<TokenModel>(resultado);
                    tokenModel.State = true;
                }

            }
            catch (Exception e)
            {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            Console.WriteLine(tokenModel.AccessToken);
            return tokenModel;
        }

        /// <summary>
        /// Obtiene el listado de oficinas disponibles para el trámite
        /// </summary>
        /// <returns>Listado de oficinas</returns>
        public static async Task<List<ItemList>> GetListsOficinas()
        {
            TokenModel tokenModel = await TokenTramiteService.GetToken(client, UrlToken, AuthenticationToken);
            if (!tokenModel.State)
            {
                //TODO: verificar retornos
                return null;
            }

            //Construye el encabezado
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            try
            {
                HttpResponseMessage resp = await client.GetAsync(UrlListarOficinas);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    OficinasModel oficinasList = JsonConvert.DeserializeObject<OficinasModel>(response);

                    if (oficinasList != null && oficinasList.Response.Mensaje.Texto.Equals("OK"))
                    {
                        return oficinasList.OficinaList;
                    }
                }
                else
                {
                    //TODO: incluir errores
                    return null;
                }

            }
            catch (Exception e)
            {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            return null;
        }

        /// <summary>
        /// Obtiene los tipos de documentos válidos para el trámite
        /// </summary>
        /// <returns>Tipos de documento</returns>
        public static async Task<ItemList> GetListTiposDocumento()
        {
            TokenModel tokenModel = await TokenTramiteService.GetToken(client, UrlToken, AuthenticationToken);
            if (!tokenModel.State)
            {
                //TODO: verificar retornos
                return null;
            }

            //Construye el encabezado
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            try
            {
                HttpResponseMessage resp = await client.GetAsync(UrlTiposDocumento);

                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    TiposDocumentoModel tiposDocumentoList = JsonConvert.DeserializeObject<TiposDocumentoModel>(response);

                    if (tiposDocumentoList != null && tiposDocumentoList.Response.Mensaje.Texto.Equals("OK"))
                    {
                        return tiposDocumentoList.TiposDocumento;
                    }
                }
                else
                {
                    //TODO: incluir errores
                    return null;
                }

            }
            catch (Exception e)
            {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            return null;
        }

        /// <summary>
        /// obtiene los tipos de pasaporte para selección
        /// </summary>
        /// <returns>Tipos de pasaporte</returns>
        public static async Task<List<ItemList>> GetListTipoPasaporte()
        {
            TokenModel tokenModel = await TokenTramiteService.GetToken(client, UrlToken, AuthenticationToken);
            if (!tokenModel.State)
            {
                //TODO: verificar retornos
                return null;
            }

            //Construye el encabezado
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            try
            {
                HttpResponseMessage resp = await client.GetAsync(UrlListarTiposPasapote);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    TiposPasaporteModel tiposPasaporteList = JsonConvert.DeserializeObject<TiposPasaporteModel>(response);

                    if (tiposPasaporteList != null && tiposPasaporteList.Response.Mensaje.Texto.Equals("OK"))
                    {
                        return tiposPasaporteList.TipoPasaporte;
                    }
                }
                else
                {
                    //TODO: incluir errores
                    return null;
                }

            }
            catch (Exception e)
            {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            return null;
        }

        /// <summary>
        /// Obtiene los tipos de motivos de solicitud
        /// </summary>
        /// <param name="tipoPasaporte">Tipo de pasaporte</param>
        /// <returns>Listado de motivos de solicitud de acuerdo al tipo</returns>    
        public static async Task<List<ItemList>> GetListMotivosSolicitud(long tipoPasaporte)
        {
            TokenModel tokenModel = await TokenTramiteService.GetToken(client, UrlToken, AuthenticationToken);
            if (!tokenModel.State)
            {
                //TODO: verificar retornos
                return null;
            }

            //Construye el encabezado
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            try
            {
                string url = string.Format("{0}?idTipoPasaporte={1}", UrlMotivos, tipoPasaporte);

                var resp = await client.GetAsync(url);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    dynamic motivosSolicitudList = JsonConvert.DeserializeObject<MotivosSolicitudModel>(response);

                    if (motivosSolicitudList != null && motivosSolicitudList.Response.Mensaje.Texto.Equals("OK"))
                    {
                        return motivosSolicitudList.MotivosSolicitud;
                    }
                }
                else
                {
                    //TODO: incluir errores
                    return null;
                }

            }
            catch (Exception e)
            {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            return null;

        }

        /// <summary>
        /// Obtiene los tipos de motivos de solicitud
        /// </summary>        
        /// <returns>Objeto con la información del tratamiento de datos personales</returns>
        public static async Task<TratamientoDatosBodyModel> GetTrataamientoDatosCancilleria()
        {

            TratamientoDatosBodyModel result = new TratamientoDatosBodyModel();

            TokenModel tokenModel = await TokenTramiteService.GetToken(client, UrlToken, AuthenticationToken);
            if (!tokenModel.State)
            {
                //TODO: verificar retornos
                return result;
            }

            //Construye el encabezado
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            try
            {
                HttpResponseMessage resp = await client.GetAsync(UrlTratamientoDatos);

                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<TratamientoDatosBodyModel>(response);
                    return result;
                }
                else
                {
                    //TODO: incluir errores
                    return result;
                }

            }
            catch (Exception e)
            {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            return result;
        }

        /// <summary>
        /// Registra la solicitud de un pasaporte
        /// </summary>
        /// <param name="modelSolicitud">modelo de datos con la información del pasaporte a solicitar</param>
        /// <returns>Objeto de tipo JsonResult con la información de la transacción</returns>    
        public static async Task<string> InsSolicitudPasaporte(DataSolicitudPasaporteModel modelSolicitud)
        {
            var dataResult = "";

            TokenModel tokenModel = await TokenTramiteService.GetToken(client, UrlToken, AuthenticationToken);
            if (!tokenModel.State)
            {
                //TODO: verificar retornos
                return dataResult;
            }

            //Mapeo de datos.
            var infoNuevaSolicitud = new InfoNuevaSolicitudModel();
            infoNuevaSolicitud.motivoSolicitud = modelSolicitud.motivoSolicitud;
            infoNuevaSolicitud.oficina = modelSolicitud.oficina;
            infoNuevaSolicitud.tipoPasaporte = modelSolicitud.tipoPasaporte;

            var infoPasaporteActual = new InfoPasaporteActualModel();
            infoPasaporteActual.digitoVerificacionOCR = modelSolicitud.digitoVerificacionOCR;
            infoPasaporteActual.fechaExpedicionPasaporte = modelSolicitud.fechaExpedicionPasaporte;
            infoPasaporteActual.numeroPasaporte = modelSolicitud.numeroPasaporte;
            
            var infoSolicitante = new InfoSolicitanteModel();
            infoSolicitante.codigoDocumento = modelSolicitud.codigoDocumento;
            infoSolicitante.correoElectronico = modelSolicitud.correoElectronico;
            infoSolicitante.fechaExpedicionDocIdentidad = modelSolicitud.fechaExpedicionDocIdentidad;
            infoSolicitante.numeroPersonal = modelSolicitud.numeroPersonal;
            
            var argSolicitudModel = new ArgSolicitudModel();
            argSolicitudModel.infoNuevaSolicitud = infoNuevaSolicitud;
            argSolicitudModel.infoPasaporteActual = infoPasaporteActual;
            argSolicitudModel.infoSolicitante = infoSolicitante;
            argSolicitudModel.ipCiudadano = "172.20.2.166";

            var bodySolicitudModel = new bodySolicitudModel();
            bodySolicitudModel.arg0 = argSolicitudModel;

            //COnversión a Json
            string output = JsonConvert.SerializeObject(bodySolicitudModel);

            //Se contruye el body
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");

            //Construye el encabezado
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            try
            {
                HttpResponseMessage resp = await client.PostAsync(UrlSolicitarPasaporte, stringContent);

                if (resp.IsSuccessStatusCode)
                {
                    dataResult = resp.Content.ReadAsStringAsync().Result;
                    //dataResult = JsonConvert.DeserializeObject<DataResultSolicittudModel> (response);                    

                    return dataResult;
                    //return response;

                }
                else
                {
                    //TODO: incluir errores                    
                    return dataResult;
                }

            }
            catch (Exception e)
            {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            return dataResult;

        }

        /// <summary>
        /// Obtiene la data de la información de pago del pasaporte
        /// </summary>
        /// <param name="solicitudId">Id de la solicitud de pasaporte</param>
        /// <returns>Objeto de tipo JsonResult con la información de la transacción</returns>    
        public static async Task<string> ObtenerDataComprobante(string solicitudId)
        {
            TokenModel tokenModel = await TokenTramiteService.GetToken(client, UrlToken, AuthenticationToken);
            if (!tokenModel.State)
            {
                //TODO: verificar retornos
                return null;
            }

            //Mapeo de datos.
            var bodyComprobanteModel = new BodyComprobanteModel();
            bodyComprobanteModel.numeroReferencia = solicitudId;

            //Conversión a Json
            var output = JsonConvert.SerializeObject(bodyComprobanteModel);        

            //Se contruye el body
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");

            //Construye el encabezado
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            try
            {
                HttpResponseMessage resp = await client.PostAsync(UrlObtenerDataComprobante, stringContent);        
                
                if (resp.IsSuccessStatusCode == true)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    dynamic result = JsonConvert.DeserializeObject<DataComprobanteModel> (response);                    
                    return result.obtenerReciboResult;

                }
                else
                {
                    //TODO: incluir errores
                    return null;
                }

            }
            catch (Exception e)
            {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            return null;

        }

        
        /// <summary>
        /// Consulta una solicitud de pasaporte.
        /// </summary>
        /// <param name="modelDataConsultaSolicitud">modelo de datos con la información para realizar la consulta de la solicitud</param>
        /// <returns>Objeto de tipo JsonResult con la información de la transacción</returns>    
        public static async Task<DataResultConsultaModel> GetDataSolicitud(DataConsultaSolicitudModel modelDataConsultaSolicitud)
        {
            var result = new DataResultConsultaModel();

            TokenModel tokenModel = await TokenTramiteService.GetToken(client, UrlToken, AuthenticationToken);
            if (!tokenModel.State)
            {
                //TODO: verificar retornos
                return result;
            }

            //Mapeo de datos.
            var bodyConsultaSolicitudModel = new BodyConsultaSolicitudModel();

            bodyConsultaSolicitudModel.arg0 = modelDataConsultaSolicitud;

            //COnversión a Json
            string output = JsonConvert.SerializeObject(bodyConsultaSolicitudModel);           

            //Se contruye el body
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");

            //Construye el encabezado
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            try
            {
                HttpResponseMessage resp = await client.PostAsync(UrlConsultarSolicitud, stringContent);

                if (resp.IsSuccessStatusCode)
                {                    
                    string response = resp.Content.ReadAsStringAsync().Result;
                    result = JsonConvert.DeserializeObject<DataResultConsultaModel>(response);
                    return result;

                }
                else
                {
                    //TODO: incluir errores
                    return result;
                }

            }
            catch (Exception e)
            {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            return result;

        }
    }

}