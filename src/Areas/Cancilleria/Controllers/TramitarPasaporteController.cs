using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Cancilleria.Services;
using tramites_servicios_webapi.common.Models;
using System.Linq;
using tramites_servicios_webapi.Cancilleria.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;



namespace tramites_servicios_webapi.Controllers
{
    [Route ("api/[controller]")]
    [ApiController]
    public class TramitarPasaporteController : ControllerBase
    {
        /// <summary>
        /// Obtiene el listado de oficinas disponibles para el trámite
        /// </summary>
        /// <returns>Listado de oficinas disponibles para el trámite</returns>
        [HttpGet("obteneroficinas")]        
        public async Task<IActionResult>  ObtenerOficinas() {
            var response = new List<SelectListItem>();
            //TODO: Modificar el método a asincrono si realiza peticiones a servidor            
            var tiposDocumento = await TramitarPasaporteService.GetListsOficinas();

            if (tiposDocumento != null ) {
                response = tiposDocumento.Select(item => new SelectListItem {
                    Value = item.Id,
                    Text = item.Nombre
                }).ToList(); 
            }
            return new JsonResult(response);
        }   

        /// <summary>
        /// Obtiene el listado de oficinas disponibles para el trámite
        /// </summary>
        /// <returns>Listado de oficinas disponibles para el trámite</returns>
        [HttpGet("obtenertiposdocumento")]        
        public async Task<IActionResult>  ObtenerTiposDocumento() {
            SelectListItem response = new SelectListItem();
            //TODO: Modificar el método a asincrono si realiza peticiones a servidor            
            var tiposDocumento = await TramitarPasaporteService.GetListTiposDocumento();

            if (tiposDocumento != null ) {
                response = new SelectListItem (tiposDocumento.Id, tiposDocumento.Nombre);
            }
            return new JsonResult(response);
        }   
        
        /// <summary>
        /// Obtiene el listado de oficinas disponibles para el trámite
        /// </summary>
        /// <returns>Listado de oficinas disponibles para el trámite</returns>
        [HttpGet("ObtenerTiposPasaporte")]        
        public async Task<IActionResult>  ObtenerTiposPasaporte() {
            var response = new List<SelectListItem>();
            //TODO: Modificar el método a asincrono si realiza peticiones a servidor            
            var tiposPasaporte = await TramitarPasaporteService.GetListTipoPasaporte();

            if (tiposPasaporte != null ) {
                response = tiposPasaporte.Select(item => new SelectListItem {
                    Value = item.Id,
                    Text = item.Nombre
                }).ToList(); 
            }
            return new JsonResult(response);
        }  

        /// <summary>
        /// Obtiene el listado de oficinas disponibles para el trámite
        /// </summary>
        /// <returns>Listado de oficinas disponibles para el trámite</returns>
        [HttpGet("ObtenerMotivosSolicitud")]        
        public async Task<IActionResult>  ObtenerMotivosSolicitud(long tipoPasaporte) {
            var response = new List<SelectListItem>();
            //TODO: Modificar el método a asincrono si realiza peticiones a servidor            
            var tiposDocumento = await TramitarPasaporteService.GetListMotivosSolicitud(tipoPasaporte);

            if (tiposDocumento != null ) {
                response = tiposDocumento.Select(item => new SelectListItem {
                    Value = item.Id,
                    Text = item.Nombre
                }).ToList(); 
            }
            return new JsonResult(response);
        }

        /// <summary>
        /// Obtiene la información del tratamiento de datos personales
        /// </summary>
        /// <returns>Información del tratamiento de datos personales</returns>
        [HttpGet("ObtenerTratamientoDatosPersonales")]
        public async Task<IActionResult>  ObtenerTratamientoDatosPersonales() {            
             var response = new TratamientoDatosBodyModel();
            //TODO: Modificar el método a asincrono si realiza peticiones a servidor            
            response = await TramitarPasaporteService.GetTrataamientoDatosCancilleria();
            
            return new JsonResult(response);

        }

        /// <summary>
        /// Realiza el resgistro de la solicitud de pasaporte.
        /// </summary>
        /// <returns></returns>
        [HttpPost("SolicitarPasaporte")]        
        public async Task<IActionResult>  SolicitarPasaporte([FromBody] DataSolicitudPasaporteModel modelSolicitud) {

            //var response = new DataResultSolicittudModel();
            var response = "";

            //TODO: Modificar el método a asincrono si realiza peticiones a servidor            
            response = await TramitarPasaporteService.InsSolicitudPasaporte(modelSolicitud);

            return new JsonResult(response);
            
        }

        /// <summary>
        /// Obtiene la data del comprobante de pago.
        /// </summary>
        /// <returns>Objeton de tipo JsonResult con la información de la transacción</returns>
        [HttpPost("ObtenerDataComprobante")]        
        public async Task<IActionResult>  ObtenerDataComprobante(string solicitudId) {
            string response = "";
            //TODO: Modificar el método a asincrono si realiza peticiones a servidor            
            var obtenerData = await TramitarPasaporteService.ObtenerDataComprobante(solicitudId);

            if (obtenerData != null ) {
                response = obtenerData;
            }
            return new JsonResult(response);
        }

        /// <summary>
        /// Realiza el resgistro de la solicitud de pasaporte.
        /// </summary>
        /// <returns></returns>
        [HttpPost("ConsultarSolicitud")]        
        public async Task<IActionResult>  ConsultarSolicitud([FromBody] DataConsultaSolicitudModel modelConsulta) {

            var response = new DataResultConsultaModel();            
  
            //TODO: Modificar el método a asincrono si realiza peticiones a servidor            
            var getDataSolicitud = await TramitarPasaporteService.GetDataSolicitud(modelConsulta);

            if (getDataSolicitud != null ) {
                response = getDataSolicitud;
            }
            return new JsonResult(response);
        }
    }
}