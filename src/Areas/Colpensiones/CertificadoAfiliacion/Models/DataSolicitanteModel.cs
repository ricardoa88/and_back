using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Colpensiones.CertificadoAfiliacion.Models
{
    /// <summary>
    /// Modelo con la información del solicitante certificado de afiliación
    /// </summary>
    public class DataSolicitanteModel
    {
        /// <summary>
        /// Tipo de documento
        /// </summary>
        public string TipoDocumento { get; set; }

        /// <summary>
        /// Numero de documento de identificación
        /// </summary>
        public string Documento { get; set; }

        /// <summary>
        /// Correo electrónico
        /// </summary>
        public string Email { get; set; }
    }
}