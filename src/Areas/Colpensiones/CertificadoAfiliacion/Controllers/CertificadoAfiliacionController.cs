using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Colpensiones.CertificadoAfiliacion.Models;
using tramites_servicios_webapi.Colpensiones.CertificadoAfiliacion.Services;

namespace tramites_servicios_webapi.Colpensiones.CertificadoAfiliacion.Controllers
{
    /// <summary>
    /// Certificados de afiliación COLPENSIONES
    /// </summary>
    [Route ("api/colpensiones/[controller]")]
    [ApiController]
    public class CertificadoAfiliacionController : ControllerBase 
    {
        /// <summary>
        /// Descarga los certificados de afiliación
        /// </summary>
        /// <param name="data">Objeto con los datos del solicitanter</param>
        /// <returns>Certificado de afiliación</returns>
        [HttpPost ("descargar")]
        public async Task<IActionResult> Descargar ([FromBody] DataSolicitanteModel data) 
        {
            var respuesta = await CertificadoAfiliacionService.Descargar (data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }

        /// <summary>
        /// Enviar certificado al correo 
        /// </summary>
        /// <param name="data">Objeto con los datos del solicitanter</param>
        /// <returns>Estado del envio de correo</returns>
        [HttpPost ("enviar")]
        public async Task<IActionResult> Enviar ([FromBody] DataSolicitanteModel data) 
        {
            var respuesta = await CertificadoAfiliacionService.Enviar (data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }
        
    }
}