using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using tramites_servicios_webapi.common.Models;
using tramites_servicios_webapi.Colpensiones.CertificadoAfiliacion.Models;

namespace tramites_servicios_webapi.Colpensiones.CertificadoAfiliacion.Services
{
    public class CertificadoAfiliacionService
    {
        private static readonly HttpClient _client;
        private const String _urlBase = "https://api.www.gov.co/services/";
        private const String _urlToken = "token";
        private const String _urlDescargar = "colpensiones-certificado/v1";
        private const String _urlEnviar = "colpensiones-envio-email-certificado/v1";


        static CertificadoAfiliacionService()
        {
            //Omitit Certificado SSL
            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual;
            handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {
                    return true;
                };

            _client = new HttpClient(handler);
            _client.BaseAddress = new Uri(_urlBase);
        }

        /// <summary>
        /// Descargar certificado de afiliación
        /// </summary>
        /// <param name="data">Modelo</param>
        /// <returns>Certificado de afiliación</returns>
        public static async Task<FileContentResult> Descargar(DataSolicitanteModel data)
        {
            // Inicializaciones
            FileContentResult resultado = null;

            //Token
            TokenModel tokenModel = await CertificadoAfiliacionService.ObtenerToken();


              //Body XML
             XNamespace ns = "http://schemas.xmlsoap.org/soap/envelope/";
             XNamespace per = "http://www.colpensiones.gov.co/contracts/1.0/personas";
             XNamespace per1 = "http://www.colpensiones.gov.co/schemas/1.0/personas";
             XNamespace com = "http://www.colpensiones.gov.co/schemas/1.0/comun";
             XNamespace tram = "http://www.colpensiones.gov.co/schemas/1.0/comun/tramites";

             XNamespace ns2 = "http://www.colpensiones.gov.co/schemas/1.0/personas";
           
            //Soap Request
            XDocument soapRequest = new XDocument(
                new XElement(ns + "Envelope",
                    new XAttribute(XNamespace.Xmlns + "per", per),
                    new XAttribute(XNamespace.Xmlns + "per1", per1),
                    new XAttribute(XNamespace.Xmlns + "com", com),
                    new XAttribute(XNamespace.Xmlns + "tram", tram),
                    new XAttribute(XNamespace.Xmlns + "soapenv", ns),
                    new XElement(ns + "Body",
                         new XElement(per + "CriteriosCertificacionAfiliacionDTO",
                            new XElement(per1 + "Contexto",
                                new XElement(com + "usuario", "USUARIO"),
                                new XElement(com + "fuente", "WEB")
                            ),
                            new XElement(per1 + "Detalle",
                                new XElement(per1 + "identificacionSolicitante",
                                    new XElement(per1 + "tipoIdentificacion", data.TipoDocumento),
                                    new XElement(per1 + "numIdentificacion", data.Documento)
                                ),
                                new XElement(per1 + "configuracionCertificado",
                                    new XElement(com + "formato", "WEB"),
                                    new XElement(com + "informacionComplementaria",
                                        new XElement(com + "propiedad",
                                            new XElement(com + "nombre", ""),
                                            new XElement(com + "valor", "")
                                        )
                                    )
                                ),
                                new XElement(per1 + "guardarGestorDocumental"),
                                new XElement(per1 + "identificacionTramite",
                                    new XElement(tram + "codigoTipo", ""),
                                    new XElement(tram + "codigoSubTipo", ""),
                                    new XElement(tram + "numRadicacion", "")
                                )
                            )
                        )
                    )
                ));

           //Default Request Headers
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("SOAPAction", "http://www.colpensiones.gov.co/contracts/1.0/personas/ContratoSvcAfiliado/GenerarCertificacionAfiliacion");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);
            StringContent stringContent = new StringContent(soapRequest.ToString(), Encoding.UTF8, "text/xml");

            //Petición API Gateway
            try
            {
                HttpResponseMessage resp = await _client.PostAsync(_urlDescargar, stringContent);
                if (resp.IsSuccessStatusCode)
                {
                    Task<Stream> streamTask = resp.Content.ReadAsStreamAsync();
                    Stream stream = streamTask.Result;
                    var sr = new StreamReader(stream);
                    var soapResponse = XDocument.Load(sr);
                    var xml = soapResponse.Descendants(ns2 + "contenido").FirstOrDefault().ToString();
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xml);
                    var json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);
                    
                    //Salida XML->JSON
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);
                    var item = jsonObj["ns2:contenido"]["#text"];
                    var stringByte = item.ToString();
                    var bytes = Convert.FromBase64String(stringByte);
                    resultado = new FileContentResult (bytes, "application/pdf");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            //Resultado
            return resultado;
        }

        /// <summary>
        /// Enviar certificado de afiliación al correo
        /// </summary>
        /// <param name="data">Modelo</param>
        /// <returns>Estado del envio</returns>
        public static async Task<JsonResult> Enviar(DataSolicitanteModel data)
        {
            // Inicializaciones
            JsonResult resultado = null;
            var mensaje = "<![CDATA[<div><div><center><img src='https://www.colpensiones.gov.co/info/rpm/media/bloque20077903.png'></center><h4>Se&#241;or(a)(es) usuario(a)(os):</h4><p>De acuerdo a su solicitud realizada a trav&#233;s de nuestro Portal Colpensiones, le hacemos llegar su <b>Certificado en Digital</b>. Para abrir el archivo, haga doble clic en el adjunto y seleccione la opci&#243;n <b>Abrir</b>. Si no puede visualizar su certificado, usted necesitar&#225; instalar la &#250;ltima versi&#243;n de Acrobat Reader.</p><br><center><h4>T&#233;rminos legales y condiciones.</h4></center><p>Este mensaje es confidencial, puede contener informaci&#243;n privilegiada y no puede ser usado ni divulgado por personas distintas de su destinatario. Recuerde que si por error recibe este correo, est&#225; prohibida su retenci&#243;n, grabaci&#243;n, utilizaci&#243;n, aprovechamiento o divulgaci&#243;n con cualquier prop&#243;sito.<br><br><b>Colpensiones</b> no asume ninguna responsabilidad por eventuales da&#241;os generados por el recibo y el uso de este material, siendo responsabilidad del destinatario verificar con sus propios medios la existencia de virus u otros defectos. Le recordamos que esta direcci&#243;n de correo electr&#243;nico es &#250;nicamente para env&#237;os autom&#225;ticos de informaci&#243;n y no para recibir respuestas o consultas. Si desea comunicarse con nosotros lo podr&#225; hacer a trav&#233;s de nuestras l&#237;neas de atenci&#243;n: <b>(1) 489 09 09</b> en Bogot&#225; - <b>(4) 283 60 90</b> en Medell&#237;n y resto del pa&#237;s: <b>018000 41 0909</b>.</p></div></div>]]>";
            //Token
            TokenModel tokenModel = await CertificadoAfiliacionService.ObtenerToken();
            String certificado = await CertificadoAfiliacionService.GetCertificado(data, tokenModel);
            
           


              //Body XML
             XNamespace ns = "http://schemas.xmlsoap.org/soap/envelope/";
             XNamespace oas = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
             XNamespace req = "http://www.colpensiones.gov.co/portal/common/WebServices/WSSE/RequestSystem";
             XNamespace sen = "http://www.colpensiones.gov.co/schemas/SendEmail";
             XNamespace wsu = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";

           
            //Soap Request
            XDocument soapRequest = new XDocument(
                new XElement(ns + "Envelope",
                    new XAttribute(XNamespace.Xmlns + "oas", oas),
                    new XAttribute(XNamespace.Xmlns + "req", req),
                    new XAttribute(XNamespace.Xmlns + "sen", sen),
                    new XAttribute(XNamespace.Xmlns + "soapenv", ns),
                    new XElement(ns + "Header",
                        new XElement(req + "System",
                            new XElement(req + "InputSystem", "SOAPUI"),
                            new XElement(req + "ApplicationID", "SOAPUI"),
                            new XElement(req + "TransactionID", "081120191755"),
                            new XElement(req + "IP")
                        ),
                        new XElement(oas + "Security",
                            new XAttribute(ns + "mustUnderstand", "1"),
                            new XAttribute(XNamespace.Xmlns + "wsu", wsu),
                            new XElement(oas + "UsernameToken",
                                new XAttribute(wsu + "Id", "UsernameToken-C8F446CB459F41EC1915717723448675"),
                                new XElement(oas + "Username", "wbsappmovil"),
                                new XElement(oas + "Password", "Valida.1791")
                            )
                        )
                    ),
                    new XElement(ns + "Body",
                        new XElement(sen + "SendEmailRequest",
                             new XElement(sen + "Notificacion",
                                 new XElement(sen + "tipoNotificacion", "Certificado de afiliacion"),
                                 new XElement(sen + "de", "servicioalciudadano@colpensiones.gov.co"),
                                 new XElement(sen + "responderA", "no-reply@colpensiones.gov.co"),
                                 new XElement(sen + "para", 
                                    new XElement(sen + "correo1", data.Email)
                                 ),
                                 new XElement(sen + "cc", 
                                    new XElement(sen + "correo1")
                                 ),
                                 new XElement(sen + "cco", 
                                    new XElement(sen + "correo1")
                                 ),
                                new XElement(sen + "asunto", "Nuevo certificado de afiliación"),
                                new XElement(sen + "mensaje", mensaje),
                                new XElement(sen + "adjuntos",
                                     new XElement(sen + "adjunto", data.TipoDocumento+""+data.Documento+".pdf"),
                                     new XElement(sen + "contenido", certificado)
                                ),
                                new XElement(sen + "esCertificacion", "false")
                             )
                        )
                    )
                ));
                
           //Default Request Headers
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("SOAPAction", "SendEmail");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);
            StringContent stringContent = new StringContent(soapRequest.ToString(), Encoding.UTF8, "text/xml");

            //Petición API Gateway
            try
            {
                HttpResponseMessage resp = await _client.PostAsync(_urlEnviar, stringContent);
                if (resp.IsSuccessStatusCode)
                {
                    Task<Stream> streamTask = resp.Content.ReadAsStreamAsync();
                    Stream stream = streamTask.Result;
                    var sr = new StreamReader(stream);
                    var soapResponse = XDocument.Load(sr);
                    resultado = new JsonResult("");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            
            //Resultado
            return resultado;
        }

        /// <summary>
        /// Obtener certificado
        /// </summary>
        /// <param name="data">Modelo</param>
        /// <param name="tokenModel">Token</param>
        /// <returns>Certificado de afiliación</returns>
        public static async Task<String> GetCertificado(DataSolicitanteModel data, TokenModel tokenModel)
        {
            // Inicializaciones
            String resultado = null;

              //Body XML
             XNamespace ns = "http://schemas.xmlsoap.org/soap/envelope/";
             XNamespace per = "http://www.colpensiones.gov.co/contracts/1.0/personas";
             XNamespace per1 = "http://www.colpensiones.gov.co/schemas/1.0/personas";
             XNamespace com = "http://www.colpensiones.gov.co/schemas/1.0/comun";
             XNamespace tram = "http://www.colpensiones.gov.co/schemas/1.0/comun/tramites";

             XNamespace ns2 = "http://www.colpensiones.gov.co/schemas/1.0/personas";
           
            //Soap Request
            XDocument soapRequest = new XDocument(
                new XElement(ns + "Envelope",
                    new XAttribute(XNamespace.Xmlns + "per", per),
                    new XAttribute(XNamespace.Xmlns + "per1", per1),
                    new XAttribute(XNamespace.Xmlns + "com", com),
                    new XAttribute(XNamespace.Xmlns + "tram", tram),
                    new XAttribute(XNamespace.Xmlns + "soapenv", ns),
                    new XElement(ns + "Body",
                         new XElement(per + "CriteriosCertificacionAfiliacionDTO",
                            new XElement(per1 + "Contexto",
                                new XElement(com + "usuario", "USUARIO"),
                                new XElement(com + "fuente", "WEB")
                            ),
                            new XElement(per1 + "Detalle",
                                new XElement(per1 + "identificacionSolicitante",
                                    new XElement(per1 + "tipoIdentificacion", data.TipoDocumento),
                                    new XElement(per1 + "numIdentificacion", data.Documento)
                                ),
                                new XElement(per1 + "configuracionCertificado",
                                    new XElement(com + "formato", "WEB"),
                                    new XElement(com + "informacionComplementaria",
                                        new XElement(com + "propiedad",
                                            new XElement(com + "nombre", ""),
                                            new XElement(com + "valor", "")
                                        )
                                    )
                                ),
                                new XElement(per1 + "guardarGestorDocumental"),
                                new XElement(per1 + "identificacionTramite",
                                    new XElement(tram + "codigoTipo", ""),
                                    new XElement(tram + "codigoSubTipo", ""),
                                    new XElement(tram + "numRadicacion", "")
                                )
                            )
                        )
                    )
                ));

           //Default Request Headers
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("SOAPAction", "http://www.colpensiones.gov.co/contracts/1.0/personas/ContratoSvcAfiliado/GenerarCertificacionAfiliacion");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);
            StringContent stringContent = new StringContent(soapRequest.ToString(), Encoding.UTF8, "text/xml");

            //Petición API Gateway
            try
            {
                HttpResponseMessage resp = await _client.PostAsync(_urlDescargar, stringContent);
                if (resp.IsSuccessStatusCode)
                {
                    Task<Stream> streamTask = resp.Content.ReadAsStreamAsync();
                    Stream stream = streamTask.Result;
                    var sr = new StreamReader(stream);
                    var soapResponse = XDocument.Load(sr);
                    var xml = soapResponse.Descendants(ns2 + "contenido").FirstOrDefault().ToString();
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(xml);
                    var json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);
                    
                    //Salida XML->JSON
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);
                    var item = jsonObj["ns2:contenido"]["#text"];
                    var stringByte = item.ToString();
                    var bytes = Convert.FromBase64String(stringByte);
                    //resultado = new FileContentResult (bytes, "application/pdf");
                    resultado = stringByte;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            //Resultado
            return resultado;
        }

        /// <summary>
        /// Obtener token tramites sena
        /// </summary>
        /// <returns>Token</returns>
        public static async Task<TokenModel> ObtenerToken()
        {
            // Inicializaciones
            String resultado = null;
            TokenModel tokenModel = null;

            //Content
            HttpContent content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials")
            });
            _client.DefaultRequestHeaders.Accept.Clear();
            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "UVJrYWpSTGZka2MzbERWalVGUm1nWEIwUmxvYTpkYXl4ZVdkQ1kydk5lenRWeUxxWG95OEpMeVVh");

            //Petición API Gateway
            try
            {
                HttpResponseMessage resp = await _client.PostAsync(_urlToken, content);
                if (resp.IsSuccessStatusCode)
                {
                    resultado = JsonConvert.SerializeObject(resultado);
                    resultado = resp.Content.ReadAsStringAsync().Result;
                    tokenModel = JsonConvert.DeserializeObject<TokenModel>(resultado);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            //Salida
            return tokenModel;
        }
    }
}