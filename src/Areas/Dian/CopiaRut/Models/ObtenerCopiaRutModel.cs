using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Dian.CopiaRut.Models {
    public class DocumentoModel {
        public string Valor { get; set; }

        public string Nombre { get; set; }

        public DominioValorModel Dominio { get; set; }
    }
    public class DominioValorModel {
        public int Codigo { get; set; }

        public string Nombre { get; set; }
    }
    public class SelectListItemModel {
        public string Text { get; set; }

        public string Value { get; set; }
    }
    public class ObtenerCopiaRutModel {
        [Required (ErrorMessage = "El tipo de documento es obligatorio.")]
        public string tipo_documento { get; set; }

        [Required (ErrorMessage = "Documento es obligatorio.")]
        public string numero_documento { get; set; }
    }
}