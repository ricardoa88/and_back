using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Dian.CopiaRut.Models;
using tramites_servicios_webapi.Dian.CopiaRut.Services;

namespace tramites_servicios_webapi.Dian.CopiaRut.Controllers {
    
    /// <summary>
    /// Servicios de la DIAN para la copia del rut
    /// </summary>
    [Route ("api/dian/[controller]")]
    [ApiController]
    public class CopiaRutController : ControllerBase {

        /// <summary>
        /// Obtiene los tipos de documentos de la DIAN para la copia del rut
        /// </summary>
        /// <returns>Listado de tipos de documentos de la entidad</returns>
        [HttpGet ("obtenerTiposDocumentos")]
        public async Task<IActionResult> ObtenerTiposDocumentos () {

            var respuesta = await CopiaRutService.ObtenerTiposDocumentos ();

            if (respuesta == null) {
                return NotFound ();
            }

            return respuesta;
        }

        /// <summary>
        /// Obtiene copa de rut del la DIAN
        /// </summary>
        /// <param name="data"></param>
        /// <returns>respuesta http</returns>
        [HttpPost ("obtenerCopiaRut")]
        public async Task<IActionResult> ObtenerCopiaRut ([FromBody] ObtenerCopiaRutModel data) {

            if (!ModelState.IsValid) {
                return BadRequest (ModelState);
            }

            var respuesta = await CopiaRutService.ObtenerCopiaRut (data);

            if (respuesta == null) {
                return NotFound ();
            }

            return respuesta;
        }
    }
}