using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using tramites_servicios_webapi.Dian.CopiaRut.Models;

namespace tramites_servicios_webapi.Dian.CopiaRut.Services {
    public class CopiaRutService {
        private static readonly HttpClient _client;
        private const String _urlBase = "http://190.60.233.151:8885/api/v1/";
        private const String _urlObtenerDocumentos = "";
        private const String _urlObtenerCopiaRut = "obtener-rut-dian/";
        static CopiaRutService () {
            _client = new HttpClient ();
            _client.BaseAddress = new Uri (_urlBase);
        }

        public static async Task<JsonResult> ObtenerTiposDocumentos () {
            JsonResult resultado = null;
            try {
                var tiposDocumentos = new List<SelectListItemModel> ();
                SelectListItemModel tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "46";
                tipoDocumento.Text = "Carné Diplomático";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "14";
                tipoDocumento.Text = "Certificado Registraduría sin identificación";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "13";
                tipoDocumento.Text = "Cédula de ciudadanía";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "22";
                tipoDocumento.Text = "Cédula de extranjería";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "43";
                tipoDocumento.Text = "Sin identificación del exterior o para uso definido por DIAN";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "42";
                tipoDocumento.Text = "Documento de identificación extranjero";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "44";
                tipoDocumento.Text = "Documento de Identificación extranjero Persona Jurídica";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "33";
                tipoDocumento.Text = "Identificación de extranjeros diferente al NIT asignado DIAN";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "15";
                tipoDocumento.Text = "Identificación sucesión";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "31";
                tipoDocumento.Text = "NIT";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "41";
                tipoDocumento.Text = "Pasaporte";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "11";
                tipoDocumento.Text = "Registro civil de nacimiento";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "34";
                tipoDocumento.Text = "No posee documento de identificación";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "21";
                tipoDocumento.Text = "Tarjeta de extranjería";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "12";
                tipoDocumento.Text = "Tarjeta de identidad";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "32";
                tipoDocumento.Text = "Tipo de documento desconocido";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "45";
                tipoDocumento.Text = "Tipo Documento Inactivación Persona";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                tipoDocumento.Value = "90";
                tipoDocumento.Text = "Sistema";
                tiposDocumentos.Add (tipoDocumento);
                tipoDocumento = new SelectListItemModel ();
                resultado = new JsonResult (tiposDocumentos);

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }

            return resultado;
        }

        public static async Task<JsonResult> ObtenerCopiaRut (ObtenerCopiaRutModel data) {

            JsonResult resultado = null;

            Dictionary<string, string> content = new Dictionary<string, string> ();
            content.Add ("numero_documento", data.numero_documento);
            content.Add ("tipo_documento", data.tipo_documento);
            string output = JsonConvert.SerializeObject (content);

            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");

            try {

                HttpResponseMessage resp = await _client.PostAsync (_urlObtenerCopiaRut, stringContent);

                if (resp.IsSuccessStatusCode) {
                    string response = resp.Content.ReadAsStringAsync ().Result;
                    var obj = JsonConvert.DeserializeObject (response);
                    resultado = new JsonResult (obj);
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }

            return resultado;
        }
    }
}