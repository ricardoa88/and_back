
namespace tramites_servicios_webapi.common.Models {
    /// <summary>
    /// Modelo con la información de identificación
    /// </summary>
    public class IdentificacionModel
    {
        /// <summary>
        /// Tipo de documento 
        /// </summary>
        public string TipoDocumento { get; set; }

        /// <summary>
        /// Numero de documento
        /// </summary>
         public string NumeroDocumento { get; set; }
    }
}