
using System;

namespace tramites_servicios_webapi.common.Models {
    public class SelectListItem {
        public string Value { get; set; }

        public string Text { get; set; }

        public SelectListItem() {}

        public SelectListItem(string value, string text) {
            this.Value = value;
            this.Text = text;
        }
    }
}