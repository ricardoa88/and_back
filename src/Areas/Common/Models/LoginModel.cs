
namespace tramites_servicios_webapi.common.Models {
    /// <summary>
    /// Modelo con para iniciar sesión
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// Usuario 
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Contraseña usuario 
        /// </summary>
         public string Password { get; set; }
    }
}