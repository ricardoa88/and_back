using System.ComponentModel.DataAnnotations;
using System.Net;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.common.Models {
    public class TokenModel {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        public bool State { get; set; }

        public string Message { get; set; }

        public TokenModel() {
            State = false;
        }
    }
}