
namespace tramites_servicios_webapi.common.Models {
    public class ResponseModel {
        public bool Success { get; set; }

        public string Message { get; set; }

        public dynamic Result { get; set; }

        public ResponseModel() {
            this.Success = false;
        }

        public ResponseModel(bool success, string message) {
            Success = success;
            Message = message;
        }
    }
}