using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using tramites_servicios_webapi.common.Models;

namespace tramites_servicios_webapi.Cancilleria.Services {

    public class TokenTramiteService {

        public TokenTramiteService() {}

        /// <summary>
        /// Método para obtener el token de ws de "cualquier" trámite
        /// </summary>
        /// <param name="client">cliente configurado en el servicio que llama</param>
        /// <param name="url"Url del ws para obtener el token</param>
        /// <param name="authenticationToken">token para realizar la petición</param>
        /// <returns>Token o no token</returns>
        public static async Task<TokenModel> GetToken(HttpClient client, string url, string authenticationToken) {
            String resultado = null;
            TokenModel tokenModel = new TokenModel();
            //Content
            HttpContent content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials")
            });
            client.DefaultRequestHeaders.Accept.Clear();
            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authenticationToken);

            try {
                HttpResponseMessage resp = await client.PostAsync(url, content);
                if (resp.IsSuccessStatusCode) {
                    resultado = resp.Content.ReadAsStringAsync ().Result;
                    tokenModel = JsonConvert.DeserializeObject<TokenModel> (resultado);
                    tokenModel.State = true;
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }
            return tokenModel;
        }
    }
}