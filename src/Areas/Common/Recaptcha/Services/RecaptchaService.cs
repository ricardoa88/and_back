using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace tramites_servicios_webapi.common.Recaptcha.Services {
    public class RecaptchaService {
        private const string GoogleUrl = "https://www.google.com/recaptcha/api/siteverify?";
        private const string secretKey = "6LeCqOcUAAAAAO8-b_rF4OxU7yt7cvkejRkv15GM";
        public RecaptchaService() { }

        /// <summary>
        /// Realiza la validación del recaptcha y retorna si pasó o no
        /// </summary>
        /// <param name="clientKey">Llave entregada por el componente recatpcha en el cliente</param>
        /// <param name="serverKey">llave generada por el recaptcha para validación con el servidor</param>
        /// <returns>Resultado de la validación</returns>
        public static async Task<bool>  RecaptchaValidation(string clientKey) {
            HttpClient httpClient = new HttpClient();

            string url = string.Format("{0}secret={1}&response={2}", GoogleUrl, secretKey, clientKey);
            HttpResponseMessage res = await httpClient.GetAsync(url);
            if (!res.IsSuccessStatusCode)  {
                return false;
            }
            
            string jsonResult = res.Content.ReadAsStringAsync().Result;
            dynamic JSONdata = JObject.Parse(jsonResult);
            if (JSONdata.success != "true") {
                return false;
            }

            return true;
        }
    }
}
