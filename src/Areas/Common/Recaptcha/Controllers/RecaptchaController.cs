using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.common.Recaptcha.Services;
using tramites_servicios_webapi.common.Models;
using tramites_servicios_webapi.common.Recaptcha.Models;

namespace tramites_servicios_webapi.Controllers
{
    [Route ("api/[controller]")]
    [ApiController]
    public class RecaptchaController : ControllerBase
    {        
        /// <summary>
        /// Genera el certificado de antecedentes fiscales de acuerdo a los datos diligenciados
        /// </summary>
        /// <param name="data">Documento y tipo de decumento de la persona</param>
        /// <returns>Archivo generado</returns>
        [HttpPost("validarRecaptcha")]
        public async Task<IActionResult> ValidarRecaptcha([FromBody] RecaptchaModel data){
            var response = new ResponseModel();

            var recaptchaResult = await RecaptchaService.RecaptchaValidation(data.Key);
            if(!recaptchaResult){
                response.Message = "Validación de Recaptcha fallida, vuelva a intentar";
                return new JsonResult(response);
            }

            response.Success = true;

            return new JsonResult(response);
        }
    }
}