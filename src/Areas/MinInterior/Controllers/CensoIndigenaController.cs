using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.MinInterior.Models;
using tramites_servicios_webapi.MinInterior.Services;

namespace tramites_servicios_webapi.Controllers {
    /// <summary>
    /// Servicios del MinInterior del Censo Indigena
    /// </summary>
    [Route ("api/[controller]")]
    [ApiController]
    public class CensoIndigenaController : ControllerBase {

        /// <summary>
        /// Obtiene los tipos de documentos del MinInterior del Censo Indigena
        /// </summary>
        /// <returns>Listado de tipos de documentos de la entidad</returns>
        [HttpGet ("obtenerTiposDocumentos")]
        public async Task<IActionResult> ObtenerTiposDocumentos () {

            var respuesta = await CensoIndigenaService.ObtenerTiposDocumentos ();

            if (respuesta == null) {
                return NotFound ();
            }

            return respuesta;
        }

        /// <summary>
        /// Genera el certificado del MinInterior del Censo Indigena
        /// </summary>
        /// <param name="data"></param>
        /// <returns>Certificado en pdf</returns>
        [HttpPost ("generarCertificado")]
        public async Task<IActionResult> GenerarCertificado ([FromBody] CensoIndigenaGenerarCertificadoModel data) {

            if (!ModelState.IsValid) {
                return BadRequest (ModelState);
            }

            var respuesta = await CensoIndigenaService.GenerarCertificado (data);

            if (respuesta == null) {
                return NotFound ();
            }

            return respuesta;
        }

        /// <summary>
        /// Valida el certificado del MinInterior del Censo Indigena
        /// </summary>
        /// <param name="data"></param>
        /// <returns>Certificado en pdf validado</returns>
        [HttpPost ("validarCertificado")]
        public async Task<IActionResult> ValidarCertificado ([FromBody] CensoIndigenaValidarCertificadoModel data) {

            if (!ModelState.IsValid) {
                return BadRequest (ModelState);
            }

            var respuesta = await CensoIndigenaService.ValidarCertificado (data);

            if (respuesta == null) {
                return NotFound ();
            }

            return respuesta;
        }
    }
}