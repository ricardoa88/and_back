using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using tramites_servicios_webapi.MinInterior.Models;

namespace tramites_servicios_webapi.MinInterior.Services {
    public class CensoIndigenaService {
        private static readonly HttpClient _client;
        private const String _urlBase = "http://datos.mininterior.gov.co/Govco/api/Indigenas/";
        private const String _urlLogin = "Login";
        private const String _urlCargarListas = "CargarListas";
        private const String _urlGenerarCertificado = "GenerarCertificado";
        private const String _urlGenerarCertificadoValidacion = "ValidarCertificado";
        static CensoIndigenaService () {
            _client = new HttpClient ();
            _client.BaseAddress = new Uri (_urlBase);
        }

        public static async Task<CensoIndigenaTokenModel> Login () {
            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue ("Basic", "d3NtaW50aWM6MW50M3IwcDNyNGIxbDFkNGRHMHZDMA==");
            String resultado = null;
            CensoIndigenaTokenModel tokenModel = null;

            try {
                HttpResponseMessage resp = await _client.PostAsync (_urlLogin, null);

                if (resp.IsSuccessStatusCode) {
                    resultado = resp.Content.ReadAsStringAsync ().Result;
                    tokenModel = JsonConvert.DeserializeObject<CensoIndigenaTokenModel> (resultado);
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }

            return tokenModel;
        }

        public static async Task<JsonResult> ObtenerTiposDocumentos () {

            CensoIndigenaTokenModel tokenModel = await CensoIndigenaService.Login ();
            Dictionary<string, string> content = new Dictionary<string, string> ();
            content.Add ("idEntidad", "1");
            content.Add ("idLista", "1");
            string output = JsonConvert.SerializeObject (content);

            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue ("Bearer", tokenModel.AccessToken);
            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");

            JsonResult resultado = null;

            try {
                HttpResponseMessage resp = await _client.PostAsync (_urlCargarListas, stringContent);

                if (resp.IsSuccessStatusCode) {
                    var documentos = JsonConvert.DeserializeObject<CensoIndigenaDocumentoModel> (resp.Content.ReadAsStringAsync ().Result);
                    var tiposDocumentos = new List<SelectListItemModel> ();
                    foreach (KeyValueModel dato in documentos.ObjLista) {
                        SelectListItemModel tipoDocumento = new SelectListItemModel ();
                        if(dato.Key != ""){
                            tipoDocumento.Text = dato.Value;
                            tipoDocumento.Value = dato.Key;
                            tiposDocumentos.Add (tipoDocumento);
                        }
                    }
                    resultado = new JsonResult (tiposDocumentos);
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }

            return resultado;
        }

        public static async Task<FileContentResult> GenerarCertificado (CensoIndigenaGenerarCertificadoModel data) {

            CensoIndigenaTokenModel tokenModel = await CensoIndigenaService.Login ();
            Dictionary<string, string> content = new Dictionary<string, string> ();
            content.Add ("NumeroDocumento", data.Documento);
            content.Add ("IdTipoDocumento", data.Codigo);
            string output = JsonConvert.SerializeObject (content);

            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue ("Bearer", tokenModel.AccessToken);
            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");

            FileContentResult resultado = null;

            try {
                HttpResponseMessage resp = await _client.PostAsync (_urlGenerarCertificado, stringContent);

                if (resp.IsSuccessStatusCode) {
                    byte[] bytes = resp.Content.ReadAsByteArrayAsync ().Result;

                    resultado = new FileContentResult (bytes, "application/pdf");
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }

            return resultado;
        }

        public static async Task<FileContentResult> ValidarCertificado (CensoIndigenaValidarCertificadoModel data) {

            CensoIndigenaTokenModel tokenModel = await CensoIndigenaService.Login ();
            Dictionary<string, string> content = new Dictionary<string, string> ();
            content.Add ("numeroDocumento", data.IdCertificado);
            string output = JsonConvert.SerializeObject (content);
            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");
            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue ("Bearer", tokenModel.AccessToken);

            FileContentResult resultado = null;

            try {
 
                HttpResponseMessage resp = await _client.PostAsync(_urlGenerarCertificadoValidacion + "?numeroDocumento=" + data.IdCertificado, stringContent);

                if (resp.IsSuccessStatusCode) {
                    byte[] bytes = resp.Content.ReadAsByteArrayAsync ().Result;

                    if (bytes != null && bytes.Length > 0){
                        resultado = new FileContentResult (bytes, "application/pdf");
                    }else {
                        resultado = null;
                    }
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }

            return resultado;
        }
    }
}