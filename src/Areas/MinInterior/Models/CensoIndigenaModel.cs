using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.MinInterior.Models {
    public class CensoIndigenaTokenModel {
        public string AccessToken { get; set; }
    }

    public class CensoIndigenaDocumentoModel {
        public int IdLista { get; set; }
        public List<KeyValueModel> ObjLista { get; set; }
    }

    public class KeyValueModel {
        public string Key { get; set; }
        public string Value { get; set; }
    }
    public class SelectListItemModel
    {
        public string Text { get; set; }

         public string Value { get; set; }
    }
    public class CensoIndigenaGenerarCertificadoModel {
        [Required (ErrorMessage = "Codigo del tipo de documento es obligatorio.")]
        public string Codigo { get; set; }

        [Required (ErrorMessage = "Documento es obligatorio.")]
        public string Documento { get; set; }
    }

    public class CensoIndigenaValidarCertificadoModel {
        [Required (ErrorMessage = "IdCertificado es obligatorio.")]
        public string IdCertificado { get; set; }
    }
}