using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Mintransporte.Services;
using tramites_servicios_webapi.common.Models;
using tramites_servicios_webapi.Mintransporte.Models;
using System.Linq;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Controllers
{
    [Route ("api/[controller]")]
    [ApiController]
    public class CertificadoLicenciaController : ControllerBase
    {        
        /// <summary>
        /// Obtiene el listado de entidades disponibles del tramite
        /// </summary>
        /// <returns>Listado de entidades</returns>
        [HttpGet("obtenerListadoEntidades")]        
        public async Task<IActionResult>  ObtenerListadoEntidades() {
            var result = new ResponseModel();

            var listadoEntidades = await CertificadoLicenciaService.GetListEntidades();
            if (listadoEntidades == null ) {
                result.Message =  "No se encontraron datos";
            }else { 
                var response = listadoEntidades.Select(item => new SelectListItem {
                    Value = Convert.ToString(item.Id),
                    Text = item.Descripcion
                }).OrderBy(m => m.Text).ToList(); 

                result.Success = true;
                result.Result = response;
            }
            return new JsonResult(result);
        }

        /// <summary>
        /// Obtiene los tipos de documentos del tramite de consulta antecedentes fiscales
        /// </summary>
        /// <returns>Listado de tipos de documentos de la entidad</returns>
        [HttpGet("ObtenerListadoPaises")]        
        public async Task<IActionResult>  ObtenerListadoPaises() {
            var result = new ResponseModel();         
            var paises = await CertificadoLicenciaService.GetListPais();
            if (paises == null ) {
                result.Message =  "No se encontraron datos";
            }else { 
                var response = paises.Select(item => new SelectListItem {
                    Value = item.Codigo,
                    Text = item.Nombre
                }).ToList(); 

                result.Success = true;
                result.Result = response.OrderBy(m => m.Text);
            }
            return new JsonResult(result);
        }

        /// <summary>
        /// Realiza la solicitud de certificado de licencia conducción
        /// </summary>
        /// <param name="model">Datos del solicitante</param>
        /// <returns>Respuesta de la solicitud</returns>
        [HttpPost("Solicitar")]
        public async Task<IActionResult> Solicitar([FromBody] CertificadoLicenciaPostModel model) {
            var result = new ResponseModel();
            var response = await CertificadoLicenciaService.Solicitar(model);
            if (response == false) {
                return BadRequest();
            }

            result.Success = true;            
            return new JsonResult(result);
        }

        /// <summary>
        /// Obtiene los tipos de documento disponible para el trámite
        /// </summary>
        /// <returns>Listado de tipos de documento</returns>
        [HttpGet("ObtenerTiposDocumento")]        
        public async Task<IActionResult>  GetTiposDocumento() {
            var result = new ResponseModel();

            var dictionary = await CertificadoLicenciaService.GetListTiposDocumento();
            if (dictionary == null ) {
                result.Message =  "No se encontraron datos";
            }else { 
                var list = new List<SelectListItem>();
                foreach(KeyValuePair<string, string> item in dictionary) {
                    list.Add(new SelectListItem(item.Key, item.Value));
                }

                result.Success = true;
                result.Result = list.OrderBy(m => m.Text);
            }
            return new JsonResult(result);
        }
    }
}