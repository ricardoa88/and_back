using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using tramites_servicios_webapi.Mintransporte.Models;
using tramites_servicios_webapi.common.Models;

namespace tramites_servicios_webapi.Mintransporte.Services {

    public class CertificadoLicenciaService {
        
        #region declaración de URLs 
            private static HttpClient client;

            private const string UrlBase = "http://190.217.54.244/api/";

            private const string UrlTokenBase = "http://190.217.54.244/";

            private const string AuthenticationToken = "ZlJBcmhDY0JUa1V3YWZBNDdjOGV4UDFSR05jYToxWTdIc3JxZjdxdWY3UUZNYkhxTlNvbmZsV2th";

            private const string UrlToken = "api-token-auth";

            private const string UrlListarPaises = "paises/";

            private const string UrlListarEntidades = "entidades/";

            private const string UrlListarTiposDocumento = "tipos_documento/";

            private const string UrlPost = "solicitudes/";

            private const string usrGovco = "gov-co-pruebas";

            private const string passGovco = "eJLgA6KyZGVuatJX";

        #endregion

        static CertificadoLicenciaService () {
            client = new HttpClient ();
        }

        /// <summary>
        /// Obtiene el token para acceder a los servicios de Mintransporte de acuerdo a las
        /// credenciales dadas para el acceso
        /// </summary>
        /// <returns>Modeo con el resultado de la solicitud</returns>
        public static async Task<CertificadoLicenciaTokenModel> GetToken() {
            //Construye el encabezado
            Dictionary<string, string> content = new Dictionary<string, string> ();
            content.Add ("username", usrGovco);
            content.Add ("password", passGovco);
            string output = JsonConvert.SerializeObject (content);

            client.DefaultRequestHeaders.Accept.Clear ();
            client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json")
            );

            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");

            try {
                HttpResponseMessage resp = await client.PostAsync (string.Format("{0}{1}", UrlTokenBase, UrlToken), stringContent);
                if (resp.IsSuccessStatusCode) {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    CertificadoLicenciaTokenModel tokenModel = JsonConvert.DeserializeObject<CertificadoLicenciaTokenModel> (response);                    
                    tokenModel.State = true;
                    return tokenModel;
                }else {
                    return null;
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }

            return new CertificadoLicenciaTokenModel();
        }

        /// <summary>
        /// Obtiene el listado de oficinas disponibles para el trámite
        /// </summary>
        /// <returns>Listado de oficinas</returns>
        public static async Task<List<PaisModel>> GetListPais() {
            CertificadoLicenciaTokenModel tokenModel = await GetToken();
            if (!tokenModel.State) {
                //TODO: verificar retornos
                return null;
            }

            //Construye el encabezado
            client.DefaultRequestHeaders.Accept.Clear ();
            client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue ("Token", tokenModel.Token);

            //Realiza la consulta            
            try {
                HttpResponseMessage resp = await client.GetAsync(string.Format("{0}{1}", UrlBase, UrlListarPaises));
                if (resp.IsSuccessStatusCode) {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    List<PaisModel> PaisesList = JsonConvert.DeserializeObject<List<PaisModel>> (response);
                    return PaisesList;
                }else {
                    //TODO: incluir errores1
                    return null;
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }
            return null;
        }

        /// <summary>UrlListarPaises
        /// Obtiene el listado dUrlListarPaisese oficinas disponibles para el trámite
        /// </summary>
        /// <returns>Listado de oficinas</returns>
        public static async Task<List<EntidadModel>> GetListEntidades() {
            CertificadoLicenciaTokenModel tokenModel = await GetToken();
            if (!tokenModel.State) {
                //TODO: verificar retornos
                return null;
            }

            //Construye el encabezado
            client.DefaultRequestHeaders.Accept.Clear ();
            client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue ("Token", tokenModel.Token);

            //Realiza la consulta            
            try {
                HttpResponseMessage resp = await client.GetAsync(string.Format("{0}{1}", UrlBase, UrlListarEntidades));
                if (resp.IsSuccessStatusCode) {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    List<EntidadModel> entidadList = JsonConvert.DeserializeObject<List<EntidadModel>> (response);
                    return entidadList;
                }else {
                    //TODO: incluir errores1
                    return null;
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }
            return null;
        }
    
        /// <summary>UrlListarPaises
        /// Obtiene el listado dUrlListarPaisese oficinas disponibles para el trámite
        /// </summary>
        /// <returns>Listado de oficinas</returns>
        public static async Task<Dictionary<string, string>> GetListTiposDocumento() {
            CertificadoLicenciaTokenModel tokenModel = await GetToken();
            if (!tokenModel.State) {
                //TODO: verificar retornos
                return null;
            }

            //Construye el encabezado
            client.DefaultRequestHeaders.Accept.Clear ();
            client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue ("Token", tokenModel.Token);

            //Realiza la consulta            
            try {
                HttpResponseMessage resp = await client.GetAsync(string.Format("{0}{1}", UrlBase, UrlListarTiposDocumento));
                if (resp.IsSuccessStatusCode) {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    Dictionary<string,string> tiposDocumento = JsonConvert.DeserializeObject<Dictionary<string, string>> (response);
                    return tiposDocumento;
                }else {
                    //TODO: incluir errores1
                    return null;
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }
            return null;
        }

        /// <summary>
        /// Realiza la solicitud de envío por correo del certificado de licencia de conducción 
        /// </summary>
        /// <param name="data">Modelo de registro</param>
        /// <returns>Estado del registro</returns>
        public static async Task<bool> Solicitar(CertificadoLicenciaPostModel data)
        {
            CertificadoLicenciaTokenModel tokenModel = await GetToken();
            if (!tokenModel.State) {
                //TODO: verificar retornos
                return false;
            }

            //Headers - Body
            client.DefaultRequestHeaders.Accept.Clear ();
            client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", tokenModel.Token);
            string output;
            if(data.SegundoApellido == "" && data.SegundoNombre == "")
            {
                CertificadoLicenciaPostModelSinSNSA _SinNA = new CertificadoLicenciaPostModelSinSNSA();
                _SinNA.CorreoElectronico = data.CorreoElectronico;
                _SinNA.NumDocumento = data.NumDocumento;
                _SinNA.Pais = data.Pais;
                _SinNA.PrimerNombre = data.PrimerNombre;
                _SinNA.PrimerApellido= data.PrimerApellido;
                _SinNA.TipoDocumento = data.TipoDocumento;
                _SinNA.TipoEntidad= data.TipoEntidad; 
                output = JsonConvert.SerializeObject(_SinNA);
            }      
            else if(data.SegundoApellido != "" && data.SegundoNombre == "")
            {
                CertificadoLicenciaPostModelSinSN _SinN = new CertificadoLicenciaPostModelSinSN();
                _SinN.CorreoElectronico = data.CorreoElectronico;
                _SinN.NumDocumento = data.NumDocumento;
                _SinN.Pais = data.Pais;
                _SinN.PrimerNombre = data.PrimerNombre;
                _SinN.PrimerApellido= data.PrimerApellido;
                _SinN.SegundoApellido= data.SegundoApellido;
                _SinN.TipoDocumento = data.TipoDocumento;
                _SinN.TipoEntidad= data.TipoEntidad; 
                 output = JsonConvert.SerializeObject(_SinN);
            }
            else if(data.SegundoApellido == "" && data.SegundoNombre != "")
            {
                CertificadoLicenciaPostModelSinSA _SinA = new CertificadoLicenciaPostModelSinSA();
                _SinA.CorreoElectronico = data.CorreoElectronico;
                _SinA.NumDocumento = data.NumDocumento;
                _SinA.Pais = data.Pais;
                _SinA.PrimerNombre = data.PrimerNombre;
                _SinA.SegundoNombre= data.SegundoNombre;
                _SinA.PrimerApellido= data.PrimerApellido;
                _SinA.TipoDocumento = data.TipoDocumento;
                _SinA.TipoEntidad= data.TipoEntidad; 
                output = JsonConvert.SerializeObject(_SinA);
            }
            else
            {
                output = JsonConvert.SerializeObject(data);
            }
            Console.WriteLine(output);
            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");

            //Petición
            try {
                HttpResponseMessage resp = await client.PostAsync(string.Format("{0}{1}", UrlBase, UrlPost), stringContent);
                Console.WriteLine(JsonConvert.SerializeObject(resp));
                if (resp.IsSuccessStatusCode) {
                     return true;
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }                          
            //Salida
            return false;
        }
    
    }

}