using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Mintransporte.Models {

    public class PaisModel {
        [JsonProperty("codigo")]
        public string Codigo { get; set; }

        [JsonProperty("nombre_completo")]
        public string Nombre { get; set; }
    }
}
