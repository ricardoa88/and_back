using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Mintransporte.Models {

    public class CertificadoLicenciaPostModelSinSNSA {
        [JsonProperty("primerNombre")]
        public string PrimerNombre { get; set; }

        [JsonProperty("primerApellido")]
        public string PrimerApellido { get; set; }

        [JsonProperty("tipoDocumento")]
        public string TipoDocumento { get; set; }

        [JsonProperty("numIdentificacion")]
        public string NumDocumento { get; set;}

        [JsonProperty("correoElectronico")]
        public string CorreoElectronico { get; set; }

        [JsonProperty("tipoEntidad")]
        public long TipoEntidad { get; set; }

        [JsonProperty("pais")]
        public string Pais { get; set; }

    }
}
