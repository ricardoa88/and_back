
namespace tramites_servicios_webapi.Mintransporte.Models { 

    public class CertificadoLicenciaTokenModel {
        public string Token { get; set; }

        public bool State { get; set; }

        public CertificadoLicenciaTokenModel() {
            this.State = false;
        }
    }
}