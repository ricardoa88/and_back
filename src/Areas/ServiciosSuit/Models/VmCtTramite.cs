﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtTramite
    {
        public VmCtTramite()
        {
            VmCtAccionCondicions = new HashSet<VmCtAccionCondicion>();
            VmCtMedioSeguimientos = new HashSet<VmCtMedioSeguimiento>();
            VmCtMomentos = new HashSet<VmCtMomento>();
            VmCtSoporteNormas = new HashSet<VmCtSoporteNorma>();
            VmCtTramiteAudiencias = new HashSet<VmCtTramiteAudiencia>();
            VmCtTramitePuntoAtencions = new HashSet<VmCtTramitePuntoAtencion>();
            VmCtTramiteTemas = new HashSet<VmCtTramiteTema>();
        }

        public string Numero { get; set; }
        public string NombreEstandarizado { get; set; }
        public string Nombre { get; set; }
        public string Proposito { get; set; }
        public string NombreResultado { get; set; }
        public string Clase { get; set; }
        public string ObtencionInmediata { get; set; }
        public string TiempoObtencion { get; set; }
        public string ObservacionTiempoObtencion { get; set; }
        public string PalabrasClave { get; set; }
        public string RealizadoMediosElectronicos { get; set; }
        public string SeguimientoSedePrincipal { get; set; }
        public string UrlPuntosAtencion { get; set; }
        public string TipoAtencionPresencial { get; set; }
        public string UrlTramiteEnLinea { get; set; }
        public string UrlResultadoWeb { get; set; }
        public string TipoFechaEjecucion { get; set; }
        public string UrlCalendarioEjecucion { get; set; }
        public string EstadoCodigo { get; set; }
        public string EstadoNombre { get; set; }
        public string ObservacionFechaGeneral { get; set; }
        public decimal? SituacionVidaId { get; set; }
        public string SituacionVidaNombre { get; set; }
        public string ObjetoNombre { get; set; }
        public string VerboNombre { get; set; }
        public string InstitucionId { get; set; }
        public string InstitucionNombre { get; set; }
        public string MunicipioCodigoDane { get; set; }
        public string MunicipioNombre { get; set; }
        public string DepartamentoCodigoDane { get; set; }
        public string DepartamentoNombre { get; set; }
        public string UrlPaginaDetalle { get; set; }
        public DateTime? FechaActualizacion { get; set; }
        public string UrlManualEnLinea { get; set; }
        public string AnotaBasicaEnLinea { get; set; }
        public string Subcategoria { get; set; }
        public string Barrio { get; set; }

        public virtual VmSgpInstitucion Institucion { get; set; }
        public virtual ICollection<VmCtAccionCondicion> VmCtAccionCondicions { get; set; }
        public virtual ICollection<VmCtMedioSeguimiento> VmCtMedioSeguimientos { get; set; }
        public virtual ICollection<VmCtMomento> VmCtMomentos { get; set; }
        public virtual ICollection<VmCtSoporteNorma> VmCtSoporteNormas { get; set; }
        public virtual ICollection<VmCtTramiteAudiencia> VmCtTramiteAudiencias { get; set; }
        public virtual ICollection<VmCtTramitePuntoAtencion> VmCtTramitePuntoAtencions { get; set; }
        public virtual ICollection<VmCtTramiteTema> VmCtTramiteTemas { get; set; }
    }
}