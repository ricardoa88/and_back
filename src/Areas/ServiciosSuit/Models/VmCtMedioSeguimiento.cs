﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtMedioSeguimiento
    {
        public decimal MedioSeguimientoId { get; set; }
        public string TramiteNumero { get; set; }
        public string TipoCanal { get; set; }
        public string HorarioAtencion { get; set; }
        public string TipoTelefono { get; set; }
        public string NumeroTelefono { get; set; }
        public string ExtensionTelFijo { get; set; }
        public string NombreCanalWeb { get; set; }
        public string UrlCanalWeb { get; set; }
        public string CorreoSeguimiento { get; set; }
        public string Municipio { get; set; }

        public virtual VmCtTramite TramiteNumeroNavigation { get; set; }
    }
}