﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtValorPago
    {
        public decimal ValorPagoId { get; set; }
        public decimal AccionCondicionId { get; set; }
        public string Descripcion { get; set; }
        public string Moneda { get; set; }
        public string TipoValor { get; set; }
        public string CantidadSmlv { get; set; }
        public decimal? Valor { get; set; }
        public decimal? Orden { get; set; }

        public virtual VmCtAccionCondicion AccionCondicion { get; set; }
    }
}