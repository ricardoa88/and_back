﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmSgpInstitucion
    {
        public VmSgpInstitucion()
        {
            VmCtPuntoAtencions = new HashSet<VmCtPuntoAtencion>();
            VmCtTramites = new HashSet<VmCtTramite>();
        }

        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Sigla { get; set; }
        public string EsDependenEspecial { get; set; }
        public string OrdrId { get; set; }
        public string OrdrNombre { get; set; }
        public string SuorId { get; set; }
        public string SuorNombre { get; set; }
        public string MuniCodigoDane { get; set; }
        public string MuniNombre { get; set; }
        public string SectId { get; set; }
        public string SectNombre { get; set; }
        public string NajuId { get; set; }
        public string NajuNombre { get; set; }
        public string ClorId { get; set; }
        public string ClorNombre { get; set; }
        public string PaginaWeb { get; set; }
        public string Email { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Fax { get; set; }
        public string NiveId { get; set; }
        public string NiveNombre { get; set; }
        public string PadreId { get; set; }
        public string PadreNombre { get; set; }
        public string TipoDependencia { get; set; }
        public string DepaCodigoDane { get; set; }
        public string DepaNombre { get; set; }

        public virtual ICollection<VmCtPuntoAtencion> VmCtPuntoAtencions { get; set; }
        public virtual ICollection<VmCtTramite> VmCtTramites { get; set; }
    }
}