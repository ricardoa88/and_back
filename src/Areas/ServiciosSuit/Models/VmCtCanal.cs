﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtCanal
    {
        public decimal CanalId { get; set; }
        public decimal AccionCondicionId { get; set; }
        public string TipoCanal { get; set; }
        public string HorarioAtencionTelef { get; set; }
        public string TipoTelefono { get; set; }
        public string NumeroTelefono { get; set; }
        public string ExtensionTelFijo { get; set; }
        public string NombreCanalWeb { get; set; }
        public string UrlCanalWeb { get; set; }
        public string Correo { get; set; }
        public string MunicipioTelefono { get; set; }

        public virtual VmCtAccionCondicion AccionCondicion { get; set; }
    }
}