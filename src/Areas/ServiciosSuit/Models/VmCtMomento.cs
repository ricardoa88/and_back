﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtMomento
    {
        public VmCtMomento()
        {
            VmCtAccionCondicions = new HashSet<VmCtAccionCondicion>();
        }

        public decimal MomentoId { get; set; }
        public string TramiteNumero { get; set; }
        public string Descripcion { get; set; }
        public int Orden { get; set; }

        public virtual VmCtTramite TramiteNumeroNavigation { get; set; }
        public virtual ICollection<VmCtAccionCondicion> VmCtAccionCondicions { get; set; }
    }
}