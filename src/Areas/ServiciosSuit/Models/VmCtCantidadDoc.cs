﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtCantidadDoc
    {
        public decimal CantidadDocId { get; set; }
        public decimal AccionCondicionId { get; set; }
        public string UnidadCantidad { get; set; }
        public decimal? Cantidad { get; set; }
        public string Observacion { get; set; }

        public virtual VmCtAccionCondicion AccionCondicion { get; set; }
    }
}