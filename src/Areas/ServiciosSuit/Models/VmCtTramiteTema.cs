﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtTramiteTema
    {
        public string TramiteNumero { get; set; }
        public string TemaNombre { get; set; }
        public int IdVmCtTramiteTema { get; set; }

        public virtual VmCtTramite TramiteNumeroNavigation { get; set; }
    }
}