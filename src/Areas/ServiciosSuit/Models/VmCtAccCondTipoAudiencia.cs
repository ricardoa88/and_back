﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtAccCondTipoAudiencia
    {
        public decimal AccionCondicionId { get; set; }
        public string TipoAudiencia { get; set; }
        public int IdVmCtAccCondTipoAudiencia { get; set; }

        public virtual VmCtAccionCondicion AccionCondicion { get; set; }
    }
}