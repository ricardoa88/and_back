﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class FichaEstandar
    {
        
        public string IdTramite { get; set; }
        public string NombreEstandarizado { get; set; }
        public string Proposito { get; set; }

        public List<PuntosMunicipios> Puntos;
    }

    public partial class PuntosMunicipios
    {

        public string MunicipioCodigoDane { get; set; }
        public string MunicipioNombre { get; set; }
        public string DepartamentoCodigoDane { get; set; }
        public string DepartamentoNombre { get; set; }
        public string IdTramiteMunicipio { get; set; }

    }

    public partial class TipoTramiteEspecifico
    {
        public string IdTramite { get; set; }
        public string Tipotramite { get; set; }
        public string UrlTramiteEnLinea { get; set; }
        public string PaginaWeb { get; set; }

    }

    public partial class FichaTramiteEspecifica
    {
        public int OrdenMomento { get; set; }
        public string Nombre { get; set; }
        public string NombreResultado { get; set; }
        public string Proposito { get; set; }
        public string UrlResultadoWeb { get; set; }
        public string UrlTramiteEnLinea { get; set; }
        public string TipoAudiencia { get; set; }
        public string TipoAccionCondicion { get; set; }
        public string DocumentoTipo { get; set; }
        public string DocumentoNombre { get; set; }
        public string DocumentoAnotacionAdicional { get; set; }
        public string PagoDisponibleEnLinea { get; set; }
        public string PagoDispEnInstitucion { get; set; }
        public string PagoDispEntidadRecaudadora { get; set; }
        public string PagoDispInstDescripcion { get; set; }
        public string PagoEnlineaUrl { get; set; }
        public string FormularioAnotacion { get; set; }
        public string FormularioNombre{ get; set; }
        public string VerificacionInstDescripcion { get; set; }
        public string AtencionDescripcion { get; set; }
        public string ExcepcionId { get; set; }
        public string Excepcion { get; set; }
        public string FormularioUrlDescarga { get; set; }
        public string CantidadSmlv { get; set; }
        public string Moneda { get; set; }
        public string TipoValor { get; set; }
        public string Valor { get; set; }
        public string CodigoRecaudo { get; set; }
        public string DescripcionOtro { get; set; }
        public string NombreCuenta { get; set; }
        public string NombreEntidad { get; set; }
        public string NumeroCuenta { get; set; }
        public string TipoCuenta { get; set; }
        public int OrdenPago { get; set; }
        public string DescripcionMomento { get; set; }
        public string DescripcionPago { get; set; }
        public int OrdenAccion { get; set; }
        public string CondicionNueva { get; set; }
        public string AccionCondicionId { get; set; }
        public string UnidadCantidad { get; set; }
        public string CantidadDoc { get; set; }
        public string ObservacionCantidad { get; set; }

    }

    public partial class Canales
    {
        public string TipoTelefono { get; set; }
        public string ExtensionTelFijo { get; set; }
        public string HorarioAtencionTelef { get; set; }
        public string MunicipioTelefono { get; set; }
        public string NombreCanalWeb { get; set; }
        public string NumeroTelefono { get; set; }
        public string TipoCanal { get; set; }
        public string Correo { get; set; }
        public string UrlCanalWeb { get; set; }

    }

    public partial class Pagos
    {
        public string CantidadSmlv { get; set; }
        public string Moneda { get; set; }
        public string TipoValor { get; set; }
        public string Valor { get; set; }
        public int OrdenPago { get; set; }
        public string DescripcionPago { get; set; }
        public string CodigoRecaudo { get; set; }
        public string DescripcionOtro { get; set; }
        public string NombreCuenta { get; set; }
        public string NombreEntidad { get; set; }
        public string NumeroCuenta { get; set; }
        public string TipoCuenta { get; set; }
        public string DescripcionMomento { get; set; }

    }

    public partial class PuntosAtencion
    {
        public string PuntoAtencionId { get; set; }
        public string PuntoAtencionNombre { get; set; }
        public string HorarioAtencion { get; set; }
        public string PuntoAtencionDireccion { get; set; }
        public string PuntoAtencionTelefono { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Municipio { get; set; }
        public string Departamento { get; set; }
        
    }


    public partial class Normatividad
    {
        public string TipoNorma { get; set; }
        public string NumeroNorma { get; set; }
        public string AnoNorma { get; set; }
        public string Articulos { get; set; }
        public string UrlNorma { get; set; }
        public string UrlDescarga { get; set; }

    }

    public partial class BasicaEspecifica
    {

        public string IdTramite { get; set; }
        public string NombreEstandarizado { get; set; }
        public string Proposito { get; set; }
        public string Entidad { get; set; }

    }
}