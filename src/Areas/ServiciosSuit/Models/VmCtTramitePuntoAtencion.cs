﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtTramitePuntoAtencion
    {
        public string TramiteNumero { get; set; }
        public decimal PuntoAtencionId { get; set; }
        public int IdVmCtTramitePuntoAtencion { get; set; }

        public virtual VmCtPuntoAtencion PuntoAtencion { get; set; }
        public virtual VmCtTramite TramiteNumeroNavigation { get; set; }
    }
}