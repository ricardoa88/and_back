﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class ServiciosSuitContext : DbContext
    {
        public ServiciosSuitContext()
        {
        }

        public ServiciosSuitContext(DbContextOptions<ServiciosSuitContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Departamento> Departamentos { get; set; }
        public virtual DbSet<Municipio> Municipios { get; set; }
        public virtual DbSet<VmCtAccCondPtoAtencion> VmCtAccCondPtoAtencions { get; set; }
        public virtual DbSet<VmCtAccCondTipoAudiencia> VmCtAccCondTipoAudiencias { get; set; }
        public virtual DbSet<VmCtAccionCondicion> VmCtAccionCondicions { get; set; }
        public virtual DbSet<VmCtCanal> VmCtCanals { get; set; }
        public virtual DbSet<VmCtCantidadDoc> VmCtCantidadDocs { get; set; }
        public virtual DbSet<VmCtEntidadPago> VmCtEntidadPagos { get; set; }
        public virtual DbSet<VmCtMedioSeguimiento> VmCtMedioSeguimientos { get; set; }
        public virtual DbSet<VmCtMomento> VmCtMomentos { get; set; }
        public virtual DbSet<VmCtPuntoAtencion> VmCtPuntoAtencions { get; set; }
        public virtual DbSet<VmCtSoporteNorma> VmCtSoporteNormas { get; set; }
        public virtual DbSet<VmCtTramite> VmCtTramites { get; set; }
        public virtual DbSet<VmCtTramiteAudiencia> VmCtTramiteAudiencias { get; set; }
        public virtual DbSet<VmCtTramitePuntoAtencion> VmCtTramitePuntoAtencions { get; set; }
        public virtual DbSet<VmCtTramiteTema> VmCtTramiteTemas { get; set; }
        public virtual DbSet<VmCtValorPago> VmCtValorPagos { get; set; }
        public virtual DbSet<VmSgpInstitucion> VmSgpInstitucions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<Departamento>(entity =>
            {
                entity.HasKey(e => e.DepCodigo)
                    .HasName("DEPARTAMENTO_PK");

                entity.ToTable("DEPARTAMENTO");

                entity.Property(e => e.DepCodigo)
                    .HasColumnName("DEP_CODIGO")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DepNombre)
                    .IsRequired()
                    .HasColumnName("DEP_NOMBRE")
                    .HasMaxLength(75)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Municipio>(entity =>
            {
                entity.HasKey(e => e.MunCodigo)
                    .HasName("MUNICIPIO_PK");

                entity.ToTable("MUNICIPIO");

                entity.Property(e => e.MunCodigo)
                    .HasColumnName("MUN_CODIGO")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DepCodigo)
                    .IsRequired()
                    .HasColumnName("DEP_CODIGO")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MunNombre)
                    .IsRequired()
                    .HasColumnName("MUN_NOMBRE")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.DepCodigoNavigation)
                    .WithMany(p => p.Municipios)
                    .HasForeignKey(d => d.DepCodigo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("DEPARTAMENTO_MUNICIPIO_FK1");
            });

            modelBuilder.Entity<VmCtAccCondPtoAtencion>(entity =>
            {
                entity.HasKey(e => e.IdVmCtAccCondPtoAtencion);

                entity.ToTable("VM_CT_ACC_COND_PTO_ATENCION");

                entity.HasIndex(e => e.AccionCondicionId)
                    .HasName("index_VM_CT_ACC_COND_PTO_ATENCION_ACCION_CONDICION_ID");

                entity.Property(e => e.IdVmCtAccCondPtoAtencion).HasColumnName("ID_VM_CT_ACC_COND_PTO_ATENCION");

                entity.Property(e => e.AccionCondicionId)
                    .HasColumnName("ACCION_CONDICION_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.PuntoAtencionId)
                    .HasColumnName("PUNTO_ATENCION_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.HasOne(d => d.AccionCondicion)
                    .WithMany(p => p.VmCtAccCondPtoAtencions)
                    .HasForeignKey(d => d.AccionCondicionId)
                    .HasConstraintName("FK_VM_CT_ACC_COND_PTO_ATENCION_VM_CT_ACCION_CONDICION");

                entity.HasOne(d => d.PuntoAtencion)
                    .WithMany(p => p.VmCtAccCondPtoAtencions)
                    .HasForeignKey(d => d.PuntoAtencionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VM_CT_ACC_COND_PTO_ATENCION_VM_CT_PUNTO_ATENCION");
            });

            modelBuilder.Entity<VmCtAccCondTipoAudiencia>(entity =>
            {
                entity.HasKey(e => e.IdVmCtAccCondTipoAudiencia);

                entity.ToTable("VM_CT_ACC_COND_TIPO_AUDIENCIA");

                entity.HasIndex(e => new { e.TipoAudiencia, e.AccionCondicionId })
                    .HasName("ix_VM_CT_ACC_COND_TIPO_AUDIENCIA_ACCION_CONDICION_ID_includes");

                entity.Property(e => e.IdVmCtAccCondTipoAudiencia).HasColumnName("ID_VM_CT_ACC_COND_TIPO_AUDIENCIA");

                entity.Property(e => e.AccionCondicionId)
                    .HasColumnName("ACCION_CONDICION_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.TipoAudiencia)
                    .IsRequired()
                    .HasColumnName("TIPO_AUDIENCIA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.AccionCondicion)
                    .WithMany(p => p.VmCtAccCondTipoAudiencias)
                    .HasForeignKey(d => d.AccionCondicionId)
                    .HasConstraintName("FK_VM_CT_ACC_COND_TIPO_AUDIENCIA_VM_CT_ACCION_CONDICION");
            });

            modelBuilder.Entity<VmCtAccionCondicion>(entity =>
            {
                entity.HasKey(e => e.AccionCondicionId);

                entity.ToTable("VM_CT_ACCION_CONDICION");

                entity.HasIndex(e => e.MomentoId)
                    .HasName("ix_VM_CT_ACCION_CONDICION_MOMENTO_ID");

                entity.HasIndex(e => e.TramiteNumero)
                    .HasName("index_VM_CT_ACCION_CONDICION_TRAMITE_NUMERO");

                entity.Property(e => e.AccionCondicionId)
                    .HasColumnName("ACCION_CONDICION_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.AtencionDescripcion)
                    .HasColumnName("ATENCION_DESCRIPCION")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentoAnotacionAdicional)
                    .HasColumnName("DOCUMENTO_ANOTACION_ADICIONAL")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentoNombre)
                    .HasColumnName("DOCUMENTO_NOMBRE")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentoTipo)
                    .HasColumnName("DOCUMENTO_TIPO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Excepcion)
                    .HasColumnName("EXCEPCION")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.ExcepcionId)
                    .HasColumnName("EXCEPCION_ID")
                    .HasColumnType("numeric(6, 0)");

                entity.Property(e => e.FormularioAnotacion)
                    .HasColumnName("FORMULARIO_ANOTACION")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.FormularioNombre)
                    .HasColumnName("FORMULARIO_NOMBRE")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.FormularioUrl)
                    .HasColumnName("FORMULARIO_URL")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.FormularioUrlDescarga)
                    .HasColumnName("FORMULARIO_URL_DESCARGA")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.MomentoId)
                    .HasColumnName("MOMENTO_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Orden)
                    .HasColumnName("ORDEN")
                    .HasColumnType("numeric(2, 0)");

                entity.Property(e => e.PagoDispEnInstitucion)
                    .IsRequired()
                    .HasColumnName("PAGO_DISP_EN_INSTITUCION")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PagoDispEntidadRecaudadora)
                    .IsRequired()
                    .HasColumnName("PAGO_DISP_ENTIDAD_RECAUDADORA")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PagoDispInstDescripcion)
                    .HasColumnName("PAGO_DISP_INST_DESCRIPCION")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.PagoDisponibleEnLinea)
                    .IsRequired()
                    .HasColumnName("PAGO_DISPONIBLE_EN_LINEA")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PagoEnLineaUrl)
                    .HasColumnName("PAGO_EN_LINEA_URL")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.TipoAccionCondicion)
                    .IsRequired()
                    .HasColumnName("TIPO_ACCION_CONDICION")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TramiteNumero)
                    .IsRequired()
                    .HasColumnName("TRAMITE_NUMERO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.VerificacionInstDescripcion)
                    .HasColumnName("VERIFICACION_INST_DESCRIPCION")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.HasOne(d => d.Momento)
                    .WithMany(p => p.VmCtAccionCondicions)
                    .HasForeignKey(d => d.MomentoId)
                    .HasConstraintName("FK_VM_CT_ACCION_CONDICION_VM_CT_MOMENTO");

                entity.HasOne(d => d.TramiteNumeroNavigation)
                    .WithMany(p => p.VmCtAccionCondicions)
                    .HasForeignKey(d => d.TramiteNumero)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VM_CT_ACCION_CONDICION_VM_CT_TRAMITE");
            });

            modelBuilder.Entity<VmCtCanal>(entity =>
            {
                entity.HasKey(e => e.CanalId);

                entity.ToTable("VM_CT_CANAL");

                entity.HasIndex(e => e.AccionCondicionId)
                    .HasName("index_VM_CT_CANAL_ACCION_CONDICION_ID");

                entity.Property(e => e.CanalId)
                    .HasColumnName("CANAL_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.AccionCondicionId)
                    .HasColumnName("ACCION_CONDICION_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Correo)
                    .HasColumnName("CORREO")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExtensionTelFijo)
                    .HasColumnName("EXTENSION_TEL_FIJO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.HorarioAtencionTelef)
                    .HasColumnName("HORARIO_ATENCION_TELEF")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MunicipioTelefono)
                    .HasColumnName("MUNICIPIO_TELEFONO")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NombreCanalWeb)
                    .HasColumnName("NOMBRE_CANAL_WEB")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroTelefono)
                    .HasColumnName("NUMERO_TELEFONO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TipoCanal)
                    .IsRequired()
                    .HasColumnName("TIPO_CANAL")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TipoTelefono)
                    .HasColumnName("TIPO_TELEFONO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UrlCanalWeb)
                    .HasColumnName("URL_CANAL_WEB")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.HasOne(d => d.AccionCondicion)
                    .WithMany(p => p.VmCtCanals)
                    .HasForeignKey(d => d.AccionCondicionId)
                    .HasConstraintName("FK_VM_CT_CANAL_VM_CT_ACCION_CONDICION");
            });

            modelBuilder.Entity<VmCtCantidadDoc>(entity =>
            {
                entity.HasKey(e => e.CantidadDocId);

                entity.ToTable("VM_CT_CANTIDAD_DOC");

                entity.HasIndex(e => e.AccionCondicionId)
                    .HasName("ix_VM_CT_CANTIDAD_DOC_ACCION_CONDICION_ID");

                entity.HasIndex(e => new { e.CantidadDocId, e.AccionCondicionId })
                    .HasName("index_VM_CT_CANTIDAD_DOC_ACCION_CONDICION_ID");

                entity.Property(e => e.CantidadDocId)
                    .HasColumnName("CANTIDAD_DOC_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.AccionCondicionId)
                    .HasColumnName("ACCION_CONDICION_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Cantidad)
                    .HasColumnName("CANTIDAD")
                    .HasColumnType("numeric(2, 0)");

                entity.Property(e => e.Observacion)
                    .HasColumnName("OBSERVACION")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UnidadCantidad)
                    .HasColumnName("UNIDAD_CANTIDAD")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.AccionCondicion)
                    .WithMany(p => p.VmCtCantidadDocs)
                    .HasForeignKey(d => d.AccionCondicionId)
                    .HasConstraintName("FK_VM_CT_CANTIDAD_DOC_VM_CT_ACCION_CONDICION");
            });

            modelBuilder.Entity<VmCtEntidadPago>(entity =>
            {
                entity.HasKey(e => e.EntidadPagoId)
                    .HasName("PK_VM_ENTIDAD_PAGO");

                entity.ToTable("VM_CT_ENTIDAD_PAGO");

                entity.HasIndex(e => new { e.EntidadPagoId, e.AccionCondicionId })
                    .HasName("index_VM_CT_ENTIDAD_PAGO_ACCION_CONDICION_ID");

                entity.Property(e => e.EntidadPagoId)
                    .HasColumnName("ENTIDAD_PAGO_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.AccionCondicionId)
                    .HasColumnName("ACCION_CONDICION_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.CodigoRecaudo)
                    .HasColumnName("CODIGO_RECAUDO")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DescripcionOtro)
                    .HasColumnName("DESCRIPCION_OTRO")
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.NombreCuenta)
                    .HasColumnName("NOMBRE_CUENTA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NombreEntidad)
                    .IsRequired()
                    .HasColumnName("NOMBRE_ENTIDAD")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroCuenta)
                    .HasColumnName("NUMERO_CUENTA")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoCuenta)
                    .IsRequired()
                    .HasColumnName("TIPO_CUENTA")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.AccionCondicion)
                    .WithMany(p => p.VmCtEntidadPagos)
                    .HasForeignKey(d => d.AccionCondicionId)
                    .HasConstraintName("FK_VM_CT_ENTIDAD_PAGO_VM_CT_ACCION_CONDICION");
            });

            modelBuilder.Entity<VmCtMedioSeguimiento>(entity =>
            {
                entity.HasKey(e => e.MedioSeguimientoId);

                entity.ToTable("VM_CT_MEDIO_SEGUIMIENTO");

                entity.HasIndex(e => new { e.TramiteNumero, e.MedioSeguimientoId })
                    .HasName("index_VM_CT_MEDIO_SEGUIMIENTO_TRAMITE_NUMERO_MEDIO_SEG");

                entity.Property(e => e.MedioSeguimientoId)
                    .HasColumnName("MEDIO_SEGUIMIENTO_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.CorreoSeguimiento)
                    .HasColumnName("CORREO_SEGUIMIENTO")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ExtensionTelFijo)
                    .HasColumnName("EXTENSION_TEL_FIJO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.HorarioAtencion)
                    .HasColumnName("HORARIO_ATENCION")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Municipio)
                    .HasColumnName("MUNICIPIO")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.NombreCanalWeb)
                    .HasColumnName("NOMBRE_CANAL_WEB")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroTelefono)
                    .HasColumnName("NUMERO_TELEFONO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoCanal)
                    .HasColumnName("TIPO_CANAL")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TipoTelefono)
                    .HasColumnName("TIPO_TELEFONO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TramiteNumero)
                    .IsRequired()
                    .HasColumnName("TRAMITE_NUMERO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UrlCanalWeb)
                    .HasColumnName("URL_CANAL_WEB")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.HasOne(d => d.TramiteNumeroNavigation)
                    .WithMany(p => p.VmCtMedioSeguimientos)
                    .HasForeignKey(d => d.TramiteNumero)
                    .HasConstraintName("FK_VM_CT_MEDIO_SEGUIMIENTO_VM_CT_TRAMITE");
            });

            modelBuilder.Entity<VmCtMomento>(entity =>
            {
                entity.HasKey(e => e.MomentoId);

                entity.ToTable("VM_CT_MOMENTO");

                entity.HasIndex(e => e.TramiteNumero)
                    .HasName("index_VM_CT_MOMENTO_TRAMITE_NUMERO");

                entity.Property(e => e.MomentoId)
                    .HasColumnName("MOMENTO_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.Descripcion)
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(4000)
                    .IsUnicode(false);

                entity.Property(e => e.Orden)
                    .HasColumnName("ORDEN")
                    .HasColumnType("numeric(2, 0)");

                entity.Property(e => e.TramiteNumero)
                    .IsRequired()
                    .HasColumnName("TRAMITE_NUMERO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.TramiteNumeroNavigation)
                    .WithMany(p => p.VmCtMomentos)
                    .HasForeignKey(d => d.TramiteNumero)
                    .HasConstraintName("FK_VM_CT_MOMENTO_VM_CT_TRAMITE");
            });

            modelBuilder.Entity<VmCtPuntoAtencion>(entity =>
            {
                entity.HasKey(e => e.PuntoAtencionId);

                entity.ToTable("VM_CT_PUNTO_ATENCION");

                entity.Property(e => e.PuntoAtencionId)
                    .HasColumnName("PUNTO_ATENCION_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.DepartamentoCodigo)
                    .IsRequired()
                    .HasColumnName("DEPARTAMENTO_CODIGO")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DepartamentoNombre)
                    .IsRequired()
                    .HasColumnName("DEPARTAMENTO_NOMBRE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EsSedePrincipal)
                    .IsRequired()
                    .HasColumnName("ES_SEDE_PRINCIPAL")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HorarioAtencion)
                    .HasColumnName("HORARIO_ATENCION")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.InstitucionId)
                    .IsRequired()
                    .HasColumnName("INSTITUCION_ID")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Latitud)
                    .HasColumnName("LATITUD")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Longitud)
                    .HasColumnName("LONGITUD")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MunicipioNombre)
                    .IsRequired()
                    .HasColumnName("MUNICIPIO_NOMBRE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MunicpioCodigo)
                    .IsRequired()
                    .HasColumnName("MUNICPIO_CODIGO")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PuntoAtencionDireccion)
                    .HasColumnName("PUNTO_ATENCION_DIRECCION")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.PuntoAtencionNombre)
                    .HasColumnName("PUNTO_ATENCION_NOMBRE")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.PuntoAtencionTelefono)
                    .HasColumnName("PUNTO_ATENCION_TELEFONO")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.HasOne(d => d.Institucion)
                    .WithMany(p => p.VmCtPuntoAtencions)
                    .HasForeignKey(d => d.InstitucionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VM_CT_PUNTO_ATENCION_VM_SGP_INSTITUCION");
            });

            modelBuilder.Entity<VmCtSoporteNorma>(entity =>
            {
                entity.HasKey(e => e.SoporteNormaId);

                entity.ToTable("VM_CT_SOPORTE_NORMA");

                entity.HasIndex(e => e.TramiteNumero)
                    .HasName("index_VM_CT_SOPORTE_NORMA_TRAMITE_NUMERO");

                entity.Property(e => e.SoporteNormaId)
                    .HasColumnName("SOPORTE_NORMA_ID")
                    .HasColumnType("numeric(18, 0)");

                entity.Property(e => e.AnoNorma)
                    .IsRequired()
                    .HasColumnName("ANO_NORMA")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Articulos)
                    .HasColumnName("ARTICULOS")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.NumeroNorma)
                    .HasColumnName("NUMERO_NORMA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TipoNorma)
                    .IsRequired()
                    .HasColumnName("TIPO_NORMA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TramiteNumero)
                    .IsRequired()
                    .HasColumnName("TRAMITE_NUMERO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UrlDescarga)
                    .HasColumnName("URL_DESCARGA")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.UrlNorma)
                    .HasColumnName("URL_NORMA")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.HasOne(d => d.TramiteNumeroNavigation)
                    .WithMany(p => p.VmCtSoporteNormas)
                    .HasForeignKey(d => d.TramiteNumero)
                    .HasConstraintName("FK_VM_CT_SOPORTE_NORMA_VM_CT_TRAMITE");
            });

            modelBuilder.Entity<VmCtTramite>(entity =>
            {
                entity.HasKey(e => e.Numero);

                entity.ToTable("VM_CT_TRAMITE");

                entity.Property(e => e.Numero)
                    .HasColumnName("NUMERO")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AnotaBasicaEnLinea)
                    .HasColumnName("ANOTA_BASICA_EN_LINEA")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Barrio)
                    .HasColumnName("BARRIO")
                    .IsUnicode(false);

                entity.Property(e => e.Clase)
                    .IsRequired()
                    .HasColumnName("CLASE")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DepartamentoCodigoDane)
                    .HasColumnName("DEPARTAMENTO_CODIGO_DANE")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.DepartamentoNombre)
                    .HasColumnName("DEPARTAMENTO_NOMBRE")
                    .HasMaxLength(62)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoCodigo)
                    .IsRequired()
                    .HasColumnName("ESTADO_CODIGO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EstadoNombre)
                    .IsRequired()
                    .HasColumnName("ESTADO_NOMBRE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FechaActualizacion)
                    .HasColumnName("FECHA_ACTUALIZACION")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.InstitucionId)
                    .HasColumnName("INSTITUCION_ID")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.InstitucionNombre)
                    .HasColumnName("INSTITUCION_NOMBRE")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.MunicipioCodigoDane)
                    .HasColumnName("MUNICIPIO_CODIGO_DANE")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.MunicipioNombre)
                    .HasColumnName("MUNICIPIO_NOMBRE")
                    .HasMaxLength(62)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.NombreEstandarizado)
                    .HasColumnName("NOMBRE_ESTANDARIZADO")
                    .HasMaxLength(252)
                    .IsUnicode(false);

                entity.Property(e => e.NombreResultado)
                    .HasColumnName("NOMBRE_RESULTADO")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.ObjetoNombre)
                    .HasColumnName("OBJETO_NOMBRE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ObservacionFechaGeneral)
                    .HasColumnName("OBSERVACION_FECHA_GENERAL")
                    .HasMaxLength(600)
                    .IsUnicode(false);

                entity.Property(e => e.ObservacionTiempoObtencion)
                    .HasColumnName("OBSERVACION_TIEMPO_OBTENCION")
                    .IsUnicode(false);

                entity.Property(e => e.ObtencionInmediata)
                    .HasColumnName("OBTENCION_INMEDIATA")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.PalabrasClave)
                    .HasColumnName("PALABRAS_CLAVE")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Proposito)
                    .HasColumnName("PROPOSITO")
                    .IsUnicode(false);

                entity.Property(e => e.RealizadoMediosElectronicos)
                    .IsRequired()
                    .HasColumnName("REALIZADO_MEDIOS_ELECTRONICOS")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SeguimientoSedePrincipal)
                    .HasColumnName("SEGUIMIENTO_SEDE_PRINCIPAL")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SituacionVidaId)
                    .HasColumnName("SITUACION_VIDA_ID")
                    .HasColumnType("numeric(4, 0)");

                entity.Property(e => e.SituacionVidaNombre)
                    .HasColumnName("SITUACION_VIDA_NOMBRE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Subcategoria)
                    .HasColumnName("SUBCATEGORIA")
                    .IsUnicode(false);

                entity.Property(e => e.TiempoObtencion)
                    .HasColumnName("TIEMPO_OBTENCION")
                    .HasMaxLength(58)
                    .IsUnicode(false);

                entity.Property(e => e.TipoAtencionPresencial)
                    .IsRequired()
                    .HasColumnName("TIPO_ATENCION_PRESENCIAL")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TipoFechaEjecucion)
                    .IsRequired()
                    .HasColumnName("TIPO_FECHA_EJECUCION")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UrlCalendarioEjecucion)
                    .HasColumnName("URL_CALENDARIO_EJECUCION")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.UrlManualEnLinea)
                    .HasColumnName("URL_MANUAL_EN_LINEA")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.UrlPaginaDetalle)
                    .HasColumnName("URL_PAGINA_DETALLE")
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.UrlPuntosAtencion)
                    .HasColumnName("URL_PUNTOS_ATENCION")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.UrlResultadoWeb)
                    .HasColumnName("URL_RESULTADO_WEB")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.UrlTramiteEnLinea)
                    .HasColumnName("URL_TRAMITE_EN_LINEA")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.VerboNombre)
                    .HasColumnName("VERBO_NOMBRE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Institucion)
                    .WithMany(p => p.VmCtTramites)
                    .HasForeignKey(d => d.InstitucionId)
                    .HasConstraintName("FK_VM_CT_TRAMITE_VM_SGP_INSTITUCION");
            });

            modelBuilder.Entity<VmCtTramiteAudiencia>(entity =>
            {
                entity.HasKey(e => e.IdVmCtTramiteAudiencia);

                entity.ToTable("VM_CT_TRAMITE_AUDIENCIA");

                entity.HasIndex(e => e.TramiteNumero)
                    .HasName("index_VM_CT_TRAMITE_AUDIENCIA_TRAMITE_NUMERO");

                entity.Property(e => e.IdVmCtTramiteAudiencia).HasColumnName("ID_VM_CT_TRAMITE_AUDIENCIA");

                entity.Property(e => e.Audiencia)
                    .HasColumnName("AUDIENCIA")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.TipoAudiencia)
                    .IsRequired()
                    .HasColumnName("TIPO_AUDIENCIA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TramiteNumero)
                    .IsRequired()
                    .HasColumnName("TRAMITE_NUMERO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.TramiteNumeroNavigation)
                    .WithMany(p => p.VmCtTramiteAudiencias)
                    .HasForeignKey(d => d.TramiteNumero)
                    .HasConstraintName("FK_VM_CT_TRAMITE_AUDIENCIA_VM_CT_TRAMITE");
            });

            modelBuilder.Entity<VmCtTramitePuntoAtencion>(entity =>
            {
                entity.HasKey(e => e.IdVmCtTramitePuntoAtencion);

                entity.ToTable("VM_CT_TRAMITE_PUNTO_ATENCION");

                entity.HasIndex(e => new { e.IdVmCtTramitePuntoAtencion, e.TramiteNumero })
                    .HasName("index_VM_CT_TRAMITE_PUNTO_ATENCION_TRAMITE_NUMERO");

                entity.Property(e => e.IdVmCtTramitePuntoAtencion).HasColumnName("ID_VM_CT_TRAMITE_PUNTO_ATENCION");

                entity.Property(e => e.PuntoAtencionId)
                    .HasColumnName("PUNTO_ATENCION_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.TramiteNumero)
                    .IsRequired()
                    .HasColumnName("TRAMITE_NUMERO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.PuntoAtencion)
                    .WithMany(p => p.VmCtTramitePuntoAtencions)
                    .HasForeignKey(d => d.PuntoAtencionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VM_CT_TRAMITE_PUNTO_ATENCION_VM_CT_PUNTO_ATENCION");

                entity.HasOne(d => d.TramiteNumeroNavigation)
                    .WithMany(p => p.VmCtTramitePuntoAtencions)
                    .HasForeignKey(d => d.TramiteNumero)
                    .HasConstraintName("FK_VM_CT_TRAMITE_PUNTO_ATENCION_VM_CT_TRAMITE");
            });

            modelBuilder.Entity<VmCtTramiteTema>(entity =>
            {
                entity.HasKey(e => e.IdVmCtTramiteTema);

                entity.ToTable("VM_CT_TRAMITE_TEMA");

                entity.HasIndex(e => new { e.IdVmCtTramiteTema, e.TramiteNumero })
                    .HasName("index_VM_CT_TRAMITE_TEMA_TRAMITE_NUMERO");

                entity.Property(e => e.IdVmCtTramiteTema).HasColumnName("ID_VM_CT_TRAMITE_TEMA");

                entity.Property(e => e.TemaNombre)
                    .IsRequired()
                    .HasColumnName("TEMA_NOMBRE")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TramiteNumero)
                    .IsRequired()
                    .HasColumnName("TRAMITE_NUMERO")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.TramiteNumeroNavigation)
                    .WithMany(p => p.VmCtTramiteTemas)
                    .HasForeignKey(d => d.TramiteNumero)
                    .HasConstraintName("FK_VM_CT_TRAMITE_TEMA_VM_CT_TRAMITE");
            });

            modelBuilder.Entity<VmCtValorPago>(entity =>
            {
                entity.HasKey(e => e.ValorPagoId);

                entity.ToTable("VM_CT_VALOR_PAGO");

                entity.HasIndex(e => e.AccionCondicionId)
                    .HasName("index_VM_CT_VALOR_PAGO_ACCION_CONDICION_ID");

                entity.Property(e => e.ValorPagoId)
                    .HasColumnName("VALOR_PAGO_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.AccionCondicionId)
                    .HasColumnName("ACCION_CONDICION_ID")
                    .HasColumnType("numeric(10, 0)");

                entity.Property(e => e.CantidadSmlv)
                    .HasColumnName("CANTIDAD_SMLV")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasColumnName("DESCRIPCION")
                    .IsUnicode(false);

                entity.Property(e => e.Moneda)
                    .IsRequired()
                    .HasColumnName("MONEDA")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Orden)
                    .HasColumnName("ORDEN")
                    .HasColumnType("numeric(3, 0)");

                entity.Property(e => e.TipoValor)
                    .IsRequired()
                    .HasColumnName("TIPO_VALOR")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Valor)
                    .HasColumnName("VALOR")
                    .HasColumnType("numeric(18, 3)");

                entity.HasOne(d => d.AccionCondicion)
                    .WithMany(p => p.VmCtValorPagos)
                    .HasForeignKey(d => d.AccionCondicionId)
                    .HasConstraintName("FK_VM_CT_VALOR_PAGO_VM_CT_ACCION_CONDICION");
            });

            modelBuilder.Entity<VmSgpInstitucion>(entity =>
            {
                entity.ToTable("VM_SGP_INSTITUCION");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ClorId)
                    .HasColumnName("CLOR_ID")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ClorNombre)
                    .HasColumnName("CLOR_NOMBRE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DepaCodigoDane)
                    .HasColumnName("DEPA_CODIGO_DANE")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.DepaNombre)
                    .HasColumnName("DEPA_NOMBRE")
                    .HasMaxLength(62)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .HasColumnName("DIRECCION")
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasColumnName("EMAIL")
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.EsDependenEspecial)
                    .HasColumnName("ES_DEPENDEN_ESPECIAL")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .HasColumnName("FAX")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MuniCodigoDane)
                    .HasColumnName("MUNI_CODIGO_DANE")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.MuniNombre)
                    .HasColumnName("MUNI_NOMBRE")
                    .HasMaxLength(62)
                    .IsUnicode(false);

                entity.Property(e => e.NajuId)
                    .HasColumnName("NAJU_ID")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.NajuNombre)
                    .HasColumnName("NAJU_NOMBRE")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.NiveId)
                    .HasColumnName("NIVE_ID")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.NiveNombre)
                    .HasColumnName("NIVE_NOMBRE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrId)
                    .HasColumnName("ORDR_ID")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.OrdrNombre)
                    .HasColumnName("ORDR_NOMBRE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PadreId)
                    .HasColumnName("PADRE_ID")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.PadreNombre)
                    .HasColumnName("PADRE_NOMBRE")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.PaginaWeb)
                    .HasColumnName("PAGINA_WEB")
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.SectId)
                    .HasColumnName("SECT_ID")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SectNombre)
                    .HasColumnName("SECT_NOMBRE")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Sigla)
                    .HasColumnName("SIGLA")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.SuorId)
                    .HasColumnName("SUOR_ID")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SuorNombre)
                    .HasColumnName("SUOR_NOMBRE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasColumnName("TELEFONO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TipoDependencia)
                    .HasColumnName("TIPO_DEPENDENCIA")
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}