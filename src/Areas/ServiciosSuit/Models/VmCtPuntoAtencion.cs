﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtPuntoAtencion
    {
        public VmCtPuntoAtencion()
        {
            VmCtAccCondPtoAtencions = new HashSet<VmCtAccCondPtoAtencion>();
            VmCtTramitePuntoAtencions = new HashSet<VmCtTramitePuntoAtencion>();
        }

        public decimal PuntoAtencionId { get; set; }
        public string PuntoAtencionNombre { get; set; }
        public string MunicpioCodigo { get; set; }
        public string MunicipioNombre { get; set; }
        public string DepartamentoCodigo { get; set; }
        public string DepartamentoNombre { get; set; }
        public string HorarioAtencion { get; set; }
        public string PuntoAtencionDireccion { get; set; }
        public string PuntoAtencionTelefono { get; set; }
        public string InstitucionId { get; set; }
        public string EsSedePrincipal { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }

        public virtual VmSgpInstitucion Institucion { get; set; }
        public virtual ICollection<VmCtAccCondPtoAtencion> VmCtAccCondPtoAtencions { get; set; }
        public virtual ICollection<VmCtTramitePuntoAtencion> VmCtTramitePuntoAtencions { get; set; }
    }
}