﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtEntidadPago
    {
        public decimal EntidadPagoId { get; set; }
        public decimal AccionCondicionId { get; set; }
        public string TipoCuenta { get; set; }
        public string NumeroCuenta { get; set; }
        public string DescripcionOtro { get; set; }
        public string NombreEntidad { get; set; }
        public string NombreCuenta { get; set; }
        public string CodigoRecaudo { get; set; }

        public virtual VmCtAccionCondicion AccionCondicion { get; set; }
    }
}