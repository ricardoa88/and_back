﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtSoporteNorma
    {
        public decimal SoporteNormaId { get; set; }
        public string TramiteNumero { get; set; }
        public string NumeroNorma { get; set; }
        public string AnoNorma { get; set; }
        public string TipoNorma { get; set; }
        public string Articulos { get; set; }
        public string UrlNorma { get; set; }
        public string UrlDescarga { get; set; }

        public virtual VmCtTramite TramiteNumeroNavigation { get; set; }
    }
}