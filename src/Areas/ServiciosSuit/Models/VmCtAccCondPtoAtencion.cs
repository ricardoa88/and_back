﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtAccCondPtoAtencion
    {
        public decimal AccionCondicionId { get; set; }
        public decimal PuntoAtencionId { get; set; }
        public int IdVmCtAccCondPtoAtencion { get; set; }

        public virtual VmCtAccionCondicion AccionCondicion { get; set; }
        public virtual VmCtPuntoAtencion PuntoAtencion { get; set; }
    }
}