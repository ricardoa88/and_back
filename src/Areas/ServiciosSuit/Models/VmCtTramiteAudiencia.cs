﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtTramiteAudiencia
    {
        public string TramiteNumero { get; set; }
        public string TipoAudiencia { get; set; }
        public string Audiencia { get; set; }
        public int IdVmCtTramiteAudiencia { get; set; }

        public virtual VmCtTramite TramiteNumeroNavigation { get; set; }
    }
}