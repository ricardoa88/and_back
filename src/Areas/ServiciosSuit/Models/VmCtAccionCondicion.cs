﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class VmCtAccionCondicion
    {
        public VmCtAccionCondicion()
        {
            VmCtAccCondPtoAtencions = new HashSet<VmCtAccCondPtoAtencion>();
            VmCtAccCondTipoAudiencias = new HashSet<VmCtAccCondTipoAudiencia>();
            VmCtCanals = new HashSet<VmCtCanal>();
            VmCtCantidadDocs = new HashSet<VmCtCantidadDoc>();
            VmCtEntidadPagos = new HashSet<VmCtEntidadPago>();
            VmCtValorPagos = new HashSet<VmCtValorPago>();
        }

        public decimal AccionCondicionId { get; set; }
        public decimal MomentoId { get; set; }
        public string TramiteNumero { get; set; }
        public string TipoAccionCondicion { get; set; }
        public decimal Orden { get; set; }
        public string DocumentoTipo { get; set; }
        public string DocumentoNombre { get; set; }
        public string DocumentoAnotacionAdicional { get; set; }
        public string PagoDisponibleEnLinea { get; set; }
        public string PagoDispEnInstitucion { get; set; }
        public string PagoDispEntidadRecaudadora { get; set; }
        public string PagoDispInstDescripcion { get; set; }
        public string PagoEnLineaUrl { get; set; }
        public string FormularioNombre { get; set; }
        public string FormularioUrl { get; set; }
        public string FormularioAnotacion { get; set; }
        public string VerificacionInstDescripcion { get; set; }
        public string AtencionDescripcion { get; set; }
        public decimal? ExcepcionId { get; set; }
        public string Excepcion { get; set; }
        public string FormularioUrlDescarga { get; set; }

        public virtual VmCtMomento Momento { get; set; }
        public virtual VmCtTramite TramiteNumeroNavigation { get; set; }
        public virtual ICollection<VmCtAccCondPtoAtencion> VmCtAccCondPtoAtencions { get; set; }
        public virtual ICollection<VmCtAccCondTipoAudiencia> VmCtAccCondTipoAudiencias { get; set; }
        public virtual ICollection<VmCtCanal> VmCtCanals { get; set; }
        public virtual ICollection<VmCtCantidadDoc> VmCtCantidadDocs { get; set; }
        public virtual ICollection<VmCtEntidadPago> VmCtEntidadPagos { get; set; }
        public virtual ICollection<VmCtValorPago> VmCtValorPagos { get; set; }
    }
}