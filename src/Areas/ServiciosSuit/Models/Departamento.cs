﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Models
{
    public partial class Departamento
    {
        public Departamento()
        {
            Municipios = new HashSet<Municipio>();
        }

        public string DepCodigo { get; set; }
        public string DepNombre { get; set; }

        public virtual ICollection<Municipio> Municipios { get; set; }
    }
}