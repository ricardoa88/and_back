﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tramites_servicios_webapi.Areas.ServiciosSuit.Models;
using tramites_servicios_webapi.Models;

namespace tramites_servicios_webapi.Areas.ServiciosSuit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FichaTramiteController : ControllerBase
    {
        private readonly ServiciosSuitContext _context;
        private readonly TramiteContext _contextMaster;

        public FichaTramiteController(ServiciosSuitContext context, TramiteContext contextMaster)
        {
            _context = context;
            _contextMaster = contextMaster;
        }

        //Servicio valida existencia y tipo de ficha tramite por id
        [HttpGet("GetTipoFichaTramiteById/{idTramite}", Name = "GetTipoFichaTramiteById")]
        public IActionResult GetTipoFichaTramiteById(string idTramite)
        {

            int ficha = (from tram in _context.VmCtTramites
                         where tram.NombreEstandarizado == ((from trami in _context.VmCtTramites
                                                             where trami.Numero == idTramite
                                                             select trami.NombreEstandarizado).FirstOrDefault())
                         select tram).Count();

            if (ficha == 0)
            {

                ficha = (from otros in _contextMaster.TBL_OTROS_SERVICIOSS
                         where otros.OTROS_SERVICIOS_ID == idTramite
                         select otros).Count();

                if (ficha == 1)
                {
                    string messageNotFound = "Ficha otro servicio";
                    var result = new { message = messageNotFound, StatusCode = 603 };
                    return Ok(result);

                }
                else {
                    string messageNotFound = "Tramite no existe";
                    var result = new { message = messageNotFound, StatusCode = 604 };
                    return NotFound(result);
                }

            }
            else if (ficha == 1)
            {
                string messageNotFound = "Ficha única";
                var result = new { message = messageNotFound, StatusCode = 601 };
                return Ok(result);

            }
            else {
                string messageNotFound = "Ficha estándar";
                var result = new { message = messageNotFound, StatusCode = 602 };
                return Ok(result);

            }

        }

        //Servicio que obtiene la información de la ficha estandar por el id
        [HttpGet("GetInfoFichaEstandarById/{idTramite}", Name = "GetInfoFichaEstandarById")]
        public ActionResult<FichaEstandar> GetInfoFichaEstandarById(string idTramite)
        {
            FichaEstandar infoFichaEstandar = new FichaEstandar();

            infoFichaEstandar = (from tram in _context.VmCtTramites
                                 where tram.Numero == idTramite
                                 select new FichaEstandar { NombreEstandarizado = tram.NombreEstandarizado,
                                     Proposito = tram.Proposito,
                                     IdTramite = tram.Numero }).FirstOrDefault();

            if (infoFichaEstandar == null)
            {

                string messageNotFound = "Tramite no tiene ficha estandar";
                var result = new { message = messageNotFound, StatusCode = 605 };
                return NotFound(result);

            }

            List<PuntosMunicipios> puntosMunicipio = new List<PuntosMunicipios>();
            puntosMunicipio = (from tra in _context.VmCtTramites
                               where tra.NombreEstandarizado == infoFichaEstandar.NombreEstandarizado
                               select new PuntosMunicipios { MunicipioCodigoDane = tra.MunicipioCodigoDane,
                                                             MunicipioNombre = tra.MunicipioNombre,
                               DepartamentoCodigoDane = tra.DepartamentoCodigoDane,
                               DepartamentoNombre = tra.DepartamentoNombre,
                               IdTramiteMunicipio = tra.Numero} ).ToList();

            if (puntosMunicipio.Count <= 1)
            {

                string messageNotFound = "Tramite no tiene ficha estandar";
                var result = new { message = messageNotFound, StatusCode = 605 };
                return NotFound(result);

            }

            infoFichaEstandar.Puntos = puntosMunicipio;

            return infoFichaEstandar;

        }

        //Servicio que obtiene la información de la ficha estandar por el id
        [HttpGet("GetTipoTramiteFichaEspecificaById/{idTramite}", Name = "GetTipoTramiteFichaEspecificaById")]
        public ActionResult<TipoTramiteEspecifico> GetTipoTramiteFichaEspecificaById(string idTramite)
        {
            TipoTramiteEspecifico tipoTramite = new TipoTramiteEspecifico();

            tipoTramite = (from tram in _context.VmCtTramites
                           join ins in _context.VmSgpInstitucions on tram.InstitucionId equals ins.Id
                                 where tram.Numero == idTramite
                                 select new TipoTramiteEspecifico
                                 {
                                     IdTramite = tram.Numero,
                                     Tipotramite = tram.RealizadoMediosElectronicos == "NO_DISPONIBLE" ? "Presencial" :
                                                   tram.RealizadoMediosElectronicos == "PARCIALMENTE"  ? "SemiPresencial" :
                                                   tram.RealizadoMediosElectronicos == "TOTALMENTE" ? "Realizar trámite en línea" : " - ",
                                     UrlTramiteEnLinea = tram.UrlTramiteEnLinea,
                                     PaginaWeb = ins.PaginaWeb
                                 }).FirstOrDefault();

            if (tipoTramite == null) {
                string messageNotFound = "Tramite no existe";
                var result = new { message = messageNotFound, StatusCode = 604 };
                return NotFound(result);
            }

            return Ok(tipoTramite);
        }


        //Servicio que obtiene los tipos de audiencia  segun el id del tramite especifico
        [HttpGet("GetTiposAudienciaById/{idTramite}", Name = "GetTiposAudienciaById")]
        public ActionResult GetTiposAudienciaById(string idTramite)
        {
            var tiposAudiencia = (from tram in _context.VmCtTramites
                                  join tau in _context.VmCtTramiteAudiencias on tram.Numero equals tau.TramiteNumero into TablaAudiencias
                                  from ta in TablaAudiencias.DefaultIfEmpty()
                                   where tram.Numero == idTramite
                                   select new
                                   {
                                       tipoaudiencia = ta.TipoAudiencia
                                   }).Distinct().ToList();

            if (tiposAudiencia.Count == 0)
            {
                string messageNotFound = "Tramite no existe";
                var result = new { message = messageNotFound, StatusCode = 604 };
                return NotFound(result);
            }

            return Ok(tiposAudiencia);
        }

        //Servicio que obtiene los momentos de acuerdo al di del tramite y al tipo de audiencia
        [HttpGet("GetMomentosByIdAudiencia/{idTramite}/{audiencia}", Name = "GetMomentosByIdAudiencia")]
        public ActionResult GetMomentosByIdAudiencia(string idTramite, string audiencia)
        {
            var momentos = (from tram in _context.VmCtTramites
                                  join tau in _context.VmCtTramiteAudiencias on tram.Numero equals tau.TramiteNumero into TablaAudiencias
                                  from ta in TablaAudiencias.DefaultIfEmpty()
                                  join mo in _context.VmCtMomentos on tram.Numero equals mo.TramiteNumero into TablaMomentos
                                  from mom in TablaMomentos.DefaultIfEmpty()
                                  where tram.Numero == idTramite  && ta.TipoAudiencia == audiencia
                                  orderby mom.Orden ascending
                            select new
                                  {
                                      Descripcion = mom.Descripcion,
                                      Orden = mom.Orden,
                                      Informacion = "[]",
                                      Excepcion = "[]",
                                  }).Distinct().ToList();

            if (momentos.Count == 0)
            {
                string messageNotFound = "Tramite no existe";
                var result = new { message = messageNotFound, StatusCode = 604 };
                return NotFound(result);
            }

            return Ok(momentos.OrderBy(p =>p.Orden));
        }

        //Servicio que obtiene los momentos de acuerdo al di del tramite y al tipo de audiencia
        [HttpGet("GetDataFichaByIdAudiencia/{idTramite}/{audiencia}/{momento}", Name = "GetDataFichaByIdAudiencia")]
        public ActionResult GetDataFichaByIdAudiencia(string idTramite, string audiencia, int momento)
        {
            var dataFicha = (from T in _context.VmCtTramites
                            join ACN in _context.VmCtAccionCondicions on T.Numero equals ACN.TramiteNumero into TablaAccionCondiccion
                            from AC in TablaAccionCondiccion.DefaultIfEmpty()
                            join MOM in _context.VmCtMomentos on T.Numero equals MOM.TramiteNumero into TablaMomentos
                            from M in TablaMomentos.DefaultIfEmpty()
                            join TAU in _context.VmCtTramiteAudiencias on T.Numero equals TAU.TramiteNumero into TablaTramiteAudiencias
                            from TA in TablaTramiteAudiencias.DefaultIfEmpty()
                            join ACT in _context.VmCtAccCondTipoAudiencias on AC.AccionCondicionId equals ACT.AccionCondicionId into TablaAccionConTipoAudiencia
                            from ACTA in TablaAccionConTipoAudiencia.DefaultIfEmpty()
                            join VAPAS in _context.VmCtValorPagos on AC.AccionCondicionId equals VAPAS.AccionCondicionId into TablaValorPagos
                            from VP in TablaValorPagos.DefaultIfEmpty()
                            join ENTP in _context.VmCtEntidadPagos on AC.AccionCondicionId equals ENTP.AccionCondicionId into TablaEntidadPagos
                            from EP in TablaEntidadPagos.DefaultIfEmpty()
                            join CANTD in _context.VmCtCantidadDocs on AC.AccionCondicionId equals CANTD.AccionCondicionId into TablaCantidadDocs
                            from DC in TablaCantidadDocs.DefaultIfEmpty()
                            join CANA in _context.VmCtCanals on AC.AccionCondicionId equals CANA.AccionCondicionId into TablaCanales
                            from CA in TablaCanales.DefaultIfEmpty()
                            where T.Numero == idTramite && TA.TipoAudiencia == audiencia && AC.MomentoId == M.MomentoId && M.Orden == momento
                            orderby M.Orden ascending, AC.Orden ascending, VP.Orden ascending
                            select new FichaTramiteEspecifica
                            {
                                OrdenMomento = M.Orden,
                                Nombre = T.Nombre,
                                NombreResultado = T.NombreResultado,
                                Proposito = T.Proposito,
                                UrlResultadoWeb = T.UrlResultadoWeb,
                                UrlTramiteEnLinea = T.UrlTramiteEnLinea,
                                TipoAudiencia = TA.TipoAudiencia,
                                TipoAccionCondicion = AC.TipoAccionCondicion,
                                DocumentoTipo = AC.DocumentoTipo,
                                DocumentoNombre = AC.DocumentoNombre,
                                DocumentoAnotacionAdicional = AC.DocumentoAnotacionAdicional,
                                PagoDisponibleEnLinea = AC.PagoDisponibleEnLinea,
                                PagoDispEnInstitucion = AC.PagoDispEnInstitucion,
                                PagoDispEntidadRecaudadora = AC.PagoDispEntidadRecaudadora,
                                PagoDispInstDescripcion = AC.PagoDispInstDescripcion,
                                PagoEnlineaUrl = AC.PagoEnLineaUrl,
                                FormularioAnotacion = AC.FormularioAnotacion,
                                FormularioNombre = AC.FormularioNombre,
                                VerificacionInstDescripcion = AC.VerificacionInstDescripcion,
                                AtencionDescripcion = AC.AtencionDescripcion,
                                ExcepcionId = Convert.ToString(Convert.ToInt32(AC.ExcepcionId)),
                                Excepcion = AC.Excepcion,
                                FormularioUrlDescarga = AC.FormularioUrlDescarga,
                                CantidadSmlv = VP.CantidadSmlv,
                                Moneda = VP.Moneda,
                                TipoValor = VP.TipoValor,
                                Valor = Convert.ToString(VP.Valor),
                                CodigoRecaudo = EP.CodigoRecaudo,
                                DescripcionOtro = EP.DescripcionOtro,
                                NombreCuenta = EP.NombreCuenta,
                                NombreEntidad = EP.NombreEntidad,
                                NumeroCuenta = EP.NumeroCuenta,
                                TipoCuenta = EP.TipoCuenta,
                                OrdenPago = Convert.ToInt32(VP.Orden),
                                DescripcionMomento = M.Descripcion,
                                DescripcionPago = VP.Descripcion,
                                OrdenAccion = Convert.ToInt32(AC.Orden),
                                CondicionNueva = AC.TipoAccionCondicion,
                                AccionCondicionId = Convert.ToString(AC.AccionCondicionId),
                                UnidadCantidad = DC.UnidadCantidad,
                                CantidadDoc = Convert.ToString(DC.Cantidad),
                                ObservacionCantidad = DC.Observacion
                            }).Distinct().ToList();

            if (dataFicha.Count == 0)
            {
                string messageNotFound = "Tramite no existe";
                var result = new { message = messageNotFound, StatusCode = 604 };
                return NotFound(result);
            }

            return Ok(dataFicha);
        }


        //Servicio que obtiene los canales de atencion de acuerdo al id del tramite, tipo de audiencia y momento
        [HttpGet("GetCanalesByMomentoIdAudiencia/{idTramite}/{audiencia}/{momento}", Name = "GetCanalesByMomentoIdAudiencia")]
        public ActionResult GetCanalesByMomentoIdAudiencia(string idTramite, string audiencia, int momento)
        {
            var canales= (from T in _context.VmCtTramites
                             join AC in _context.VmCtAccionCondicions on T.Numero equals AC.TramiteNumero
                             join M in _context.VmCtMomentos on T.Numero equals M.TramiteNumero
                             join TA in _context.VmCtTramiteAudiencias on T.Numero equals TA.TramiteNumero
                             join CAN in _context.VmCtCanals on AC.AccionCondicionId equals CAN.AccionCondicionId
                             where T.Numero == idTramite && TA.TipoAudiencia == audiencia && AC.MomentoId == M.MomentoId && M.Orden == momento
                             orderby AC.Orden ascending
                             select new Canales
                             {
                                 TipoTelefono = CAN.TipoTelefono,
                                 ExtensionTelFijo = CAN.ExtensionTelFijo,
                                 HorarioAtencionTelef = CAN.HorarioAtencionTelef,
                                 MunicipioTelefono = CAN.MunicipioTelefono,
                                 NombreCanalWeb = CAN.NombreCanalWeb,
                                 NumeroTelefono = CAN.NumeroTelefono,
                                 TipoCanal = CAN.TipoCanal,
                                 Correo = CAN.Correo,
                                 UrlCanalWeb = CAN.UrlCanalWeb
                             }).Distinct().ToList();

            if (canales.Count == 0)
            {
                string messageNotFound = "Tramite no existe";
                var result = new { message = messageNotFound, StatusCode = 604 };
                return NotFound(result);
            }

            return Ok(canales);
        }

        //Servicio que obtiene los pagos de acuerdo al id del tramite, tipo de audiencia y momento
        [HttpGet("GetPagosByMomentoIdAudiencia/{idTramite}/{audiencia}/{momento}", Name = "GetPagosByMomentoIdAudiencia")]
        public ActionResult GetPagosByMomentoIdAudiencia(string idTramite, string audiencia, int momento)
        {
            var pagos = (from T in _context.VmCtTramites
                            join ACN in _context.VmCtAccionCondicions on T.Numero equals ACN.TramiteNumero into TablaAccionCondiccion
                            from AC in TablaAccionCondiccion.DefaultIfEmpty()
                            join MOM in _context.VmCtMomentos on T.Numero equals MOM.TramiteNumero into TablaMomentos
                            from M in TablaMomentos.DefaultIfEmpty()
                            join TAU in _context.VmCtTramiteAudiencias on T.Numero equals TAU.TramiteNumero into TablaTramiteAudiencias
                            from TA in TablaTramiteAudiencias.DefaultIfEmpty()
                            join ACT in _context.VmCtAccCondTipoAudiencias on AC.AccionCondicionId equals ACT.AccionCondicionId into TablaAccionConTipoAudiencia
                            from ACTA in TablaAccionConTipoAudiencia.DefaultIfEmpty()
                            join VAPAS in _context.VmCtValorPagos on AC.AccionCondicionId equals VAPAS.AccionCondicionId into TablaValorPagos
                            from VP in TablaValorPagos.DefaultIfEmpty()
                            join ENTP in _context.VmCtEntidadPagos on AC.AccionCondicionId equals ENTP.AccionCondicionId into TablaEntidadPagos
                            from EP in TablaEntidadPagos.DefaultIfEmpty()
                            join CANTD in _context.VmCtCantidadDocs on AC.AccionCondicionId equals CANTD.AccionCondicionId into TablaCantidadDocs
                            from DC in TablaCantidadDocs.DefaultIfEmpty()
                            where T.Numero == idTramite && TA.TipoAudiencia == audiencia && AC.MomentoId == M.MomentoId && M.Orden == momento
                            orderby VP.Orden ascending
                            select new Pagos
                            {
                                CantidadSmlv = VP.CantidadSmlv,
                                Moneda = VP.Moneda,
                                TipoValor = VP.TipoValor,
                                Valor = Convert.ToString(VP.Valor),
                                OrdenPago = Convert.ToInt32(VP.Orden),
                                DescripcionPago = VP.Descripcion,
                                CodigoRecaudo = EP.CodigoRecaudo,
                                DescripcionOtro = EP.DescripcionOtro,
                                NombreCuenta = EP.NombreCuenta,
                                NombreEntidad = EP.NombreEntidad,
                                NumeroCuenta = EP.NumeroCuenta,
                                TipoCuenta = EP.TipoCuenta,
                                DescripcionMomento = M.Descripcion,
                            }).Distinct().ToList();

            if (pagos.Count == 0)
            {
                string messageNotFound = "Tramite no existe";
                var result = new { message = messageNotFound, StatusCode = 604 };
                return NotFound(result);
            }

            return Ok(pagos);
        }

        //Servicio que obtiene los puntos de atención de acuerdo al id del tramite
        [HttpGet("GetPuntosAtencionById/{idTramite}", Name = "GetPuntosAtencionById")]
        public ActionResult GetPuntosAtencionById(string idTramite)
        {
            var puntosAtencion = (from T in _context.VmCtTramites
                         join PTA in _context.VmCtTramitePuntoAtencions on T.Numero equals PTA.TramiteNumero
                         join PT in _context.VmCtPuntoAtencions on PTA.PuntoAtencionId equals PT.PuntoAtencionId
                         where T.Numero == idTramite
                         orderby PT.MunicipioNombre ascending, PT.DepartamentoNombre ascending, PT.PuntoAtencionDireccion ascending
                         select new PuntosAtencion
                         {
                             PuntoAtencionId = Convert.ToString(Convert.ToInt32(PT.PuntoAtencionId)),
                             PuntoAtencionNombre = PT.PuntoAtencionNombre,
                             HorarioAtencion = PT.HorarioAtencion,
                             PuntoAtencionDireccion = PT.PuntoAtencionDireccion,
                             PuntoAtencionTelefono = PT.PuntoAtencionTelefono,
                             Latitud = PT.Latitud,
                             Longitud = PT.Longitud,
                             Municipio = PT.MunicipioNombre,
                             Departamento = PT.DepartamentoNombre
                         }).Distinct().ToList();

            if (puntosAtencion.Count == 0)
            {
                string messageNotFound = "Tramite no contiene puntos de atención";
                var result = new { message = messageNotFound, StatusCode = 606 };
                return NotFound(result);
            }

            return Ok(puntosAtencion);
        }

        //Servicio que obtiene los puntos de atención de acuerdo al id del tramite
        [HttpGet("GetPuntosAtencionPorTramiteMonetoAccion/{idTramite}/{idMomento}/{idAccion}", Name = "GetPuntosAtencionPorTramiteMonetoAccion")]
        public ActionResult GetPuntosAtencionPorTramiteMonetoAccion(string idTramite, string idMomento, string idAccion)
        {
            var puntosAtencion = (from T in _context.VmCtTramites
                         join PTA in _context.VmCtTramitePuntoAtencions on T.Numero equals PTA.TramiteNumero
                         join PT in _context.VmCtPuntoAtencions on PTA.PuntoAtencionId equals PT.PuntoAtencionId
                         where T.Numero == idTramite
                         orderby PT.MunicipioNombre ascending, PT.DepartamentoNombre ascending, PT.PuntoAtencionDireccion ascending
                         select new PuntosAtencion
                         {
                             PuntoAtencionId = Convert.ToString(Convert.ToInt32(PT.PuntoAtencionId)),
                             PuntoAtencionNombre = PT.PuntoAtencionNombre,
                             HorarioAtencion = PT.HorarioAtencion,
                             PuntoAtencionDireccion = PT.PuntoAtencionDireccion,
                             PuntoAtencionTelefono = PT.PuntoAtencionTelefono,
                             Latitud = PT.Latitud,
                             Longitud = PT.Longitud,
                             Municipio = PT.MunicipioNombre,
                             Departamento = PT.DepartamentoNombre
                         }).Distinct().ToList();

            if (puntosAtencion.Count == 0)
            {
                string messageNotFound = "Tramite no contiene puntos de atención";
                var result = new { message = messageNotFound, StatusCode = 606 };
                return NotFound(result);
            }

            return Ok(puntosAtencion);
        }


        //Servicio que obtiene la normatividad de acuerdo al id del tramite
        [HttpGet("GetNormatividadById/{idTramite}", Name = "GetNormatividadById")]
        public ActionResult GetNormatividadById(string idTramite)
        {
            var normatividad = (from T in _context.VmCtTramites
                                  join SN in _context.VmCtSoporteNormas on T.Numero equals SN.TramiteNumero
                                  where T.Numero == idTramite
                                  orderby SN.SoporteNormaId ascending
                                  select new Normatividad
                                  {
                                      TipoNorma = SN.TipoNorma,
                                      NumeroNorma = SN.NumeroNorma,
                                      AnoNorma = SN.AnoNorma,
                                      Articulos = SN.Articulos,
                                      UrlNorma = SN.UrlNorma,
                                      UrlDescarga = SN.UrlDescarga
                                  }).Distinct().ToList();

            if (normatividad.Count == 0)
            {
                string messageNotFound = "Tramite no contiene normatividad";
                var result = new { message = messageNotFound, StatusCode = 607 };
                return NotFound(result);
            }

            return Ok(normatividad);
        }

        //Servicio que obtiene la información de la ficha estandar por el id
        [HttpGet("GetInfoBasicaEspecificaById/{idTramite}", Name = "GetInfoBasicaEspecificaById")]
        public ActionResult<BasicaEspecifica> GetInfoBasicaEspecificaById(string idTramite)
        {
            BasicaEspecifica infoBasica = new BasicaEspecifica();

            infoBasica = (from tram in _context.VmCtTramites
                                 where tram.Numero == idTramite
                                 select new BasicaEspecifica
                                 {
                                     NombreEstandarizado = tram.NombreEstandarizado,
                                     Proposito = tram.Proposito,
                                     IdTramite = tram.Numero,
                                     Entidad = tram.InstitucionNombre
                                 }).FirstOrDefault();

            if (infoBasica == null)
            {

                string messageNotFound = "Tramite no existe";
                var result = new { message = messageNotFound, StatusCode = 604 };
                return NotFound(result);

            }

            return infoBasica;

        }


        // GET: api/FichaTramite
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VmCtTramite>>> GetVmCtTramites()
        {
            return await _context.VmCtTramites.ToListAsync();
        }

        // GET: api/FichaTramite/5
        [HttpGet("{id}")]
        public async Task<ActionResult<VmCtTramite>> GetVmCtTramite(string id)
        {
            var vmCtTramite = await _context.VmCtTramites.FindAsync(id);

            if (vmCtTramite == null)
            {
                return NotFound();
            }

            return vmCtTramite;
        }

        // PUT: api/FichaTramite/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutVmCtTramite(string id, VmCtTramite vmCtTramite)
        {
            if (id != vmCtTramite.Numero)
            {
                return BadRequest();
            }

            _context.Entry(vmCtTramite).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!VmCtTramiteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FichaTramite
        [HttpPost]
        public async Task<ActionResult<VmCtTramite>> PostVmCtTramite(VmCtTramite vmCtTramite)
        {
            _context.VmCtTramites.Add(vmCtTramite);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (VmCtTramiteExists(vmCtTramite.Numero))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetVmCtTramite", new { id = vmCtTramite.Numero }, vmCtTramite);
        }

        // DELETE: api/FichaTramite/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<VmCtTramite>> DeleteVmCtTramite(string id)
        {
            var vmCtTramite = await _context.VmCtTramites.FindAsync(id);
            if (vmCtTramite == null)
            {
                return NotFound();
            }

            _context.VmCtTramites.Remove(vmCtTramite);
            await _context.SaveChangesAsync();

            return vmCtTramite;
        }

        private bool VmCtTramiteExists(string id)
        {
            return _context.VmCtTramites.Any(e => e.Numero == id);
        }
    }
}
