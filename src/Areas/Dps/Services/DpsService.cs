using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Contraloria.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;


namespace tramites_servicios_webapi.Contraloria.Services {
    public class DpsService {
        private const string UrlBase = "http://consultacertiunidos.prosperidadsocial.gov.co/";
        private const string UrlVinculacion = "api/ConsultaCertiunidos/ObtenerPersonaUnidosPorDocumento";
        private const string UrlVerificacion = "api/ConsultaCertiunidos/ObtenerPersonaUnidosPorCodigoVerificacion";

    public static async Task<FileContentResult> DownloadVinculacion(string tipoDocumento, string documento)
    {
        UriBuilder builder = new UriBuilder($"{UrlBase}{UrlVinculacion}");
        builder.Query = $"tipoDocumento={tipoDocumento}&numeroDocumento={documento}";
        FileContentResult fileVinculacion  = null;
        using (var client = new HttpClient()){
            using (var result = await client.GetAsync(builder.Uri)){
                if (result.IsSuccessStatusCode) {
                    byte[] bytes = result.Content.ReadAsByteArrayAsync ().Result;
                    fileVinculacion =  new FileContentResult(bytes, "application/pdf");
                }
            }
        }

        return fileVinculacion;
    }

    public static async Task<FileContentResult> DownloadVerificacion(string codigoVerificacion)
    {
         UriBuilder builder = new UriBuilder($"{UrlBase}{UrlVerificacion}");
         builder.Query = $"codigoVerificacion={codigoVerificacion}";
         FileContentResult fileVerificacion  = null;
        using (var client = new HttpClient()){
            using (var result = await client.GetAsync(builder.Uri)){
                if (result.IsSuccessStatusCode){
                    byte[] bytes = result.Content.ReadAsByteArrayAsync ().Result;
                    fileVerificacion =  new FileContentResult(bytes, "application/pdf");
                }
            }
        }

        return fileVerificacion;
    }

    }

}