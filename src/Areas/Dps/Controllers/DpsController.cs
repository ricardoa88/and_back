using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Contraloria.Services;
using tramites_servicios_webapi.Contraloria.Models;

namespace tramites_servicios_webapi.Controllers
{
    [Route ("api/[controller]")]
    [ApiController]
    public class DpsController : ControllerBase
    {   
        // GET: api/Dps/Vinculacion  
        /// <summary>
        /// Genera el certificado de Vinculacion a la estrategia unidos
        /// </summary>
        /// <param name="tipoDocumento">Tipo de documento de la persona</param>
        /// <param name="documento">Numero de documento de la persona</param>
        /// <returns>Archivo generado</returns>
        [HttpGet("Vinculacion")]        
        public  async Task<FileContentResult> Vinculacion(
            [FromQuery(Name = "tipoDocumento")] string tipoDocumento,
            [FromQuery(Name = "numeroDocumento")] string documento) {
           
            return  await DpsService.DownloadVinculacion(tipoDocumento, documento);
        }   

        //GET: api/Dps/Verificacion
        /// <summary>
        /// Genera el certificado de Vinculacion a la estrategia unidos
        /// </summary>
        /// <param name="codigoVerificacion">Codigo del documento</param>
        /// <returns>Archivo generado</returns>
        [HttpGet("Verificacion")]        
        public  async Task<FileContentResult> Verificacion(
            [FromQuery(Name = "codigoVerificacion")] string codigoVerificacion) {
           
            return  await DpsService.DownloadVerificacion(codigoVerificacion);
        } 
    }    
}