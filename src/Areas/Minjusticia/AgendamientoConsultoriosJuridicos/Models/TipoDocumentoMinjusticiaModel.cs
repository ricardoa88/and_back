using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class TipoDocumentoMinjusticiaModel: MinjusticiaBodyResult  {
        public List<DetalleTipoDocumentoModel> Vistas { get; set; }

        public TipoDocumentoMinjusticiaModel() { }

        public TipoDocumentoMinjusticiaModel(string mensaje, bool status) {
            Mensaje = mensaje;
            OperacionExitosa = status;
        }
    }

    public class DetalleTipoDocumentoModel {

        public long Id { get; set; }

        [JsonProperty("TipoDocumentoNombre")]
        public string Nombre { get; set; }
    }
}
