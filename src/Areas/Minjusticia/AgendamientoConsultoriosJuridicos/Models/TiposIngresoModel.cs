using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class TipoIngresoMinjusticiaModel: MinjusticiaBodyResult  {
        public List<DetalleTipoIngresoModel> Vistas { get; set; }

        public TipoIngresoMinjusticiaModel() { }

        public TipoIngresoMinjusticiaModel(string mensaje, bool status) {
            Mensaje = mensaje;
            OperacionExitosa = status;
        }
    }

    public class DetalleTipoIngresoModel {

        public long Id { get; set; }

        [JsonProperty("TipoIngresosDescripcion")]
        public string Nombre { get; set; }
    }
}
