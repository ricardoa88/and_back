using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class RegistroUsuarioRespuestaModel: MinjusticiaBodyResult {
        public DetalleRegistroUsuario Vista { get; set; }

        public RegistroUsuarioRespuestaModel() {}

        public RegistroUsuarioRespuestaModel(string mensaje, bool status) {
            Mensaje = mensaje;
            OperacionExitosa = status;
        }
    }   

    public class DetalleRegistroUsuario {
        public long Id { get; set; }
    }
}