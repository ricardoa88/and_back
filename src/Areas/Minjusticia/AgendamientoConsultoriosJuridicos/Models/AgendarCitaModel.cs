using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class AgendarCitaModel {
        public string Username { get; set; }

        public long CiudadanoId { get; set; }

        public long DisponibilidadId { get; set; }

        public long TemaConsultaId { get; set; }

        public long EstadoCitaDetalleId { get; set; }

        public bool CitaConsultorioEstado { get; set;}

        public AgendarCitaModel() {
            this.EstadoCitaDetalleId = 1;
            this.CitaConsultorioEstado = true;
        }
    }
}