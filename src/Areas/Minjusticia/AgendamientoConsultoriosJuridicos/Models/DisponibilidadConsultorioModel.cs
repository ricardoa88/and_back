using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class ConsultaDisponibilidadRequest {
        public long IdConsultorio { get; set; }

        public string Fecha { get; set; }
    }

    public class DisponibilidadMinjusticiaModel: MinjusticiaBodyResult {
        public List<DisponibilidadModel> Vistas { get; set; }

        public DisponibilidadMinjusticiaModel() {}

        public DisponibilidadMinjusticiaModel(string mensaje, bool status) {
            Mensaje = mensaje;
            OperacionExitosa = status;
        }
    }  
    public class DisponibilidadModel {

        public long Id { get; set; }

        [JsonProperty("DisponibilidadHora")]
        public string Hora { get; set;}

    }
}