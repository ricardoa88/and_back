using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class HistoricoCitasServiceModel {

        public long Id { get; set;}

        public bool CitaConsultorioEstado { get; set; }

        public long CiudadanoId { get; set; }

        public long DisponibilidadId { get; set; }

        public long TemaConsultaId { get; set; }

        public List<TemaModel> TemasConsultas { get; set; }

        public List<DisponibilidadHistoricoServiceModel> Disponibilidades { get; set; }

    }

    public class DisponibilidadHistoricoServiceModel {

        public DateTime DisponibilidadFecha { get; set; }

        public string DisponibilidadHora {get; set; }

        public List<ConsultorioDetalleModel> Consultorios { get; set; }
    }

    public class HistoricoCitasModel {

        public long Id { get; set; }

        public long IdCiudadano { get; set; }
        
         public bool CitaConsultorioEstado { get; set; }

        public long DisponibilidadId { get; set; }

        public long TemaConsultaId { get; set; }
 
        public DateTime Fecha { get; set; }

        public string Hora { get; set; }

        public string Tema { get; set; }

        public string Consultorio { get; set; }

        public long EstadoCitaDetalleId { get; set; }

        public HistoricoCitasModel() { } 

        public bool ValidateCurrentDate() {
            return this.Fecha > DateTime.Now.Date;
        }
    }
}