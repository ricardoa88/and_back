using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class RegistroUsuarioMinjusticiaModel {
        
        public DatosUsuarioMinjusticiaModel DatosUsuario { get; set; }

        public DatosCiudadanoMinjusticiaModel DatosCiudadano { get; set; }

        public RegistroUsuarioMinjusticiaModel() {}
    }   

    public class DatosUsuarioMinjusticiaModel {

        [Required]
        public string UsuarioPrimerNombre {get; set;}

        public string UsuarioSegundoNombre { get; set; }

        [Required]
        public string UsuarioPrimerApellido { get; set;}

        public string UsuarioSegundoApellido { get; set;}

        public string UsuarioTelefono { get; set; }

        [Required]
        public string UsuarioEmail { get; set; }

        [Required]
        public string UsuarioIdentificacion { get; set; }

        [Required]
        public string Contrasena { get; set; }

        public bool UsuarioEstado { get; set; }

        public DatosUsuarioMinjusticiaModel() {
            this.UsuarioEstado = true;
         }
    }

    public class DatosCiudadanoMinjusticiaModel {
        
        public long UsuarioId { get; set; }

        [Required]
        public string CiudadanoDireccion { get; set; }

        [Required]
        public string CiudadanoDocumentoIdentidad { get; set; }

        public long EstratoId { get; set; }

        public long MunicipioId { get; set; }

        public long TipoDocumentoId { get; set; }

        public long TipoIngresosId { get; set; }

        public bool CiudadanoEstado { get; set; }

        public DatosCiudadanoMinjusticiaModel() {
            this.CiudadanoEstado = true;
        }
    }
}
