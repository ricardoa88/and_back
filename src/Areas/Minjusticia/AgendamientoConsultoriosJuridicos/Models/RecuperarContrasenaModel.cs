using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class SolicitudCambioContrasenaModel {
        public string UsuarioIdentificacion { get; set; }

        public SolicitudCambioContrasenaModel() {}
    }

    public class CambioContrasenaModel {
        [Required]
        public string UsuarioIdentificacion { get; set; }

        [Required]
        public string Contrasena { get; set; }

        [Required]
        public string ContrasenaVerificacion { get; set; }

        [Required]
        public string CodigoRecuperacion { get; set; }

        public CambioContrasenaModel() { }

        public bool VerificarContrasenas() {
            return String.Compare(this.Contrasena, this.ContrasenaVerificacion) == 0;
        }
    }

    public class SolicitudRecuperarContrasenaModel: MinjusticiaBodyResult {

        public SolicitudContrasenaModel Vista { get; set; }

        public SolicitudRecuperarContrasenaModel() {}

        public SolicitudRecuperarContrasenaModel(string mensaje, bool status) {
            Mensaje = mensaje;
            OperacionExitosa = status;
        }
    }

    public class SolicitudContrasenaModel {
        public long? Id { get; set; }

        public string CodigoRecuperacion { get; set; }
    }
}