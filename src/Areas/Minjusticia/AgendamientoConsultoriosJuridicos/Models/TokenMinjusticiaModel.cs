using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using Newtonsoft.Json;
using tramites_servicios_webapi.common.Models;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class TokenMinjusticiaModel : TokenModel {

        [JsonProperty("expires_in")]
        public long ExpireIn { get; set; }

        public string Username { get; set; } 
    }
}