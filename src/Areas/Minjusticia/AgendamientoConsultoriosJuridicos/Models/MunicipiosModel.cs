using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class MunicipiosModel: MinjusticiaBodyResult {
        public List<Municipio> Vistas { get; set; }

        public MunicipiosModel() {}

        public MunicipiosModel(string mensaje, bool status) {
            Mensaje = mensaje;
            OperacionExitosa = status;
        }
    }
}