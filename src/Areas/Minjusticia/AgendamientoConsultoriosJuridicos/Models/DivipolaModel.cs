using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class DivipolaModel: MinjusticiaBodyResult {

        public List<Departamento> Vistas {get; set;}

        public DivipolaModel() {}

        public DivipolaModel(string mensaje, bool status) {
            Mensaje = mensaje;
            OperacionExitosa = status;
        }
    }

    public class Departamento {
        public long Id { get; set; }

        [JsonProperty("DepartamentoNombre")]
        public string Nombre { get; set; }

        [JsonProperty("DepartamentoCodigoDane")]
        public string CodigoDane { get; set; }

        public List<Municipio> Municipios { get; set; }
    }

    public class Municipio {
        public long Id { get; set; }

        [JsonProperty("MunicipioNombre")]
        public string Nombre { get; set; }

        [JsonProperty("MunicipioCodigoDane")]
        public string CodigoDane { get; set; }        

        [JsonProperty("MunicipioLatitud")]
        public double? Latitud { get; set; }

        [JsonProperty("MunicipioLongitud")]
        public double? Longitud { get; set; }
    }
}