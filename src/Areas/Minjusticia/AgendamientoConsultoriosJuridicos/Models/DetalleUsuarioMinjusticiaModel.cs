using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class DetalleUsuarioMinjusticiaModel {
        public long Id { get; set; }

        public string UsuarioIdentificacion { get; set; }

        public string UsuarioPrimerNombre {get; set; }

        public string UsuarioSegundoNombre {get; set; }

        public string UsuarioPrimerApellido {get; set; }

        public string UsuarioSegundoApellido {get; set; }
    }
}