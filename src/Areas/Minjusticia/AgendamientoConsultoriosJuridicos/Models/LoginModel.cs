using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class LoginMinjusticiaModel {
        public string UserName { get; set; }

        public string Password { get; set; }

        public LoginMinjusticiaModel() {}

        public LoginMinjusticiaModel(string user, string password) {
            UserName = user;
            Password = password;
        }
    }
}
