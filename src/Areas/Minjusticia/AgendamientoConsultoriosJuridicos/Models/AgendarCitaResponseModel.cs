using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class AgendarCitaResponseModel: MinjusticiaBodyResult {
        public DetalleCitaResponse Vista { get; set; }

        public AgendarCitaResponseModel() {}

        public AgendarCitaResponseModel(string mensaje, bool status) {
            Mensaje = mensaje;
            OperacionExitosa = status;
        }
    }

    public class DetalleCitaResponse {
        public long Id { get; set; }

        public bool CitaConsultorioEstado { get; set; }
    }
}