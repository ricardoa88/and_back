using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class ConsultoriosModel: MinjusticiaBodyResult {
        public List<ConsultorioDetalleModel> Vistas { get; set; }

        public ConsultoriosModel() {}

        public ConsultoriosModel(string mensaje, bool status) {
            Mensaje = mensaje;
            OperacionExitosa = status;
        }
    }   

    public class ConsultorioDetalleModel {

        public long Id { get; set; }

        [JsonProperty("ConsultorioNombre")]
        public string Nombre { get; set; }
        
        [JsonProperty("ConsultorioDireccion")]
        public string Direccion { get; set; }

        [JsonProperty("ConsultorioTelefono")]
        public string Telefono { get; set; }        

        [JsonProperty("ConsultorioEmail")]
        public string Email { get; set; }

        [JsonProperty("ConsultorioLatidud")]
        public double Latitud { get; set; }

        [JsonProperty("ConsultorioLongitud")]
        public double Longitud { get; set; }
    }
}