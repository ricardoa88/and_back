using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class TemasConsultaModel: MinjusticiaBodyResult {
        public List<TemaModel> Vistas { get; set; }

        public TemasConsultaModel() {}

        public TemasConsultaModel(string mensaje, bool status) {
            Mensaje = mensaje;
            OperacionExitosa = status;
        }
    }

    public class TemaModel {

        public long Id { get; set; }
        
        [JsonProperty("TemaConsultaNombre")]
        public string Nombre { get; set; }
    }
}
