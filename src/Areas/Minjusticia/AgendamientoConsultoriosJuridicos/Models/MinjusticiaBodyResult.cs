using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class MinjusticiaBodyResult {
        public string Mensaje { get; set; }

        public bool OperacionExitosa { get; set; }

        public MinjusticiaBodyResult() { }

        public MinjusticiaBodyResult(bool operacionExitosa, string mensaje) {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
    }
}