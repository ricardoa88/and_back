using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class EstratoModel: MinjusticiaBodyResult  {
        public List<DetalleEstratoModel> Vistas { get; set; }

        public EstratoModel() { }

        public EstratoModel(string mensaje, bool status) {
            Mensaje = mensaje;
            OperacionExitosa = status;
        }
    }

    public class DetalleEstratoModel {

        public long Id { get; set; }

        [JsonProperty("EstratoNombre")]
        public string Nombre { get; set; }
    }
}
