using System.ComponentModel.DataAnnotations;
using System.Net;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models {

    public class ListMinjusticiaModel<T>: MinjusticiaBodyResult {
        
        public List<T> Vistas { get; set; }

        public ListMinjusticiaModel() {}

        public ListMinjusticiaModel(string mensaje, bool status) {
            Mensaje = mensaje;
            OperacionExitosa = status;
        }
    }
}