using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models;
using tramites_servicios_webapi.common.Models;

namespace tramites_servicios_webapi.Mintransporte.Services {

    public class AutenticacionMinjusticiaService {
        
        #region declaración de URLs 
            private static HttpClient client;

            private const string UrlBase = "https://seguridad-identity-dev.azurewebsites.net/";

            private const string UrlAutenticacion = "Token";

        #endregion

        static AutenticacionMinjusticiaService () {
            client = new HttpClient ();
            client.BaseAddress = new Uri (UrlBase);
        }

        /// <summary>
        /// Inicia sesión en minjusticia con las credenciales enviadas
        /// </summary>
        /// <param name="data">Credenciales del usaurio</param>
        /// <returns>Estado del registro</returns>
        public static async Task<TokenMinjusticiaModel> Login(LoginMinjusticiaModel data)
        {
            var tokenModel = new TokenMinjusticiaModel();
            //Content
            HttpContent content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", data.UserName),
                new KeyValuePair<string, string>("password", data.Password),
            });
            client.DefaultRequestHeaders.Accept.Clear();
            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

            try {
                HttpResponseMessage resp = await client.PostAsync(UrlAutenticacion, content);
                Console.WriteLine(JsonConvert.SerializeObject(resp));
                if (resp.IsSuccessStatusCode) {
                    var resultado = resp.Content.ReadAsStringAsync ().Result;
                    Console.WriteLine(resp.Content.ReadAsStringAsync ().Result);
                    tokenModel = JsonConvert.DeserializeObject<TokenMinjusticiaModel> (resultado);
                    tokenModel.State = true;
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }

            return tokenModel;
        }
    
    }

}