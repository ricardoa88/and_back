using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models;
using tramites_servicios_webapi.common.Models;

namespace tramites_servicios_webapi.Mintransporte.Services {

    public class AgendamientoConsultorioService {
        
        #region declaración de URLs 
            private static HttpClient client;

            private static string userToken = "5555555555";

            private static string passwordToken = "Clave123+.";

            private const string UrlBase = "https://seguridad-identity-dev.azurewebsites.net/";

            private const string UrlAutenticacion = "Token";

            private const string UrlDivipola = "api/administracion/ObtenerDepartamento";

            private const string UrlMunicipiosWithConsultorios = "api/administracion/ObtenerMunicipio";

            private const string UrlConsultorios = "api/agendamiento/ObtenerConsultorio";

            private const string UrlEstratos = "api/administracion/ObtenerEstrato";

            private const string UrlTemas = "api/agendamiento/ObtenerTemaConsulta";

            private const string UrlTiposDocumento = "api/administracion/ObtenerTipoDocumento";

            private const string UrlTiposIngreso = "api/administracion/ObtenerTipoIngresos";

            private const string UrlRegistroUsuario = "api/Usuario/RegistrarUsuario";

            private const string UrlRegistroCiudadano = "api/Agendamiento/CrearCiudadano";

            private const string UrlDisponibilidadCita = "api/Agendamiento/ObtenerDisponibilidad";

            private const string UrlDetalleUsuario = "api/seguridad/ObtenerUsuario";

            private const string UrlDetalleCiudadano = "api/Agendamiento/ObtenerCiudadano";

            private const string UrlRegistroCita = "api/Agendamiento/CrearCitaConsultorio";

            private const string UrlHistoricoCitas = "api/Agendamiento/ObtenerCitaConsultorio";

            private const string UrlSolicitudRecuperarContrasena = "api/Usuario/RecuperarContrasena";

            private const string UrlRecuperarContrasena = "api/Usuario/RestablecerContrasena";

            private const string UrlCancelarCita = "api/Agendamiento/ActualizarCitaConsultorio";
        #endregion

        static AgendamientoConsultorioService () {
            client = new HttpClient ();
            client.BaseAddress = new Uri (UrlBase);
        }

        #region Autenticación y login
            /// <summary>
            /// Inicia sesión en minjusticia con las credenciales enviadas
            /// </summary>
            /// <param name="data">Credenciales del usaurio</param>
            /// <returns>Estado del registro</returns>
            public static async Task<TokenMinjusticiaModel> Login(LoginMinjusticiaModel data)        
            {
                var tokenModel = new TokenMinjusticiaModel();
                //Content
                HttpContent content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", data.UserName),
                    new KeyValuePair<string, string>("password", data.Password),
                });
                client.DefaultRequestHeaders.Accept.Clear();
                content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");

                try {
                    HttpResponseMessage resp = await client.PostAsync(UrlAutenticacion, content);                
                    if (resp.IsSuccessStatusCode) {
                        var resultado = resp.Content.ReadAsStringAsync ().Result;
                        tokenModel = JsonConvert.DeserializeObject<TokenMinjusticiaModel> (resultado);
                        tokenModel.State = true;
                    }

                } catch (Exception e) {
                    //TODO: manejador de excepciones y mongo
                    tokenModel.Message = e.Message;
                    Console.ReadLine ();
                }

                return tokenModel;
            }
        #endregion Autenticación y login
        
        #region consulta de paramétricas 
            /// <summary> 
            /// Obtiene el listado de departamentos con sus respectivos municipios
            /// </summary>
            /// <returns>Listado de departamentos y municipios</returns>
            public static async Task<DivipolaModel> GetDivipola()
            {
                DivipolaModel result = new DivipolaModel();

                //Obtiene el token actual
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    result.Mensaje = tokenModel.Message;
                    return result;                
                }

                //Construye el encabezado
                Dictionary<string, string> content = new Dictionary<string, string> ();
                content.Add ("Id", "0");
                string output = JsonConvert.SerializeObject (content);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

                //Realiza la consulta            
                StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");
                var request = new HttpRequestMessage(new HttpMethod("GET"), UrlDivipola);
                request.Content = stringContent;
                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    if (resp.IsSuccessStatusCode)
                    {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<DivipolaModel>(response);
                    }
                    else {
                        result = new DivipolaModel("Se presentó un error al consultar los consultorios", false);
                    }
                }
                catch (Exception e)
                {
                    result = new DivipolaModel(e.Message, false);
                    //TODO: manejador de excepciones y mongo
                    Console.WriteLine(e.Message);
                }
                return result;
            }

                        /// <summary>
            /// Obtiene el listado de departamentos con sus respectivos municipios
            /// </summary>
            /// <returns>Listado de departamentos y municipios</returns>
            public static async Task<MunicipiosModel> GetMunicipiosWithConsultorios()
            {
                MunicipiosModel result = new MunicipiosModel();

                //Obtiene el token actual
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    result.Mensaje = tokenModel.Message;
                    return result;                
                }

                //Construye el encabezado
                Dictionary<string, string> content = new Dictionary<string, string> ();
                content.Add ("ConConsultorio", "true");
                string output = JsonConvert.SerializeObject (content);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

                //Realiza la consulta            
                StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");
                var request = new HttpRequestMessage(new HttpMethod("GET"), UrlMunicipiosWithConsultorios);
                request.Content = stringContent;
                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    if (resp.IsSuccessStatusCode)
                    {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<MunicipiosModel>(response);
                    }
                    else {
                        result = new MunicipiosModel("Se presentó un error al consultar los consultorios", false);
                    }
                }
                catch (Exception e)
                {
                    result = new MunicipiosModel(e.Message, false);
                    //TODO: manejador de excepciones y mongo
                    Console.WriteLine(e.Message);
                }
                return result;
            }

            /// <summary>
            /// Obtiene el listado de consultorios disponibles por municipio
            /// </summary>
            /// <returns>Listado de consultorios</returns>
            public static async Task<ConsultoriosModel> GetListConsultoriosByMunicipio(long idMunicipio)
            {
                ConsultoriosModel result = new ConsultoriosModel();

                //Obtiene el token actual
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    result.Mensaje = tokenModel.Message;
                    return result;                
                }

                //Construye el encabezado
                Dictionary<string, string> content = new Dictionary<string, string> ();
                content.Add ("MunicipioId", Convert.ToString(idMunicipio));
                string output = JsonConvert.SerializeObject (content);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

                //Realiza la consulta            
                StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");
                var request = new HttpRequestMessage(new HttpMethod("GET"), UrlConsultorios);
                request.Content = stringContent;
                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<ConsultoriosModel>(response);
                    }
                    else {
                        result = new ConsultoriosModel("Se presentó un error al consultar los consultorios", false);
                    }
                }
                catch (Exception e) {
                    result = new ConsultoriosModel(e.Message, false);
                }
                
                return result;
            }

            /// <summary>
            /// Obtiene el listado de temas
            /// </summary>
            /// <returns>Listado de temas</returns>
            public static async Task<TemasConsultaModel> GetTemas()
            {
                TemasConsultaModel result = new TemasConsultaModel();

                //Obtiene el token actual
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    result.Mensaje = tokenModel.Message;
                    return result;                
                }

                //Construye el encabezado
                Dictionary<string, string> content = new Dictionary<string, string> ();
                content.Add ("id", "0");
                string output = JsonConvert.SerializeObject (content);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );

                //Realiza la consulta            
                StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");
                var request = new HttpRequestMessage(new HttpMethod("GET"), UrlTemas);
                //Construye el encabezado
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

                //Construye el body
                request.Content = stringContent;

                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<TemasConsultaModel>(response);
                    }
                    else {
                        result = new TemasConsultaModel("Se presentó un error al consultar los consultorios", false);
                    }
                }
                catch (Exception e) {
                    result = new TemasConsultaModel(e.Message, false);
                }

                return result;
            }
        
            /// <summary>
            /// Obtiene el listado de Estratos
            /// </summary>
            /// <returns>Listado de Estratos</returns>
            public static async Task<EstratoModel> GetEstratos() {
                EstratoModel result = new EstratoModel();

                //Obtiene el token actual
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    result.Mensaje = tokenModel.Message;
                    return result;                
                }

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );

                //Realiza la consulta            
                var request = new HttpRequestMessage(new HttpMethod("GET"), UrlEstratos);
                //Construye el encabezado
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

                StringContent stringContent = new StringContent ("{}", Encoding.UTF8, "application/json");
                request.Content = stringContent;

                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    Console.WriteLine(JsonConvert.SerializeObject(resp));
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<EstratoModel>(response);
                    }
                    else {
                        result = new EstratoModel("Se presentó un error al consultar los consultorios", false);
                    }
                }
                catch (Exception e) {
                    result = new EstratoModel(e.Message, false);
                }

                return result;
            }

            /// <summary>
            /// /// Obtiene el listado de temas
            /// </summary>
            /// <returns>Listado de temas</returns>
            public static async Task<TipoDocumentoMinjusticiaModel> GetTiposDocumento() {
                TipoDocumentoMinjusticiaModel result = new TipoDocumentoMinjusticiaModel();

                //Obtiene el token actual
                var tokenModel = await  Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    result.Mensaje = tokenModel.Message;
                    return result;                
                }

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );

                //Realiza la consulta            
                var request = new HttpRequestMessage(new HttpMethod("GET"), UrlTiposDocumento);
                //Construye el encabezado
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

                StringContent stringContent = new StringContent ("{}", Encoding.UTF8, "application/json");
                request.Content = stringContent;

                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    Console.WriteLine(JsonConvert.SerializeObject(resp));
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<TipoDocumentoMinjusticiaModel>(response);
                    }
                    else {
                        result = new TipoDocumentoMinjusticiaModel("Se presentó un error al consultar los consultorios", false);
                    }
                }
                catch (Exception e) {
                    result = new TipoDocumentoMinjusticiaModel(e.Message, false);
                }

                return result;
            }

            /// <summary>
            /// /// Obtiene el listado de tipos de ingreso
            /// </summary>
            /// <returns>Listado de tipos de ingreso</returns>
            public static async Task<TipoIngresoMinjusticiaModel> GetTiposIngreso() {
                TipoIngresoMinjusticiaModel result = new TipoIngresoMinjusticiaModel();

                //Obtiene el token actual
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    result.Mensaje = tokenModel.Message;
                    return result;                
                }

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );

                //Realiza la consulta            
                var request = new HttpRequestMessage(new HttpMethod("GET"), UrlTiposIngreso);
                //Construye el encabezado
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

                StringContent stringContent = new StringContent ("{}", Encoding.UTF8, "application/json");
                request.Content = stringContent;

                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    Console.WriteLine(JsonConvert.SerializeObject(resp));
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<TipoIngresoMinjusticiaModel>(response);
                    }
                    else {
                        result = new TipoIngresoMinjusticiaModel("Se presentó un error al consultar los consultorios", false);
                    }
                }
                catch (Exception e) {
                    result = new TipoIngresoMinjusticiaModel(e.Message, false);
                }

                return result;
            }
    
        #endregion consulta de paramétricas

        #region Datos de cita 
            /// <summary>
            /// Obtiene el listado de consultorios disponibles por municipio
            /// </summary>
            /// <returns>Listado de consultorios</returns>
            public static async Task<DisponibilidadMinjusticiaModel> GetListDisponibilidadConsultorio(long idConsultorio, string fecha)
            {
                DisponibilidadMinjusticiaModel result = new DisponibilidadMinjusticiaModel();

                //Obtiene el token actual
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    result.Mensaje = tokenModel.Message;
                    return result;                
                }

                //Construye el encabezado
                Dictionary<string, string> content = new Dictionary<string, string> ();
                content.Add ("DisponibilidadFecha", fecha);
                content.Add ("ConsultorioId", Convert.ToString(idConsultorio));
                string output = JsonConvert.SerializeObject (content);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

                //Realiza la consulta            
                StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");
                var request = new HttpRequestMessage(new HttpMethod("GET"), UrlDisponibilidadCita);
                request.Content = stringContent;
                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<DisponibilidadMinjusticiaModel>(response);
                    }
                    else {
                        result = new DisponibilidadMinjusticiaModel("Se presentó un error al consultar la disponibilidad del consultorio", false);
                    }
                }
                catch (Exception e) {
                    result = new DisponibilidadMinjusticiaModel(e.Message, false);
                }
                
                return result;
            }
        #endregion Datos de cita

        #region Registro de usuarios 
            /// <summary>
            /// Realiza los registro de usuario y ciudadano para poder acceder a los servicios de minjusticia
            /// </summary>
            /// <param name="data">Datos del ciudadano y del usuario para registrar</param>
            /// <returns>Estado del registro</returns>
            public static async Task<MinjusticiaBodyResult> RegistrarUsuarioMinjusticia(RegistroUsuarioMinjusticiaModel data)        
            {                
                //Obtiene el token actual
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    return new MinjusticiaBodyResult(false, tokenModel.Message);
                }

                var datosUsuario = await RegistrarUsuario(data.DatosUsuario, tokenModel.AccessToken);  
                if (!datosUsuario.OperacionExitosa) {
                    return new MinjusticiaBodyResult(false, datosUsuario.Mensaje);
                }

                data.DatosCiudadano.UsuarioId = datosUsuario.Vista.Id;
                var registroCuidadano = await RegistrarCiudadano(data.DatosCiudadano, tokenModel.AccessToken);
                if (!registroCuidadano.OperacionExitosa) {
                    return new MinjusticiaBodyResult(false, registroCuidadano.Mensaje);
                }

                return new MinjusticiaBodyResult(true, registroCuidadano.Mensaje);

            }

            /// <summary>
            /// Registra el usuario en minjusticia a partir de los datos diligenciados
            /// </summary>
            /// <param name="data">Datos del usuario</param>
            /// <returns>Estado del registro</returns>
            public static async Task<RegistroUsuarioRespuestaModel> RegistrarUsuario(DatosUsuarioMinjusticiaModel data, string token)        
            {
                RegistroUsuarioRespuestaModel result = new RegistroUsuarioRespuestaModel();

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                //Construye el cuerpo del mensaje
                var request = new HttpRequestMessage(new HttpMethod("POST"), UrlRegistroUsuario);
                request.Content = new StringContent (JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<RegistroUsuarioRespuestaModel>(response);
                    }
                    else {
                        result = new RegistroUsuarioRespuestaModel("Se presentó un error al registrar el usuario", false);
                    }
                }
                catch (Exception e) {
                    result = new RegistroUsuarioRespuestaModel(e.Message, false);
                }

                return result;            
            }

            /// <summary>
            /// Realiza el registro de ciudadano a partir del usuario creado anteriormente
            /// </summary>
            /// <param name="data">Datos del ciudadano</param>
            /// <returns>Estado del registro</returns>
            public static async Task<RegistroUsuarioRespuestaModel> RegistrarCiudadano(DatosCiudadanoMinjusticiaModel data, string token)        
            {
                RegistroUsuarioRespuestaModel result = new RegistroUsuarioRespuestaModel();

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                //Construye el cuerpo del mensaje
                var request = new HttpRequestMessage(new HttpMethod("POST"), UrlRegistroCiudadano);
                request.Content = new StringContent (JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<RegistroUsuarioRespuestaModel>(response);
                    }
                    else {
                        result = new RegistroUsuarioRespuestaModel("Se presentó un error al registrar el ciudadano", false);
                    }
                }
                catch (Exception e) {
                    result = new RegistroUsuarioRespuestaModel(e.Message, false);
                }

                return result;    
            }
        
        #endregion Registro de usuarios

        #region Agendamiento de cita
            /// <summary>
            /// Realiza el registro de la cita consultando el id del ciudadano
            /// </summary>
            /// <param name="data">Datos del ciudadano y del usuario para registrar</param>
            /// <returns>Estado del registro</returns>
            public static async Task<AgendarCitaResponseModel> SaveCita(AgendarCitaModel data, string token)        
            { 
                //Obtiene el token para las consultas genéricas
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    return new AgendarCitaResponseModel(tokenModel.Message, false);
                }

                //Llamar consulta de usuario
                var detalleUsuario = await GetUsuarioByUsername(data.Username, tokenModel.AccessToken);
                if (!detalleUsuario.OperacionExitosa) {
                    return new AgendarCitaResponseModel(detalleUsuario.Mensaje, detalleUsuario.OperacionExitosa );
                }

                //llamar detalle de ciudadano
                var detalleCiudadano = await GetCiudadanoByUsuario(detalleUsuario.Vistas[0].Id, tokenModel.AccessToken);
                if (!detalleCiudadano.OperacionExitosa) {
                    return new AgendarCitaResponseModel(detalleCiudadano.Mensaje, detalleCiudadano.OperacionExitosa);
                }

                //llamar guardado de la cita
                data.CiudadanoId = detalleCiudadano.Vistas[0].Id;
                var registroCita = await GuardarCita(data, token);
                if(!registroCita.OperacionExitosa){
                    return new AgendarCitaResponseModel(registroCita.Mensaje, registroCita.OperacionExitosa);
                }

                return registroCita;
            }
            
            /// <summary>
            /// Realiza el registro de la cita consultando el id del ciudadano
            /// </summary>
            /// <param name="data">Datos del ciudadano y del usuario para registrar</param>
            /// <returns>Estado del registro</returns>
            private static async Task<AgendarCitaResponseModel> GuardarCita(AgendarCitaModel data, string token)        
            { 
                var result = new AgendarCitaResponseModel();

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                //Construye el cuerpo del mensaje
                var request = new HttpRequestMessage(new HttpMethod("POST"), UrlRegistroCita);
                request.Content = new StringContent (JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

                try {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    Console.WriteLine(JsonConvert.SerializeObject(resp));
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<AgendarCitaResponseModel>(response);
                        Console.WriteLine(JsonConvert.SerializeObject(result));
                    }
                    else {
                        result = new AgendarCitaResponseModel("Se presentó un error al registrar el usuario", false);
                    }
                }
                catch (Exception e) {
                    result = new AgendarCitaResponseModel(e.Message, false);
                }

                return result;      
            }

        #endregion Agendamiento de cita 

        #region Datos del usuario
            /// <summary>
            /// Realiza el registro de la cita consultando el id del ciudadano
            /// </summary>
            /// <param name="data">Datos del ciudadano y del usuario para registrar</param>
            /// <returns>Estado del registro</returns>
            public static async Task<ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>> GetUserInformation(string username)        
            { 
                //Obtiene el token para las consultas genéricas
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    return new ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>(tokenModel.Message, false);
                }

                //Llamar consulta de usuario
                return await GetUsuarioByUsername(username, tokenModel.AccessToken);
            }

            /// <summary>
            /// Realiza el registro de la cita consultando el id del ciudadano
            /// </summary>
            /// <param name="data">Datos del ciudadano y del usuario para registrar</param>
            /// <returns>Estado del registro</returns>
            private static async Task<ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>> GetUsuarioByUsername(string userName, string token)        
            {   
                var result = new ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>();

                //Construye el encabezado
                Dictionary<string, string> content = new Dictionary<string, string> ();
                content.Add ("UsuarioIdentificacion", userName);
                string output = JsonConvert.SerializeObject (content);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                //Realiza la consulta            
                StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");
                var request = new HttpRequestMessage(new HttpMethod("GET"), UrlDetalleUsuario);
                request.Content = stringContent;
                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>>(response);
                        if (result.Vistas == null || result.Vistas.Count == 0){
                            result = new ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>("No se encontro información del usuario", false);
                        }
                    }
                    else {
                        result = new ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>("Se presentó un error al consultar la información del usuario", false);
                    }
                }
                catch (Exception e) {
                    result = new ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>(e.Message, false);
                }
                
                return result;
            }

            /// <summary>
            /// Realiza el registro de la cita consultando el id del ciudadano
            /// </summary>
            /// <param name="data">Datos del ciudadano y del usuario para registrar</param>
            /// <returns>Estado del registro</returns>
            private static async Task<ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>> GetCiudadanoByUsuario (long usuarioId, string token)        
            { 
                var result = new ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>();

                //Construye el encabezado
                Dictionary<string, string> content = new Dictionary<string, string> ();
                content.Add ("UsuarioId", Convert.ToString(usuarioId));
                string output = JsonConvert.SerializeObject (content);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                //Realiza la consulta            
                StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");
                var request = new HttpRequestMessage(new HttpMethod("GET"), UrlDetalleCiudadano);
                request.Content = stringContent;
                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);                    
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;                        
                        result = JsonConvert.DeserializeObject<ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>>(response);
                        if (result.Vistas == null || result.Vistas.Count == 0){
                            result = new ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>("No se encontro información del ciudadano", false);
                        }
                    }
                    else {
                        result = new ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>("Se presentó un error al consultar la información del ciudadano", false);
                    }
                }
                catch (Exception e) {
                    result = new ListMinjusticiaModel<DetalleUsuarioMinjusticiaModel>(e.Message, false);
                }
                
                return result;
            }
        #endregion Datos del usuario
    
        #region Historico de citas 
            /// <summary>
            /// Obtiene los datos de ciudadano y consulta el historico de citas
            /// </summary>
            /// <param name="data">Historico de citas</param>
            /// <returns>Estado del registro</returns>
            public static async Task<ListMinjusticiaModel<HistoricoCitasServiceModel>> GetHistoricoCitasCiudadano(string username)        
            { 
                //Obtiene el token para las consultas genéricas
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    return new ListMinjusticiaModel<HistoricoCitasServiceModel>(tokenModel.Message, false);
                }

                //Llamar consulta de usuario
                var detalleUsuario = await GetUsuarioByUsername(username, tokenModel.AccessToken);
                if (!detalleUsuario.OperacionExitosa) {
                    return new ListMinjusticiaModel<HistoricoCitasServiceModel>(detalleUsuario.Mensaje, detalleUsuario.OperacionExitosa );
                }

                //llamar detalle de ciudadano
                var detalleCiudadano = await GetCiudadanoByUsuario(detalleUsuario.Vistas[0].Id, tokenModel.AccessToken);
                if (!detalleCiudadano.OperacionExitosa) {
                    return new ListMinjusticiaModel<HistoricoCitasServiceModel>(detalleCiudadano.Mensaje, detalleCiudadano.OperacionExitosa);
                }

                return await GetListHistoricoCitasCiudadano(tokenModel.AccessToken, detalleCiudadano.Vistas[0].Id);
            }

            /// <summary>
            /// Realiza la consulta del historico de citas por id de ciudadano
            /// </summary>
            /// <param name="data">Historico de citas</param>
            /// <returns>Estado del registro</returns>
            private static async Task<ListMinjusticiaModel<HistoricoCitasServiceModel> > GetListHistoricoCitasCiudadano(string token, long idCiudadano)        
            { 
                ListMinjusticiaModel<HistoricoCitasServiceModel> result = new ListMinjusticiaModel<HistoricoCitasServiceModel>();

                //Construye el encabezado
                Dictionary<string, string> content = new Dictionary<string, string> ();
                content.Add ("CiudadanoId", Convert.ToString(idCiudadano));
                string output = JsonConvert.SerializeObject (content);

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                //Realiza la consulta            
                StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");
                var request = new HttpRequestMessage(new HttpMethod("GET"), UrlHistoricoCitas);
                request.Content = stringContent;
                try
                {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    Console.WriteLine(JsonConvert.SerializeObject(resp));
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<ListMinjusticiaModel<HistoricoCitasServiceModel>>(response);
                    }
                    else {
                        result = new ListMinjusticiaModel<HistoricoCitasServiceModel>("Se presentó un error al consultar el historico", false);
                    }
                }
                catch (Exception e) {
                    result = new ListMinjusticiaModel<HistoricoCitasServiceModel>(e.Message, false);
                }
                
                return result;
            }

            
        #endregion Historico de citas
    
        #region Recuperación de contraseña
            /// <summary>
            /// Realiza la solicitud de cambio de contreña
            /// </summary>
            /// <param name="data">Datos del ciudadano para la solicitud</param>
            /// <returns>Estado del registro</returns>
            public static async Task<SolicitudRecuperarContrasenaModel> SolicitarCambioContrasena(SolicitudCambioContrasenaModel data)        
            { 
                var result = new SolicitudRecuperarContrasenaModel();

                //Obtiene el token actual
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    return new SolicitudRecuperarContrasenaModel(tokenModel.Message, false);
                }

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

                //Construye el cuerpo del mensaje
                var request = new HttpRequestMessage(new HttpMethod("POST"), UrlSolicitudRecuperarContrasena);
                request.Content = new StringContent (JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

                try {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    Console.WriteLine(JsonConvert.SerializeObject(resp));
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<SolicitudRecuperarContrasenaModel>(response);
                        Console.WriteLine(JsonConvert.SerializeObject(result));
                    }
                    else {
                        result = new SolicitudRecuperarContrasenaModel("Se presentó un error al registrar el usuario", false);
                    }
                }
                catch (Exception e) {
                    result = new SolicitudRecuperarContrasenaModel(e.Message, false);
                }

                return result;      
            }

            // <summary>
            /// Realiza la solicitud de cambio de contreña
            /// </summary>
            /// <param name="data">Datos del ciudadano para la solicitud</param>
            /// <returns>Estado del registro</returns>
            public static async Task<SolicitudRecuperarContrasenaModel> CambioContrasena(CambioContrasenaModel data)        
            { 
                var result = new SolicitudRecuperarContrasenaModel();

                //Obtiene el token actual
                var tokenModel = await Login(new LoginMinjusticiaModel(userToken, passwordToken));
                if (!tokenModel.State){
                    return new SolicitudRecuperarContrasenaModel(tokenModel.Message, false);
                }

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

                //Construye el cuerpo del mensaje
                var request = new HttpRequestMessage(new HttpMethod("POST"), UrlRecuperarContrasena);
                request.Content = new StringContent (JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

                try {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    Console.WriteLine(JsonConvert.SerializeObject(resp));
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<SolicitudRecuperarContrasenaModel>(response);
                        Console.WriteLine(JsonConvert.SerializeObject(result));
                    }
                    else {
                        result = new SolicitudRecuperarContrasenaModel("Se presentó un error al registrar el usuario", false);
                    }
                }
                catch (Exception e) {
                    result = new SolicitudRecuperarContrasenaModel(e.Message, false);
                }

                return result;      
            }
        #endregion Recuperación de contraseña
    
        #region Cancelar cita 
            /// <summary>
            /// Realiza el registro de la cita consultando el id del ciudadano
            /// </summary>
            /// <param name="data">Datos del ciudadano y del usuario para registrar</param>
            /// <returns>Estado del registro</returns>
            public static async Task<AgendarCitaResponseModel> CancelarCita(HistoricoCitasModel data, string token)        
            { 
                var result = new AgendarCitaResponseModel();

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json")
                );
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                //Construye el cuerpo del mensaje
                var request = new HttpRequestMessage(new HttpMethod("PUT"), UrlCancelarCita);
                request.Content = new StringContent (JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

                try {
                    HttpResponseMessage resp = await client.SendAsync(request);
                    Console.WriteLine(JsonConvert.SerializeObject(resp));
                    if (resp.IsSuccessStatusCode) {
                        string response = resp.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<AgendarCitaResponseModel>(response);
                        Console.WriteLine(JsonConvert.SerializeObject(result));
                    }
                    else {
                        result = new AgendarCitaResponseModel("Se presentó un error al cancelar la cita", false);
                    }
                }
                catch (Exception e) {
                    result = new AgendarCitaResponseModel(e.Message, false);
                }

                return result;      
            }
        #endregion Cancelar cita
    }
}