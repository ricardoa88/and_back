using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Mintransporte.Services;
using tramites_servicios_webapi.common.Models;
using tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models;
using System.Linq;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Controllers
{
    [Route ("api/[controller]")]
    [ApiController]
    public class AgendamientoConsultoriosController : ControllerBase
    {        
        #region Consulta de paramétricos
            /// <summary>
            /// Obtiene el listado de departamento y cuidad disponibles del tramite
            /// </summary>
            /// <returns>Listado de entidades</returns>
            [HttpGet("ObtenerDivipola")]        
            public async Task<IActionResult>  ObtenerDivipola() {
                var result = new ResponseModel();

                DivipolaModel listadoDivipola = await AgendamientoConsultorioService.GetDivipola();
                
                if(listadoDivipola.OperacionExitosa) {
                    result.Success = true;
                    result.Result = listadoDivipola.Vistas;
                }

                result.Message = listadoDivipola.Mensaje;
                return new JsonResult(result);            
            }

            /// <summary>
            /// Obtiene el listado de departamento y cuidad disponibles del tramite
            /// </summary>
            /// <returns>Listado de entidades</returns>
            [HttpGet("ObtenerMunicipiosConConsultorios")]        
            public async Task<IActionResult>  ObtenerMunicipiosConConsultorios() {
                var result = new ResponseModel();

                MunicipiosModel listadoMunicipio = await AgendamientoConsultorioService.GetMunicipiosWithConsultorios();
                
                if(listadoMunicipio.OperacionExitosa) {
                    result.Success = true;
                    result.Result = listadoMunicipio.Vistas;
                }

                result.Message = listadoMunicipio.Mensaje;
                return new JsonResult(result);            
            }            

            /// <summary>
            /// Obtiene el listado de consultorios por municipio
            /// </summary>
            /// <returns>Listado de consultorios</returns>
            [HttpGet("ObtenerConsultorios")]        
            public async Task<IActionResult>  ObtenerConsultorios(long idMunicipio) {
                var result = new ResponseModel();

                ConsultoriosModel listadoConsultorios = await AgendamientoConsultorioService.GetListConsultoriosByMunicipio(idMunicipio);
                
                if(listadoConsultorios.OperacionExitosa) {
                    result.Success = true;
                    result.Result = listadoConsultorios.Vistas;
                }

                result.Message = listadoConsultorios.Mensaje;
                return new JsonResult(result);            
            }

            /// <summary>
            /// Obtiene el listado de temas para agendar una cita
            /// </summary>
            /// <returns>Listado de temas</returns>
            [HttpGet("ObtenerTemas")]        
            public async Task<IActionResult>  ObtenerTemas() {
                var result = new ResponseModel();

                TemasConsultaModel listadoTemas = await AgendamientoConsultorioService.GetTemas();
                
                if(listadoTemas.OperacionExitosa) {
                    result.Success = true;
                    result.Result = listadoTemas.Vistas.Select(item => new SelectListItem {
                        Value = Convert.ToString(item.Id),
                        Text = item.Nombre
                    }).ToList();
                }

                result.Message = listadoTemas.Mensaje;
                return new JsonResult(result);
            }

            /// <summary>
            /// Obtiene el listado de estratos para realizar el registro de personas
            /// </summary>
            /// <returns>Listado de estratos</returns>
            [HttpGet("ObtenerEstratos")]        
            public async Task<IActionResult>  ObtenerEstratos() {
                var result = new ResponseModel();

                EstratoModel listadoEstratos = await AgendamientoConsultorioService.GetEstratos();
                
                if(listadoEstratos.OperacionExitosa) {
                    result.Success = true;
                    result.Result = listadoEstratos.Vistas.Select(item => new SelectListItem {
                        Value = Convert.ToString(item.Id),
                        Text = item.Nombre
                    }).ToList();
                }

                result.Message = listadoEstratos.Mensaje;
                return new JsonResult(result);            
            }

            /// <summary>
            /// Obtiene el listado de estratos para realizar el registro de personas
            /// </summary>
            /// <returns>Listado de estratos</returns>
            [HttpGet("ObtenerTiposDocumento")]        
            public async Task<IActionResult>  ObtenerTiposDocumento() {
                var result = new ResponseModel();

                TipoDocumentoMinjusticiaModel listadoDocumentos = await AgendamientoConsultorioService.GetTiposDocumento();
                
                if(listadoDocumentos.OperacionExitosa) {
                    result.Success = true;
                    result.Result = listadoDocumentos.Vistas.Select(item => new SelectListItem {
                        Value = Convert.ToString(item.Id),
                        Text = item.Nombre
                    }).ToList();
                }

                result.Message = listadoDocumentos.Mensaje;
                return new JsonResult(result);
            }

            /// <summary>
            /// Obtiene el listado de estratos para realizar el registro de personas
            /// </summary>
            /// <returns>Listado de estratos</returns>
            [HttpGet("ObtenerTiposIngreso")]        
            public async Task<IActionResult>  ObtenerTiposIngreso() {
                var result = new ResponseModel();

                TipoIngresoMinjusticiaModel listadoIngresos = await AgendamientoConsultorioService.GetTiposIngreso();
                
                if(listadoIngresos.OperacionExitosa) {
                    result.Success = true;
                    result.Result = listadoIngresos.Vistas.Select(item => new SelectListItem {
                        Value = Convert.ToString(item.Id),
                        Text = item.Nombre
                    }).ToList();
                }

                result.Message = listadoIngresos.Mensaje;
                return new JsonResult(result);
            }    
        #endregion Consulta de paramétricos

        #region Datos de cita 
            /// <summary>
            /// Obtiene la disponibilidad de un consultorio
            /// </summary>
            /// <returns>Listado de estratos</returns>
            [HttpPost("ObtenerDisponibilidadConsultorio")]        
            public async Task<IActionResult>  ObtenerDisponibilidadConsultorio([FromBody] ConsultaDisponibilidadRequest model) {
                var result = new ResponseModel();

                DisponibilidadMinjusticiaModel listado = await AgendamientoConsultorioService.GetListDisponibilidadConsultorio(model.IdConsultorio, model.Fecha);
                
                if(listado.OperacionExitosa) {
                    result.Success = true;
                    result.Result = listado.Vistas;
                }

                result.Message = listado.Mensaje;
                return new JsonResult(result);
            } 
        #endregion Datos de cita

        #region Agendar y cancelar cita e información de citas
            // <summary>
            /// Obtiene el listado de estratos para realizar el registro de personas
            /// </summary>
            /// <returns>Listado de estratos</returns>
            [HttpPost("AgendarCita")]        
            public async Task<IActionResult>  AgendarCita([FromBody] AgendarCitaModel model) {
                var result = new ResponseModel();

                var token = Convert.ToString(Request.Headers["Token"]);

                if(!String.IsNullOrEmpty(token)){
                    AgendarCitaResponseModel cita = await AgendamientoConsultorioService.SaveCita(model, token);
                    
                    if(cita.OperacionExitosa) {
                        result.Success = true;
                        result.Result = cita.Vista.Id;
                    }

                    result.Message = cita.Mensaje;
                }
                return new JsonResult(result);
            } 

            /// <summary>
            /// Obtiene el historico de citas por usuario
            /// </summary>
            /// <returns>Listado de estratos</returns>
            [HttpGet("ObtenerHistoricoCitas")]        
            public async Task<IActionResult>  ObtenerHistoricoCitas(string username) {
                var result = new ResponseModel();

                ListMinjusticiaModel<HistoricoCitasServiceModel> listado = await AgendamientoConsultorioService.GetHistoricoCitasCiudadano(username);
                
                if(listado.OperacionExitosa) {
                    result.Success = true;
                    result.Result = listado.Vistas.Select(item => new HistoricoCitasModel {
                        Id = item.Id,
                        CitaConsultorioEstado = item.CitaConsultorioEstado,
                        DisponibilidadId = item.DisponibilidadId,
                        TemaConsultaId = item.TemaConsultaId,
                        Fecha = item.Disponibilidades[0].DisponibilidadFecha,
                        Hora = item.Disponibilidades[0].DisponibilidadHora,
                        Tema = item.TemasConsultas[0].Nombre,
                        Consultorio = item.Disponibilidades[0].Consultorios[0].Nombre,
                        IdCiudadano = item.CiudadanoId
                    }).ToList().OrderBy(m => m.Fecha);
                }

                result.Message = listado.Mensaje;
                return new JsonResult(result);
            }   

            // <summary>
            /// Cancela una cita
            /// </summary>
            /// <returns>Estado del proceso</returns>
            [HttpPost("CancelarCita")]        
            public async Task<IActionResult>  CancelarCita([FromBody] HistoricoCitasModel model) {
                var result = new ResponseModel();

                var token = Convert.ToString(Request.Headers["Token"]);

                if (!model.ValidateCurrentDate()) {
                    result.Message = "La cita no puede ser cancelada";
                    return new JsonResult(result); 
                }

                if(String.IsNullOrEmpty(token)){ 
                    return new JsonResult(result);                    
                }

                model.EstadoCitaDetalleId = 2;
                AgendarCitaResponseModel cita = await AgendamientoConsultorioService.CancelarCita(model, token);
                if(cita.OperacionExitosa) {
                    result.Success = true;
                }

                result.Message = cita.Mensaje;
                return new JsonResult(result); 
            } 

        #endregion Agendar y cancelar cita e información de citas
    }
}