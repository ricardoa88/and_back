using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Mintransporte.Services;
using tramites_servicios_webapi.common.Models;
using tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models;
using System.Linq;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Controllers
{
    [Route ("api/[controller]")]
    [ApiController]
    public class AutenticacionMinjusticiaController : ControllerBase
    {        
        /// <summary>
        /// Obtiene el listado de entidades disponibles del tramite
        /// </summary>
        /// <returns>Listado de entidades</returns>
        [HttpPost("Login")]        
        public async Task<IActionResult>  Login([FromBody] LoginMinjusticiaModel model) {
            var result = new ResponseModel();

            var login = await AutenticacionMinjusticiaService.Login(model);
            if(login.State == true){
                result.Success = true;
                result.Result = login;   
            }
            return new JsonResult(result);
        }

            
        #region Registro de usuarios en minjusticia
            /// <summary>
            /// Realiza el registro de usuario y cuidadano para poder agendar
            /// </summary>
            /// <returns>Listado de entidades</returns>
            [HttpPost("RegistrarUsuario")]        
            public async Task<IActionResult>  RegistrarUsuario([FromBody] RegistroUsuarioMinjusticiaModel model) {
                var result = new ResponseModel();

                if (!ModelState.IsValid) {
                    return BadRequest ("Los datos diligenciados tienen inconsistencias.");
                }

                MinjusticiaBodyResult registroUsuario = await AgendamientoConsultorioService.RegistrarUsuarioMinjusticia(model);                
                if(registroUsuario.OperacionExitosa) {
                    result.Success = true;                    
                }

                result.Message = registroUsuario.Mensaje;
                return new JsonResult(result);   
            }

        #endregion Registro de usuarios en minjusticia 
    
        #region Detalle de usuario
            /// <summary>
            /// Obtiene el listado de entidades disponibles del tramite
            /// </summary>
            /// <returns>Listado de entidades</returns>
            [HttpGet("ObtenerDatosUsuario")]        
            public async Task<IActionResult>  ObtenerDatosUsuario(string username) {
                var result = new ResponseModel();

                var userInformation = await AgendamientoConsultorioService.GetUserInformation(username);
                if(userInformation.OperacionExitosa == true){
                    result.Success = true;
                    result.Result = userInformation.Vistas[0];   
                }

                result.Message = userInformation.Mensaje;

                return new JsonResult(result);
            }
        #endregion Detalle de usuario

        #region Recuperación de contraseña
            /// <summary>
            /// Realiza el registro de usuario y cuidadano para poder agendar
            /// </summary>
            /// <returns>Listado de entidades</returns>
            [HttpPost("SolicitudRecuperacionContrasena")]        
            public async Task<IActionResult>  SolicitudRecuperacionContrasena([FromBody] SolicitudCambioContrasenaModel model) {
                var result = new ResponseModel();

                SolicitudRecuperarContrasenaModel registroUsuario = await AgendamientoConsultorioService.SolicitarCambioContrasena(model);                
                if(registroUsuario.OperacionExitosa) {
                    result.Success = true;                    
                }

                //TODO: Incluir consulta de token para iniciar sesión

                result.Message = registroUsuario.Mensaje;
                return new JsonResult(result);   
            }

            /// <summary>
            /// Realiza el registro de usuario y cuidadano para poder agendar
            /// </summary>
            /// <returns>Listado de entidades</returns>
            [HttpPost("RecuperarContrasena")]        
            public async Task<IActionResult>  RecuperarContrasena([FromBody] CambioContrasenaModel model) {
                var result = new ResponseModel();

                if (!ModelState.IsValid || model == null || !model.VerificarContrasenas() ) {
                    return BadRequest ("Los datos diligenciados tienen inconsistencias.");
                }

                SolicitudRecuperarContrasenaModel registroUsuario = await AgendamientoConsultorioService.CambioContrasena(model);                
                if(registroUsuario.OperacionExitosa) {
                    result.Success = true;                    
                }

                result.Message = registroUsuario.Mensaje;
                return new JsonResult(result);   
            }
        #endregion
    }
}