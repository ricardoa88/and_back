using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using tramites_servicios_webapi.invima.ConsultaRegistroSanitario.Models;
using System.Diagnostics;

namespace tramites_servicios_webapi.invima.ConsultaRegistroSanitario.Services{
     
    /// <summary>
    /// Servicios Aportes Parafiscales del ICBF
    /// </summary>
    public class ConsultaRegistroSanitarioService
    {
        private static readonly HttpClient client;
        private const String urlBase = "https://farmacoweb.invima.gov.co/ServicioConsultaRegistros/api/";
        private const string urlListaConsultaRegistro = "https://farmacoweb.invima.gov.co/ServicioConsultaRegistros/api/gestion-codigosAtc/grupos";
        private const string urlConsultaATC = "https://farmacoweb.invima.gov.co/ServicioConsultaRegistros/api/gestion-registros/registrosPorIdSustanciaAtc/";
        private const string urlConsultaDetalleATC = "https://farmacoweb.invima.gov.co/ServicioConsultaRegistros/api/registro-sanitario/detalleRegistro?nroexpediente=";
        private const int consultaRegistroExpediente = 1;
        private const int consultaRegistroNombreProducto = 2;
        private const int consultaRegistroRegistro = 3;
        private const int consultaRegistroPrincipio = 4;
        private const int consultaDetalleATC = 5;
        private const int descargarPDF = 6;


        static ConsultaRegistroSanitarioService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(urlBase);
        }

        /// <summary>
        /// Obtiene las listas de la consulta ATC
        /// </summary>
        /// <param name="url">url del servicio a consumir</param>
        /// <param name="value">codigo atc para la consulta</param>
        /// <returns>Listado de registros</returns>
        public static async Task<JsonResult> GetListasATC(string url,string value)
        {
            JsonResult resultado = null;  
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add (new MediaTypeWithQualityHeaderValue ("application/json"));
            StringContent stringContent = new StringContent ("", Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), url + value);
            try {
                HttpResponseMessage resp = await client.SendAsync(request);

                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     var listaModel = JsonConvert.DeserializeObject<List<ConsultaATCModel>> (response);
                     Console.WriteLine(listaModel.ToString());
                     var selectListItems = new List<SelectListItemModel> ();
                     
                     foreach(ConsultaATCModel dato in listaModel)
                     {
                         SelectListItemModel selectListItem = new SelectListItemModel();
                         selectListItem.Text = dato.codigoAtc + " - " +dato.descripcion;
                         selectListItem.Value = dato.codigoAtc;
                         selectListItems.Add(selectListItem);
                     }
                     resultado = new JsonResult(selectListItems);
                }
            } catch (Exception e) {
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }              
            return resultado;
        }        

        /// <summary>
        /// Consulta el listado de Grupos asociados a los tramites Invima
        /// </summary>
        /// <returns>Listado de grupos</returns>
        public static async Task<JsonResult> GetListasRegistro()
        {
            JsonResult resultado = null;  
            client.DefaultRequestHeaders.Accept.Clear ();
            client.DefaultRequestHeaders.Accept.Add (new MediaTypeWithQualityHeaderValue ("application/json"));
            StringContent stringContent = new StringContent ("", Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), urlListaConsultaRegistro);
            try {
                HttpResponseMessage resp = await client.SendAsync(request);

                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     var listaModel = JsonConvert.DeserializeObject<List<ConsultaRegistroModel>> (response);
                     Console.WriteLine(listaModel.ToString());
                     var selectListItems = new List<SelectListItemModel> ();
                     
                     foreach(ConsultaRegistroModel dato in listaModel)
                     {
                         SelectListItemModel selectListItem = new SelectListItemModel();
                         selectListItem.Text = dato.nbr_grupo;
                         selectListItem.Value = dato.cdg_grupo;
                         selectListItems.Add(selectListItem);
                     }
                     resultado = new JsonResult(selectListItems);
                }
            } catch (Exception e) {
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }              
            return resultado;
        }

        /// <summary>
        /// Obtiene la consulta general ATC
        /// </summary>
        /// <param name="value">codigo atc para la consulta</param>
        /// <returns>Listado de registros</returns>
        public static async Task<JsonResult> GetConsultaGeneralATC(string value)
        {
            JsonResult resultado = null;  
            client.DefaultRequestHeaders.Accept.Clear ();
            client.DefaultRequestHeaders.Accept.Add (new MediaTypeWithQualityHeaderValue ("application/json"));
            StringContent stringContent = new StringContent ("", Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), urlConsultaATC + value);
            try {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     var listaModel = JsonConvert.DeserializeObject<List<ConsultaGeneralATCBody>> (response);
                     ConsultaGeneralATCHeader consultaATC = new ConsultaGeneralATCHeader();
                     consultaATC.X_Total_Records = resp.Headers.FirstOrDefault(i=>i.Key=="X-Total-Records").Value.FirstOrDefault();
                     int max_index = 0;
                     string link_next = string.Empty;
                     if (resp.Headers.Contains("Link"))
                     {
                        var links = resp.Headers.GetValues("Link").ToList();                        
                        foreach(string cad in links)
                            if(cad.IndexOf("last") > 0){
                                link_next  = cad.ToString().Split(new char[]{';'})[0];  
                                link_next = link_next.Trim( new Char[] { '<', '>' });
                                max_index = int.Parse(link_next.Substring(link_next.Length - 1));
                                link_next = link_next.Substring(0,link_next.Length - 1);                                
                            }                        
                        if(links.Count > 0)                        
                        {
                            consultaATC.linkNext = links[0];
                            consultaATC.linkFirst = links[1];
                            consultaATC.linkLast = links[2];
                        }
                     }
                     consultaATC.ObjLista = listaModel;
                     if (max_index > 0){
                         consultaATC.ObjLista.Clear();
                        for(int i= 1; i <= max_index;i++)
                        {
                            client.DefaultRequestHeaders.Accept.Clear ();
                            client.DefaultRequestHeaders.Accept.Add (new MediaTypeWithQualityHeaderValue ("application/json"));
                            request = new HttpRequestMessage(new HttpMethod("GET"), link_next + i.ToString());                            
                            try
                            {
                                resp = await client.SendAsync(request);
                                if (resp.IsSuccessStatusCode) {
                                    response = resp.Content.ReadAsStringAsync().Result;
                                    listaModel = JsonConvert.DeserializeObject<List<ConsultaGeneralATCBody>> (response);
                                    foreach(ConsultaGeneralATCBody elem in listaModel)
                                        consultaATC.ObjLista.Add(elem);
                                }
                            }
                            catch (Exception e)
                            {                 
                                Console.WriteLine (e.Message);
                                Console.ReadLine ();           
                            }
                        }
                     }
                     resultado = new JsonResult(consultaATC);
                }
            } catch (Exception e) {
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }              
            return resultado;
        }            


 /// <summary>
        /// Consulta el listado de Registro
        /// </summary>
        /// <returns>Listado de grupos</returns>
        public static async Task<JsonResult> GetReistro(int tipoConsultaRegistro, string value, string group= null)
        {
            JsonResult resultado = null;  
            client.DefaultRequestHeaders.Accept.Clear ();
            client.DefaultRequestHeaders.Accept.Add (new MediaTypeWithQualityHeaderValue ("application/json"));
            UriBuilder urlListaConsultaRegistro = GetUriConsultaRegistro(tipoConsultaRegistro, value, group);
            var request = new HttpRequestMessage(new HttpMethod("GET"), urlListaConsultaRegistro.Uri);
            Console.WriteLine ("URL DE CPNSULTA: " + urlListaConsultaRegistro.Uri);    
            try {
                HttpResponseMessage resp = await client.SendAsync(request);
                
                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     var listaModel = JsonConvert.DeserializeObject<List<ConsultaRegistro>> (response);

                     int max_index = 0;
                     string link_next = string.Empty;
                     if (resp.Headers.Contains("Link"))
                     {
                        var links = resp.Headers.GetValues("Link").ToList();                        
                        foreach(string cad in links)
                            if(cad.IndexOf("last") > 0){
                                link_next  = cad.ToString().Split(new char[]{';'})[0];  
                                link_next = link_next.Trim( new Char[] { '<', '>' });
                                max_index = int.Parse(link_next.Substring(link_next.Length - 1));
                                link_next = link_next.Substring(0,link_next.Length - 1);                                
                            }                        
            
                     }
                    if (max_index > 0){
                         listaModel.Clear();
                        for(int i= 1; i <= max_index;i++)
                        {
                            client.DefaultRequestHeaders.Accept.Clear ();
                            client.DefaultRequestHeaders.Accept.Add (new MediaTypeWithQualityHeaderValue ("application/json"));
                            request = new HttpRequestMessage(new HttpMethod("GET"), link_next + i.ToString());         
                            try
                            {
                                resp = await client.SendAsync(request);
                                if (resp.IsSuccessStatusCode) {
                                    response = resp.Content.ReadAsStringAsync().Result;
                                    var listaModel_temp = JsonConvert.DeserializeObject<List<ConsultaRegistro>> (response);
                                    foreach(ConsultaRegistro elem in listaModel_temp)
                                            listaModel.Add(elem);
                                }
                            }
                            catch (Exception e)
                            {                 
                                Console.WriteLine (e.Message);
                                Console.ReadLine ();           
                            }
                        }
                     }
                     resultado = new JsonResult(listaModel);
                }
            } catch (Exception e) {
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }              
            return resultado;
        }
    public static  UriBuilder GetUriConsultaRegistro(int tipoConsultaRegistro, string value, string group= null){
         UriBuilder urlConsultaRegistro  = null;
         switch (tipoConsultaRegistro){

             case consultaRegistroExpediente:
               urlConsultaRegistro = new UriBuilder($"{urlBase}registro-sanitario/registroPorNumeroExpediente/{value}");
             break;

            case consultaRegistroNombreProducto:
               urlConsultaRegistro = new UriBuilder($"{urlBase}registro-sanitario/registroPorNombreProducto");
               urlConsultaRegistro.Query = $"nombre_producto={value}&cdg_grupo={group}";
             break;

            case consultaRegistroRegistro:
               urlConsultaRegistro = new UriBuilder($"{urlBase}registro-sanitario/registroPorRegistroSanitario");
               urlConsultaRegistro.Query = $"registro_sanitario={value}&cdg_grupo={group}";
             break;

            case consultaRegistroPrincipio:
               urlConsultaRegistro = new UriBuilder($"{urlBase}registro-sanitario/registroPorPrincipioActivo");
               urlConsultaRegistro.Query = $"principio={value}&cdg_grupo={group}";
             break;

            case consultaDetalleATC:
               urlConsultaRegistro = new UriBuilder($"{urlBase}registro-sanitario/detalleRegistro");
               urlConsultaRegistro.Query = $"nroexpediente={value}&cdg_grupo={group}";
             break;      

            case descargarPDF:
               urlConsultaRegistro = new UriBuilder($"{urlBase}registro-sanitario/obtenerPDFRCA");
               urlConsultaRegistro.Query = $"nroexpediente={value}&cdg_grupo=2&cdgproducto={group}";
             break;           
         }

         return urlConsultaRegistro;
    }

    /// <summary>
    /// Obtiene la consulta detalle ATC
    /// </summary>
    /// <param name="value">codigo expediente para la consulta</param>
    /// <returns>Listado de registros</returns>
      public static async Task<JsonResult> getConsultaDetalleATC(string value)
      {
        JsonResult resultado = null;  
        client.DefaultRequestHeaders.Accept.Clear ();
        client.DefaultRequestHeaders.Accept.Add (new MediaTypeWithQualityHeaderValue ("application/json"));
        StringContent stringContent = new StringContent ("", Encoding.UTF8, "application/json");
        UriBuilder urlListaConsultaRegistro = GetUriConsultaRegistro(consultaDetalleATC, value, "2");
        var request = new HttpRequestMessage(new HttpMethod("GET"),urlListaConsultaRegistro.Uri);
        Debug.Write(urlConsultaDetalleATC + value);
        try {
            HttpResponseMessage resp = await client.SendAsync(request);
            if (resp.IsSuccessStatusCode) {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    var listaModel = JsonConvert.DeserializeObject<ConsultaDetalleATCHeader> (response);                     
                    resultado = new JsonResult(listaModel);
            }
        } catch (Exception e) {
            Console.WriteLine (e.Message);
            Console.ReadLine ();
        }              
        return resultado;
    }  

        /// <summary>
        /// Obtiene la consulta detalle Registro
        /// </summary>
        /// <param name="value">codigo expediente para la consulta</param>
        /// <returns>Listado de registros</returns>
        public static async Task<JsonResult> getConsultaDetalleRegistro(string value, string group)
        {
            JsonResult resultado = null;  
            client.DefaultRequestHeaders.Accept.Clear ();
           // client.DefaultRequestHeaders.Accept.Add (new MediaTypeWithQualityHeaderValue ("application/json"));
            UriBuilder urlConsultaRegistro = new UriBuilder($"{urlBase}registro-sanitario/detalleRegistro");
            urlConsultaRegistro.Query = $"nroexpediente={value}&cdg_grupo={group}";
            var request = new HttpRequestMessage(new HttpMethod("GET"), urlConsultaRegistro.Uri);
            try {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     var listaModel = JsonConvert.DeserializeObject<ConsultaDetalleRegistro> (response);                     
                     resultado = new JsonResult(listaModel);
                }
            } catch (Exception e) {
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }              
            return resultado;
        }

        public static async Task<FileContentResult> DescargarPDF(string nroexpediente,string cdgproducto)
        {
            UriBuilder builder = GetUriConsultaRegistro(descargarPDF, nroexpediente, cdgproducto);
            FileContentResult archivoPDF  = null;
            using (var client = new HttpClient()){
                using (var result = await client.GetAsync(builder.Uri)){
                    if (result.IsSuccessStatusCode){
                        string response = result.Content.ReadAsStringAsync().Result;
                        var retornoPDF = JsonConvert.DeserializeObject<archivoPDF> (response);
                        byte[] data = System.Convert.FromBase64String(retornoPDF.pdf);
                        string base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data);
                        byte[] bytes = Convert.FromBase64String(base64Decoded);
                        archivoPDF =  new FileContentResult(bytes, "application/pdf");
                    }
                }
            }
            return archivoPDF;
        }         


    }

    
}