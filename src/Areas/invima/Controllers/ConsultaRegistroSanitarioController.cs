using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.invima.ConsultaRegistroSanitario.Services;
using tramites_servicios_webapi.invima.ConsultaRegistroSanitario.Models;

namespace tramites_servicios_webapi.invima.ConsultaRegistroSanitario.Controllers
{
    /// <summary>
    /// Consulta Registro Sanitario del INVIMA
    /// </summary>
    [Route ("api/invima/[controller]")]
    [ApiController]
    public class ConsultaRegistroSanitarioController : ControllerBase 
    {                      
        /// <summary>
        /// Retorna la lista de sistema organico utilizada en la consulta ATC
        /// </summary>
        /// <returns>Listado de sistema organicos</returns>
        [HttpGet("ListaSistemaOrganico", Name = "ListaSistemaOrganico")]
        public async Task<IActionResult> getSistemaOrganico()
        {            
            var respuesta = await ConsultaRegistroSanitarioService.GetListasATC("gestion-codigosAtc/sistemas","");
            if (respuesta == null) 
                return NotFound ();
            return respuesta;
        }

        /// <summary>
        /// Retorna la lista de los grupos farmacologicos utilizados en la consulta ATC
        /// </summary>
        /// <param name="value">codigo atc del sistema organico</param>
        /// <returns>Listado de grupos farmacologicos</returns>
        [HttpGet("ListaGrupoFarmacologico/{value}", Name = "ListaGrupoFarmacologico")]
        public async Task<IActionResult> getGrupoFarmacologico(string value)
        {            
            var respuesta = await ConsultaRegistroSanitarioService.GetListasATC("gestion-codigosAtc/grupos-farmacologicos/sistema/",value);
            if (respuesta == null) 
                return NotFound ();
            return respuesta;
        }

        /// <summary>
        /// Retorna la lista de los sub grupo farmacologicos utilizados en la consulta ATC
        /// </summary>
        /// <param name="value">codigo atc del grupo farmacologico</param>
        /// <returns>Listado de sub grupos farmacologicos</returns>
        [HttpGet("ListaSubGrupoFarmacologico/{value}", Name = "ListaSubGrupoFarmacologico")]
        public async Task<IActionResult> getSubGrupoFarmacologico(string value)
        {            
            var respuesta = await ConsultaRegistroSanitarioService.GetListasATC("gestion-codigosAtc/subgrupos-farmacologicos/grupo-farmacologico/",value);
            if (respuesta == null) 
                return NotFound ();
            return respuesta;
        }

        /// <summary>
        /// Retorna la lista de los sub grupos quimicos utilizados en la consulta ATC
        /// </summary>
        /// <param name="value">codigo atc del sub grupo farmacologico</param>
        /// <returns>Listado de sub grupos quimicos</returns>
        [HttpGet("ListaSubGrupoQuimico/{value}", Name = "ListaSubGrupoQuimico")]
        public async Task<IActionResult> getSubGrupoQuimico(string value)
        {            
            var respuesta = await ConsultaRegistroSanitarioService.GetListasATC("gestion-codigosAtc/subgrupos-quimicos/subgrupo-farmacologico/",value);
            if (respuesta == null) 
                return NotFound ();
            return respuesta;
        }        

        /// <summary>
        /// Retorna la lista de las sustancias utilizadas en la consulta ATC
        /// </summary>
        /// <param name="value">codigo atc del sub grupo quimico</param>
        /// <returns>Listado de sustancias</returns>
        [HttpGet("ListaSustancia/{value}", Name = "ListaSustancia")]
        public async Task<IActionResult> getSustancia(string value)
        {            
            var respuesta = await ConsultaRegistroSanitarioService.GetListasATC("gestion-codigosAtc/sustancias/subgrupo-quimico/",value);
            if (respuesta == null) 
                return NotFound ();
            return respuesta;
        }

        /// <summary>
        /// el listado de Grupos asociados a los tramites Invima
        /// </summary>
        /// <returns>Listado de grupos asociados</returns>
        [HttpGet("ListaConsultaRegistroGrupo", Name = "ListaConsultaRegistroGrupo")]
        public async Task<IActionResult> getConsultaRegistroGrupo()
        {            
            var respuesta = await ConsultaRegistroSanitarioService.GetListasRegistro();
            if (respuesta == null) 
                return NotFound ();
            return respuesta;
        }      

        /// <summary>
        /// Retorna la lista de las sustancias utilizadas en la consulta ATC, usando el nombre como filtro
        /// </summary>
        /// <param name="value">nombre de la sustancia</param>
        /// <returns>Listado de sustancias</returns>
        [HttpGet("ListaSustanciaxNombre/{value}", Name = "ListaSustanciaxNombre")]
        public async Task<IActionResult> getSustanciaxNombre(string value)
        {            
            var respuesta = await ConsultaRegistroSanitarioService.GetListasATC("gestion-codigosAtc/localizarSustanciaPorNombreAproximado/",value);
            if (respuesta == null) 
                return NotFound ();
            return respuesta;
        }          

        /// <summary>
        /// Retorna la consulta general ATC
        /// </summary>
        /// <param name="value">nombre de la sustancia</param>
        /// <returns>Listado de sustancias</returns>
        [HttpGet("ConsultaGeneralATC/{value}", Name = "ConsultaGeneralATC")]
        public async Task<IActionResult> GetConsultaGeneralATC(string value)
        {            
            var respuesta = await ConsultaRegistroSanitarioService.GetConsultaGeneralATC(value);
            if (respuesta == null) 
                return NotFound ();
            return respuesta;
        }  


                /// <summary>
        /// Retorna la consulta general ATC
        /// </summary>
        /// <param name="value">nombre de la sustancia</param>
        /// <returns>Listado de sustancias</returns>
        [HttpGet("ConsultaRegistro/{tipoConsultaRegistro}", Name = "ConsultaRegistro")]
        public async Task<IActionResult> getConsultaRegistro(int tipoConsultaRegistro,
            [FromQuery(Name = "group")] string group,
            [FromQuery(Name = "value")] string value)
        {            
            var respuesta = await ConsultaRegistroSanitarioService.GetReistro(tipoConsultaRegistro, value, group);
            if (respuesta == null) 
                return NotFound ();
            return respuesta;
        }  

        /// <summary>
        /// Retorna la lista de las sustancias utilizadas en la consulta ATC, usando el codigo ATC como filtro
        /// </summary>
        /// <param name="value">codigo ATC la sustancia</param>
        /// <returns>Listado de sustancias</returns>
        [HttpGet("ListaSustanciaxATC/{value}", Name = "ListaSustanciaxATC")]
        public async Task<IActionResult> getSustanciaxATC(string value)
        {            
            var respuesta = await ConsultaRegistroSanitarioService.GetListasATC("gestion-codigosAtc/localizarSustanciaPorAtc/",value);
            if (respuesta == null) 
                return NotFound ();
            return respuesta;
        }       

       /// <summary>
        /// Retorna la consulta detalle ATC
        /// </summary>
        /// <param name="value">nombre de la sustancia</param>
        /// <returns>Listado de sustancias</returns>
        [HttpGet("ConsultaDetalleATC/{value}", Name = "ConsultaDetalleATC")]
        public async Task<IActionResult> getConsultaDetalleATC(string value)
        {            
            var respuesta = await ConsultaRegistroSanitarioService.getConsultaDetalleATC(value);
            if (respuesta == null) 
                return NotFound ();
            return respuesta;
        } 

        /// <summary>
        /// Retorna la consulta detalle registro
        /// </summary>
        /// <param name="value">nombre de la sustancia</param>
        /// <param name="group">nombre de grupo</param>
        /// <returns>Listado de sustancias</returns>
        [HttpGet("ConsultaDetalleRegistro/{value}/group/{group}", Name = "ConsultaDetalleRegistro")]
        public async Task<IActionResult> getConsultaDetalleRestrio(string value, string group)
        {            
            var respuesta = await ConsultaRegistroSanitarioService.getConsultaDetalleRegistro(value,group);
            if (respuesta == null) 
                return NotFound ();
            return respuesta;
        }              


        /// <summary>
        /// Retorna el pdf resultante
        /// </summary>
        /// <param name="value">numero de expediente</param>
        /// <param name="group">codigo del grupo</param>
        /// <returns>Listado de sustancias</returns>
        [HttpGet("DescargarPDF/{value}/group/{group}", Name = "DescargarPDF")]
        public async Task<FileContentResult> getDescargarPDF(string value, string group)
        {            
            return await ConsultaRegistroSanitarioService.DescargarPDF(value,group);
        }   

    }
}
