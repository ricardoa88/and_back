using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.invima.ConsultaRegistroSanitario.Models {

    /// <summary>
    /// Modelo de la consulta ATC
    /// </summary>
    public class ConsultaATCModel {
        public string codigoAtc {get; set;}
        public string descripcion {get; set;}
        public int nivel {get; set;}
        public string atc {get; set;}
    }

    /// <summary>
    /// Modelo de la consulta por Registro
    /// </summary>
    public class ConsultaRegistroModel
    {
        public string cdg_grupo { get; set; }
        public string nbr_grupo { get; set; }
    } 

    /// <summary>
    /// Modelo item de lista
    /// </summary>
    public class SelectListItemModel
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }       

    public class ConsultaGeneralATCHeader
     {
        public string linkNext { get; set; }
        public string linkFirst { get; set; }
        public string linkLast { get; set; }
        public string X_Total_Records { get; set; }
        public List<ConsultaGeneralATCBody> ObjLista { get; set; }
    }

    public class ConsultaGeneralATCBody
     {
        public string numeroexpediente { get; set; }
        public string numeroregistrosanitario { get; set; }
        public string nombreproducto { get; set; }
        public string estado { get; set; }
        public string fechavencimiento { get; set; }
        public string modalidad { get; set; }
    }

    public class ConsultaDetalleATCRoles{
        public string rol { get; set; }
        public string razonSocial { get; set; }
        public string numeroIdentificacion { get; set; }
        public string tipoIdentificacion { get; set; }
        public string email { get; set; }
        public string direccion { get; set; }
        public string ciudad { get; set; }
        public string pais { get; set; }
        public string departamento { get; set; }
        public string territorio { get; set; }
        public string cdgTerritorio { get; set; }
        public string cdgPais { get; set; }
    }

    public class ConsultaDetalleATCPrincipios{
        public string orden { get; set; }
        public string principio_activo { get; set; }
        public string cantidad { get; set; }
        public string unidad_medida { get; set; }
    }

    public class ConsultaDetalleATCPresentacionCum{
        public string consecutivo { get; set; }
        public string nroexpediente { get; set; }
        public string termino { get; set; }
        public string unidades { get; set; }
        public string cantidadUnidades { get; set; }
        public string presentacion { get; set; }
        public string condicionVenta { get; set; }
        public string fechaInscripcion { get; set; }
        public string estado { get; set; }
        public string fechaInactivacion { get; set; }
    }

    public class ConsultaDetalleATCAtcs{
        public string codigo { get; set; }
        public string sustancia_quimica { get; set; }
        public string sistema_organico { get; set; }
        public string grupo_farmacologico { get; set; }
        public string subgrupo_farmacologico { get; set; }
        public string subgrupo_quimico { get; set; }
    }

    public class ConsultaDetalleATCHeader{
        public string idregistrosanitario { get; set; }
        public string numeroregistrosanitario { get; set; }
        public string fechavencimiento { get; set; }
        public string fechaExpedicion { get; set; }
        public string estado { get; set; }
        public string numeroexpediente { get; set; }
        public string idtramite { get; set; }
        public string modalidad { get; set; }
        public string nombreproducto { get; set; }
        public string descripcioncomun { get; set; }
        public string titulares { get; set; }
        public string inserto { get; set; }
        public string observaciones { get; set; }
        public string formaFarmaceutica { get; set; }
        public string franja { get; set; }
        public string indicaciones { get; set; }
        public string contraindicaciones { get; set; }
        public string viasAdministracion { get; set; }
        public string vidaUtil { get; set; }
        public string condicionVenta { get; set; }
        public string generico { get; set; }
        public string importadores { get; set; }
        public string fabricantes { get; set; }
        public string titularidad { get; set; }
        public List<ConsultaDetalleATCPrincipios> principios { get; set; }
        public List<ConsultaDetalleATCAtcs> atcs { get; set; }  
        public List<ConsultaDetalleATCRoles> roles { get; set; }
        public string marcas { get; set; }
        public string tratamiento { get; set; }
        public string condicionesConservacion { get; set; }
        public List<ConsultaDetalleATCPresentacionCum> presentacionescum { get; set; }
        public List<string> presentaciones { get; set; }
        public string concentracionDecreto { get; set; }
        public string grado_alcoholico { get; set; }
        public string clasificacion { get; set; }
        public string grp_cosmetico_amparado { get; set; }
        public string usos { get; set; }
        public string precauciones { get; set; }
        public string cdgproducto { get; set; }
        public string cdg_forma_csmtca { get; set; }
        public string forma_cosmetica { get; set; }
        public string referencia { get; set; }
        public string riesgo { get; set; }
        public string miembroscomprometidos { get; set; }
        public string plagas_objetivo { get; set; }
        public string antidotos { get; set; }
        public string advertencias { get; set; }
        public string observacionesGrupo { get; set; }
        public string concepto_toxicologico { get; set; }
        public string cdg_ctgr_toxicologica { get; set; }
        public string ctgr_toxicologica { get; set; }
        public string nivel_riesgo { get; set; }
        public string clase { get; set; }
        public string variedades { get; set; }
        public string referencia_autorizada { get; set; }
        public string area_analisis { get; set; }
        public string uso_terapeutico { get; set; }
        public string nta_farmacovigilancia { get; set; }
        public string diluciones { get; set; }
        public string tipo_homeopatico { get; set; }
        public string prefijo_registro { get; set; }
        public string condiciones_conservacion { get; set; }
        public string propiedades { get; set; }
        public string proclama { get; set; }
        public string natural { get; set; }
        public string concentracion { get; set; }
        public string descripcionConcentracion { get; set; }
        public string embargo { get; set; }
    }

    public class ConsultaRegistro
     {
        public string estado { get; set; }
        public string fechavencimiento { get; set; }
        public string modalidad { get; set; }
        public string nombreproducto { get; set; }
        public string numeroexpediente { get; set; }
        public string numeroregistrosanitario { get; set; }
         public string titulares { get; set; }
    }

    public class ConsultaDetalleRegistro{
	public object advertencias { get; set; }
	public object antidotos { get; set; }
	public object area_analisis { get; set; }
	public object atcs { get; set; }  
	public object cdg_ctgr_toxicologica { get; set; }
	public object cdg_forma_csmtca { get; set; }
	public object cdg_formaf { get; set; }
	public object cdgproducto { get; set; }
	public object clase { get; set; }
	public object clasificacion { get; set; }
	public object concentracion { get; set; }
	public object concentracionDecreto { get; set; }
	public object concepto_toxicologico { get; set; }
	public object condicionVenta { get; set; }
	public object condicionesConservacion { get; set; }
	public object condiciones_conservacion { get; set; }
	public object contraindicaciones { get; set; }
	public object ctgr_toxicologica { get; set; }
	public object descripcionConcentracion { get; set; }
	public object descripcioncomun { get; set; }
	public object diluciones { get; set; }
	public object embargo { get; set; }
	public object estado { get; set; }

	public object fabricantes { get; set; }
	public object fechaExpedicion { get; set; }
	public object fechavencimiento { get; set; }
	public object formaFarmaceutica { get; set; }
	public object forma_cosmetica { get; set; }
	public object franja { get; set; }
	public object idtramite { get; set; }
	public object importadores { get; set; }
	public object indicaciones { get; set; }
	public object inserto { get; set; }
	public object marcas { get; set; }

	public object miembroscomprometidos { get; set; }
	public object modalidad { get; set; }
	public object natural { get; set; }
	public object nivel_riesgo { get; set; }
	public object nombreproducto { get; set; }
	public object nta_farmacovigilancia { get; set; }
	public object numeroexpediente { get; set; }

	public object numeroregistrosanitario { get; set; }
	public object observaciones { get; set; }
	public object observacionesGrupo { get; set; }
	public object plagas_objetivo { get; set; }
	public object precauciones { get; set; }
	public object prefijo_registro { get; set; }
	public object presentaciones { get; set; } 
	public object presentacionescum { get; set; }
	public object principios { get; set; } 
	public object proclama { get; set; }
	public object propiedades { get; set; }

    public object generico { get; set; }
	public object referencia { get; set; }
	public object referencia_autorizada { get; set; }
	public object riesgo { get; set; }
	public object roles { get; set; }    //arrray
	

}
    public class archivoPDF{
        public string cdgproducto { get; set; }
        public string pdf { get; set; }
    }

}