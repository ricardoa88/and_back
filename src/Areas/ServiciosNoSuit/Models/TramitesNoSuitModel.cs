namespace tramites_servicios_webapi.Areas.ServiciosNoSuit.Models
{
    public class TramitesNoSuitModel
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
    }
}