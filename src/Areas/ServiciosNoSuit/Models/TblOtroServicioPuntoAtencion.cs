﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosNoSuit.Models
{
    public partial class TblOtroServicioPuntoAtencion
    {
        public int OtroServicioPuntoAtencionId { get; set; }
        public string OtrosServiciosId { get; set; }
        public decimal PuntoAtencionId { get; set; }
        public int? CodigoEstado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }
    }
}