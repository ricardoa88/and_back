﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosNoSuit.Models
{
    public partial class TblOtrosServiciosCondicionesAdd
    {
        public int CondicionesAddId { get; set; }
        public string CondicionesAdiconales { get; set; }
        public string OtrosServiciosId { get; set; }
        public int? CodigoEstado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual TblOtrosServicios OtrosServicios { get; set; }
    }
}