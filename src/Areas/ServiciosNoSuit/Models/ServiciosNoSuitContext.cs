﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace tramites_servicios_webapi.Areas.ServiciosNoSuit.Models
{
    public partial class ServiciosNoSuitContext : DbContext
    {
        public ServiciosNoSuitContext()
        {
        }

        public ServiciosNoSuitContext(DbContextOptions<ServiciosNoSuitContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblDocumentacionRequerida> TblDocumentacionRequeridas { get; set; }
        public virtual DbSet<TblOtrosServicios> TblOtrosServicioss { get; set; }
        public virtual DbSet<TblOtrosServiciosCondicionesAdd> TblOtrosServiciosCondicionesAdds { get; set; }
        public virtual DbSet<TblOtroServicioPuntoAtencion> TblOtroServicioPuntoAtencions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<TblDocumentacionRequerida>(entity =>
            {
                entity.HasKey(e => e.DocRequeridoId)
                    .HasName("PK__TBL_DOCU__51A2F9397B8C541C");

                entity.ToTable("TBL_DOCUMENTACION_REQUERIDA", "tramites_y_servicios");

                entity.Property(e => e.DocRequeridoId).HasColumnName("DOC_REQUERIDO_ID");

                entity.Property(e => e.CodigoEstado)
                    .HasColumnName("CODIGO_ESTADO")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("FECHA_CREACION")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("FECHA_MODIFICACION")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OtrosServiciosId)
                    .IsRequired()
                    .HasColumnName("OTROS_SERVICIOS_ID")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.HasOne(d => d.OtrosServicios)
                    .WithMany(p => p.TblDocumentacionRequerida)
                    .HasForeignKey(d => d.OtrosServiciosId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_DOCUM__OTROS__1F98B2C1");
            });

            modelBuilder.Entity<TblOtroServicioPuntoAtencion>(entity =>
            {
                entity.HasKey(e => e.OtroServicioPuntoAtencionId)
                    .HasName("PK__TBL_OTRO__C21595B5B7250AE9");

                entity.ToTable("TBL_OTRO_SERVICIO_PUNTO_ATENCION", "tramites_y_servicios");

                entity.Property(e => e.OtroServicioPuntoAtencionId).HasColumnName("OTRO_SERVICIO_PUNTO_ATENCION_ID");

                entity.Property(e => e.CodigoEstado)
                    .HasColumnName("CODIGO_ESTADO")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("FECHA_CREACION")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("FECHA_MODIFICACION")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OtrosServiciosId)
                    .IsRequired()
                    .HasColumnName("OTROS_SERVICIOS_ID")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.PuntoAtencionId)
                    .HasColumnName("PUNTO_ATENCION_ID")
                    .HasColumnType("numeric(10, 0)");
            });

            modelBuilder.Entity<TblOtrosServicios>(entity =>
            {
                entity.HasKey(e => e.OtrosServiciosId)
                    .HasName("PK__TBL_OTRO__FC01C3DF8F586E04");

                entity.ToTable("TBL_OTROS_SERVICIOS", "tramites_y_servicios");

                entity.Property(e => e.OtrosServiciosId)
                    .HasColumnName("OTROS_SERVICIOS_ID")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CodigoEstado)
                    .HasColumnName("CODIGO_ESTADO")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Consideraciones)
                    .HasColumnName("CONSIDERACIONES")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasColumnName("DESCRIPCION")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.EntidadId)
                    .IsRequired()
                    .HasColumnName("ENTIDAD_ID")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("FECHA_CREACION")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("FECHA_MODIFICACION")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.MunicipioCodigoDane)
                    .IsRequired()
                    .HasColumnName("MUNICIPIO_CODIGO_DANE")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasColumnName("NOMBRE")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PalabrasClave)
                    .HasColumnName("PALABRAS_CLAVE")
                    .IsUnicode(false);

                entity.Property(e => e.Proposito)
                    .HasColumnName("PROPOSITO")
                    .IsUnicode(false);

                entity.Property(e => e.ResultadoOtroServicio)
                    .HasColumnName("RESULTADO_OTRO_SERVICIO")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.UrlServicioEnLinea)
                    .HasColumnName("URL_SERVICIO_EN_LINEA")
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.UsuarioAutorizoGovco)
                    .HasColumnName("USUARIO_AUTORIZO_GOVCO")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.UsuarioCreadorEntidad)
                    .IsRequired()
                    .HasColumnName("USUARIO_CREADOR_ENTIDAD")
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblOtrosServiciosCondicionesAdd>(entity =>
            {
                entity.HasKey(e => e.CondicionesAddId)
                    .HasName("PK__TBL_OTRO__9255017FB6924A03");

                entity.ToTable("TBL_OTROS_SERVICIOS_CONDICIONES_ADD", "tramites_y_servicios");

                entity.Property(e => e.CondicionesAddId).HasColumnName("CONDICIONES_ADD_ID");

                entity.Property(e => e.CodigoEstado)
                    .HasColumnName("CODIGO_ESTADO")
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.CondicionesAdiconales)
                    .HasColumnName("CONDICIONES_ADICONALES")
                    .IsUnicode(false);

                entity.Property(e => e.FechaCreacion)
                    .HasColumnName("FECHA_CREACION")
                    .HasColumnType("smalldatetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.FechaModificacion)
                    .HasColumnName("FECHA_MODIFICACION")
                    .HasColumnType("smalldatetime");

                entity.Property(e => e.OtrosServiciosId)
                    .IsRequired()
                    .HasColumnName("OTROS_SERVICIOS_ID")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.HasOne(d => d.OtrosServicios)
                    .WithMany(p => p.TblOtrosServiciosCondicionesAdd)
                    .HasForeignKey(d => d.OtrosServiciosId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__TBL_OTROS__OTROS__30C33EC3");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}