﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosNoSuit.Models
{
    public partial class FichaNoSuit
    {
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Proposito { get; set; }
        public string Consideraciones { get; set; }
        public string ResultadoOtroServicio { get; set; }
        public int? CodigoEstado { get; set; }
        public string NombreEntidad { get; set; }
        public string UrlServicioEnLinea { get; set; }

    }

    public partial class DocumentacioRequerida
    {
        public string DescripcionDocumento { get; set; }
    }

    public partial class ConsideracionesAdicionales
    {
        public string CondicionesAdicionales { get; set; }
    }

    public partial class PuntosAtencionNosuit
    {
        public string PuntosAtencionId { get; set; }
    }

    
}