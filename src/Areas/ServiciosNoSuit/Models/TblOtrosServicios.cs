﻿using System;
using System.Collections.Generic;

namespace tramites_servicios_webapi.Areas.ServiciosNoSuit.Models
{
    public partial class TblOtrosServicios
    {
        public TblOtrosServicios()
        {
            TblDocumentacionRequerida = new HashSet<TblDocumentacionRequerida>();
            TblOtrosServiciosCondicionesAdd = new HashSet<TblOtrosServiciosCondicionesAdd>();
        }

        public string OtrosServiciosId { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Proposito { get; set; }
        public string PalabrasClave { get; set; }
        public string Consideraciones { get; set; }
        public string ResultadoOtroServicio { get; set; }
        public int? CodigoEstado { get; set; }
        public string EntidadId { get; set; }
        public string MunicipioCodigoDane { get; set; }
        public string UrlServicioEnLinea { get; set; }
        public string UsuarioCreadorEntidad { get; set; }
        public string UsuarioAutorizoGovco { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime? FechaModificacion { get; set; }

        public virtual ICollection<TblDocumentacionRequerida> TblDocumentacionRequerida { get; set; }
        public virtual ICollection<TblOtrosServiciosCondicionesAdd> TblOtrosServiciosCondicionesAdd { get; set; }
    }
}