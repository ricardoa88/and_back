﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tramites_servicios_webapi.Areas.ServiciosNoSuit.Models;
using tramites_servicios_webapi.Areas.ServiciosSuit.Models;
using tramites_servicios_webapi.Models;

namespace tramites_servicios_webapi.Areas.ServiciosNoSuit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FichaNoSuitTramiteController : ControllerBase
    {
        private readonly ServiciosNoSuitContext _context;
        private readonly ServiciosSuitContext _contextSuit;

        public FichaNoSuitTramiteController(ServiciosNoSuitContext context, ServiciosSuitContext contextSuit)
        {
            _context = context;
            _contextSuit = contextSuit;
        }

        //Servicio obtiene la data de la ficha no suit
        [HttpGet("GetNotSuiteTramiteById/{idTramite}", Name = "GetNotSuiteTramiteById")]
        public ActionResult<FichaNoSuit> GetNotSuiteTramiteById(string idTramite)
        {
            FichaNoSuit fichaNoSuit = new FichaNoSuit();
            fichaNoSuit = (from ots in _context.TblOtrosServicioss
                            where ots.OtrosServiciosId == idTramite
                            select new FichaNoSuit {
                                Nombre = ots.Nombre,
                                Descripcion = ots.Descripcion,
                                Proposito = ots.Proposito,
                                Consideraciones = ots.Consideraciones,
                                ResultadoOtroServicio = ots.ResultadoOtroServicio,
                                CodigoEstado = ots.CodigoEstado,
                                NombreEntidad = ots.EntidadId,
                                UrlServicioEnLinea = ots.UrlServicioEnLinea
                            }).FirstOrDefault();
                   
            if (fichaNoSuit == null)
            {
                
                string messageNotFound = "Tramite no existe";
                var result = new { message = messageNotFound, StatusCode = 604 };
                return Ok(result);

            }

            var nombreEntidad = (from ins in _contextSuit.VmSgpInstitucions
                                 where ins.Id == fichaNoSuit.NombreEntidad
                                 select new { NombreEntidad =ins.Nombre }).FirstOrDefault();

            fichaNoSuit.NombreEntidad = nombreEntidad.NombreEntidad;
            return Ok(fichaNoSuit);

        }

        //Servicio obtiene la data de la ficha no suit
        [HttpGet("GetDocumentacionRequeridaById/{idTramite}", Name = "GetDocumentacionRequeridaById")]
        public ActionResult<List<DocumentacioRequerida>> GetDocumentacionRequeridaById(string idTramite)
        {
            List<DocumentacioRequerida> documentacion = new List<DocumentacioRequerida>();
            documentacion = (from dr in _context.TblDocumentacionRequeridas
                           where dr.OtrosServiciosId == idTramite
                           select new DocumentacioRequerida
                           {
                               DescripcionDocumento = dr.Descripcion
                           }).ToList();

            if (documentacion.Count == 0)
            {

                string messageNotFound = "Tramite no existe";
                var result = new { message = messageNotFound, StatusCode = 604 };
                return Ok(result);

            }
            return Ok(documentacion);

        }

        //Servicio obtiene la data de la ficha no suit
        [HttpGet("GetConsideracionesAdicionalesById/{idTramite}", Name = "GetConsideracionesAdicionalesById")]
        public ActionResult<List<ConsideracionesAdicionales>> GetConsideracionesAdicionalesById(string idTramite)
        {
            List<ConsideracionesAdicionales> consideraciones = new List<ConsideracionesAdicionales>();
            consideraciones = (from oc in _context.TblOtrosServiciosCondicionesAdds
                             where oc.OtrosServiciosId == idTramite
                             select new ConsideracionesAdicionales
                             {
                                 CondicionesAdicionales = oc.CondicionesAdiconales
                             }).ToList();

            if (consideraciones.Count == 0)
            {

                string messageNotFound = "Tramite no existe";
                var result = new { message = messageNotFound, StatusCode = 604 };
                return Ok(result);

            }
            return Ok(consideraciones);

        }

        //Servicio obtiene la data de la ficha no suit
        [HttpGet("GetPuntosAtencionNoSuitById/{idTramite}", Name = "GetPuntosAtencionNoSuitById")]
        public ActionResult<List<ConsideracionesAdicionales>> GetPuntosAtencionNoSuitById(string idTramite)
        {
            List<PuntosAtencionNosuit> puntos = new List<PuntosAtencionNosuit>();
            puntos = (from pa in _context.TblOtroServicioPuntoAtencions
                               where pa.OtrosServiciosId == idTramite
                               select new PuntosAtencionNosuit
                               {
                                   PuntosAtencionId = Convert.ToString(pa.PuntoAtencionId)
                               }).ToList();

            if (puntos.Count == 0)
            {

                string messageNotFound = "Tramite no existe";
                var result = new { message = messageNotFound, StatusCode = 604 };
                return Ok(result);

            }
            return Ok(puntos);

        }

    }
}
