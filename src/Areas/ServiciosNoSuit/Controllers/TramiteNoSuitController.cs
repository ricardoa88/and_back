using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Areas.ServiciosNoSuit.Models;

namespace tramites_servicios_webapi.Areas.ServiciosNoSuit.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TramitesNoSuitController : ControllerBase
    {
        private readonly ServiciosNoSuitContext _context;

        public TramitesNoSuitController(ServiciosNoSuitContext context)
        {
            _context = context;
        }

        [HttpGet("Get", Name = "Get")]
        public ActionResult<List<TramitesNoSuitModel>> Get()
        {
            var model = new List<TramitesNoSuitModel>();
            model = (from ots in _context.TblOtrosServicioss
                            select new TramitesNoSuitModel {
                                Id = ots.OtrosServiciosId,
                                Nombre = ots.Nombre
                            }).ToList();
            return Ok(model);
        }
    }
}