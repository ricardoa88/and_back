using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Minsalud.ConsultaPrestadores.Models
{
    /// <summary>
    /// Modelo con la información de las listas icbf
    /// </summary>
    public class ListaModel
    {
        /// <summary>
        /// Codigo de la parametrica
        /// </summary>
        public string codigo { get; set; }

        /// <summary>
        /// valor de la parametrica
        /// </summary>
         public string nombre { get; set; }
    }
}