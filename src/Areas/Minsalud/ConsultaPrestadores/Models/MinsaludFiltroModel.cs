using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Minsalud.ConsultaPrestadores.Models
{
    /// <summary>
    /// Modelo clave - valor
    /// </summary>
    public class MinsaludFiltroModel
    {

        public string historia { get; set; }
        public string numero_documento { get; set; }
        public string naturaleza_juridica { get; set; }
        public string departamento_prestador { get; set; }
        public string municipio_prestador { get; set; }
        public string codigo_prestador { get; set; }
        public string nombre_prestador { get; set; }
        public string clase_prestador { get; set; }
        public string ese { get; set; }
        public string nivel_atencion { get; set; }
        public string caracter_territorial { get; set; }
        public string departamento_sede { get; set; }
        public string municipio_sede { get; set; }
        public string codigo_sede { get; set; }
        public string sede_principal { get; set; }
        public string nombre_sede { get; set; }
        public string zona { get; set; }  
        public string grupo { get; set; }
        public string codigo_servicio { get; set; }   
        public string intramural_ambulatorio { get; set; }   
        public string intramural_hospitalario { get; set; }   
        public string extramural_domiciliario { get; set; }   
        public string extramural_otras { get; set; }   
        public string extramural_unidad { get; set; }   
        public string telemedicina_centro { get; set; }   
        public string telemedicina_institucion { get; set; }   
        public string complejidad_baja { get; set; }   
        public string complejidad_media { get; set; }   
        public string complejidad_alta { get; set; }   
        public string servicio_nombre { get; set; }   
        public string concepto { get; set; }   
    }
}