using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Minsalud.ConsultaPrestadores.Models
{
    /// <summary>
    /// Modelo con el tipo de lista que se solicita
    /// </summary>
    public class InputTipoLista
    {
        /// <summary>
        /// Valor de la lista que se solicita
        /// </summary>
        public string Valor { get; set; }

    }
}