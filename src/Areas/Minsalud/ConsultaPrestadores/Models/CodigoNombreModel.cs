using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Minsalud.ConsultaPrestadores.Models
{
    /// <summary>
    /// Modelo clave - valor
    /// </summary>
    public class CodigoNombreModel
    {
        /// <summary>
        /// Identificación item
        /// </summary>
        public string codigo { get; set; }

        /// <summary>
        /// Valor del item
        /// </summary>
         public string nombre { get; set; }
    }
}