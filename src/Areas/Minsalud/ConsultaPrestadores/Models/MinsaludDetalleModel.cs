using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Minsalud.ConsultaPrestadores.Models
{
    /// <summary>
    /// Modelo clave - valor
    /// </summary>
    public class MinsaludDetalleModel
    {
        public int id { get; set; }
        public string barrio { get; set; }
        public string caracter_territorial { get; set; }
        public string centro_poblado { get; set; }
        public string clase_prestador { get; set; }
        public string codigo_prestador { get; set; }
        public string codigo_sede { get; set; }
        public string codigo_sede_prestador { get; set; }
        public string correo_electronico { get; set; }
        public string departamento_prestador { get; set; }
        public string departamento_sede { get; set; }
        public string direccion { get; set; }
        public string ese { get; set; }
        public string fax { get; set; }
        public string fecha_apertura { get; set; }
        public string gerente { get; set; }
        public string municipio { get; set; }
        public string municipio_prestador { get; set; }
        public string municipio_sede { get; set; }
        public string naturaleza_juridica { get; set; }
        public string nivel_atencion { get; set; }
        public string nombre_prestador { get; set; }
        public string nombre_sede { get; set; }
        public string nombre_sede_prestador { get; set; }
        public string numero_documento { get; set; }
        public string sede { get; set; }
        public string sede_principal { get; set; }
        public string telefono { get; set; }
        public string tipo_documento { get; set; }
        public string zona { get; set; }
        public string serv_nombre { get; set; }
        public string dv { get; set; }
        public string indigena { get; set; }
        public string grupo_capacidad { get; set; }
        public string coca_codigo { get; set; }
        public string coca_nombre { get; set; }
        public string cantidad { get; set; }
        public string numero_placa { get; set; }
        public string modalidad { get; set; }
        public string modelo { get; set; }
        public string numero_tarjeta { get; set; }
        public string codigo_habilitacion { get; set; }
        public string numero_distintivo { get; set; }
        public string razon_social { get; set; }
        public string fecha_radicacion { get; set; }
        public string fecha_vencimiento { get; set; }
        public bool ambulatorio { get; set; }
        public bool hospitalario { get; set; }
        public bool unidad_movil { get; set; }
        public bool domiciliario { get; set; }
        public bool otras_extramural { get; set; }
        public bool centro_referencia { get; set; }
        public bool institucion_remisora { get; set; }
        public bool complejidad_baja { get; set; }
        public bool complejidad_media { get; set; }
        public bool complejidad_alta { get; set; }
        public string complejidad { get; set; }
        public string concepto { get; set; }

    }
}