using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Minsalud.ConsultaPrestadores.Models
{
    /// <summary>
    /// Modelo clave - valor
    /// </summary>
    public class MinsaludDetalleExternoModel
    {
        public string departamento { get; set; }
        public string municipio { get; set; }
        public string codigo_prestador { get; set; }
        public string nombre_prestador { get; set; }
        public string codigo_habilitacion { get; set; }
        public string numero_sede { get; set; }
        public string nombre { get; set; }
        public string gerente { get; set; }
        public string tipo_zona { get; set; }
        public string direccion { get; set; }
        public string barrio { get; set; }
        public string cepo_codigo { get; set; }
        public string centro_poblado { get; set; }
        public string telefono { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string fecha_apertura { get; set; }
        public string fecha_cierre { get; set; }
        public string nits_nit { get; set; }
        public string dv { get; set; }
        public string clase_persona { get; set; }
        public string naju_codigo { get; set; }
        public string naturaleza { get; set; }
        public string clpr_codigo { get; set; }
        public string clase_prestador { get; set; }
        public string ese { get; set; }
        public string nivel { get; set; }
        public string caracter { get; set; }
        public string sede_principal { get; set; }
        public string habilitado { get; set; }
        public string horario_lunes { get; set; }
        public string horario_martes { get; set; }
        public string horario_miercoles { get; set; }
        public string horario_jueves { get; set; }
        public string horario_viernes { get; set; }
        public string horario_sabado { get; set; }
        public string horario_domingo { get; set; }
        public string numero_sede_principal { get; set; }

        public string valor1 { get; set; }
        public string valor2 { get; set; }

        //dATOS ADICIONALES DE PRESTADORES
        public string depa_nombre { get; set; }
        public string muni_nombre { get; set; }
        public string razon_social { get; set; }
        public string clpr_nombre { get; set; }
        public string acreditado { get; set; }
        public string fecha_radicacion { get; set; }
        public string fecha_vencimiento { get; set; }
        public string naju_nombre { get; set; }

        //cAMPOS QUE FALTAN DE SERVICIOS
        public string habi_codigo_habilitacion { get; set; }
        public string sede_nombre { get; set; }
        public string grse_codigo { get; set; }
        public string grse_nombre { get; set; }
        public string serv_codigo { get; set; }
        public string serv_nombre { get; set; }
        public string ambulatorio { get; set; }
        public string hospitalario { get; set; }
        public string unidad_movil { get; set; }
        public string domiciliario { get; set; }
        public string otras_extramural { get; set; }
        public string centro_referencia { get; set; }
        public string institucion_remisora { get; set; }
        public string complejidad_baja { get; set; }
        public string complejidad_media { get; set; }
        public string complejidad_alta { get; set; }

        //Campos adicionales para capacidad
        public string indigena { get; set; }
        public string grupo_capacidad { get; set; }
        public string coca_codigo { get; set; }
        public string coca_nombre { get; set; }
        public string cantidad { get; set; }
        public string numero_placa { get; set; }
        public string modalidad { get; set; }
        public string modelo { get; set; }
        public string numero_tarjeta { get; set; }

        //Campos faltantes para seguridad
        public string numero_distintivo { get; set; }

        MinsaludDetalleExternoModel()
        {
            string defaultText = "--";
            departamento = defaultText;
            municipio = defaultText;
            codigo_prestador = defaultText;
            nombre_prestador = defaultText;
            codigo_habilitacion = defaultText;
            numero_sede = defaultText;
            nombre = defaultText;
            gerente = defaultText;
            tipo_zona = defaultText;
            direccion = defaultText;
            barrio = defaultText;
            cepo_codigo = defaultText;
            centro_poblado = defaultText;
            telefono = defaultText;
            fax = defaultText;
            email = defaultText;
            fecha_apertura = defaultText;
            fecha_cierre = defaultText;
            nits_nit = defaultText;
            dv = defaultText;
            clase_persona = defaultText;
            naju_codigo = defaultText;
            naturaleza = defaultText;
            clpr_codigo = defaultText;
            clase_prestador = defaultText;
            ese = defaultText;
            nivel = defaultText;
            caracter = defaultText;
            sede_principal = defaultText;
            habilitado = defaultText;
            horario_lunes = defaultText;
            horario_martes = defaultText;
            horario_miercoles = defaultText;
            horario_jueves = defaultText;
            horario_viernes = defaultText;
            horario_sabado = defaultText;
            horario_domingo = defaultText;
            numero_sede_principal = defaultText;

            valor1 = defaultText;
            valor2 = defaultText;

            //dATOS ADICIONALES DE PRESTADORES
            depa_nombre = defaultText;
            muni_nombre = defaultText;
            razon_social = defaultText;
            clpr_nombre = defaultText;
            acreditado = defaultText;
            fecha_radicacion = defaultText;
            fecha_vencimiento = defaultText;
            naju_nombre = defaultText;

            //cAMPOS QUE FALTAN DE SERVICIOS
            habi_codigo_habilitacion = defaultText;
            sede_nombre = defaultText;
            grse_codigo = defaultText;
            grse_nombre = defaultText;
            serv_codigo = defaultText;
            serv_nombre = defaultText;
            ambulatorio = defaultText;
            hospitalario = defaultText;
            unidad_movil = defaultText;
            domiciliario = defaultText;
            otras_extramural = defaultText;
            centro_referencia = defaultText;
            institucion_remisora = defaultText;
            complejidad_baja = defaultText;
            complejidad_media = defaultText;
            complejidad_alta = defaultText;

            //Campos adicionales para capacidad
            indigena = defaultText;
            grupo_capacidad = defaultText;
            coca_codigo = defaultText;
            coca_nombre = defaultText;
            cantidad = defaultText;
            numero_placa = defaultText;
            modalidad = defaultText;
            modelo = defaultText;
            numero_tarjeta = defaultText;

            //Campos faltantes para seguridad
            numero_distintivo = defaultText;
        }

    }
}