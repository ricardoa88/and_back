using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using tramites_servicios_webapi.Minsalud.ConsultaPrestadores.Models;


using System.Data;
using System.Diagnostics;
using FastMember;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;

using RestSharp;
using RestSharp.Authenticators;

namespace tramites_servicios_webapi.Minsalud.ConsultaPrestadores.Services
{
    /// <summary>
    /// Servicios Aportes Parafiscales del ICBF
    /// </summary>
    public class ConsultaPrestadoresService
    {
        private static readonly HttpClient _client;
        private const String _urlBase = "http://prestadores.minsalud.gov.co/WSREPS/REPS/Listas/values?parametro=";
        private const String _urlBaseDetalle = "http://prestadores.minsalud.gov.co/WSREPS/REPS/";

        static ConsultaPrestadoresService()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(_urlBase);
        }

        /// <summary>
        /// Consulta de lista de valores solicitada
        /// </summary>
        /// <param name="data">Modelo</param>
        /// <returns>Listado de valores solicitados</returns>
        public static async Task<JsonResult> GetListas(InputTipoLista data)
        {
            // Inicializaciones
            JsonResult resultado = null;  
            String _urlComplemento = _urlBase+data.Valor; 
            //Console.WriteLine("_urlComplemento="+_urlComplemento+".................");
            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (new MediaTypeWithQualityHeaderValue ("application/json"));
            StringContent stringContent = new StringContent ("", Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), _urlComplemento);

            //Petición
            ////Console.WriteLine("Paso 1.........");
            try {
                HttpResponseMessage resp = await _client.SendAsync(request);

                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     List<CodigoNombreModel> listaModel = JsonConvert.DeserializeObject<List<CodigoNombreModel>> (response);

                    //Se mapea el resultado al objeto que requerido por las listas
                     var selectListItems = new List<SelectListItemModel> ();
                         SelectListItemModel selectListItem = new SelectListItemModel();
                         selectListItem.Text = "Seleccionar..";
                         selectListItem.Value = "";
                         selectListItems.Add(selectListItem);
                     foreach(CodigoNombreModel dato in listaModel)
                     {
                         selectListItem = new SelectListItemModel();
                         selectListItem.Text = dato.nombre;

                        //Si es ESE se pone el nombre en el valor, debido a que por ese es que se filtra
                        if(data.Valor=="ESE"){
                            selectListItem.Value = dato.nombre;
                        }else{
                            selectListItem.Value = dato.codigo;
                        }

                         
                         selectListItems.Add(selectListItem);
                     }

                     resultado = new JsonResult(selectListItems);

                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                ////Console.WriteLine (e.Message);
                //Console.ReadLine ();
            }
              
            ////Console.WriteLine("Paso 3.........");
            //Salida
            return resultado;
        }


        public static string generarUrlDetalle(string tipo , MinsaludFiltroModel data){
            String _urlComplemento = ""; 
            String _urlParametros = "";
            if(data.historia=="Habilitados"){
                //***Datos con parametros
                _urlParametros = "/values?codigo_habilitacion="+data.codigo_prestador+"&nombre="+data.nombre_prestador+"&nits_nit="+data.numero_documento+"&clpr_codigo="+data.clase_prestador+"&ese="+data.ese+"&gerente=&nivel="+data.nivel_atencion+"&caracter="+data.caracter_territorial+"&acreditado=&habilitado=&fecha_radicacion=&fecha_vencimiento=&fecha_cierre=&depa_codigo="+data.departamento_prestador+"&muni_codigo="+data.municipio_prestador+"&clase_persona=&naju_codigo="+data.naturaleza_juridica;
                //***Datos quemados
                //_urlParametros = "/values?codigo_habilitacion=&nombre=&nits_nit=899999026&clpr_codigo=1&ese=&gerente=&nivel=&caracter=&acreditado=&habilitado=&fecha_radicacion=&fecha_vencimiento=&fecha_cierre=&depa_codigo=&muni_codigo=&clase_persona=&naju_codigo=";
                
                _urlComplemento = _urlBaseDetalle + data.historia + tipo +_urlParametros;                 
            }
            
            if(data.historia=="Sedes"){
                //***Datos quemados
                //_urlParametros = "/values?habi_codigo_habilitacion=&codigo_habilitacion=9126300019&numero_sede=&nombre=&tipo_zona=RURAL&nits_nit=&clpr_codigo=&ese=SI&nivel=&caracter=DEPARTAMENTAL&sede_principal=NO&habilitado=&fecha_apertura=&fecha_cierre=&habi_depa_codigo=&habi_muni_codigo=&clase_persona=&naju_codigo=4&sede_depa_codigo=&sede_muni_codigo=&gerente=&estado_sede=";
                //***Datos con parametros
                _urlParametros = "/values?habi_codigo_habilitacion="+data.codigo_prestador+"&codigo_habilitacion="+data.codigo_sede+"&numero_sede=&nombre="+data.nombre_sede+"&tipo_zona="+data.zona+"&nits_nit="+data.numero_documento+"&clpr_codigo="+data.clase_prestador+"&ese="+data.ese+"&nivel="+data.nivel_atencion+"&caracter="+data.caracter_territorial+"&sede_principal="+data.sede_principal+"&habilitado=&fecha_apertura=&fecha_cierre=&habi_depa_codigo="+data.departamento_prestador+"&habi_muni_codigo="+data.municipio_prestador+"&clase_persona=&naju_codigo="+data.naturaleza_juridica+"&sede_depa_codigo="+data.departamento_sede+"&sede_muni_codigo="+data.municipio_sede+"&gerente=&estado_sede=";
                _urlComplemento = _urlBaseDetalle + data.historia + tipo +_urlParametros;                 
            }

            if(data.historia=="Servicios"){
                //***Datos quemados
                //_urlParametros = "/values?habi_codigo_habilitacion=7348301012&codigo_habilitacion=&numero_sede=&serv_codigo=&grse_codigo=&sede_depa_codigo=&sede_muni_codigo=&ambulatorio=&hospitalario=&unidad_movil=&domiciliario=&otras_extramural=&centro_referencia=&institucion_remisora=&complejidad_baja=&complejidad_media=&complejidad_alta=&numero_distintivo=&nits_nit=&clpr_codigo=&ese=&nivel=&caracter=&indigena=&habilitado=&clase_persona=&naju_codigo=&estado_servicio=&estado_sede=";
                //***Datos con parametros
                _urlParametros = "/values?habi_codigo_habilitacion="+data.codigo_prestador+"&codigo_habilitacion="+data.codigo_sede+"&numero_sede=&serv_codigo="+data.servicio_nombre+"&grse_codigo="+data.grupo+"&sede_depa_codigo="+data.departamento_sede+"&sede_muni_codigo"+data.municipio_sede+"=&ambulatorio="+data.intramural_ambulatorio+"&hospitalario="+data.intramural_hospitalario+"&unidad_movil="+data.extramural_unidad+"&domiciliario="+data.extramural_domiciliario+"&otras_extramural="+data.extramural_otras+"&centro_referencia="+data.telemedicina_centro+"&institucion_remisora="+data.telemedicina_institucion+"&complejidad_baja="+data.complejidad_baja+"&complejidad_media="+data.complejidad_media+"&complejidad_alta="+data.complejidad_alta+"&numero_distintivo=&nits_nit="+data.numero_documento+"&clpr_codigo="+data.clase_prestador+"&ese="+data.ese+"&nivel="+data.nivel_atencion+"&caracter="+data.caracter_territorial+"&indigena=&habilitado=&clase_persona=&naju_codigo="+data.naturaleza_juridica+"&estado_servicio=&estado_sede=";
                _urlComplemento = _urlBaseDetalle + data.historia + tipo +_urlParametros;                 
            }

            if(data.historia=="Capacidad"){
                //***Datos quemados
                //_urlParametros = "/values?habi_codigo_habilitacion=7348301012&codigo_habilitacion=&numero_sede=&coca_codigo=&coca_grupo_capacidad=&sede_depa_codigo=&sede_muni_codigo=&nits_nit=&clpr_codigo=&ese=&nivel=&caracter=&indigena=&habilitado=&clase_persona=&naju_codigo=&estado_sede=&numero_placa=";
                //***Datos con parametros
                _urlParametros = "/values?habi_codigo_habilitacion="+data.codigo_prestador+"&codigo_habilitacion="+data.codigo_sede+"&numero_sede=&coca_codigo="+data.grupo+"&coca_grupo_capacidad="+data.concepto+"&sede_depa_codigo="+data.departamento_sede+"&sede_muni_codigo="+data.municipio_sede+"&nits_nit="+data.numero_documento+"&clpr_codigo="+data.clase_prestador+"&ese="+data.ese+"&nivel="+data.nivel_atencion+"&caracter="+data.caracter_territorial+"&indigena=&habilitado=&clase_persona=&naju_codigo="+data.naturaleza_juridica+"&estado_sede=&numero_placa=";
                _urlComplemento = _urlBaseDetalle + data.historia + tipo +_urlParametros;                 
            }

            if(data.historia=="Medidas"){
                //***Datos quemados
                //_urlParametros = "/values?habi_codigo_habilitacion=0500104260&codigo_habilitacion=&numero_sede=&serv_codigo=&grse_codigo=&sede_depa_codigo=&sede_muni_codigo=&ambulatorio=&hospitalario=&unidad_movil=&domiciliario=&otras_extramural=&centro_referencia=&institucion_remisora=&complejidad_baja=&complejidad_media=&complejidad_alta=&numero_distintivo=&nits_nit=&clpr_codigo=&ese=&nivel=&caracter=&indigena=&habilitado=&clase_persona=&naju_codigo=&estado_servicio=&estado_sede=";
                //***Datos con parametros
                _urlParametros = "/values?habi_codigo_habilitacion="+data.codigo_prestador+"&codigo_habilitacion="+data.codigo_sede+"&numero_sede=&serv_codigo="+data.servicio_nombre+"&grse_codigo="+data.grupo+"&sede_depa_codigo="+data.departamento_sede+"&sede_muni_codigo="+data.municipio_sede+"&ambulatorio="+data.intramural_ambulatorio+"&hospitalario="+data.intramural_hospitalario+"&unidad_movil="+data.extramural_unidad+"&domiciliario="+data.extramural_domiciliario+"&otras_extramural="+data.extramural_otras+"&centro_referencia="+data.telemedicina_centro+"&institucion_remisora="+data.telemedicina_institucion+"&complejidad_baja="+data.complejidad_baja+"&complejidad_media="+data.complejidad_media+"&complejidad_alta="+data.complejidad_alta+"&numero_distintivo=&nits_nit="+data.numero_documento+"&clpr_codigo="+data.clase_prestador+"&ese="+data.ese+"&nivel="+data.nivel_atencion+"&caracter="+data.caracter_territorial+"&indigena=&habilitado=&clase_persona=&naju_codigo="+data.naturaleza_juridica+"&estado_servicio=&estado_sede=";
                _urlComplemento = _urlBaseDetalle + data.historia + tipo +_urlParametros;                 
            }

            if(data.historia=="Sanciones"){
                //***Datos quemados
                //_urlParametros = "/values?habi_codigo_habilitacion=&codigo_habilitacion=&numero_sede=&serv_codigo=&grse_codigo=&sede_depa_codigo=&sede_muni_codigo=&ambulatorio=&hospitalario=&unidad_movil=&domiciliario=&otras_extramural=&centro_referencia=&institucion_remisora=&complejidad_baja=&complejidad_media=&complejidad_alta=&numero_distintivo=&nits_nit=&clpr_codigo=&ese=&nivel=&caracter=&indigena=&habilitado=&clase_persona=&naju_codigo=&estado_servicio=&estado_sede=";
                //***Datos con parametros
                _urlParametros = "/values?habi_codigo_habilitacion="+data.codigo_prestador+"&codigo_habilitacion="+data.codigo_sede+"&numero_sede=&serv_codigo="+data.servicio_nombre+"&grse_codigo="+data.grupo+"&sede_depa_codigo="+data.departamento_sede+"&sede_muni_codigo="+data.municipio_sede+"&ambulatorio="+data.intramural_ambulatorio+"&hospitalario="+data.intramural_hospitalario+"&unidad_movil="+data.extramural_unidad+"&domiciliario="+data.extramural_domiciliario+"&otras_extramural="+data.extramural_otras+"&centro_referencia="+data.telemedicina_centro+"&institucion_remisora="+data.telemedicina_institucion+"&complejidad_baja="+data.complejidad_baja+"&complejidad_media="+data.complejidad_media+"&complejidad_alta="+data.complejidad_alta+"&numero_distintivo=&nits_nit="+data.numero_documento+"&clpr_codigo="+data.clase_prestador+"&ese="+data.ese+"&nivel="+data.nivel_atencion+"&caracter="+data.caracter_territorial+"&indigena=&habilitado=&clase_persona=&naju_codigo="+data.naturaleza_juridica+"&estado_servicio=&estado_sede=";
                _urlComplemento = _urlBaseDetalle + data.historia + tipo +_urlParametros;                 
            }

            return _urlComplemento;
        }
         
        /// <summary>
        /// Consulta de lista de valores solicitada
        /// </summary>
        /// <param name="data">Modelo</param>
        /// <returns>Listado de valores solicitados</returns>
        public static async Task<JsonResult> getDetalle(MinsaludFiltroModel data)
        {
            // Inicializaciones
            JsonResult resultado = null;  
            String _urlComplemento = ""; 
            //String _urlParametros = "";
            int id=0;

            //Se genera la URL de manera dinámica, según de cual pestaña provenga la petición
            _urlComplemento = generarUrlDetalle(null , data);

            //Console.WriteLine("getDetalle _urlComplemento="+_urlComplemento+".................");
            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (new MediaTypeWithQualityHeaderValue ("application/json"));
            StringContent stringContent = new StringContent ("", Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), _urlComplemento);
            try {
                HttpResponseMessage resp = await _client.SendAsync(request);

                if (resp.IsSuccessStatusCode) {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    List<MinsaludDetalleExternoModel> listaModel = JsonConvert.DeserializeObject<List<MinsaludDetalleExternoModel>> (response);

                    //se inicializa un id autogenerado
                    id=0;

                    //Se mapea el resultado al objeto que requerido por las listas
                    var items = new List<MinsaludDetalleModel> ();
                    var item = new MinsaludDetalleModel();
                    var listaModelLen = listaModel.Count < 1000 ? listaModel.Count: 1000;
                  
                    for (int i = 0; i < listaModelLen; i++)
                    {
                        MinsaludDetalleExternoModel dato = listaModel[i];
                         item.nombre_prestador  = ValidateField(dato.nombre_prestador);
                            item.codigo_sede_prestador  = ValidateField(dato.codigo_prestador);
                            item.sede  =ValidateField("");
                            item.nombre_sede_prestador  =  ValidateField(dato.sede_nombre);
                            item.zona  = ValidateField(dato.tipo_zona);
                            item.direccion  = ValidateField(dato.direccion);
                            item.telefono  = ValidateField(dato.telefono);
                            item.tipo_documento  =  ValidateField("");
                            item.numero_documento  = ValidateField(dato.nits_nit);
                            item.naturaleza_juridica  = ValidateField(dato.naturaleza);
                            item.departamento_prestador  = ValidateField(dato.departamento);
                            item.municipio_prestador  = ValidateField(dato.municipio);
                            item.codigo_prestador  = ValidateField(dato.codigo_prestador);
                            item.nombre_prestador  = ValidateField(dato.nombre_prestador);
                            item.clase_prestador  = ValidateField(dato.clase_prestador);
                            item.ese  = ValidateField(dato.ese);
                            item.nivel_atencion  = ValidateField(dato.nivel);
                            item.caracter_territorial  = ValidateField(dato.caracter);
                            item.departamento_sede  = ValidateField(dato.departamento);
                            item.municipio_sede  = ValidateField(dato.municipio);
                            item.codigo_sede  = ValidateField("");
                            item.nombre_sede  = ValidateField("");
                            item.sede_principal  =ValidateField( dato.numero_sede_principal);
                            item.gerente  = ValidateField(dato.gerente);
                            item.zona  = ValidateField(dato.tipo_zona);
                            item.direccion  = ValidateField(dato.direccion);
                            item.barrio  = ValidateField(dato.barrio);
                            item.centro_poblado  = ValidateField(dato.centro_poblado);
                            item.fax  = ValidateField(dato.fax);
                            item.telefono  = ValidateField(dato.telefono);
                            item.correo_electronico  = ValidateField(dato.email);
                            item.fecha_apertura  = ValidateField(dato.fecha_apertura);
                            item.razon_social  = ValidateField(dato.razon_social);
                            item.fecha_radicacion  = ValidateField(dato.fecha_radicacion);
                            item.fecha_vencimiento  = ValidateField(dato.fecha_vencimiento);

                            //se adiciona el id
                            item.id  = id;

                            item.ambulatorio  = (dato.ambulatorio=="SI");
                            item.hospitalario  = (dato.hospitalario=="SI");
                            item.unidad_movil  = (dato.unidad_movil=="SI");
                            item.domiciliario  = (dato.domiciliario=="SI");
                            item.otras_extramural  = (dato.otras_extramural=="SI");
                            item.centro_referencia  = (dato.centro_referencia=="SI");
                            item.institucion_remisora  = (dato.institucion_remisora=="SI");

                            item.complejidad_baja  = (dato.complejidad_baja=="SI");
                            item.complejidad_media  = (dato.complejidad_media=="SI");
                            item.complejidad_alta  = (dato.complejidad_alta=="SI");

                            if(item.complejidad_baja)
                                item.complejidad="complejidad_baja";
                            if(item.complejidad_media)
                                item.complejidad="complejidad_media";
                            if(item.complejidad_alta)
                                item.complejidad="complejidad_alta";
                        
                        //Se asignan campos si la pestaña de Habilitados
                        if(data.historia=="Habilitados"){
                            item.nombre_prestador  = ValidateField(dato.nombre);
                            item.codigo_prestador  = ValidateField(dato.codigo_habilitacion);
                            item.departamento_prestador = ValidateField(dato.depa_nombre);
                            item.municipio_prestador = ValidateField(dato.muni_nombre);
                        }

                        if(data.historia=="Sedes"){
                            item.nombre_sede  = ValidateField(dato.nombre);
                            item.codigo_sede  = ValidateField(dato.codigo_habilitacion);
                            item.departamento_sede = ValidateField(dato.departamento);
                            item.municipio_sede = ValidateField(dato.municipio);
                            item.departamento_prestador = ValidateField(dato.departamento);
                            item.municipio_prestador = ValidateField(dato.municipio);
                            item.sede_principal = ValidateField(dato.sede_principal);
                            item.gerente = ValidateField(dato.gerente);
                            item.fecha_radicacion  = ValidateField(dato.fecha_apertura);
                            item.fecha_vencimiento  = ValidateField(dato.fecha_cierre);
                        }

                        //Se asignan campos si la pestaña de Servicios
                        if(data.historia=="Servicios"){
                            
                            item.codigo_prestador  = ValidateField(dato.codigo_habilitacion);
                            item.clase_prestador  = ValidateField(dato.clpr_nombre);
                            item.nombre_sede  = ValidateField(dato.sede_nombre);
                            item.codigo_sede  = ValidateField(dato.numero_sede);
                            item.departamento_sede = ValidateField(dato.depa_nombre);
                            item.municipio_sede = ValidateField(dato.muni_nombre);
                            item.departamento_prestador = ValidateField(dato.depa_nombre);
                            item.municipio_prestador = ValidateField(dato.muni_nombre);
                            item.serv_nombre = ValidateField(dato.serv_nombre);
                            item.dv = ValidateField(dato.dv);
                            item.naturaleza_juridica  = ValidateField(dato.naju_nombre);
                            item.grupo_capacidad  = ValidateField(dato.grse_nombre);
                            item.numero_distintivo  = ValidateField(dato.numero_distintivo);
                        }

                        //Se asignan campos si la pestaña de Capacidad
                        if(data.historia=="Capacidad"){
                            item.codigo_sede  = ValidateField(dato.codigo_habilitacion);
                            item.nombre_sede  = ValidateField(dato.sede_nombre);
                            item.codigo_prestador  = ValidateField(dato.habi_codigo_habilitacion);
                            item.nombre_prestador  = ValidateField(dato.nombre_prestador);
                            item.departamento_prestador = ValidateField(dato.depa_nombre);
                            item.municipio_prestador = ValidateField(dato.muni_nombre);
                            item.departamento_sede = ValidateField(dato.depa_nombre);
                            item.municipio_sede = ValidateField(dato.muni_nombre);
                            item.grupo_capacidad = ValidateField(dato.grupo_capacidad);
                            item.cantidad = ValidateField(dato.cantidad);
                            item.numero_placa = ValidateField(dato.numero_placa);
                            item.modalidad = ValidateField(dato.modalidad);
                            item.modelo = ValidateField(dato.modelo);
                            item.numero_tarjeta = ValidateField(dato.numero_tarjeta);
                            item.naturaleza_juridica  = ValidateField(dato.naju_nombre);
                            item.clase_prestador  = ValidateField(dato.clpr_nombre);
                            item.grupo_capacidad  = ValidateField(dato.grupo_capacidad);
                            item.cantidad  = ValidateField(dato.cantidad);
                            item.concepto  = ValidateField(dato.coca_nombre);
                        }


                        //Se asignan campos si la pestaña de Medidas de seguridad
                        if(data.historia=="Medidas"){
                            item.codigo_sede  = ValidateField(dato.codigo_habilitacion);
                            item.nombre_sede  = ValidateField(dato.sede_nombre);
                            item.codigo_prestador  = ValidateField(dato.habi_codigo_habilitacion);
                            item.nombre_prestador  = ValidateField(dato.nombre_prestador);
                            item.departamento_prestador = ValidateField(dato.depa_nombre);
                            item.municipio_prestador = ValidateField(dato.muni_nombre);
                            item.departamento_sede = ValidateField(dato.depa_nombre);
                            item.municipio_sede = ValidateField(dato.muni_nombre);
                            item.serv_nombre = ValidateField(dato.serv_nombre);
                            item.numero_distintivo = ValidateField(dato.numero_distintivo);
                            item.naturaleza_juridica  = ValidateField(dato.naju_nombre);
                            item.clase_prestador  = ValidateField(dato.clpr_nombre);
                            item.grupo_capacidad  = ValidateField(dato.grse_nombre);
                        }

                        //Se asignan campos si la pestaña de Sanciones
                        if(data.historia=="Sanciones"){
                            item.codigo_sede  = ValidateField(dato.codigo_habilitacion);
                            item.nombre_sede  = ValidateField(dato.sede_nombre);
                            item.codigo_prestador  = ValidateField(dato.habi_codigo_habilitacion);
                            item.nombre_prestador  = ValidateField(dato.nombre_prestador);
                            item.departamento_prestador = ValidateField(dato.depa_nombre);
                            item.municipio_prestador = ValidateField(dato.muni_nombre);
                            item.departamento_sede = ValidateField(dato.depa_nombre);
                            item.municipio_sede = ValidateField(dato.muni_nombre);
                            item.serv_nombre = ValidateField(dato.serv_nombre);
                            item.numero_distintivo = ValidateField(dato.numero_distintivo);
                            item.naturaleza_juridica  = ValidateField(dato.naju_nombre);
                            item.clase_prestador  = ValidateField(dato.clpr_nombre);
                            item.grupo_capacidad  = ValidateField(dato.grse_nombre);
                        }

                         items.Add(item);
                         id += 1;
                    }


                    resultado = new JsonResult(items);

                }

            } catch (Exception e) {}
            return resultado;
        }

        /// <summary>
        /// Consulta de lista de valores solicitada
        /// </summary>
        /// <param name="data">Modelo</param>
        /// <returns>Listado de valores solicitados</returns>
        public static async Task<FileContentResult> getDetalleExcel(MinsaludFiltroModel data)
        {
            // Inicializaciones
            FileContentResult file = null;
            string _urlComplemento = generarUrlDetalle("Excel" , data);          

            var client = new RestClient(_urlComplemento);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Content-Type", "application/vnd.ms-excel");
            request.AddHeader("content-disposition", "attachment; filename=myfile.xlsx");
            IRestResponse response = client.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK) {
                return null;                
            }
            else {
                var responseExcel = await client.ExecuteTaskAsync(request);
                byte[] fileBytes = Convert.FromBase64String(responseExcel.Content);
                file = new FileContentResult (fileBytes, "application/vnd.ms-excel");
            }
            return file;
        }

        private static string ValidateField(string field){
            string defaultText = "--";
            return (field != null && field.Trim().Length > 0 ? field : defaultText);
        }
    }
}
