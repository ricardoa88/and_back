using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Minsalud.ConsultaPrestadores.Models;
using tramites_servicios_webapi.Minsalud.ConsultaPrestadores.Services;

namespace tramites_servicios_webapi.Minsalud.ConsultaPrestadores.Controllers
{
    /// <summary>
    /// Aportes parafiscales del ICBF
    /// </summary>
    [Route ("api/minsalud/[controller]")]
    [ApiController]
    public class ConsultaPrestadoresController : ControllerBase 
    {

        /// <summary>
        /// Consulta las listas utilizadas en minsalud
        /// </summary>
        /// <param name="data">Objeto con tipo de lista a consultar</param>
        /// <returns>Listado de resultados</returns>
        [HttpPost ("listas")]
        public async Task<IActionResult> GetListas ([FromBody] InputTipoLista data) 
        {
            var respuesta = await ConsultaPrestadoresService.GetListas(data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }


        /// <summary>
        /// Consulta las listas utilizadas en minsalud
        /// </summary>
        /// <param name="data">Objeto con tipo de lista a consultar</param>
        /// <returns>Listado de resultados</returns>
        [HttpPost ("detalle")]
        public async Task<IActionResult> getDetalle([FromBody] MinsaludFiltroModel data) 
        {
            var respuesta = await ConsultaPrestadoresService.getDetalle(data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }

        /// <summary>
        /// Obtiene detalle
        /// </summary>
        /// <param name="data">Objeto con tipo de lista a consultar</param>
        /// <returns>Listado de resultados</returns>
        [HttpPost ("detalleExcel")]
        public async Task<IActionResult> getDetalleExcel([FromBody] MinsaludFiltroModel data) 
        {
            var respuesta = await ConsultaPrestadoresService.getDetalleExcel(data);

            if(respuesta == null){
                return BadRequest ("No fue posible descargar el documento generado.");            
            }
            
            return respuesta as FileContentResult;
        }

        
    }
}