using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.common.Models;
using tramites_servicios_webapi.Fna.ReciboPago.Models;
using tramites_servicios_webapi.Fna.ReciboPago.Services;

namespace tramites_servicios_webapi.Fna.ReciboPago.Controllers
{
    /// <summary>
    /// Recibo de pago créditos Colpensiones
    /// </summary>
    [Route ("api/fna/[controller]")]
    [ApiController]
    public class ReciboPagoController : ControllerBase 
    {
        /// <summary>
        /// Iniciar sesión usuario FNA
        /// </summary>
        /// <param name="data">Usuario y Contraseña</param>
        /// <returns>Estado login</returns>
        [HttpPost ("login")]
        public async Task<IActionResult> Login ([FromBody] LoginFNAModel data) 
        {
            var respuesta = await ReciboPagoService.Login (data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }

        /// <summary>
        /// Créditos fna
        /// </summary>
        /// <param name="data">Identificación del usuario</param>
        /// <returns>Listado de créditos</returns>
        [HttpPost ("list")]
        public async Task<IActionResult> List (IdentificacionFNAModel data) 
        {
            var respuesta = await ReciboPagoService.List (data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }

        /// <summary>
        /// Descarga los certificados de afiliación
        /// </summary>
        /// <param name="data">Objeto con los datos del solicitanter</param>
        /// <returns>Certificado de afiliación</returns>
        [HttpPost ("descargar")]
        public async Task<IActionResult> Descargar (RadicadoModel data) 
        {
            var respuesta = await ReciboPagoService.Descargar (data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }
        
    }
}