
namespace tramites_servicios_webapi.Fna.ReciboPago.Models {
    /// <summary>
    /// Modelo con la información de identificación
    /// </summary>
    public class IdentificacionFNAModel
    {
        /// <summary>
        /// Tipo de documento 
        /// </summary>
        public string TipoDocumento { get; set; }

        /// <summary>
        /// Numero de documento
        /// </summary>
         public string NumeroDocumento { get; set; }
    }
}