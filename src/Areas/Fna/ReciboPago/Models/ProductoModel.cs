namespace tramites_servicios_webapi.Fna.ReciboPago.Models
{
    /// <summary>
    /// Listado de productos usuario FNA
    /// </summary>
    public class ProductoModel
    {
        /// <summary>
        /// Tipo del producto
        /// </summary>
        public TipoProductoModel tipo { get; set; }

        /// <summary>
        /// Numero del producto
        /// </summary>
        public string numero { get; set; }

        /// <summary>
        /// Codigo del estado
        /// </summary>
        public string codigoEstado { get; set; }

        /// <summary>
        /// Codigo de clase
        /// </summary>
        public string codigoClase { get; set; }

        /// <summary>
        /// Fecha de creación
        /// </summary>
        public string fechaCreacion { get; set; }

        /// <summary>
        /// Saldo
        /// </summary>
        public string saldo { get; set; }

        /// <summary>
        /// Empresa
        /// </summary>
        public string Empresa { get; set; }

        /// <summary>
        /// Monto Desembolso
        /// </summary>
        public string montoDesembolso { get; set; }

        /// <summary>
        /// Valor de la Cuota
        /// </summary>
        public string valorCuota { get; set; }

        /// <summary>
        /// Fecha de Pago Cuota
        /// </summary>
        public string fechaPagoCuota { get; set; }

        /// <summary>
        /// Valor Cuota Con Seguro
        /// </summary>
        public string valorCuotaConSeguro { get; set; }

        /// <summary>
        /// Saldo Vencido
        /// </summary>
        public string saldoVencido { get; set; } 
    }

    /// <summary>
    /// Tipo de productos usuario FNA
    /// </summary>
    public class TipoProductoModel
    {
        /// <summary>
        /// Codigo del tipo 
        /// </summary>
        public string codigo { get; set; }

        /// <summary>
        /// Nombre del tipo
        /// </summary>
        public string nombre { get; set; }

    }
}