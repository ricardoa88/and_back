namespace tramites_servicios_webapi.Fna.ReciboPago.Models
{
    /// <summary>
    /// Listado de creditos FNA
    /// </summary>
    public class CreditoModel
    {
        /// <summary>
        /// Nombre del crédito
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Número del crédito
        /// </summary>
        public string Numero { get; set; }

         /// <summary>
        /// Estado del crédito
        /// </summary>
        public string Estado { get; set; }

        /// <summary>
        /// Valor del crédito
        /// </summary>
        public string Valor { get; set; }
    }
}