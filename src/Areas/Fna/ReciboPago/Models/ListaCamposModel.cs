namespace tramites_servicios_webapi.Fna.ReciboPago.Models
{
    /// <summary>
    /// Radicado producto fna
    /// </summary>
    public class ListaCamposModel
    {
        /// <summary>
        /// Campo
        /// </summary>
        public string Campo { get; set; }

        /// <summary>
        /// Llave
        /// </summary>
        public string Llave { get; set; }

        /// <summary>
        /// Valor
        /// </summary>
        public string Valor { get; set; }
    }
}