namespace tramites_servicios_webapi.Fna.ReciboPago.Models
{
    /// <summary>
    /// Radicado producto fna
    /// </summary>
    public class RadicadoModel
    {
        /// <summary>
        /// Número del crédito
        /// </summary>
        public string Numero { get; set; }
    }
}