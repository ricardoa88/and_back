using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Fna.ReciboPago.Models {
    /// <summary>
    /// Modelo información usuario FNA
    /// </summary>
    public class UsuarioFnaModel
    {
        /// <summary>
        /// Nombre usuario
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// Tipo de documento
        /// </summary>
        public string TipoDocumento { get; set; }

        /// <summary>
        /// Numero de documento
        /// </summary>
        public string Documento { get; set; }
    }
}