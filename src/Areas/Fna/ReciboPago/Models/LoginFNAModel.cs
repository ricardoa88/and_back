namespace tramites_servicios_webapi.Fna.ReciboPago.Models {
    public class LoginFNAModel
    {
        /// <summary>
        /// Usuario 
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Contraseña usuario 
        /// </summary>
         public string Password { get; set; }
    }
}