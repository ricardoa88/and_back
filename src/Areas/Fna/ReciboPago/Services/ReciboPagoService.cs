using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using tramites_servicios_webapi.common.Models;
using tramites_servicios_webapi.Fna.ReciboPago.Models;
using System.Text.RegularExpressions;

namespace tramites_servicios_webapi.Fna.ReciboPago.Services
{
    public class ReciboPagoService
    {
        private static readonly HttpClient _client;
        private const String _urlBase = "https://192.168.1.46:8243/";
        private const String _urlToken = "token";
        private const String _urlLogin = "fna-login/v1";
        private const String _urlListProductos = "fna-productos-cliente/v1";
        private const String _urlRadicadoFactura = "fna-radicados-factura/v1";
        private const String _urlCodigoFactura = "fna-codigo-factura/v1";
        private const String _urlDescargarFactura = "fna-obtener-factura/v1";

        static ReciboPagoService()
        {
            //Omitit Certificado SSL
            var handler = new HttpClientHandler();
            handler.ClientCertificateOptions = ClientCertificateOption.Manual;
            handler.ServerCertificateCustomValidationCallback =
                (httpRequestMessage, cert, cetChain, policyErrors) =>
                {
                    return true;
                };

            _client = new HttpClient(handler);
            _client.BaseAddress = new Uri(_urlBase);
        }
        /// <summary>
        /// Iniciar sesión usuario FNA
        /// </summary>
        /// <param name="data">Usuario y Contaseña FNA</param>
        /// <returns>Estado del login</returns>
        public static async Task<JsonResult> Login(LoginFNAModel data)
        {
            // Inicializaciones
            JsonResult resultado = null; 
            var responseModel = new ResponseModel();   

            //Token
            TokenModel tokenModel = await ReciboPagoService.ObtenerToken();

            //Body XML
            XNamespace ns = "http://www.w3.org/2003/05/soap-envelope";
            XNamespace v1 = "http://www.fna.gov.co/esb/services/soap/seguridad/autenticarclientes/v1";

            //Soap Request
            XDocument soapRequest = new XDocument(
                new XElement(ns + "Envelope",
                    new XAttribute(XNamespace.Xmlns + "v1", v1),
                    new XAttribute(XNamespace.Xmlns + "soap", ns),
                    new XElement(ns + "Header",
                         new XElement(v1 + "headerRq",
                            new XElement("systemId", 002),
                            new XElement("clientTransactionID", 1575477048960),
                            new XElement("endHost", "127.0.0.1"),
                            new XElement("endUser", "avictoria"),
                            new XElement("datetime", "2016-03-30T11:23:42-05:00")                            
                        )
                    ),
                    new XElement(ns + "Body",
                         new XElement(v1 + "autenticarclienteRq",
                            new XElement("cliente",
                                new XElement("login", data.User),
                                new XElement("clave", data.Password),
                                new XElement("codCanal", 1)
                            )
                        )
                    )   
                )
            ); 

            //Default Request Headers
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("SOAPAction", "autenticarcliente");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "Y254X2VzYl9nb3Zjb19jOllCWWtVNyNo");
            _client.DefaultRequestHeaders.Add("Token", "Bearer "+tokenModel.AccessToken);
            StringContent stringContent = new StringContent(soapRequest.ToString(), Encoding.UTF8, "text/xml");

            //Petición API Gateway     
            try
            {
                HttpResponseMessage resp = await _client.PostAsync(_urlLogin, stringContent);
                if (resp.IsSuccessStatusCode)
                {
                    Task<Stream> streamTask = resp.Content.ReadAsStreamAsync();
                    Stream stream = streamTask.Result;
                    var sr = new StreamReader(stream);
                    var soapResponse = XDocument.Load(sr);
                  
                    var statusXml = soapResponse.Descendants("status").FirstOrDefault().ToString();
                    XmlDocument statusDoc = new XmlDocument();
                    statusDoc.LoadXml(statusXml);
                    var statusJson = Newtonsoft.Json.JsonConvert.SerializeXmlNode(statusDoc);
                    dynamic statusJsonObj = JsonConvert.DeserializeObject(statusJson);
                    var status = statusJsonObj["status"]["statusType"];

                    if(status == "ERROR")
                    {
                         responseModel.Message = statusJsonObj["status"]["systemDesc"];
                    }
                    else
                    {
                        var xml = soapResponse.Descendants("datosCliente").FirstOrDefault().ToString();
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(xml);
                        var json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);

                        var usuarioFna = new UsuarioFnaModel();
                        
                        //Salida XML->JSON
                        dynamic jsonObj = JsonConvert.DeserializeObject(json);
                        usuarioFna.Nombre = jsonObj["datosCliente"]["personaNatural"]["nombreCompleto"];
                        usuarioFna.Documento = jsonObj["datosCliente"]["personaNatural"]["identificacion"]["numeroDocumento"];
                        usuarioFna.TipoDocumento = jsonObj["datosCliente"]["personaNatural"]["identificacion"]["tipoDocumento"];

                        responseModel.Success = true;
                        responseModel.Result= usuarioFna;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            //Success
            resultado = new JsonResult(responseModel);
            return resultado; 
        }

        /// <summary>
        /// Listado de creditos fna
        /// </summary>
        /// <param name="data">Identifcación del usuario fna</param>
        /// <returns>Listado de créditos</returns>
        public static async Task<JsonResult> List(IdentificacionFNAModel data)
        {
            // Inicializaciones
            JsonResult resultado = null; 
            var responseModel = new ResponseModel();   
            var creditos = new List<CreditoModel>();

            //Token
            TokenModel tokenModel = await ReciboPagoService.ObtenerToken();

            //Body XML
            XNamespace ns = "http://www.w3.org/2003/05/soap-envelope";
            XNamespace v2 = "http://www.fna.gov.co/esb/services/soap/clnteprsnantral/recuperarproductosV2/v2";

            //Soap Request
            XDocument soapRequest = new XDocument(
                new XElement(ns + "Envelope",
                    new XAttribute(XNamespace.Xmlns + "v2", v2),
                    new XAttribute(XNamespace.Xmlns + "soap", ns),
                    new XElement(ns + "Header",
                         new XElement(v2 + "headerRq",
                            new XElement("systemId", 002),
                            new XElement("endHost", "localhost"),
                            new XElement("endUser", "GOVCO")                        
                        )
                    ),
                    new XElement(ns + "Body",
                         new XElement(v2 + "RecuperarProdIdentificacionRq",
                            new XElement("identificacion",
                                new XElement("numeroDocumento", data.NumeroDocumento),
                                new XElement("tipoDocumento", data.TipoDocumento)
                            ),
                            new XElement("tipoProductoClienteId", 7),
                            new XElement("estadoProducto", 1),
                            new XElement("estadoProducto", 4),
                            new XElement("estadoProducto", 9)
                        )
                    )   
                )
            ); 

            //Default Request Headers
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("SOAPAction", "RecuperarProdIdentificacion");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "Y254X2VzYl9nb3Zjb19jOllCWWtVNyNo");
            _client.DefaultRequestHeaders.Add("Token", "Bearer "+tokenModel.AccessToken);
            StringContent stringContent = new StringContent(soapRequest.ToString(), Encoding.UTF8, "text/xml");

            //Petición API Gateway       
            try
            {
                HttpResponseMessage resp = await _client.PostAsync(_urlListProductos, stringContent);
                if (resp.IsSuccessStatusCode)
                {
                    Task<Stream> streamTask = resp.Content.ReadAsStreamAsync();
                    Stream stream = streamTask.Result;
                    var sr = new StreamReader(stream);
                    var soapResponse = XDocument.Load(sr);
                  
                    var statusXml = soapResponse.Descendants("status").FirstOrDefault().ToString();
                    XmlDocument statusDoc = new XmlDocument();
                    statusDoc.LoadXml(statusXml);
                    var statusJson = Newtonsoft.Json.JsonConvert.SerializeXmlNode(statusDoc);
                    

                    dynamic statusJsonObj = JsonConvert.DeserializeObject(statusJson);
                    var status = statusJsonObj["status"]["statusType"];

                    if(status == "ERROR")
                    {
                         responseModel.Message = statusJsonObj["status"]["errorMessages"]["errorMessage"]["message"];
                    }
                    else
                    {
                        responseModel.Success = true;
                        responseModel.Result = creditos;
                        var xml = soapResponse.Descendants("productos").FirstOrDefault().ToString();
                        if(xml!= null){
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(xml);
                            var json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);
                            dynamic jsonObj = JsonConvert.DeserializeObject(json);

                            var productosJson = "["+jsonObj["productos"]["producto"].ToString()+"]";

                            var productos = new List<ProductoModel>();
                            productos = JsonConvert.DeserializeObject<IEnumerable<ProductoModel>>(productosJson);

                            
                            foreach (ProductoModel producto in productos)
                            {
                                var credito = new CreditoModel();
                                credito.Nombre = producto.tipo.nombre;
                                credito.Numero = producto.numero;
                                credito.Estado = producto.codigoEstado;
                                credito.Valor = producto.montoDesembolso;
                                creditos.Add(credito);
                            }

                            responseModel.Result = creditos;
                            
                        } 
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            //Success
            resultado = new JsonResult(responseModel);  
            return resultado; 
        }

        /// <summary>
        /// Descargar recibo de pago
        /// </summary>
        /// <returns>Recibo de pago</returns>
        public static async Task<FileContentResult> Descargar(RadicadoModel data)
        {
            // Inicializaciones
            FileContentResult resultado = null;

            var responseModel = new ResponseModel();   

            //Token
            TokenModel tokenModel = await ReciboPagoService.ObtenerToken();

            //Radicado de factura
            var radicado = await ReciboPagoService.RadicadoFactura(data.Numero, tokenModel);

            //Codigo factura
            var codigoFactura = await ReciboPagoService.CodigoFactura(radicado, tokenModel);

           
           //Body XML
           XNamespace ns = "http://www.w3.org/2003/05/soap-envelope";
           XNamespace v1 = "http://www.fna.gov.co/esb/services/soap/gestordocumental/obtenerdocumento/v1";

           //Soap Request
           XDocument soapRequest = new XDocument(
               new XElement(ns + "Envelope",
                   new XAttribute(XNamespace.Xmlns + "v1", v1),
                   new XAttribute(XNamespace.Xmlns + "soap", ns),
                   new XElement(ns + "Header",
                        new XElement(v1 + "headerRq",
                           new XElement("systemId", 111),
                           new XElement("endHost", "localhost"),
                           new XElement("endUser", "GOVCO")                         
                       )
                   ),
                   new XElement(ns + "Body",
                        new XElement(v1 + "obtenerdocumentoRq",
                           new XElement("archivo",
                               new XElement("codigoDirectorio", codigoFactura)
                           ),
                           new XElement("autenticacion",
                               new XElement("token", "AoBjPcK/zmYBv50GiIChaOfWQdpUKI4SRrbEYbrwNcJ8LnT0A8rXOQ=="),
                               new XElement("usuario", "RECIBOS_DE_PAGOS_GOVCO")
                           )
                       )
                   )   
               )
           ); 
           
           Console.WriteLine(soapRequest);

           //Default Request Headers
           _client.DefaultRequestHeaders.Accept.Clear();
           _client.DefaultRequestHeaders.Clear();
           _client.DefaultRequestHeaders.Add("SOAPAction", "obtenerdocumento");
           _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "Y254X2VzYl9nb3Zjb19jOllCWWtVNyNo");
           _client.DefaultRequestHeaders.Add("Token", "Bearer "+tokenModel.AccessToken);
           StringContent stringContent = new StringContent(soapRequest.ToString(), Encoding.UTF8, "application/soap+xml");

           //Petición API Gateway     
           try
           {
               HttpResponseMessage resp = await _client.PostAsync(_urlDescargarFactura, stringContent);

               if (resp.IsSuccessStatusCode)
               {
                    string res = resp.Content.ReadAsStringAsync().Result;

                    string pattern = Regex.Escape("<codigoDirectorio>") + "(.*?)</codigoDirectorio>";
                    //string input = "[EDAD]:25:NUMERICO;[SUELDO]:1200:NUMERICO;[ESTADOCIVIL]:'C':TEXTO";

                    MatchCollection matches = Regex.Matches(res, pattern);
                    var texto = "";
                    foreach (Match match in matches)
                        texto = match.Value;

                    texto = texto.Replace("<codigoDirectorio>", "");
                    texto = texto.Replace("</codigoDirectorio>", "");

                    var stringByte = texto.ToString();
                    var bytes = Convert.FromBase64String(stringByte);
                    resultado = new FileContentResult (bytes, "application/pdf");
                      
               }
           }
           catch (Exception e)
           {
               Console.WriteLine(e.Message);
               Console.ReadLine();
           }

            //Resultado
            return resultado;
        }

        /// <summary>
        /// Obtener token tramites fna
        /// </summary>
        /// <returns>Token</returns>
        public static async Task<TokenModel> ObtenerToken()
        {
            // Inicializaciones
            String resultado = null;
            TokenModel tokenModel = null;

            //Content
            HttpContent content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials")
            });
            _client.DefaultRequestHeaders.Accept.Clear();
            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "ZlJBcmhDY0JUa1V3YWZBNDdjOGV4UDFSR05jYToxWTdIc3JxZjdxdWY3UUZNYkhxTlNvbmZsV2th");

            //Petición API Gateway
            try
            {
                HttpResponseMessage resp = await _client.PostAsync(_urlToken, content);
                if (resp.IsSuccessStatusCode)
                {
                    resultado = JsonConvert.SerializeObject(resultado);
                    resultado = resp.Content.ReadAsStringAsync().Result;
                    tokenModel = JsonConvert.DeserializeObject<TokenModel>(resultado);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            //Salida
            return tokenModel;
        }

        /// <summary>
        /// Obtener el radicado de la factura
        /// </summary>
        /// <param name="numero">Numero de l producto crédito</param>
        /// <param name="tokenModel">Token api gateway</param>
        /// <returns>Codigo barras del radicado</returns>
        public static async Task<String> RadicadoFactura(string numero, TokenModel tokenModel)
        {
            // Inicializaciones
            string resultado = null; 
            var responseModel = new ResponseModel();   

            //Body XML
            XNamespace ns = "http://www.w3.org/2003/05/soap-envelope";
            XNamespace v1 = "http://www.fna.gov.co/esb/services/soap/gestordocumental/obtenerinformacionformulario/v1";

            //Soap Request
            XDocument soapRequest = new XDocument(
                new XElement(ns + "Envelope",
                    new XAttribute(XNamespace.Xmlns + "v1", v1),
                    new XAttribute(XNamespace.Xmlns + "soap", ns),
                    new XElement(ns + "Header",
                         new XElement(v1 + "headerRq",
                            new XElement("systemId", 111),
                            new XElement("endHost", "localhost"),
                            new XElement("endUser", "GOVCO")                         
                        )
                    ),
                    new XElement(ns + "Body",
                         new XElement(v1 + "obtenerInformacionFormularioRq",
                            new XElement("campo", "C0004"),
                            new XElement("operacion", "="),
                            new XElement("valor", numero),
                            new XElement("codigoFormulario", 39),
                            new XElement("autenticacion",
                                new XElement("token", "AoBjPcK/zmYBv50GiIChaOfWQdpUKI4SRrbEYbrwNcJ8LnT0A8rXOQ=="),
                                new XElement("usuario", "RECIBOS_DE_PAGOS_GOVCO")
                            )
                        )
                    )   
                )
            ); 

            //Default Request Headers
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("SOAPAction", "obtenerInformacionFormulario");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "Y254X2VzYl9nb3Zjb19jOllCWWtVNyNo");
            _client.DefaultRequestHeaders.Add("Token", "Bearer "+tokenModel.AccessToken);
            StringContent stringContent = new StringContent(soapRequest.ToString(), Encoding.UTF8, "text/xml");

            //Petición API Gateway     
            try
            {
                HttpResponseMessage resp = await _client.PostAsync(_urlRadicadoFactura, stringContent);
                if (resp.IsSuccessStatusCode)
                {
                    Task<Stream> streamTask = resp.Content.ReadAsStreamAsync();
                    Stream stream = streamTask.Result;
                    var sr = new StreamReader(stream);
                    var soapResponse = XDocument.Load(sr);
                  
                    var statusXml = soapResponse.Descendants("status").FirstOrDefault().ToString();
                    XmlDocument statusDoc = new XmlDocument();
                    statusDoc.LoadXml(statusXml);
                    var statusJson = Newtonsoft.Json.JsonConvert.SerializeXmlNode(statusDoc);
                    dynamic statusJsonObj = JsonConvert.DeserializeObject(statusJson);
                    var status = statusJsonObj["status"]["statusType"];

                    if(status == "ERROR")
                    {
                         responseModel.Message = statusJsonObj["status"]["statusDesc"];
                    }
                    else
                    {
                        var xml = soapResponse.Descendants(v1+ "obtenerInformacionFormularioRs").FirstOrDefault().ToString();
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(xml);
                        var json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);
                        
                        dynamic jsonObj = JsonConvert.DeserializeObject(json);
                        
                        var listaCamposJson = jsonObj["v1:obtenerInformacionFormularioRs"]["listaCampos"].ToString();

                        var listaCampos = new List<ListaCamposModel>();
                        listaCampos = JsonConvert.DeserializeObject<IEnumerable<ListaCamposModel>>(listaCamposJson);
                        

                        foreach(ListaCamposModel obj in listaCampos)
                        {
                            if(obj.Campo == "Radicado"){
                                resultado = obj.Valor;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            //Success
            return resultado; 
        }

        /// <summary>
        /// Obtener el codigo de la factura
        /// </summary>
        /// <param name="radicado">Numero radicado</param>
        /// <param name="tokenModel">Token api gateway</param>
        /// <returns>Codigo de la factura</returns>
        public static async Task<String> CodigoFactura(string radicado, TokenModel tokenModel)
        {
            // Inicializaciones
            string resultado = null; 
            var responseModel = new ResponseModel();   

            //Body XML
            XNamespace ns = "http://www.w3.org/2003/05/soap-envelope";
            XNamespace v1 = "http://www.fna.gov.co/esb/services/soap/gestordocumental/consultadocumento/v1";

            //Soap Request
            XDocument soapRequest = new XDocument(
                new XElement(ns + "Envelope",
                    new XAttribute(XNamespace.Xmlns + "v1", v1),
                    new XAttribute(XNamespace.Xmlns + "soap", ns),
                    new XElement(ns + "Header",
                         new XElement(v1 + "headerRq",
                            new XElement("systemId", 111),
                            new XElement("endHost", "localhost"),
                            new XElement("endUser", "GOVCO")                         
                        )
                    ),
                    new XElement(ns + "Body",
                         new XElement(v1 + "consultaDocumentoRq",
                            new XElement("codigoRadicado", radicado),
                            new XElement("autenticacion",
                                new XElement("token", "AoBjPcK/zmYBv50GiIChaOfWQdpUKI4SRrbEYbrwNcJ8LnT0A8rXOQ=="),
                                new XElement("usuario", "RECIBOS_DE_PAGOS_GOVCO")
                            )
                        )
                    )   
                )
            ); 

            //Default Request Headers
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Clear();
            _client.DefaultRequestHeaders.Add("SOAPAction", "consultaDocumento");
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "Y254X2VzYl9nb3Zjb19jOllCWWtVNyNo");
            _client.DefaultRequestHeaders.Add("Token", "Bearer "+tokenModel.AccessToken);
            StringContent stringContent = new StringContent(soapRequest.ToString(), Encoding.UTF8, "text/xml");

            //Petición API Gateway     
            try
            {
                HttpResponseMessage resp = await _client.PostAsync(_urlCodigoFactura, stringContent);
                if (resp.IsSuccessStatusCode)
                {
                    Task<Stream> streamTask = resp.Content.ReadAsStreamAsync();
                    Stream stream = streamTask.Result;
                    var sr = new StreamReader(stream);
                    var soapResponse = XDocument.Load(sr);
                  
                    var statusXml = soapResponse.Descendants("status").FirstOrDefault().ToString();
                    XmlDocument statusDoc = new XmlDocument();
                    statusDoc.LoadXml(statusXml);
                    var statusJson = Newtonsoft.Json.JsonConvert.SerializeXmlNode(statusDoc);
                    dynamic statusJsonObj = JsonConvert.DeserializeObject(statusJson);
                    var status = statusJsonObj["status"]["statusType"];

                    if(status == "ERROR")
                    {
                         responseModel.Message = statusJsonObj["status"]["statusDesc"];
                    }
                    else
                    {
                        var xml = soapResponse.Descendants(v1+ "consultaDocumentoRs").FirstOrDefault().ToString();
                        XmlDocument doc = new XmlDocument();
                        doc.LoadXml(xml);
                        var json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(doc);
                        
                        dynamic jsonObj = JsonConvert.DeserializeObject(json);
                        
                        var codigoDirectorio = jsonObj["v1:consultaDocumentoRs"]["archivo"]["codigoDirectorio"].ToString();
                        resultado = codigoDirectorio; 
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            //Success
            return resultado; 
        }
    }
}