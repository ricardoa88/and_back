using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Models
{
    /// <summary>
    /// Modelo con la información de identificacion del solicitante
    /// </summary>
    public class IdentificacionModel
    {
        /// <summary>
        /// Identificación  del solicitante
        /// </summary>
        public string Identificacion { get; set; }
    }
}