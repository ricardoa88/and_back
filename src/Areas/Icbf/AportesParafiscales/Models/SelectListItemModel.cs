using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Models
{
    /// <summary>
    /// Modelo item de lista
    /// </summary>
    public class SelectListItemModel
    {
        /// <summary>
        /// Identificación item
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Valor del item
        /// </summary>
         public int Value { get; set; }
    }
}