using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Models
{
    /// <summary>
    /// Token Modl
    /// </summary>
    public class TokenModel
    {
        /// <summary>
        /// Identificación key
        /// </summary>
        public string key { get; set; }
    }
}