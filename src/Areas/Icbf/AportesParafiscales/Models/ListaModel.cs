using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Models
{
    /// <summary>
    /// Modelo con la información de las listas icbf
    /// </summary>
    public class ListaModel
    {
        /// <summary>
        /// Identificación  de la entidad (Default = 1)
        /// </summary>
        public int IdEntidad { get; set; }

        /// <summary>
        /// Identificación  de la lista (1=Operadores, 2=Periodos)
        /// </summary>
         public int IdLista { get; set; }

        /// <summary>
        /// Listado de peridos o operadores
        /// </summary>
         public List<KeyValueModel> ObjLista {get; set;}

         /// <summary>
         /// Total de registros response
         /// </summary>
         public int TotalRegistros { get; set; }
    }
}