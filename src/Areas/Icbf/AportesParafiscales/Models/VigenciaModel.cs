using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Models
{
    /// <summary>
    /// Modelo con la información las videncias aportes parafiscales icbf
    /// </summary>
    public class VigenciaModel
    {
        /// <summary>
        /// Vigencia en años
        /// </summary>
        public string Vigencia { get; set; }
    }
}