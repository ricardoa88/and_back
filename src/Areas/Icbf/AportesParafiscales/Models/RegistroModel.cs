using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Models
{
    /// <summary>
    /// Modelo con la información del usuario a registrar
    /// </summary>
    public class RegistroModel
    {
        /// <summary>
        /// Identificación  del usuario
        /// </summary>
        public string Identificacion { get; set; }

        /// <summary>
        /// Email del usuario
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Numero de la planilla
        /// </summary>
        public long Planilla { get; set; }

        /// <summary>
        /// Codigo del operador
        /// </summary>
        public int CodigoOperador { get; set; }

        /// <summary>
        /// Año del periodo
        /// </summary>
        public int AnoPeriodo { get; set; }

        /// <summary>
        /// Mes del periodo
        /// </summary>
        public int MesPeriodo { get; set; }

        /// <summary>
        /// Contraseña del usuario
        /// </summary>
        public string Contrasena { get; set; }

        /// <summary>
        /// Confirmación de la contraseña
        /// </summary>
        public string ContrasenaConfirmacion { get; set; }

        /// <summary>
        /// Pregunta de recordación
        /// </summary>
        public string PreguntaRecordacion { get; set; }

        /// <summary>
        /// Respuesta a la pregunta de recordación
        /// </summary>
        public string Respuesta { get; set; }

        /// <summary>
        /// Autorización de datos (true - false)
        /// </summary>
        public bool AutorizacionDatos { get; set; }

        /// <summary>
        ///  Captcha google
        /// </summary>
        public string Recaptcha { get; set; }

    }
}