using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Models
{
    /// <summary>
    /// Modelo clave - valor
    /// </summary>
    public class KeyValueModel
    {
        /// <summary>
        /// Identificación key
        /// </summary>
        public int Key { get; set; }

        /// <summary>
        /// Valor del key
        /// </summary>
         public string Value { get; set; }
    }
}