using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Models
{
    /// <summary>
    /// Modelo con la información de la certificación a descargar
    /// </summary>
    public class DataCertificacionModel
    {
        /// <summary>
        /// Identificación  del solicitante
        /// </summary>
        public string Identificacion { get; set; }

        /// <summary>
        /// Vigencia de la certificación
        /// </summary>
        public string Vigencia { get; set; }

        /// <summary>
        /// Código de verificación de la certificación
        /// </summary>
        public string CodigoVerificacion { get; set; }

         
    }
}