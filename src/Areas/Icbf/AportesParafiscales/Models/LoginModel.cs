using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Models
{
    /// <summary>
    /// Modelo con para iniciar sesión
    /// </summary>
    public class LoginModel
    {
        /// <summary>
        /// Usuario ICBF
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Contraseña usuario ICBF
        /// </summary>
         public string Password { get; set; }
    }
}