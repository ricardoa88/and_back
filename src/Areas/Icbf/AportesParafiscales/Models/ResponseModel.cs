using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Models
{
    /// <summary>
    /// Modelo con el estado de un response
    /// </summary>
    public class ResponseModel
    {
        /// <summary>
        /// Estado de la respuesta
        /// </summary>
        public int Error { get; set; }

        /// <summary>
        /// Mensaje de la respuesta
        /// </summary>
         public string Mensaje { get; set; }
    }
}