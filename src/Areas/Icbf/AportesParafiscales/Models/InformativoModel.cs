using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Models
{
    /// <summary>
    /// Modelo con la información del los informativos ICBF
    /// </summary>
    public class InformativoModel
    {
         /// <summary>
        /// Identificación  de la entidad (Default = 1)
        /// </summary>
        public int IdEntidad { get; set; }

        /// <summary>
        /// Identificación  del informativo (1. Politica de tratamiento de datos personales)
        /// </summary>
         public int IdInformativo { get; set; }

        /// <summary>
        /// Url de respuesta
        /// </summary>
        public string Url { get; set; }

    }
}