using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using tramites_servicios_webapi.Icbf.AportesParafiscales.Models;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Services
{
    /// <summary>
    /// Servicios Aportes Parafiscales del ICBF
    /// </summary>
    public class AportesParafiscalesService
    {
        private static readonly HttpClient _client;
        private const String _urlBasePruebas = "https://pruebaftpsiuce.azurewebsites.net/Api/PilaWeb/";
        private const String _urlBase = "https://apis.icbf.gov.co:4443/Api/PilaWeb/";
        private const String _urlCargarListas = "CargarListas";
        private const String _urlCargarVigencias = "CargarVigencias";
        private const String _urlRegistrarUsuario = "RegistrarUsuario";
        private const String _urlRecordarContrasena = "RecordarContrasena";
        private const String _urlVerificarCertificadoIdVigencia = "VerificarGenerarCertificado";
        private const String _urlVerificarCertificadoCodigo = "VerificarGenerarCertificadoCodigo";
        private const String _urlGenerarCertificadoIdVigencia = "GenerarCertificado";
        private const String _urlGenerarCertificadoCodigo = "GenerarCertificadoCodigo";
        private const String _urlLogin = "IniciarSesion";
        private const String _urlCargarinformativos = "CargarInformativos";
        private static readonly LoginModel _dataToken;
        private const String _dataUserToken = "932999998";
        private const String _dataPassToken = "899999239";

        static AportesParafiscalesService()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri(_urlBase);
            _dataToken = new LoginModel();
            _dataToken.User = _dataUserToken;
            _dataToken.Password = _dataPassToken;

        }

        /// <summary>
        /// Consulta de listas aportes parafiscales
        /// </summary>
        /// <param name="data">Modelo</param>
        /// <returns>Listado operadores o periodos</returns>
        public static async Task<JsonResult> GetListas(InputListaModel data)
        {
            // Inicializaciones
            JsonResult resultado = null;  

             //Token
            TokenModel tokenModel = await AportesParafiscalesService.ObtenerToken(_dataToken);

            //Headers - Body
            Dictionary<string, int> content = new Dictionary<string, int> ();
            content.Add ("idEntidad", data.IdEntidad);
            content.Add ("idLista", data.IdLista);
            string output = JsonConvert.SerializeObject (content);
            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.key);
             StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), _urlCargarListas);
            request.Content = stringContent;

            //Petición
            try {
                HttpResponseMessage resp = await _client.SendAsync(request);

                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     ListaModel listaModel = JsonConvert.DeserializeObject<ListaModel> (response);
                     var selectListItems = new List<SelectListItemModel> ();
                     
                     foreach(KeyValueModel dato in listaModel.ObjLista)
                     {
                         SelectListItemModel selectListItem = new SelectListItemModel();
                         selectListItem.Text = dato.Value;
                         selectListItem.Value = dato.Key;
                         selectListItems.Add(selectListItem);
                     }

                     resultado = new JsonResult(selectListItems);
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }
              
            //Salida
            return resultado;
        }

        /// <summary>
        /// Consulta listado de vigencias de un solictante
        /// </summary>
        /// <param name="data">Modelo</param>
        /// <returns>Listado de videncias</returns>
        public static async Task<JsonResult> GetVigencias(IdentificacionModel data)
        {
            // Inicializaciones
            JsonResult resultado = null;  

             //Token
            TokenModel tokenModel = await AportesParafiscalesService.ObtenerToken(_dataToken);

            //Headers - Body
            Dictionary<string, string> content = new Dictionary<string, string> ();
            content.Add ("identificacion", data.Identificacion);
            string output = JsonConvert.SerializeObject (content);
            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
             _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.key);
            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");

            //Petición
            try {
                HttpResponseMessage resp = await _client.PostAsync(_urlCargarVigencias, stringContent);

                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     ListaModel listaModel = JsonConvert.DeserializeObject<ListaModel> (response);
                     var selectListItems = new List<SelectListItemModel> ();
                     
                     foreach(KeyValueModel dato in listaModel.ObjLista)
                     {
                         SelectListItemModel selectListItem = new SelectListItemModel();
                         selectListItem.Text = dato.Value;
                         selectListItem.Value = dato.Key;
                         selectListItems.Add(selectListItem);
                     }

                     resultado = new JsonResult(selectListItems);
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }
                          
            //Salida
            return resultado;
        }

        /// <summary>
        /// Registra Usuario - Aportes parafiscales ICBF
        /// </summary>
        /// <param name="data">Modelo de registro</param>
        /// <returns>Estado del registro</returns>
        public static async Task<JsonResult> Registrar(RegistroModel data)
        {
            // Inicializaciones
            JsonResult resultado = null;  

             //Token
            TokenModel tokenModel = await AportesParafiscalesService.ObtenerToken(_dataToken);

            //Headers - Body
            string output = JsonConvert.SerializeObject (data);
            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.key);
            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");

            //Petición
            try {
                HttpResponseMessage resp = await _client.PostAsync(_urlRegistrarUsuario, stringContent);

                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     var obj = JsonConvert.DeserializeObject(response);
                     resultado = new JsonResult(obj);
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }
                          
            //Salida
            return resultado;
        }

        /// <summary>
        /// Recuperar contraseña usuario ICBF
        /// </summary>
        /// <param name="data">Identificacion del usuario</param>
        /// <returns>Estado de la recuperación</returns>
        public static async Task<JsonResult> ForgotPassword(IdentificacionModel data)
        {
            // Inicializaciones
            JsonResult resultado = null;

            //Token
            TokenModel tokenModel = await AportesParafiscalesService.ObtenerToken(_dataToken);  

            //Headers - Body
            string output = JsonConvert.SerializeObject (data);
            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.key);
            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");

            //Petición
            try {
                HttpResponseMessage resp = await _client.PostAsync(_urlRecordarContrasena, stringContent);

                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     var obj = JsonConvert.DeserializeObject(response);
                     resultado = new JsonResult(obj);
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }
                          
            //Salida
            return resultado;
        }

        /// <summary>
        /// Verificar información del certificado
        /// </summary>
        /// <param name="data">Información del certificado </param>
        /// <returns>Estado de la verificación</returns>
        public static async Task<JsonResult> VerificarGeneracionCertificado(DataCertificacionModel data)
        {
            // Inicializaciones
            JsonResult resultado = null;  
            string _urlVerificarCertificado = "";

            //Token
            TokenModel tokenModel = await AportesParafiscalesService.ObtenerToken(_dataToken);  

            //Headers - Body
            Dictionary<string, string> content = new Dictionary<string, string> ();
            if(data.CodigoVerificacion != null && data.CodigoVerificacion != "")
            {
                 content.Add ("codigoVerificacion", data.CodigoVerificacion);
                 _urlVerificarCertificado = _urlVerificarCertificadoCodigo;
                
            }
            else{
                content.Add ("identificacion", data.Identificacion);
                content.Add ("vigencia", data.Vigencia);
                 _urlVerificarCertificado = _urlVerificarCertificadoIdVigencia;
            }

            string output = JsonConvert.SerializeObject (content);

            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.key);
            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");

            //Petición
            try {
                HttpResponseMessage resp = await _client.PostAsync(_urlVerificarCertificado, stringContent);

                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     var obj = JsonConvert.DeserializeObject(response);
                     resultado = new JsonResult(obj);
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }
                          
            //Salida
            return resultado;
        }

        /// <summary>
        /// Generar certificado
        /// </summary>
        /// <param name="data">Información del certificado </param>
        /// <returns>Certificación</returns>
        public static async Task<FileContentResult> GenerarCertificado(DataCertificacionModel data)
        {
            // Inicializaciones
            FileContentResult resultado = null;
            string _urlGenerarCertificado = "";

            TokenModel tokenModel = await AportesParafiscalesService.ObtenerToken(_dataToken);  

            //Headers - Body
            Dictionary<string, string> content = new Dictionary<string, string> ();
            if(data.CodigoVerificacion != null && data.CodigoVerificacion != "")
            {
                 content.Add ("codigoVerificacion", data.CodigoVerificacion);
                 _urlGenerarCertificado = _urlGenerarCertificadoCodigo;
                
            }
            else{
                content.Add ("identificacion", data.Identificacion);
                content.Add ("vigencia", data.Vigencia);
                 _urlGenerarCertificado = _urlGenerarCertificadoIdVigencia;
            }

            string output = JsonConvert.SerializeObject (content);

            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.key);
            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");

            //Petición
            try {
                HttpResponseMessage resp = await _client.PostAsync(_urlGenerarCertificado, stringContent);

                if (resp.IsSuccessStatusCode) {
                    
                    byte[] bytes = resp.Content.ReadAsByteArrayAsync ().Result;
                    resultado = new FileContentResult (bytes, "application/pdf");
                
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }
                          
            //Salida
            return resultado;
        }

        /// <summary>
        /// Iniciar sesión usuario ICBF
        /// </summary>
        /// <param name="data">Usuario y Contaseña ICBF</param>
        /// <returns>Estado del login</returns>
        public static async Task<JsonResult> Login(LoginModel data)
        {
            // Inicializaciones
            JsonResult resultado = null;  
            

            //Decofificar Base 64
            string base64Decoded = data.User+":"+data.Password;
            string base64Encoded;
            byte[] dataByte = System.Text.ASCIIEncoding.ASCII.GetBytes(base64Decoded);
            base64Encoded = System.Convert.ToBase64String(dataByte);


            //Headers - Body
            string output = JsonConvert.SerializeObject (data);
            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue ("Basic", base64Encoded);
            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");

            //Petición
            try {
                HttpResponseMessage resp = await _client.PostAsync(_urlLogin, stringContent);
                ResponseModel result = new ResponseModel();
                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     var obj = JsonConvert.DeserializeObject(response);
                    
                     result.Error = 0;
                     result.Mensaje = "Login exitoso"; 
                     resultado = new JsonResult(result);
                }
                else{
                     result.Error = 1;
                     result.Mensaje = "Credenciales invalidas";
                     resultado = new JsonResult(result);
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }

            return resultado;

        }

        /// <summary>
        /// Consulta los informativos
        /// </summary>
        /// <param name="data">Modelo</param>
        /// <returns>Informativo</returns>
        public static async Task<JsonResult> GetInformativos(InformativoModel data)
        {
            // Inicializaciones
            JsonResult resultado = null;  

            //Token
            TokenModel tokenModel = await AportesParafiscalesService.ObtenerToken(_dataToken);  

            //Headers - Body
            Dictionary<string, int> content = new Dictionary<string, int> ();
            content.Add ("idEntidad", data.IdEntidad);
            content.Add ("idInformativo", data.IdInformativo);
            string output = JsonConvert.SerializeObject (content);
            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.key);
             StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), _urlCargarinformativos);
            request.Content = stringContent;

            //Petición
            try {
                HttpResponseMessage resp = await _client.SendAsync(request);

                if (resp.IsSuccessStatusCode) {
                     string response = resp.Content.ReadAsStringAsync().Result;
                     InformativoModel informativoModel = JsonConvert.DeserializeObject<InformativoModel> (response);
                     resultado = new JsonResult(informativoModel);
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }
            
            //Salida
            return resultado;
        }

        /// <summary>
        /// Obtener token 
        /// </summary>
        /// <param name="data">Usuario y Contaseña ICBF</param>
        /// <returns>Token</returns>
        public static async Task<TokenModel> ObtenerToken(LoginModel data)
        {
            String resultado = null;
            TokenModel tokenModel = null;
            

            //Decofificar Base 64
            string base64Decoded = data.User+":"+data.Password;
            string base64Encoded;
            byte[] dataByte = System.Text.ASCIIEncoding.ASCII.GetBytes(base64Decoded);
            base64Encoded = System.Convert.ToBase64String(dataByte);


            //Headers - Body
            string output = JsonConvert.SerializeObject (data);
            _client.DefaultRequestHeaders.Accept.Clear ();
            _client.DefaultRequestHeaders.Accept.Add (
                new MediaTypeWithQualityHeaderValue ("application/json"));
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue ("Basic", base64Encoded);
            StringContent stringContent = new StringContent (output, Encoding.UTF8, "application/json");

            //Petición
            try {
                HttpResponseMessage resp = await _client.PostAsync(_urlLogin, stringContent);
                ResponseModel result = new ResponseModel();
                if (resp.IsSuccessStatusCode) {

                    resultado = JsonConvert.SerializeObject(resultado);
                    resultado = resp.Content.ReadAsStringAsync().Result;
                    tokenModel = JsonConvert.DeserializeObject<TokenModel>(resultado);                    
                }
            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }

            //Salida
            return tokenModel;

        }
    }
}