using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Icbf.AportesParafiscales.Models;
using tramites_servicios_webapi.Icbf.AportesParafiscales.Services;

namespace tramites_servicios_webapi.Icbf.AportesParafiscales.Controllers
{
    /// <summary>
    /// Aportes parafiscales del ICBF
    /// </summary>
    [Route ("api/icbf/[controller]")]
    [ApiController]
    public class AportesParafiscalesController : ControllerBase 
    {

        /// <summary>
        /// Consulta las listas utilizadas en aportes parafiscales
        /// </summary>
        /// <param name="data">Objeto con tipo de lista a consultar</param>
        /// <returns>Listado de operadores o peridos</returns>
        [HttpPost ("listas")]
        public async Task<IActionResult> GetListas ([FromBody] InputListaModel data) 
        {
            var respuesta = await AportesParafiscalesService.GetListas(data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }

        /// <summary>
        /// Consulta de las vigencias aportes parafiscales icbf
        /// </summary>
        /// <param name="data">Objeto con el numero de identificación</param>
        /// <returns>Listado de ovigencias</returns>
        [HttpPost ("vigencias")]
        public async Task<IActionResult> GetVigencias ([FromBody] IdentificacionModel data) 
        {
            var respuesta = await AportesParafiscalesService.GetVigencias(data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }

        /// <summary>
        /// Registra un usuario - Aportes parafiscales ICBF
        /// </summary>
        /// <param name="data">Objeto con la información del usuario</param>
        /// <returns>Estado del registro</returns>
        [HttpPost ("registro")]
        public async Task<IActionResult> Registro ([FromBody] RegistroModel data) 
        {
            var respuesta = await AportesParafiscalesService.Registrar(data);
            if (respuesta == null) 
            {
                return NotFound ();
            }

            return respuesta;
        }

        /// <summary>
        /// Recuperar contraseña usuario ICBF
        /// </summary>
        /// <param name="data">Objeto con la información del usuario</param>
        /// <returns>Estado de la recuperación de contraseña</returns>
        [HttpPost ("forgot-password")]
        public async Task<IActionResult> ForgotPassword ([FromBody] IdentificacionModel data) 
        {
            var respuesta = await AportesParafiscalesService.ForgotPassword(data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }

        /// <summary>
        /// Verificar la información antes de generar un certificado
        /// </summary>
        /// <param name="data">Objeto con la información del certificado</param>
        /// <returns>Estado de la verificación</returns>
        [HttpPost ("verificar")]
        public async Task<IActionResult> VerificarGeneracionCertificado ([FromBody] DataCertificacionModel data) 
        {
            var respuesta = await AportesParafiscalesService.VerificarGeneracionCertificado(data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }

        /// <summary>
        /// Generar certificado aportes parafiscales ICBF
        /// </summary>
        /// <param name="data">Objeto con la información del certificado</param>
        /// <returns>Certificado</returns>
        [HttpPost ("generar")]
        public async Task<IActionResult> GenerarCertificado ([FromBody] DataCertificacionModel data) 
        {
            var respuesta = await AportesParafiscalesService.GenerarCertificado(data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }

        /// <summary>
        /// Iniciar sesión ICBF
        /// </summary>
        /// <param name="data">Objeto con la información del usuario</param>
        /// <returns>Estado de login</returns>
        [HttpPost ("login")]
        public async Task<IActionResult> Login ([FromBody] LoginModel data) 
        {
            var respuesta = await AportesParafiscalesService.Login(data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }

        /// <summary>
        /// Consulta los informativos ICBF
        /// </summary>
        /// <param name="data">Objeto con el numero la información del informativo</param>
        /// <returns>Informativo</returns>
        [HttpPost ("informativos")]
        public async Task<IActionResult> GetInformativo ([FromBody] InformativoModel data) 
        {
            var respuesta = await AportesParafiscalesService.GetInformativos(data);

            if (respuesta == null) 
            {
                return NotFound ();
            }
            return respuesta;
        }
        
    }
}