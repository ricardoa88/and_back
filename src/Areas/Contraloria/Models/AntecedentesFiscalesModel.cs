using System.ComponentModel.DataAnnotations;
using System.Net;
using Newtonsoft.Json;

namespace tramites_servicios_webapi.Contraloria.Models {

    public class ContraloriaTiposDocumento {
        public string Value {get; set;}

        public string Text {get; set;}
    }

    public class ContraloriaDatosSolicitud {
        public string TipoDocumento { get; set; }

        public string Documento { get; set; }
    }

    public class ContraloriaToken {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
    }


}