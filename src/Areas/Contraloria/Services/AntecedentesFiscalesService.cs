using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Contraloria.Models;
using tramites_servicios_webapi.common.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;

namespace tramites_servicios_webapi.Contraloria.Services {
    public class AntecedentesFiscalesService {
        private static readonly HttpClient client;
        private const string UrlBase = "https://api.www.gov.co/services/";
        private const string UrlLogin = "token";
        private const string UrlContraloriaToken = "token-contraloria/v1/";
        private const string UrlGenerarCertificado = "certificado-contraloria/v1/";

        private const string AuthenticationToken = "UVJrYWpSTGZka2MzbERWalVGUm1nWEIwUmxvYTpkYXl4ZVdkQ1kydk5lenRWeUxxWG95OEpMeVVh";

        private const string ContraloriaToken = "M1VPbHhoRXhEMXhHV2ZGSWZxMHRqWHV0b3RBYTptaWJPZm05N0JmNnVLR0pJQzdiSDN4azVlWW9h";

        static AntecedentesFiscalesService () {
            client = new HttpClient ();
            client.BaseAddress = new Uri (UrlBase);
        }

        public static JsonResult ObtenerTiposDocumento() {
            var result = new List<ContraloriaTiposDocumento> {
                new ContraloriaTiposDocumento { Value = "CC", Text = "Cédula de ciudadanía"},
                new ContraloriaTiposDocumento { Value = "CE", Text = "Cédula de extranjería"},
                new ContraloriaTiposDocumento { Value = "PA", Text = "Pasaporte"},
                new ContraloriaTiposDocumento { Value = "TI", Text = "Tarjeta de identidad"}                
            };
            return new JsonResult(result);
        }
        
        /// <summary>
        /// Obtiene el token generado por la entidad para govco
        /// </summary>
        /// <returns>Token generado</returns>
        public static async Task<ContraloriaToken> GetToken() {            
            String resultado = null;
            ContraloriaToken tokenModel = new ContraloriaToken();

            //Content
            HttpContent content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials")
            });
            client.DefaultRequestHeaders.Accept.Clear();
            // content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlLogin);
            request.Content = content;

            //Construye el encabezado
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", AuthenticationToken);

            try {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode) {
                    resultado = resp.Content.ReadAsStringAsync ().Result;
                    tokenModel = JsonConvert.DeserializeObject<ContraloriaToken> (resultado);
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }

            Console.WriteLine (tokenModel.AccessToken);
            return tokenModel;
        }

        /// <summary>
        /// Autentica con la información dada y el token generado
        /// </summary>
        /// <param name="token">Token generado por la entidad</param>
        /// <returns>Token</returns>
        public static async Task<ContraloriaToken> Login(string token) {
            String resultado = null;
            ContraloriaToken tokenModel = new ContraloriaToken();
            client.DefaultRequestHeaders.Accept.Clear ();

            //Content
            HttpContent content = new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("grant_type", "client_credentials")
            });
            
            client.DefaultRequestHeaders.Accept.Clear();

            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlContraloriaToken);
            request.Content = content;

            //Construye el encabezado
            request.Headers.Authorization = new AuthenticationHeaderValue("Basic", ContraloriaToken);
            request.Headers.Add("Token", "Bearer " + token);

            try {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode) {
                    resultado = resp.Content.ReadAsStringAsync ().Result;
                    tokenModel = JsonConvert.DeserializeObject<ContraloriaToken> (resultado);
                }

            } catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                Console.WriteLine (e.Message);
                Console.ReadLine ();
            }

            return tokenModel;
        }

        /// <summary>
        /// Descarga el archivo del certificado de anteedentes fiscales
        /// </summary>
        /// <param name="model">Datos de la persona para la solicitud</param>
        /// <returns>Archivo y estado de la transacción</returns>
        public static async Task<ResponseModel> GenerarAntecedentes(ContraloriaDatosSolicitud model) {
            ResponseModel responseModel = new ResponseModel();

            //Obtiene los tokens necesarios
            ContraloriaToken tokenModel = await GetToken ();             
            ContraloriaToken AuthorizationModel = await Login (tokenModel.AccessToken);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );

            //Construye el cuerpo del mensaje
            Dictionary<string, string> content = new Dictionary<string, string> ();
            content.Add ("numeroDocumento", model.Documento);
            content.Add ("tipoDocumento", model.TipoDocumento);
            string output = JsonConvert.SerializeObject (content);

            //Crea la petición y asigna el cuerpo del mensaje
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlGenerarCertificado);
            request.Content = new StringContent (output, Encoding.UTF8, "application/json");

            //Construye el encabezado
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", AuthorizationModel.AccessToken);
            request.Headers.Add("entidad", "AGENCIA NACIONAL DIGITAL");
            request.Headers.Add("sistema", "APP CERTIFICACION CONTRATISTAS AND");
            request.Headers.Add("usuario", "1");
            request.Headers.Add("IdentificadorRequest", "2019-07-24 10:52:01.199675");
            request.Headers.Add("Token", "Bearer " + tokenModel.AccessToken);

            try {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode) {
                    string response = resp.Content.ReadAsStringAsync().Result;                    
                    var json = JObject.Parse(response);
                    var item = json["CertificadoPersonaNaturalResponse"]["CertificadoPersonaNaturalResult"]["certificadoPersonaNatural"]["archivoB64"];
                    
                    var stringByte = item.ToString();
                    var bytes = Convert.FromBase64String(stringByte);

                    responseModel.Success = true;
                    responseModel.Result = new FileContentResult (bytes, "application/pdf");
                }
                else {
                    //TODO: incluir errores
                    responseModel.Message = "Se presentó un error al procesar la solicitud";
                }
            }catch (Exception e) {
                //TODO: manejador de excepciones y mongo
                responseModel.Message = "El servicio solicitado no se encuentra disponible";
                Console.WriteLine (e.Message);
            }

            return responseModel;
        }
    }
}