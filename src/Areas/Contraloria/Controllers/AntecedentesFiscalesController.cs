using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Contraloria.Services;
using tramites_servicios_webapi.Contraloria.Models;
using tramites_servicios_webapi.common.Models;

namespace tramites_servicios_webapi.Controllers
{
    [Route ("api/[controller]")]
    [ApiController]
    public class AntecedentesFiscalesController : ControllerBase
    {        
        /// <summary>
        /// Obtiene los tipos de documentos del tramite de consulta antecedentes fiscales
        /// </summary>
        /// <returns>Listado de tipos de documentos de la entidad</returns>
        [HttpGet("obtenerTiposDocumento")]        
        public async Task<IActionResult>  ObtenerTiposDocumento() {
            //TODO: Modificar el método a asincrono si realiza peticiones a servidor            
            var tiposDocumento = AntecedentesFiscalesService.ObtenerTiposDocumento();
            return tiposDocumento;
        }   

        /// <summary>
        /// Genera el certificado de antecedentes fiscales de acuerdo a los datos diligenciados
        /// </summary>
        /// <param name="data">Documento y tipo de decumento de la persona</param>
        /// <returns>Archivo generado</returns>
        [HttpPost("generarCertificado")]
        public async Task<IActionResult> GenerarCertificado([FromBody] ContraloriaDatosSolicitud data){
            if (!ModelState.IsValid) {
                return BadRequest ("Los datos diligenciados tienen inconsistencias.");
            }
            
            ResponseModel result = await AntecedentesFiscalesService.GenerarAntecedentes(data);

            if(!result.Success){
                return BadRequest (result.Message);            
            }
            
            return result.Result as FileContentResult;
        }
    }
}