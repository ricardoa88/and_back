using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Mintransporte.Services;
using tramites_servicios_webapi.common.Models;
using tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models;
using System.Linq;
using System.Collections.Generic;
using tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models;
using tramites_servicios_webapi.Areas.MinCultura.SolicitudExportacion.Models;

namespace tramites_servicios_webapi.Controllers
{
    [Route ("api/[controller]")]
    [ApiController]
    public class SolicitudExportacionController : ControllerBase
    {
        /// <summary> 
        /// Obtener todos los Tipos de Documentos de Identidad
        /// </summary>

        [HttpGet("ObtenerListasCrearSolicitud")]
        public async Task<IActionResult> ObtenerListasCrearSolicitud()
        {
            var result = new ResponseModel();
            var response = new ReturnModelListasCrearSolicitud();

            var resultGetObtenerTiposDocumentosIndentidad = await SolicitudExportacionService.GetObtenerTiposDocumentosIndentidad();

            if (resultGetObtenerTiposDocumentosIndentidad.OperacionExitosa)
            {
                response.TiposDocumento = resultGetObtenerTiposDocumentosIndentidad.TiposDocumentos.Select(m => new SelectListItem {Value= m.DocId.ToString(), Text= m.DocNombre });
            }

            ReturnModelObtenerDepartamentos resultGetObtenerDepartamentos = await SolicitudExportacionService.GetObtenerDepartamentos();

            if (resultGetObtenerDepartamentos.OperacionExitosa)
            {
                response.Departamentos = resultGetObtenerDepartamentos.ZonasGeograficas.Select(m => new SelectListItem { Value = m.ZonId, Text = m.ZonNombre });
            }

            

            ReturnModelObtenerPaises resultGetObtenerPaises = await SolicitudExportacionService.GetObtenerPaises();

            if (resultGetObtenerPaises.OperacionExitosa)
            {
                response.Paises = resultGetObtenerPaises.Paises.Select(m => new SelectListItem { Value = m.ZopId.ToString(), Text = m.ZopNombre });
            }

            result.Success = true;
            result.Result = response;
            result.Message = resultGetObtenerTiposDocumentosIndentidad.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Consultar Solicitudes Por Nro Consecutivo
        /// </summary>

        [HttpPost("ObtenerSolicitudPorNroConsecutivo")]
        public async Task<IActionResult> ObtenerSolicitudPorNroConsecutivo(RequestModelObtenerSolicitudPorNroConsecutivo requestModel)
        {
            var result = new ResponseModel();

            ReturnModelObtenerSolicitudPorNroConsecutivo resultObtenerSolicitudPorNroConsecutivo = await SolicitudExportacionService.ObtenerSolicitudPorNroConsecutivo(requestModel);

            if (resultObtenerSolicitudPorNroConsecutivo.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultObtenerSolicitudPorNroConsecutivo;
            }

            result.Message = resultObtenerSolicitudPorNroConsecutivo.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Consultar Lista Anexos Solicitudes Por Solicitud ID
        /// </summary>

        [HttpPost("ObtenerListaAnexos")]
        public async Task<IActionResult> ObtenerListaAnexos(RequestModelObtenerListaAnexos requestModel)
        {
            var result = new ResponseModel();

            ReturnModelObtenerListaAnexos resultObtenerListaAnexos = await SolicitudExportacionService.ObtenerListaAnexos(requestModel);

            if (resultObtenerListaAnexos.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultObtenerListaAnexos;
            }

            result.Message = resultObtenerListaAnexos.Mensaje;
            return new JsonResult(result);
        }

        ///// <summary> 
        ///// Consultar Lista Conceptos Solicitudes Por Solicitud ID
        ///// </summary>

        //[HttpPost("ObtenerListaConceptosSolicitud")]
        //public async Task<IActionResult> ObtenerListaConceptosSolicitud(RequestModelObtenerListaConceptosSolicitud requestModel)
        //{
        //    var result = new ResponseModel();

        //    ReturnModelObtenerListaConceptosSolicitud resultObtenerListaConceptosSolicitud = await SolicitudExportacionService.ObtenerListaConceptosSolicitud(requestModel);

        //    if (resultObtenerListaConceptosSolicitud.OperacionExitosa)
        //    {
        //        result.Success = true;
        //        result.Result = resultObtenerListaConceptosSolicitud;
        //    }

        //    result.Message = resultObtenerListaConceptosSolicitud.Mensaje;
        //    return new JsonResult(result);
        //}

        /// <summary> 
        /// Consultar Lista Conceptos Obras Por Solicitud ID
        /// </summary>

        [HttpPost("ObtenerListaConceptosObras")]
        public async Task<IActionResult> ObtenerListaConceptosObras(RequestModelObtenerListaConceptosObras requestModel)
        {
            var result = new ResponseModel();

            ReturnModelObtenerListaConceptosObras resultObtenerListaConceptosObras = await SolicitudExportacionService.ObtenerListaConceptosObras(requestModel);

            if (resultObtenerListaConceptosObras.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultObtenerListaConceptosObras;
            }

            result.Message = resultObtenerListaConceptosObras.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Consultar Lista Prestamos
        /// </summary>

        [HttpPost("ObtenerListaPrestamo")]
        public async Task<IActionResult> ObtenerListaPrestamo(RequestModelObtenerListaPrestamo requestModel)
        {
            var result = new ResponseModel();

            ReturnModelObtenerListaPrestamo resultObtenerListaPrestamo = await SolicitudExportacionService.ObtenerListaPrestamo(requestModel);

            if (resultObtenerListaPrestamo.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultObtenerListaPrestamo;
            }

            result.Message = resultObtenerListaPrestamo.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Consultar Lista Deterioros
        /// </summary>

        [HttpPost("ObtenerListaDeterioro")]
        public async Task<IActionResult> ObtenerListaDeterioro(RequestModelObtenerListaDeterioro requestModel)
        {
            var result = new ResponseModel();

            ReturnModelObtenerListaDeterioro resultObtenerListaDeterioro = await SolicitudExportacionService.ObtenerListaDeterioro(requestModel);

            if (resultObtenerListaDeterioro.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultObtenerListaDeterioro;
            }

            result.Message = resultObtenerListaDeterioro.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Consultar Solicitudes Por Id
        /// </summary>

        [HttpPost("ObtenerSolicitudPorId")]
        public async Task<IActionResult> ObtenerSolicitudPorId(RequestModelObtenerSolicitudPorId requestModel)
        {
            var result = new ResponseModel();

            ReturnModelObtenerSolicitudPorId resultObtenerSolicitudPorId = await SolicitudExportacionService.ObtenerSolicitudPorId(requestModel);

            if (resultObtenerSolicitudPorId.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultObtenerSolicitudPorId;
            }

            result.Message = resultObtenerSolicitudPorId.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Consultar las Solicitudes por Rangos
        /// </summary>

        [HttpPost("ObtenerSolicitudes")]
        public async Task<IActionResult> ObtenerSolicitudes(RequestModelObtenerSolicitudes requestModel)
        {
            var result = new ResponseModel();

            ReturnModelObtenerSolicitudes resultObtenerSolicitudes = await SolicitudExportacionService.ObtenerSolicitudes(requestModel);

            if (resultObtenerSolicitudes.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultObtenerSolicitudes;
            }

            result.Message = resultObtenerSolicitudes.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Consultar Obras Por Id
        /// </summary>

        [HttpPost("ObtenerObras")]
        public async Task<IActionResult> ObtenerObras(RequestModelObtenerObras requestModel)
        {
            var result = new ResponseModel();

            ReturnModelObtenerObras resultObtenerObras = await SolicitudExportacionService.ObtenerObras(requestModel);

            if (resultObtenerObras.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultObtenerObras;
            }

            result.Message = resultObtenerObras.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Consultar Anexo por ID
        /// </summary>

        [HttpPost("ObtenerAnexo")]
        public async Task<IActionResult> ObtenerAnexo(RequestModelObtenerAnexo requestModel)
        {
            var result = new ResponseModel();

            ReturnModelObtenerAnexo resultObtenerAnexo = await SolicitudExportacionService.ObtenerAnexo(requestModel);

            if (resultObtenerAnexo.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultObtenerAnexo;
            }

            result.Message = resultObtenerAnexo.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Consultar Concepto Solicitud Por Obra Id
        /// </summary>

        //[HttpPost("ObtenerConceptoSolicitud")]
        //public async Task<IActionResult> ObtenerConceptoSolicitud(RequestModelObtenerConceptoSolicitud requestModel)
        //{
        //    var result = new ResponseModel();

        //    ReturnModelObtenerConceptoSolicitud resultObtenerConceptoSolicitud = await SolicitudExportacionService.ObtenerConceptoSolicitud(requestModel);

        //    if (resultObtenerConceptoSolicitud.OperacionExitosa)
        //    {
        //        result.Success = true;
        //        result.Result = resultObtenerConceptoSolicitud;
        //    }

        //    result.Message = resultObtenerConceptoSolicitud.Mensaje;
        //    return new JsonResult(result);
        //}

        ///// <summary> 
        ///// Consultar Solicitud Por Nro Doc Intermediario
        ///// </summary>

        //[HttpPost("ObtenerSolicitudPorIntermediario")]
        //public async Task<IActionResult> ObtenerSolicitudPorIntermediario(RequestModelObtenerSolicitudPorIntermediario requestModel)
        //{
        //    var result = new ResponseModel();

        //    ReturnModelObtenerSolicitudPorIntermediario resultObtenerSolicitudPorIntermediario = await SolicitudExportacionService.ObtenerSolicitudPorIntermediario(requestModel);

        //    if (resultObtenerSolicitudPorIntermediario.OperacionExitosa)
        //    {
        //        result.Success = true;
        //        result.Result = resultObtenerSolicitudPorIntermediario;
        //    }

        //    result.Message = resultObtenerSolicitudPorIntermediario.Mensaje;
        //    return new JsonResult(result);
        //}

        ///// <summary> 
        ///// Consultar Solicitud Por Nro Doc del Solicitante
        ///// </summary>

        //[HttpPost("ObtenerSolicitudPorSolicitante")]
        //public async Task<IActionResult> ObtenerSolicitudPorSolicitante(RequestModelObtenerSolicitudPorSolicitante requestModel)
        //{
        //    var result = new ResponseModel();

        //    ReturnModelObtenerSolicitudPorSolicitante resultObtenerSolicitudPorSolicitante = await SolicitudExportacionService.ObtenerSolicitudPorSolicitante(requestModel);

        //    if (resultObtenerSolicitudPorSolicitante.OperacionExitosa)
        //    {
        //        result.Success = true;
        //        result.Result = resultObtenerSolicitudPorSolicitante;
        //    }

        //    result.Message = resultObtenerSolicitudPorSolicitante.Mensaje;
        //    return new JsonResult(result);
        //}

        /// <summary> 
        /// Consulta Reporte por Solicitud ID
        /// </summary>

        [HttpPost("ObtenerReporte")]
        public async Task<IActionResult> ObtenerReporte(RequestModelObtenerReporte requestModel)
        {
            var result = new ResponseModel();

            ReturnModelObtenerReporte resultObtenerReporte = await SolicitudExportacionService.ObtenerReporte(requestModel);

            if (resultObtenerReporte.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultObtenerReporte;
            }

            result.Message = resultObtenerReporte.Mensaje;
            return new JsonResult(result);
        }

        

        /// <summary> 
        /// Crear Solicitud
        /// </summary>

        [HttpPost("CrearSolicitud")]
        public async Task<IActionResult> CrearSolicitud(RequestModelCrearSolicitud requestModel)
        {
            var result = new ResponseModel();
            ReturnModelCrearSolicitud resultCrearSolicitud = await SolicitudExportacionService.CrearSolicitud(requestModel);

            if (resultCrearSolicitud.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultCrearSolicitud;
            }

            result.Message = resultCrearSolicitud.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Actualizar Solicitud
        /// </summary>

        [HttpPost("ActualizarSolicitud")]
        public async Task<IActionResult> ActualizarSolicitud(RequestModelActualizarSolicitud requestModel)
        {
            var result = new ResponseModel();

            ReturnModelActualizarSolicitud resultActualizarSolicitud = await SolicitudExportacionService.ActualizarSolicitud(requestModel);

            if (resultActualizarSolicitud.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultActualizarSolicitud;
            }

            result.Message = resultActualizarSolicitud.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Enviar Solicitud
        /// </summary>

        [HttpPost("EnviarSolicitud")]
        public async Task<IActionResult> EnviarSolicitud(RequestModelEnviarSolicitud requestModel)
        {
            var result = new ResponseModel();

            ReturnModelEnviarSolicitud resultEnviarSolicitud = await SolicitudExportacionService.EnviarSolicitud(requestModel);

            if (resultEnviarSolicitud.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultEnviarSolicitud;
            }

            result.Message = resultEnviarSolicitud.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Crear Obra
        /// </summary>

        [HttpPost("CrearObra")]
        public async Task<IActionResult> CrearObra(RequestModelCrearObra requestModel)
        {
            var result = new ResponseModel();

            ReturnModelCrearObra resultCrearObra = await SolicitudExportacionService.CrearObra(requestModel);

            if (resultCrearObra.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultCrearObra;
            }

            result.Message = resultCrearObra.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Actualizar Obra
        /// </summary>

        [HttpPost("ActualizarObra")]
        public async Task<IActionResult> ActualizarObra(RequestModelActualizarObra requestModel)
        {
            var result = new ResponseModel();

            ReturnModelActualizarObra resultActualizarObra = await SolicitudExportacionService.ActualizarObra(requestModel);

            if (resultActualizarObra.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultActualizarObra;
            }

            result.Message = resultActualizarObra.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Eliminar Obra
        /// </summary>

        [HttpPost("EliminarObra")]
        public async Task<IActionResult> EliminarObra(RequestModelEliminarObra requestModel)
        {
            var result = new ResponseModel();

            ReturnModelEliminarObra resultEliminarObra = await SolicitudExportacionService.EliminarObra(requestModel);

            if (resultEliminarObra.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultEliminarObra;
            }

            result.Message = resultEliminarObra.Mensaje;
            return new JsonResult(result);
        }

        ///// <summary> 
        ///// Iniciar Sesion Solicitud
        ///// </summary>

        //[HttpPost("Autenticacion")]
        //public async Task<IActionResult> Autenticacion(RequestModelAutenticacion requestModel)
        //{
        //    var result = new ResponseModel();

        //    ReturnModelAutenticacion resultAutenticacion = await SolicitudExportacionService.Autenticacion(requestModel);

        //    if (resultAutenticacion.OperacionExitosa)
        //    {
        //        result.Success = true;
        //        result.Result = resultAutenticacion;
        //    }

        //    result.Message = resultAutenticacion.Mensaje;
        //    return new JsonResult(result);
        //}

        /// <summary> 
        /// Obtener Municipios Por Depto Id
        /// </summary>

        [HttpGet("ObtenerMunicipios")]
        public async Task<IActionResult> ObtenerMunicipios(string padreId)
        {
            var result = new ResponseModel();

            ReturnModelObtenerMunicipios resultGetObtenerMunicipios = await SolicitudExportacionService.GetObtenerMunicipios(padreId);

            if (resultGetObtenerMunicipios.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerMunicipios.ZonasGeograficas.Select(m => new SelectListItem { Value = m.ZonId, Text = m.ZonNombre }); ;
            }

            result.Message = resultGetObtenerMunicipios.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener Municipios Por Mpio Id
        /// </summary>

        [HttpGet("ObtenerMunicipio")]
        public async Task<IActionResult> ObtenerMunicipio(int id)
        {
            var result = new ResponseModel();

            ReturnModelObtenerMunicipio resultGetObtenerMunicipio = await SolicitudExportacionService.GetObtenerMunicipio(id);

            if (resultGetObtenerMunicipio.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerMunicipio;
            }

            result.Message = resultGetObtenerMunicipio.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener todos los Departamentos
        /// </summary>

        [HttpGet("ObtenerDepartamentos")]
        public async Task<IActionResult> ObtenerDepartamentos()
        {
            var result = new ResponseModel();

            ReturnModelObtenerDepartamentos resultGetObtenerDepartamentos = await SolicitudExportacionService.GetObtenerDepartamentos();

            if (resultGetObtenerDepartamentos.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerDepartamentos.ZonasGeograficas.Select(m => new SelectListItem { Value = m.ZonId, Text = m.ZonNombre });
            }

            result.Message = resultGetObtenerDepartamentos.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener todos los Tipos de Motivos de Salidas
        /// </summary>

        [HttpGet("ObtenerTiposMotivos")]
        public async Task<IActionResult> ObtenerTiposMotivos()
        {
            var result = new ResponseModel();

            ReturnModelObtenerTiposMotivos resultGetObtenerTiposMotivos = await SolicitudExportacionService.GetObtenerTiposMotivos();

            if (resultGetObtenerTiposMotivos.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerTiposMotivos;
            }

            result.Message = resultGetObtenerTiposMotivos.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener todos los Tipos de Respuestas
        /// </summary>

        [HttpGet("ObtenerTiposRespuestas")]
        public async Task<IActionResult> ObtenerTiposRespuestas()
        {
            var result = new ResponseModel();

            ReturnModelObtenerTiposRespuestas resultGetObtenerTiposRespuestas = await SolicitudExportacionService.GetObtenerTiposRespuestas();

            if (resultGetObtenerTiposRespuestas.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerTiposRespuestas;
            }

            result.Message = resultGetObtenerTiposRespuestas.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener Clasificaciones Tipologicas
        /// </summary>

        [HttpGet("ObtenerClasificacionesTipologicas")]
        public async Task<IActionResult> ObtenerClasificacionesTipologicas()
        {
            var result = new ResponseModel();

            ReturnModelObtenerClasificacionesTipologicas resultGetObtenerClasificacionesTipologicas = await SolicitudExportacionService.GetObtenerClasificacionesTipologicas();

            if (resultGetObtenerClasificacionesTipologicas.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerClasificacionesTipologicas;
            }

            result.Message = resultGetObtenerClasificacionesTipologicas.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener Clasificaciones Tipologicas Grupos
        /// </summary>

        [HttpGet("ObtenerClasificacionesTipologicasGrupo")]
        public async Task<IActionResult> ObtenerClasificacionesTipologicasGrupo()
        {
            var result = new ResponseModel();

            ReturnModelObtenerClasificacionesTipologicasGrupo resultGetObtenerClasificacionesTipologicasGrupo = await SolicitudExportacionService.GetObtenerClasificacionesTipologicasGrupo();

            if (resultGetObtenerClasificacionesTipologicasGrupo.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerClasificacionesTipologicasGrupo;
            }

            result.Message = resultGetObtenerClasificacionesTipologicasGrupo.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener Clasificaciones Tipologicas x PadreId
        /// </summary>

       /* [HttpGet("ObtenerClasificacionesTipologicas")]
        public async Task<IActionResult> ObtenerClasificacionesTipologicas(int cltPadreId)
        {
            var result = new ResponseModel();

            ReturnModelObtenerClasificacionesTipologicas resultGetObtenerClasificacionesTipologicas = await SolicitudExportacionService.GetObtenerClasificacionesTipologicas(cltPadreId);

            if (resultGetObtenerClasificacionesTipologicas.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerClasificacionesTipologicas;
            }

            result.Message = resultGetObtenerClasificacionesTipologicas.Mensaje;
            return new JsonResult(result);
        }*/

        /// <summary> 
        /// Obtener todos los Paises
        /// </summary>

        [HttpGet("ObtenerPaises")]
        public async Task<IActionResult> ObtenerPaises()
        {
            var result = new ResponseModel();

            ReturnModelObtenerPaises resultGetObtenerPaises = await SolicitudExportacionService.GetObtenerPaises();

            if (resultGetObtenerPaises.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerPaises.Paises.Select(m => new SelectListItem { Value = m.ZopId.ToString(), Text = m.ZopNombre });
            }

            result.Message = resultGetObtenerPaises.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener todos los Tipos de Documentos de Identidad
        /// </summary>

        [HttpGet("ObtenerTiposDocumentosIndentidad")]
        public async Task<IActionResult> ObtenerTiposDocumentosIndentidad()
        {
            var result = new ResponseModel();

            var resultGetObtenerTiposDocumentosIndentidad = await SolicitudExportacionService.GetObtenerTiposDocumentosIndentidad();

            if (resultGetObtenerTiposDocumentosIndentidad.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerTiposDocumentosIndentidad.TiposDocumentos.Select(m => new SelectListItem { Value = m.DocId.ToString(), Text = m.DocNombre });
            }

            result.Message = resultGetObtenerTiposDocumentosIndentidad.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener todos los Tipos de Epocas
        /// </summary>

        [HttpGet("ObtenerTiposEpocas")]
        public async Task<IActionResult> ObtenerTiposEpocas()
        {
            var result = new ResponseModel();

            ReturnModelObtenerTiposEpocas resultGetObtenerTiposEpocas = await SolicitudExportacionService.GetObtenerTiposEpocas();

            if (resultGetObtenerTiposEpocas.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerTiposEpocas;
            }

            result.Message = resultGetObtenerTiposEpocas.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener todos los Tipos de Tecnica por Grupo Id
        /// </summary>

        [HttpGet("ObtenerTiposTecnicas")]
        public async Task<IActionResult> ObtenerTiposTecnicas(int grupoId)
        {
            var result = new ResponseModel();

            ReturnModelObtenerTiposTecnicas resultGetObtenerTiposTecnicas = await SolicitudExportacionService.GetObtenerTiposTecnicas(grupoId);

            if (resultGetObtenerTiposTecnicas.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerTiposTecnicas;
            }

            result.Message = resultGetObtenerTiposTecnicas.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener todos los Tipos de Personas
        /// </summary>

        [HttpGet("ObtenerTiposBasPersonas")]
        public async Task<IActionResult> ObtenerTiposBasPersonas()
        {
            var result = new ResponseModel();

            ReturnModelObtenerTiposBasPersonas resultGetObtenerTiposBasPersonas = await SolicitudExportacionService.GetObtenerTiposBasPersonas();

            if (resultGetObtenerTiposBasPersonas.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerTiposBasPersonas.TiposPersonas.Select(m => new SelectListItem { Value = m.TipTipoPersona.ToString(), Text = m.TipNombre });
            }

            result.Message = resultGetObtenerTiposBasPersonas.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener todos los Tipos de Permanencia
        /// </summary>

        [HttpGet("ObtenerTiposPermanencia")]
        public async Task<IActionResult> ObtenerTiposPermanencia()
        {
            var result = new ResponseModel();

            ReturnModelObtenerTiposPermanencia resultGetObtenerTiposPermanencia = await SolicitudExportacionService.GetObtenerTiposPermanencia();

            if (resultGetObtenerTiposPermanencia.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerTiposPermanencia.TiposGenericos.Select(m => new SelectListItem { Value = m.Id.ToString(), Text = m.Valor }); 
            }

            result.Message = resultGetObtenerTiposPermanencia.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener todos los Tipos de firma
        /// </summary>

        [HttpGet("ObtenerTiposFirma")]
        public async Task<IActionResult> ObtenerTiposFirma()
        {
            var result = new ResponseModel();

            ReturnModelObtenerTiposFirma resultGetObtenerTiposFirma = await SolicitudExportacionService.GetObtenerTiposFirma();

            if (resultGetObtenerTiposFirma.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerTiposFirma;
            }

            result.Message = resultGetObtenerTiposFirma.Mensaje;
            return new JsonResult(result);
        }

        /// <summary> 
        /// Obtener todos Fines de Exportacion
        /// </summary>

        [HttpGet("ObtenerFinesExportacion")]
        public async Task<IActionResult> ObtenerFinesExportacion()
        {
            var result = new ResponseModel();

            var resultGetObtenerFinesExportacion = await SolicitudExportacionService.GetObtenerFinesExportacion();

            if (resultGetObtenerFinesExportacion.OperacionExitosa)
            {
                result.Success = true;
                result.Result = resultGetObtenerFinesExportacion.TiposMotivos.Select(m => new SelectListItem { Value = m.TmsId.ToString(), Text = m.TmsNombre }); 
            }

            result.Message = resultGetObtenerFinesExportacion.Mensaje;
            return new JsonResult(result);
        }



    }
}