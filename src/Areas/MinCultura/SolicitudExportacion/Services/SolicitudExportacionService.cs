using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using tramites_servicios_webapi.common.Models;
using tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models;
using tramites_servicios_webapi.Utils;

namespace tramites_servicios_webapi.Mintransporte.Services
{

    public class SolicitudExportacionService
    {
        #region declaración de URLs 
        private static HttpClient client;


        private static string ClientCredentials = "ZlJBcmhDY0JUa1V3YWZBNDdjOGV4UDFSR05jYToxWTdIc3JxZjdxdWY3UUZNYkhxTlNvbmZsV2th";

        private static string UrlBase = "https://192.168.1.46:8243/";

        private const string UrlToken = "token";

        private const string UrlObtenerSolicitudPorNroConsecutivo = "ObtenerSolicitudPorNroConsecutivo/V1";

        private const string UrlObtenerListaAnexos = "ObtenerListaAnexos/V1";

        private const string UrlObtenerListaConceptosSolicitud = "ObtenerListaConceptosSolicitud/V1";

        private const string UrlObtenerListaConceptosObras = "ObtenerListaConceptosObras/V1";

        private const string UrlObtenerListaPrestamo = "ObtenerListaPrestamo/V1";

        private const string UrlObtenerListaDeterioro = "ObtenerListaDeterioro/V1";

        private const string UrlObtenerSolicitudPorId = "ObtenerSolicitudPorId/V1";

        private const string UrlObtenerSolicitudes = "ObtenerSolicitudes/V1";

        private const string UrlObtenerObras = "ObtenerObras/V1";

        private const string UrlObtenerAnexo = "ObtenerAnexo/V1";

        private const string UrlObtenerConceptoSolicitud = "ObtenerConceptoSolicitud/V1";

        private const string UrlObtenerSolicitudPorIntermediario = "ObtenerSolicitudPorIntermediario/V1";

        private const string UrlObtenerSolicitudPorSolicitante = "ObtenerSolicitudPorSolicitante/V1";

        private const string UrlObtenerReporte = "ObtenerReporte/V1";

        private const string UrlCrearSolicitud = "CrearSolicitud/V1";

        private const string UrlActualizarSolicitud = "ActualizarSolicitud/V1";

        private const string UrlEnviarSolicitud = "EnviarSolicitud/V1";

        private const string UrlCrearObra = "CrearObra/V1";

        private const string UrlActualizarObra = "ActualizarObra/V1";

        private const string UrlEliminarObra = "EliminarObra/V1";

        private const string UrlAutenticacion = "Autenticacion/V1";

        private const string UrlObtenerMunicipios = "ObtenerMunicipios/V1?padreId={0}";

        private const string UrlObtenerMunicipio = "ObtenerMunicipio/V1?id=11001";

        private const string UrlObtenerDepartamentos = "ObtenerDepartamentos/V1";

        private const string UrlObtenerTiposMotivos = "ObtenerTiposMotivos/V1";

        private const string UrlObtenerTiposRespuestas = "ObtenerTiposRespuestas/V1";

        private const string UrlObtenerClasificacionesTipologicas = "ObtenerClasificacionesTipologicas/V1";

        private const string UrlObtenerClasificacionesTipologicasGrupo = "ObtenerClasificacionesTipologicasGrupo/V1";

        private const string UrlObtenerPaises = "ObtenerPaises/v1";

        private const string UrlObtenerTiposDocumentosIndentidad = "ObtenerTiposDocumentosIndentidad/V1";

        private const string UrlObtenerTiposEpocas = "ObtenerTiposEpocas/V1";

        private const string UrlObtenerTiposTecnicas = "ObtenerTiposTecnicas/V1?grupoId=11";

        private const string UrlObtenerTiposBasPersonas = "ObtenerTiposBasPersonas/V1";

        private const string UrlObtenerTiposPermanencia = "ObtenerTiposPermanencia/V1";

        private const string UrlObtenerTiposFirma = "ObtenerTiposFirma/V1";

        private const string UrlObtenerFinesExportacion = "ObtenerFinesExportacion/V1";


        #endregion

        #region Constructor
        static SolicitudExportacionService()
        {
            var httpClientHandler = new HttpClientHandler();

            httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };

            client = new HttpClient(httpClientHandler);

            //client = new HttpClient();
            client.BaseAddress = new Uri(UrlBase);
        }
        #endregion

        #region Generacion de token
        /// <summary>
        /// Obtener token tramites fna
        /// </summary>
        /// <returns>Token</returns>
        public static async Task<TokenModel> ObtenerToken()
        {
            // Inicializaciones
            String resultado = null;
            TokenModel tokenModel = null;

            //Content
            HttpContent content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("grant_type", "client_credentials")
            });
            client.DefaultRequestHeaders.Accept.Clear();
            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", ClientCredentials);

            //Petición API Gateway
            try
            {
                HttpResponseMessage resp = await client.PostAsync(UrlToken, content);
                if (resp.IsSuccessStatusCode)
                {
                    resultado = JsonConvert.SerializeObject(resp);
                    resultado = resp.Content.ReadAsStringAsync().Result;
                    tokenModel = JsonConvert.DeserializeObject<TokenModel>(resultado);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }

            //Salida
            return tokenModel;
        }
        #endregion

        #region APIs
        /// <summary> 
        /// Consultar Solicitudes Por Nro Consecutivo
        /// </summary>

        public static async Task<ReturnModelObtenerSolicitudPorNroConsecutivo> ObtenerSolicitudPorNroConsecutivo(RequestModelObtenerSolicitudPorNroConsecutivo requestModel)
        {
            ReturnModelObtenerSolicitudPorNroConsecutivo result = new ReturnModelObtenerSolicitudPorNroConsecutivo();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerSolicitudPorNroConsecutivo);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerSolicitudPorNroConsecutivo>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerSolicitudPorNroConsecutivo(false, "Se presentó un error al Consultar Solicitudes Por Nro Consecutivo");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerSolicitudPorNroConsecutivo(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Consultar Lista Anexos Solicitudes Por Solicitud ID
        /// </summary>

        public static async Task<ReturnModelObtenerListaAnexos> ObtenerListaAnexos(RequestModelObtenerListaAnexos requestModel)
        {
            ReturnModelObtenerListaAnexos result = new ReturnModelObtenerListaAnexos();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerListaAnexos);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerListaAnexos>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerListaAnexos(false, "Se presentó un error al Consultar Lista Anexos Solicitudes Por Solicitud ID");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerListaAnexos(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        ///// <summary> 
        ///// Consultar Lista Conceptos Solicitudes Por Solicitud ID
        ///// </summary>

        //public static async Task<ReturnModelObtenerListaConceptosSolicitud> ObtenerListaConceptosSolicitud(RequestModelObtenerListaConceptosSolicitud requestModel)
        //{
        //    ReturnModelObtenerListaConceptosSolicitud result = new ReturnModelObtenerListaConceptosSolicitud();

        //    //Obtiene el token actual
        //    TokenModel tokenModel = await ObtenerToken();
        //    string output = "";

        //    output = JsonConvert.SerializeObject(requestModel);

        //    client.DefaultRequestHeaders.Accept.Clear();
        //    client.DefaultRequestHeaders.Accept.Add(
        //        new MediaTypeWithQualityHeaderValue("application/json")
        //    );
        //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

        //    //Realiza la consulta            
        //    StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
        //    var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerListaConceptosSolicitud);
        //    request.Content = stringContent;

        //    try
        //    {
        //        HttpResponseMessage resp = await client.SendAsync(request);
        //        if (resp.IsSuccessStatusCode)
        //        {
        //            string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
        //            result = JsonConvert.DeserializeObject<ReturnModelObtenerListaConceptosSolicitud>(response);
        //            result.OperacionExitosa = true;
        //        }
        //        else
        //        {
        //            result = new ReturnModelObtenerListaConceptosSolicitud(false, "Se presentó un error al Consultar Lista Conceptos Solicitudes Por Solicitud ID");
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        result = new ReturnModelObtenerListaConceptosSolicitud(false, e.Message);
        //        //TODO: manejador de excepciones y mongo
        //        Console.WriteLine(e.Message);
        //    }
        //    return result;
        //}

        /// <summary> 
        /// Consultar Lista Conceptos Obras Por Solicitud ID
        /// </summary>

        public static async Task<ReturnModelObtenerListaConceptosObras> ObtenerListaConceptosObras(RequestModelObtenerListaConceptosObras requestModel)
        {
            ReturnModelObtenerListaConceptosObras result = new ReturnModelObtenerListaConceptosObras();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerListaConceptosObras);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerListaConceptosObras>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerListaConceptosObras(false, "Se presentó un error al Consultar Lista Conceptos Obras Por Solicitud ID");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerListaConceptosObras(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Consultar Lista Prestamos
        /// </summary>

        public static async Task<ReturnModelObtenerListaPrestamo> ObtenerListaPrestamo(RequestModelObtenerListaPrestamo requestModel)
        {
            ReturnModelObtenerListaPrestamo result = new ReturnModelObtenerListaPrestamo();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerListaPrestamo);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerListaPrestamo>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerListaPrestamo(false, "Se presentó un error al Consultar Lista Prestamos");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerListaPrestamo(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Consultar Lista Deterioros
        /// </summary>

        public static async Task<ReturnModelObtenerListaDeterioro> ObtenerListaDeterioro(RequestModelObtenerListaDeterioro requestModel)
        {
            ReturnModelObtenerListaDeterioro result = new ReturnModelObtenerListaDeterioro();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerListaDeterioro);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerListaDeterioro>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerListaDeterioro(false, "Se presentó un error al Consultar Lista Deterioros");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerListaDeterioro(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Consultar Solicitudes Por Id
        /// </summary>

        public static async Task<ReturnModelObtenerSolicitudPorId> ObtenerSolicitudPorId(RequestModelObtenerSolicitudPorId requestModel)
        {
            ReturnModelObtenerSolicitudPorId result = new ReturnModelObtenerSolicitudPorId();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerSolicitudPorId);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerSolicitudPorId>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerSolicitudPorId(false, "Se presentó un error al Consultar Solicitudes Por Id");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerSolicitudPorId(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Consultar las Solicitudes por Rangos
        /// </summary>

        public static async Task<ReturnModelObtenerSolicitudes> ObtenerSolicitudes(RequestModelObtenerSolicitudes requestModel)
        {
            ReturnModelObtenerSolicitudes result = new ReturnModelObtenerSolicitudes();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerSolicitudes);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerSolicitudes>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerSolicitudes(false, "Se presentó un error al Consultar las Solicitudes por Rangos");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerSolicitudes(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Consultar Obras Por Id
        /// </summary>

        public static async Task<ReturnModelObtenerObras> ObtenerObras(RequestModelObtenerObras requestModel)
        {
            ReturnModelObtenerObras result = new ReturnModelObtenerObras();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerObras);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerObras>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerObras(false, "Se presentó un error al Consultar Obras Por Id");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerObras(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Consultar Anexo por ID
        /// </summary>

        public static async Task<ReturnModelObtenerAnexo> ObtenerAnexo(RequestModelObtenerAnexo requestModel)
        {
            ReturnModelObtenerAnexo result = new ReturnModelObtenerAnexo();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerAnexo);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerAnexo>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerAnexo(false, "Se presentó un error al Consultar Anexo por ID");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerAnexo(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        ///// <summary> 
        ///// Consultar Concepto Solicitud Por Obra Id
        ///// </summary>

        public static async Task<ReturnModelObtenerConceptoSolicitud> ObtenerConceptoSolicitud(RequestModelObtenerConceptoSolicitud requestModel)
        {
            ReturnModelObtenerConceptoSolicitud result = new ReturnModelObtenerConceptoSolicitud();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerConceptoSolicitud);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerConceptoSolicitud>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerConceptoSolicitud(false, "Se presentó un error al Consultar Concepto Solicitud Por Obra Id");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerConceptoSolicitud(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Consultar Solicitud Por Nro Doc Intermediario
        /// </summary>

        public static async Task<ReturnModelObtenerSolicitudPorIntermediario> ObtenerSolicitudPorIntermediario(RequestModelObtenerSolicitudPorIntermediario requestModel)
        {
            ReturnModelObtenerSolicitudPorIntermediario result = new ReturnModelObtenerSolicitudPorIntermediario();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerSolicitudPorIntermediario);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerSolicitudPorIntermediario>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerSolicitudPorIntermediario(false, "Se presentó un error al Consultar Solicitud Por Nro Doc Intermediario");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerSolicitudPorIntermediario(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Consultar Solicitud Por Nro Doc del Solicitante
        /// </summary>

        public static async Task<ReturnModelObtenerSolicitudPorSolicitante> ObtenerSolicitudPorSolicitante(RequestModelObtenerSolicitudPorSolicitante requestModel)
        {
            ReturnModelObtenerSolicitudPorSolicitante result = new ReturnModelObtenerSolicitudPorSolicitante();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerSolicitudPorSolicitante);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerSolicitudPorSolicitante>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerSolicitudPorSolicitante(false, "Se presentó un error al Consultar Solicitud Por Nro Doc del Solicitante");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerSolicitudPorSolicitante(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Consulta Reporte por Solicitud ID
        /// </summary>

        public static async Task<ReturnModelObtenerReporte> ObtenerReporte(RequestModelObtenerReporte requestModel)
        {
            ReturnModelObtenerReporte result = new ReturnModelObtenerReporte();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlObtenerReporte);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerReporte>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerReporte(false, "Se presentó un error al Consulta Reporte por Solicitud ID");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerReporte(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        
        /// <summary> 
        /// Crear Solicitud
        /// </summary>

        public static async Task<ReturnModelCrearSolicitud> CrearSolicitud(RequestModelCrearSolicitud requestModel)
        {
            ReturnModelCrearSolicitud result = new ReturnModelCrearSolicitud();
            requestModel.SosNombreRepresentante = "k";
            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlCrearSolicitud);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelCrearSolicitud>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelCrearSolicitud(false, "Se presentó un error al Crear Solicitud");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelCrearSolicitud(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Actualizar Solicitud
        /// </summary>

        public static async Task<ReturnModelActualizarSolicitud> ActualizarSolicitud(RequestModelActualizarSolicitud requestModel)
        {
            ReturnModelActualizarSolicitud result = new ReturnModelActualizarSolicitud();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlActualizarSolicitud);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelActualizarSolicitud>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelActualizarSolicitud(false, "Se presentó un error al Actualizar Solicitud");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelActualizarSolicitud(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Enviar Solicitud
        /// </summary>

        public static async Task<ReturnModelEnviarSolicitud> EnviarSolicitud(RequestModelEnviarSolicitud requestModel)
        {
            ReturnModelEnviarSolicitud result = new ReturnModelEnviarSolicitud();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlEnviarSolicitud);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelEnviarSolicitud>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelEnviarSolicitud(false, "Se presentó un error al Enviar Solicitud");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelEnviarSolicitud(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Crear Obra
        /// </summary>

        public static async Task<ReturnModelCrearObra> CrearObra(RequestModelCrearObra requestModel)
        {
            ReturnModelCrearObra result = new ReturnModelCrearObra();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlCrearObra);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelCrearObra>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelCrearObra(false, "Se presentó un error al Crear Obra");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelCrearObra(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Actualizar Obra
        /// </summary>

        public static async Task<ReturnModelActualizarObra> ActualizarObra(RequestModelActualizarObra requestModel)
        {
            ReturnModelActualizarObra result = new ReturnModelActualizarObra();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlActualizarObra);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelActualizarObra>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelActualizarObra(false, "Se presentó un error al Actualizar Obra");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelActualizarObra(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Eliminar Obra
        /// </summary>

        public static async Task<ReturnModelEliminarObra> EliminarObra(RequestModelEliminarObra requestModel)
        {
            ReturnModelEliminarObra result = new ReturnModelEliminarObra();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";

            output = JsonConvert.SerializeObject(requestModel);

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), UrlEliminarObra);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelEliminarObra>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelEliminarObra(false, "Se presentó un error al Eliminar Obra");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelEliminarObra(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        ///// <summary> 
        ///// Iniciar Sesion Solicitud
        ///// </summary>

        //public static async Task<ReturnModelAutenticacion> Autenticacion(RequestModelAutenticacion requestModel)
        //{
        //    ReturnModelAutenticacion result = new ReturnModelAutenticacion();

        //    //Obtiene el token actual
        //    TokenModel tokenModel = await ObtenerToken();
        //    string output = "";

        //    output = JsonConvert.SerializeObject(requestModel);

        //    client.DefaultRequestHeaders.Accept.Clear();
        //    client.DefaultRequestHeaders.Accept.Add(
        //        new MediaTypeWithQualityHeaderValue("application/json")
        //    );
        //    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

        //    //Realiza la consulta            
        //    StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
        //    var request = new HttpRequestMessage(new HttpMethod("POST"), UrlAutenticacion);
        //    request.Content = stringContent;

        //    try
        //    {
        //        HttpResponseMessage resp = await client.SendAsync(request);
        //        if (resp.IsSuccessStatusCode)
        //        {
        //            string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
        //            result = JsonConvert.DeserializeObject<ReturnModelAutenticacion>(response);
        //            result.OperacionExitosa = true;
        //        }
        //        else
        //        {
        //            result = new ReturnModelAutenticacion(false, "Se presentó un error al Iniciar Sesion Solicitud");
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        result = new ReturnModelAutenticacion(false, e.Message);
        //        //TODO: manejador de excepciones y mongo
        //        Console.WriteLine(e.Message);
        //    }
        //    return result;
        //}

        /// <summary> 
        /// Obtener Municipios Por Depto Id
        /// </summary>

        public static async Task<ReturnModelObtenerMunicipios> GetObtenerMunicipios(string padreId)
        {
            ReturnModelObtenerMunicipios result = new ReturnModelObtenerMunicipios();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";
           


            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), string.Format(UrlObtenerMunicipios,padreId));
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerMunicipios>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerMunicipios(false, "Se presentó un error al Obtener Municipios Por Depto Id");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerMunicipios(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener Municipios Por Mpio Id
        /// </summary>

        public static async Task<ReturnModelObtenerMunicipio> GetObtenerMunicipio(int id)
        {
            ReturnModelObtenerMunicipio result = new ReturnModelObtenerMunicipio();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";
            //Construye el encabezado
            Dictionary<string, string> content = new Dictionary<string, string>();
            content.Add("id", id.ToString());
            output = JsonConvert.SerializeObject(content);


            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerMunicipio);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerMunicipio>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerMunicipio(false, "Se presentó un error al Obtener Municipios Por Mpio Id");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerMunicipio(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener todos los Departamentos
        /// </summary>

        public static async Task<ReturnModelObtenerDepartamentos> GetObtenerDepartamentos()
        {
            ReturnModelObtenerDepartamentos result = new ReturnModelObtenerDepartamentos();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerDepartamentos);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerDepartamentos>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerDepartamentos(false, "Se presentó un error al Obtener todos los Departamentos");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerDepartamentos(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener todos los Tipos de Motivos de Salidas
        /// </summary>

        public static async Task<ReturnModelObtenerTiposMotivos> GetObtenerTiposMotivos()
        {
            ReturnModelObtenerTiposMotivos result = new ReturnModelObtenerTiposMotivos();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerTiposMotivos);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerTiposMotivos>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerTiposMotivos(false, "Se presentó un error al Obtener todos los Tipos de Motivos de Salidas");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerTiposMotivos(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener todos los Tipos de Respuestas
        /// </summary>

        public static async Task<ReturnModelObtenerTiposRespuestas> GetObtenerTiposRespuestas()
        {
            ReturnModelObtenerTiposRespuestas result = new ReturnModelObtenerTiposRespuestas();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerTiposRespuestas);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerTiposRespuestas>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerTiposRespuestas(false, "Se presentó un error al Obtener todos los Tipos de Respuestas");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerTiposRespuestas(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener Clasificaciones Tipologicas
        /// </summary>

        public static async Task<ReturnModelObtenerClasificacionesTipologicas> GetObtenerClasificacionesTipologicas()
        {
            ReturnModelObtenerClasificacionesTipologicas result = new ReturnModelObtenerClasificacionesTipologicas();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerClasificacionesTipologicas);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerClasificacionesTipologicas>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerClasificacionesTipologicas(false, "Se presentó un error al Obtener Clasificaciones Tipologicas");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerClasificacionesTipologicas(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener Clasificaciones Tipologicas Grupos
        /// </summary>

        public static async Task<ReturnModelObtenerClasificacionesTipologicasGrupo> GetObtenerClasificacionesTipologicasGrupo()
        {
            ReturnModelObtenerClasificacionesTipologicasGrupo result = new ReturnModelObtenerClasificacionesTipologicasGrupo();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerClasificacionesTipologicasGrupo);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerClasificacionesTipologicasGrupo>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerClasificacionesTipologicasGrupo(false, "Se presentó un error al Obtener Clasificaciones Tipologicas Grupos");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerClasificacionesTipologicasGrupo(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener Clasificaciones Tipologicas x PadreId
        /// </summary>

        public static async Task<ReturnModelObtenerClasificacionesTipologicas> GetObtenerClasificacionesTipologicas(int cltPadreId)
        {
            ReturnModelObtenerClasificacionesTipologicas result = new ReturnModelObtenerClasificacionesTipologicas();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";
            //Construye el encabezado
            Dictionary<string, string> content = new Dictionary<string, string>();
            content.Add("cltPadreId", cltPadreId.ToString());
            output = JsonConvert.SerializeObject(content);


            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerClasificacionesTipologicas);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerClasificacionesTipologicas>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerClasificacionesTipologicas(false, "Se presentó un error al Obtener Clasificaciones Tipologicas x PadreId");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerClasificacionesTipologicas(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener todos los Paises
        /// </summary>

        public static async Task<ReturnModelObtenerPaises> GetObtenerPaises()
        {
            ReturnModelObtenerPaises result = new ReturnModelObtenerPaises();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerPaises);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerPaises>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerPaises(false, "Se presentó un error al Obtener todos los Paises");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerPaises(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener todos los Tipos de Documentos de Identidad
        /// </summary>

        public static async Task<ReturnModelObtenerTiposDocumentosIndentidad> GetObtenerTiposDocumentosIndentidad()
        {
            ReturnModelObtenerTiposDocumentosIndentidad result = new ReturnModelObtenerTiposDocumentosIndentidad();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerTiposDocumentosIndentidad);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerTiposDocumentosIndentidad>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerTiposDocumentosIndentidad(false, "Se presentó un error al Obtener todos los Tipos de Documentos de Identidad");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerTiposDocumentosIndentidad(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener todos los Tipos de Epocas
        /// </summary>

        public static async Task<ReturnModelObtenerTiposEpocas> GetObtenerTiposEpocas()
        {
            ReturnModelObtenerTiposEpocas result = new ReturnModelObtenerTiposEpocas();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerTiposEpocas);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerTiposEpocas>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerTiposEpocas(false, "Se presentó un error al Obtener todos los Tipos de Epocas");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerTiposEpocas(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener todos los Tipos de Tecnica por Grupo Id
        /// </summary>

        public static async Task<ReturnModelObtenerTiposTecnicas> GetObtenerTiposTecnicas(int grupoId)
        {
            ReturnModelObtenerTiposTecnicas result = new ReturnModelObtenerTiposTecnicas();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";
            //Construye el encabezado
            Dictionary<string, string> content = new Dictionary<string, string>();
            content.Add("grupoId", grupoId.ToString());
            output = JsonConvert.SerializeObject(content);


            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerTiposTecnicas);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerTiposTecnicas>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerTiposTecnicas(false, "Se presentó un error al Obtener todos los Tipos de Tecnica por Grupo Id");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerTiposTecnicas(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener todos los Tipos de Personas
        /// </summary>

        public static async Task<ReturnModelObtenerTiposBasPersonas> GetObtenerTiposBasPersonas()
        {
            ReturnModelObtenerTiposBasPersonas result = new ReturnModelObtenerTiposBasPersonas();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerTiposBasPersonas);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerTiposBasPersonas>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerTiposBasPersonas(false, "Se presentó un error al Obtener todos los Tipos de Personas");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerTiposBasPersonas(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener todos los Tipos de Permanencia
        /// </summary>

        public static async Task<ReturnModelObtenerTiposPermanencia> GetObtenerTiposPermanencia()
        {
            ReturnModelObtenerTiposPermanencia result = new ReturnModelObtenerTiposPermanencia();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerTiposPermanencia);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerTiposPermanencia>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerTiposPermanencia(false, "Se presentó un error al Obtener todos los Tipos de Permanencia");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerTiposPermanencia(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener todos los Tipos de firma
        /// </summary>

        public static async Task<ReturnModelObtenerTiposFirma> GetObtenerTiposFirma()
        {
            ReturnModelObtenerTiposFirma result = new ReturnModelObtenerTiposFirma();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerTiposFirma);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerTiposFirma>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerTiposFirma(false, "Se presentó un error al Obtener todos los Tipos de firma");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerTiposFirma(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }

        /// <summary> 
        /// Obtener todos Fines de Exportacion
        /// </summary>

        public static async Task<ReturnModelObtenerFinesExportacion> GetObtenerFinesExportacion()
        {
            ReturnModelObtenerFinesExportacion result = new ReturnModelObtenerFinesExportacion();

            //Obtiene el token actual
            TokenModel tokenModel = await ObtenerToken();
            string output = "";



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json")
            );
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", tokenModel.AccessToken);

            //Realiza la consulta            
            StringContent stringContent = new StringContent(output, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), UrlObtenerFinesExportacion);
            request.Content = stringContent;

            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result.Replace(".0,", ",").PropertiesToPascalCase();
                    result = JsonConvert.DeserializeObject<ReturnModelObtenerFinesExportacion>(response);
                    result.OperacionExitosa = true;
                }
                else
                {
                    result = new ReturnModelObtenerFinesExportacion(false, "Se presentó un error al Obtener todos Fines de Exportacion");
                }
            }
            catch (Exception e)
            {
                result = new ReturnModelObtenerFinesExportacion(false, e.Message);
                //TODO: manejador de excepciones y mongo
                Console.WriteLine(e.Message);
            }
            return result;
        }


        #endregion
    }
}