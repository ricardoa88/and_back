namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class Tiposmotivo
    {
        public object[] PatSolicitudSalidaObras { get; set; }
        public int TmsId { get; set; }
        public string TmsNombre { get; set; }
        public string TmsEstado { get; set; }
    }

}
