using System;

namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerSolicitudPorNroConsecutivo : MinCulturaBodyResult
    {

        public ReturnModelObtenerSolicitudPorNroConsecutivo() { }

        public ReturnModelObtenerSolicitudPorNroConsecutivo(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
        
        public Solicitudsalidaobra SolicitudSalidaObra { get; set; }
        public object Solicitud { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

}
