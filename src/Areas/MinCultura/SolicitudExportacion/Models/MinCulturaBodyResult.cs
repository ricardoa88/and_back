namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{

    public class MinCulturaBodyResult
    {
        public string Mensaje { get; set; }

        public bool OperacionExitosa { get; set; }

        public MinCulturaBodyResult() { }

        public MinCulturaBodyResult(bool operacionExitosa, string mensaje) {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
    }
}
