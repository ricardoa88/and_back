namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerClasificacionesTipologicas : MinCulturaBodyResult
    {

        public ReturnModelObtenerClasificacionesTipologicas() { }

        public ReturnModelObtenerClasificacionesTipologicas(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }

        public Clasificaciontipologica[] ClasificacionTipologica { get; set; }
        public object ClasificacionTipologicaGrupo { get; set; }
        public object ClasificacionTipologicaGrupoId { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Clasificaciontipologica
    {
        public int CltId { get; set; }
        public string CtlCodigo { get; set; }
        public string CltNombre { get; set; }
        public int CltPadreId { get; set; }
        public int CltNivel { get; set; }
        public string CltNombreNivel { get; set; }
        public int PagId { get; set; }
        public string CltRegistroHijos { get; set; }
    }

}
