namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class Error
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

}
