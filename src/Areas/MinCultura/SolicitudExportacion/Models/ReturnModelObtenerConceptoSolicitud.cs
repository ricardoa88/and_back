namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerConceptoSolicitud : MinCulturaBodyResult
    {

        public ReturnModelObtenerConceptoSolicitud() { }

        public ReturnModelObtenerConceptoSolicitud(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }

        public Concepto Concepto { get; set; }
        public Concepto[] Conceptos { get; set; }
        public object ConceptosObras { get; set; }
        public object Prestamos { get; set; }
        public object Deterioros { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }


}
