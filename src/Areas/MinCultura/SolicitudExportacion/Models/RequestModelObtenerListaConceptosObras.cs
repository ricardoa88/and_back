namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class RequestModelObtenerListaConceptosObras
    {
        public int SosId { get; set; }
        public int? IdFicha { get; set; }
        public int? IdConcepto { get; set; }
    }

}
