using System;

namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerSolicitudPorSolicitante : MinCulturaBodyResult
    {

        public ReturnModelObtenerSolicitudPorSolicitante() { }

        public ReturnModelObtenerSolicitudPorSolicitante(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
    
        public Solicitudsalidaobra SolicitudSalidaObras { get; set; }
        public Solicitudsalidaobra[] SolicitudSolicitantesSalidaObras { get; set; }
        public object SolicitudSolicitanteSalidaObras { get; set; }
        public bool Success { get; set; }
        public object Errors { get; set; }
    }

    

}
