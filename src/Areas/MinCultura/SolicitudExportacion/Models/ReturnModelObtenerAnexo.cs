namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerAnexo : MinCulturaBodyResult
    {

        public ReturnModelObtenerAnexo() { }

        public ReturnModelObtenerAnexo(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }

        public object ListaAnexos { get; set; }
        public Anexo Anexo { get; set; }
        public object PatAnexo { get; set; }
        public object ListPatAnexos { get; set; }
        public object AnexosSolicitud { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

}
