namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelEliminarObra : MinCulturaBodyResult
    {

        public ReturnModelEliminarObra() { }

        public ReturnModelEliminarObra(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
    
        public Fichatecnicabienes FichaTecnicaBienes { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

}
