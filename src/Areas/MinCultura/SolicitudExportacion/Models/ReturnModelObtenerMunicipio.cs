namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerMunicipio : MinCulturaBodyResult
    {

        public ReturnModelObtenerMunicipio() { }

        public ReturnModelObtenerMunicipio(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
        public object Paises { get; set; }
        public object ZonasGeograficas { get; set; }
        public Zonageografica ZonaGeografica { get; set; }
        public object TiposDocumentos { get; set; }
        public object TiposEpocas { get; set; }
        public object TiposTecnicas { get; set; }
        public object TiposPersonas { get; set; }
        public object TiposMotivos { get; set; }
        public object TiposRespuestas { get; set; }
        public object TiposGenericos { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Zonageografica
    {
        public object[] PatSolicitudSalidaObras { get; set; }
        public int ZonId { get; set; }
        public string ZonNombre { get; set; }
        public int ZonPadreId { get; set; }
        public int ZonPoblacion { get; set; }
        public string X { get; set; }
        public float ZonLatitud { get; set; }
        public float ZonLongitud { get; set; }
        public bool EsCorrimiento { get; set; }
    }

}
