namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class RequestModelObtenerListaDeterioro
    {
        public int IdFicha { get; set; }
        public int? IdPrestamo { get; set; }
    }

}
