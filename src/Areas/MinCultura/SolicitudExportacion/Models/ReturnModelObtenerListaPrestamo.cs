using System;

namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerListaPrestamo : MinCulturaBodyResult
    {

        public ReturnModelObtenerListaPrestamo() { }

        public ReturnModelObtenerListaPrestamo(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }

        public object Concepto { get; set; }
        public object Conceptos { get; set; }
        public object ConceptosObras { get; set; }
        public Prestamo[] Prestamos { get; set; }
        public object Deterioros { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Prestamo
    {
        public int IdPrestamo { get; set; }
        public int SosId { get; set; }
        public string RegistroMincultura { get; set; }
        public string CodigoObjetoEntidad { get; set; }
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public object Firmado { get; set; }
        public string Atributo { get; set; }
        public string Epoca { get; set; }
        public int Fechado { get; set; }
        public int Tecnica { get; set; }
        public string Alto { get; set; }
        public string Ancho { get; set; }
        public string Largo { get; set; }
        public string Profundidad { get; set; }
        public string Espesor { get; set; }
        public string DiametroMay { get; set; }
        public string DiametroMen { get; set; }
        public string Peso { get; set; }
        public string DescripcionObjeto { get; set; }
        public string ElementoRelacionado { get; set; }
        public string CodigoEntidad { get; set; }
        public string Dimsension { get; set; }
        public int TecnicaRelacion { get; set; }
        public object DescripcionRelacion { get; set; }
        public string Exposicion { get; set; }
        public string EntidadRespCol { get; set; }
        public string EntidadRespExt { get; set; }
        public object PaisExp { get; set; }
        public string CiudadExp { get; set; }
        public string LugarExp { get; set; }
        public string Justificacion { get; set; }
        public string Avaluador { get; set; }
        public string Avaluo { get; set; }
        public DateTime Fecha { get; set; }
        public string Aseguradora { get; set; }
        public string Poliza { get; set; }
        public string Valor { get; set; }
        public string EmpresaEmpaque { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public string EntregadoPor { get; set; }
        public object Firma { get; set; }
        public object FechaExposicion { get; set; }
        public string ObservacionesExp { get; set; }
        public string ReintegroEntregado { get; set; }
        public string ReintegroFirmaEnt { get; set; }
        public string ReintegroRecibido { get; set; }
        public string ReintegroFirmaRec { get; set; }
        public string ObservacionesReintegro { get; set; }
        public string Temperatura { get; set; }
        public string Luz { get; set; }
        public string Hr { get; set; }
        public string RevisadoPor { get; set; }
        public object Bueno { get; set; }
        public object Regular { get; set; }
        public object Malo { get; set; }
        public int Restaurado { get; set; }
        public DateTime FechaRestauracion { get; set; }
        public string Responsable { get; set; }
        public string Recomendaciones { get; set; }
        public DateTime FechaSalida { get; set; }
        public DateTime FechaLlegada { get; set; }
        public int FicId { get; set; }
        public object Curador { get; set; }
    }

}
