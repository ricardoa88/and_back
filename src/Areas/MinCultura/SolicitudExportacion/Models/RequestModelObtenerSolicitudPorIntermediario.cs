namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class RequestModelObtenerSolicitudPorIntermediario
    {
        public string NroDocumentoIntermediario { get; set; }
    }

}
