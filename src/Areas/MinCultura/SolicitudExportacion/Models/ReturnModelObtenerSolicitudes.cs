using System;

namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerSolicitudes : MinCulturaBodyResult
    {

        public ReturnModelObtenerSolicitudes() { }

        public ReturnModelObtenerSolicitudes(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
        public Solicitudsalidaobra[] SolicitudSalidaObras { get; set; }
        public object SolicitudSolicitantesSalidaObras { get; set; }
        public object SolicitudSolicitanteSalidaObras { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

}
