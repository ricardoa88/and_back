namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerTiposEpocas : MinCulturaBodyResult
    {

        public ReturnModelObtenerTiposEpocas() { }

        public ReturnModelObtenerTiposEpocas(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
        public object Paises { get; set; }
        public object ZonasGeograficas { get; set; }
        public object ZonaGeografica { get; set; }
        public object TiposDocumentos { get; set; }
        public Tiposepoca[] TiposEpocas { get; set; }
        public object TiposTecnicas { get; set; }
        public object TiposPersonas { get; set; }
        public object TiposMotivos { get; set; }
        public object TiposRespuestas { get; set; }
        public object TiposGenericos { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Tiposepoca
    {
        public int TepId { get; set; }
        public string TepNombre { get; set; }
    }

}
