using System.Collections.Generic;

namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelCrearSolicitud : MinCulturaBodyResult
    {

        public ReturnModelCrearSolicitud() { }

        public ReturnModelCrearSolicitud(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
        public Solicitudsalidaobra SolicitudSalidaObra { get; set; }
        public object Solicitud { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

}
