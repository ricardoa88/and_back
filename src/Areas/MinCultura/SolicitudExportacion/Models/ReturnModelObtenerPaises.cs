namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerPaises : MinCulturaBodyResult
    {

        public ReturnModelObtenerPaises() { }

        public ReturnModelObtenerPaises(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
    
        public Pais[] Paises { get; set; }
        public object ZonasGeograficas { get; set; }
        public object ZonaGeografica { get; set; }
        public object TiposDocumentos { get; set; }
        public object TiposEpocas { get; set; }
        public object TiposTecnicas { get; set; }
        public object TiposPersonas { get; set; }
        public object TiposMotivos { get; set; }
        public object TiposRespuestas { get; set; }
        public object TiposGenericos { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Pais
    {
        public object[] PatSolicitudSalidaObras { get; set; }
        public int ZopId { get; set; }
        public string ZopNombre { get; set; }
        public string ZopCodigoInternet { get; set; }
    }

}
