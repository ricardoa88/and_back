namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class RequestModelCrearObra
    {
        public int SosId { get; set; }
        public int DocId { get; set; }
        public string FichaTitulo { get; set; }
        public string FichaAutor { get; set; }
        public string FechaElaboracionObra { get; set; }
        public string FichaTecnica { get; set; }
        public string FichaPropietario { get; set; }
        public string FichaNroDocumentoIdentidad { get; set; }
        public string FichaObservaciones { get; set; }
        public object FichaFoto { get; set; }
        public string FichaAlto { get; set; }
        public string FichaLargo { get; set; }
        public string FichaAncho { get; set; }
        public string FichaProfundidad { get; set; }
        public string FichaEspesor { get; set; }
        public int TepId { get; set; }
        public int IdFirmado { get; set; }
        public int CtlPadreId { get; set; }
        public int CtlHijoId { get; set; }
        public int CtlId { get; set; }
        public int Cantidad { get; set; }
        public Anexo[] Anexos { get; set; }
    }


}
