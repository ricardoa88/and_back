using System;

namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class Solicitudsalidaobra
    {
        public object BasTiposDocumentosIdentidad { get; set; }
        public object BasZonasGeograficas { get; set; }
        public object BasZonasPaises { get; set; }
        public object[] PatSipaConceptoObras { get; set; }
        public object PatTiposMotivosSalidas { get; set; }
        public object[] PatFichaTecnicaBienes { get; set; }
        public object[] PatAnexos { get; set; }
        public int? SosId { get; set; }
        public string Ciudad { get; set; }
        public int? DocIdSolicitante { get; set; }
        public int? ZopId { get; set; }
        public string ZonId { get; set; }
        public string SosNombreSolicitante { get; set; }
        public string SosNroDocumentoSolicitante { get; set; }
        public int? SosNroFoliosAnexos { get; set; }
        public DateTime? SosFechaParaDarConcepto { get; set; }
        public int? SosCantidad { get; set; }
        public string Estado { get; set; }
        public string SosConsecutivo { get; set; }
        public DateTime? SosFechaRadicacion { get; set; }
        public int? TmsId { get; set; }
        public string SosLugarExpedicion { get; set; }
        public string SosDireccionSolicitante { get; set; }
        public string SosTelefonoSolicitante { get; set; }
        public string SosCorreoSolicitante { get; set; }
        public string SosNombreIntermediario { get; set; }
        public int? DocIdIntermediario { get; set; }
        public string SosNroDocumentoIntermediario { get; set; }
        public string SosDireccionIntermediario { get; set; }
        public string SosTelefonoIntermediario { get; set; }
        public string SosSinoIntermediario { get; set; }
        public string SosSinoAnexos { get; set; }
        public string SosSinoProrroga { get; set; }
        public string ZopNombre { get; set; }
        public string SosConsecutivoIndice { get; set; }
        public string SosTipoPersonaId { get; set; }
        public int? SosZonPadreId { get; set; }
        public string SosZonId { get; set; }
        public int? IntZopId { get; set; }
        public string IntCiudad { get; set; }
        public int? IntUbicacionZopId { get; set; }
        public string IntUbicacionCiudad { get; set; }
        public string IntUbicacionEmail { get; set; }
        public object ProrrogaFechaRegreso { get; set; }
        public string ProrrogaMotivo { get; set; }
        public int? DestinoZopId { get; set; }
        public string DestinoCiudad { get; set; }
        public string DestinoDireccion { get; set; }
        public string DestinoFinExportacion { get; set; }
        public string DestinoEntidad { get; set; }
        public string DestinoTelefono { get; set; }
        public string DestinoTiempoPermanencia { get; set; }
        public int? DestinoTipoTiempoPermanencia { get; set; }
        public string ReitegroObservaciones { get; set; }
        public object SosNombreRepresentante { get; set; }
        public int? UsuId { get; set; }
        public string Notificacion { get; set; }
        public bool? Aceptahabeasdata { get; set; }
        public bool? Requiereintermediario { get; set; }
        public object Tieneobrasrechazadas { get; set; }
    }

}
