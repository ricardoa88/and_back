namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerReporte : MinCulturaBodyResult
    {

        public ReturnModelObtenerReporte() { }

        public ReturnModelObtenerReporte(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
        public object Reporte { get; set; }
        public Listareporte[] ListaReportes { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Listareporte
    {
        public int SosId { get; set; }
        public int Restricciones { get; set; }
        public string Respuesta { get; set; }
        public Reporte[] Reportes { get; set; }
    }

    public class Reporte
    {
        public string NombreArchivo { get; set; }
        public string Archivo { get; set; }
    }

}
