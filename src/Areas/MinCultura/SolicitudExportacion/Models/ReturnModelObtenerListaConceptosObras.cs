using System;

namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerListaConceptosObras : MinCulturaBodyResult
    {

        public ReturnModelObtenerListaConceptosObras() { }

        public ReturnModelObtenerListaConceptosObras(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }

        public object Concepto { get; set; }
        public object Conceptos { get; set; }
        public Conceptosobra[] ConceptosObras { get; set; }
        public object Prestamos { get; set; }
        public object Deterioros { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Conceptosobra
    {
        public int IdConceptoObra { get; set; }
        public int SosId { get; set; }
        public int FicId { get; set; }
        public int IdEstadoObra { get; set; }
        public DateTime FechaEstado { get; set; }
        public int InspeccionFisica { get; set; }
        public object FechaInspeccion { get; set; }
        public int IdConceptoTecnico { get; set; }
        public string Observacion { get; set; }
        public int Restricciones { get; set; }
        public DateTime FechaExpedicion { get; set; }
        public DateTime FechaValidez { get; set; }
        public string Nota { get; set; }
        public string NumeroSeguridad { get; set; }
        public string FicConsecutivoObra { get; set; }
        public int RevisadoExportacion { get; set; }
        public string ObservacionExportacion { get; set; }
    }

}
