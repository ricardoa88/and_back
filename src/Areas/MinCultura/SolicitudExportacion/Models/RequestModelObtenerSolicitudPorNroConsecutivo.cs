namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class RequestModelObtenerSolicitudPorNroConsecutivo
    {
        public string NroConsecutivo { get; set; }
    }

}
