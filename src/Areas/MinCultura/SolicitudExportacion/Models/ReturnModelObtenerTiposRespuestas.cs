namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerTiposRespuestas : MinCulturaBodyResult
    {

        public ReturnModelObtenerTiposRespuestas() { }

        public ReturnModelObtenerTiposRespuestas(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
        public object Paises { get; set; }
        public object ZonasGeograficas { get; set; }
        public object ZonaGeografica { get; set; }
        public object TiposDocumentos { get; set; }
        public object TiposEpocas { get; set; }
        public object TiposTecnicas { get; set; }
        public object TiposPersonas { get; set; }
        public object TiposMotivos { get; set; }
        public Tiposrespuesta[] TiposRespuestas { get; set; }
        public object TiposGenericos { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Tiposrespuesta
    {
        public int TirId { get; set; }
        public string TirNombre { get; set; }
        public string TirConcepto { get; set; }
        public string TirNorma { get; set; }
    }

}
