using System;

namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerListaConceptosSolicitud : MinCulturaBodyResult
    {

        public ReturnModelObtenerListaConceptosSolicitud() { }

        public ReturnModelObtenerListaConceptosSolicitud(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
        public object Concepto { get; set; }
        public Concepto[] Conceptos { get; set; }
        public object ConceptosObras { get; set; }
        public object Prestamos { get; set; }
        public object Deterioros { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Concepto
    {
        public int IdConcepto { get; set; }
        public float SosId { get; set; }
        public int IdEstado { get; set; }
        public DateTime FechaConcepto { get; set; }
        public string ObservacionConcepto { get; set; }
        public float UsuId { get; set; }
    }

}
