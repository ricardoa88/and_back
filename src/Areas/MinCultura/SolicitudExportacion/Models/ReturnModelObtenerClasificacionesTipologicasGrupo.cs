namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerClasificacionesTipologicasGrupo : MinCulturaBodyResult
    {

        public ReturnModelObtenerClasificacionesTipologicasGrupo() { }

        public ReturnModelObtenerClasificacionesTipologicasGrupo(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }

        public object ClasificacionTipologica { get; set; }
        public Clasificaciontipologicagrupo[] ClasificacionTipologicaGrupo { get; set; }
        public object ClasificacionTipologicaGrupoId { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Clasificaciontipologicagrupo
    {
        public int CltId { get; set; }
        public string CltNombre { get; set; }
    }

}
