namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerObras : MinCulturaBodyResult
    {

        public ReturnModelObtenerObras() { }

        public ReturnModelObtenerObras(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
    
        public Obra[] Obras { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Obra
    {
        public int FicId { get; set; }
        public int TibId { get; set; }
        public object InbId { get; set; }
        public object ResNroConcepto { get; set; }
        public int SosId { get; set; }
        public int DocId { get; set; }
        public string FicTitulo { get; set; }
        public string FicAutor { get; set; }
        public string FicFechaElaboracionObra { get; set; }
        public string FicTecnica { get; set; }
        public string FicDimensiones { get; set; }
        public string FicPropietario { get; set; }
        public string FicNroDocumentoIdentidad { get; set; }
        public string FicObservaciones { get; set; }
        public string FicFoto { get; set; }
        public int? TobId { get; set; }
        public string FicConsecutivoObra { get; set; }
        public string FicConsecutivoSeguridad { get; set; }
        public int CltId { get; set; }
        public string FicAlto { get; set; }
        public string FicLargo { get; set; }
        public string FicAncho { get; set; }
        public string FicProfundidad { get; set; }
        public string FicEspesor { get; set; }
        public int? TepId { get; set; }
        public int? IdFirmado { get; set; }
        public int? CtlPadreId { get; set; }
        public int? TitId { get; set; }
        public int? Cantidad { get; set; }
        public int? CtlHijoId { get; set; }
    }

}
