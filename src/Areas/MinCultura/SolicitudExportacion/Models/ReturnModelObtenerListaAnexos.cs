namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerListaAnexos : MinCulturaBodyResult
    {

        public ReturnModelObtenerListaAnexos() { }

        public ReturnModelObtenerListaAnexos(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
        public Anexo?[] ListaAnexos { get; set; }
        public object Anexo { get; set; }
        public object PatAnexo { get; set; }
        public object ListPatAnexos { get; set; }
        public object AnexosSolicitud { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }
   
}
