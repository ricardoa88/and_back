namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class Fichatecnicabienes
    {
        public int FicId { get; set; }
        public int TibId { get; set; }
        public int? InbId { get; set; }
        public object ResNroConcepto { get; set; }
        public int SosId { get; set; }
        public int DocId { get; set; }
        public object FicTitulo { get; set; }
        public object FicAutor { get; set; }
        public object FicFechaElaboracionObra { get; set; }
        public object FicTecnica { get; set; }
        public object FicDimensiones { get; set; }
        public object FicPropietario { get; set; }
        public object FicNroDocumentoIdentidad { get; set; }
        public string FicObservaciones { get; set; }
        public object FicFoto { get; set; }
        public int? TobId { get; set; }
        public string FicConsecutivoObra { get; set; }
        public object FicConsecutivoSeguridad { get; set; }
        public int? CltId { get; set; }
        public object Cantidad { get; set; }
        public object FicAlto { get; set; }
        public object FicLargo { get; set; }
        public object FicAncho { get; set; }
        public object FicProfundidad { get; set; }
        public object FicEspesor { get; set; }
        public int? TepId { get; set; }
        public object IdFirmado { get; set; }
        public int? CtlPadreId { get; set; }
        public int? TitId { get; set; }
        public int? CtlHijoId { get; set; }
        public object BasTiposDocumentosIdentidad { get; set; }
        public object[] PatSipaConceptoObras { get; set; }
        public object PatSolicitudSalidaObras { get; set; }
        public object[] PatAnexos { get; set; }
    }

}
