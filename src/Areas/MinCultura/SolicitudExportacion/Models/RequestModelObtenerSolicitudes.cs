namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class RequestModelObtenerSolicitudes
    {
        public object FechaRadicacionInicial { get; set; }
        public object FechaRadicacionFinal { get; set; }
        public string NroDocumentoSolicitante { get; set; }
        public string NroConsecutivo { get; set; }
        public object Estado { get; set; }
    }

}
