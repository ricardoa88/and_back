namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerMunicipios : MinCulturaBodyResult
    {

        public ReturnModelObtenerMunicipios() { }

        public ReturnModelObtenerMunicipios(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }

        public object Paises { get; set; }
        public Zonasgeografica[] ZonasGeograficas { get; set; }
        public object ZonaGeografica { get; set; }
        public object TiposDocumentos { get; set; }
        public object TiposEpocas { get; set; }
        public object TiposTecnicas { get; set; }
        public object TiposPersonas { get; set; }
        public object TiposMotivos { get; set; }
        public object TiposRespuestas { get; set; }
        public object TiposGenericos { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }


}
