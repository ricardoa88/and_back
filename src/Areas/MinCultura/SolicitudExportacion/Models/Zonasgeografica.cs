namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class Zonasgeografica
    {
        public object[] PatSolicitudSalidaObras { get; set; }
        public string ZonId { get; set; }
        public string ZonNombre { get; set; }
        public string ZonPadreId { get; set; }
        public int? ZonPoblacion { get; set; }
        public string X { get; set; }
        public object ZonLatitud { get; set; }
        public object ZonLongitud { get; set; }
        public bool EsCorrimiento { get; set; }
    }

}
