namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class Anexo
    {
        public int? AnexoId { get; set; }
        public int? SolicitudId { get; set; }
        public string NombreArchivo { get; set; }
        public int? SeccionId { get; set; }
        public string NroDocumentoSolicitante { get; set; }
        public string TipoDocumentoSolicitante { get; set; }
        public string ArchivoBinario { get; set; }
        public int? FicId { get; set; }
        public int? PrestamoId { get; set; }
        public string Descripcion { get; set; }

    }

}
