namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerTiposBasPersonas : MinCulturaBodyResult
    {

        public ReturnModelObtenerTiposBasPersonas() { }

        public ReturnModelObtenerTiposBasPersonas(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
        public object Paises { get; set; }
        public object ZonasGeograficas { get; set; }
        public object ZonaGeografica { get; set; }
        public object TiposDocumentos { get; set; }
        public object TiposEpocas { get; set; }
        public object TiposTecnicas { get; set; }
        public Tipospersona[] TiposPersonas { get; set; }
        public object TiposMotivos { get; set; }
        public object TiposRespuestas { get; set; }
        public object TiposGenericos { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Tipospersona
    {
        public int TipTipoPersona { get; set; }
        public string TipNombre { get; set; }
    }

}
