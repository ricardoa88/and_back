namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerTiposTecnicas : MinCulturaBodyResult
    {

        public ReturnModelObtenerTiposTecnicas() { }

        public ReturnModelObtenerTiposTecnicas(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
        public object Paises { get; set; }
        public object ZonasGeograficas { get; set; }
        public object ZonaGeografica { get; set; }
        public object TiposDocumentos { get; set; }
        public object TiposEpocas { get; set; }
        public Tipostecnica[] TiposTecnicas { get; set; }
        public object TiposPersonas { get; set; }
        public object TiposMotivos { get; set; }
        public object TiposRespuestas { get; set; }
        public object TiposGenericos { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Tipostecnica
    {
        public int TitId { get; set; }
        public int GruId { get; set; }
        public string TitNombre { get; set; }
    }

}
