﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tramites_servicios_webapi.common.Models;
using tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models;

namespace tramites_servicios_webapi.Areas.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelListasCrearSolicitud : MinCulturaBodyResult
    {

        public ReturnModelListasCrearSolicitud() { }

        public ReturnModelListasCrearSolicitud(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
    
        public IEnumerable<SelectListItem> TiposDocumento { get; set; }
        public IEnumerable<SelectListItem> TipoSolicitante { get; set; }
        public IEnumerable<SelectListItem> Paises { get; set; }
        public IEnumerable<SelectListItem> Departamentos { get; set; }


    }
}
