namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerTiposDocumentosIndentidad : MinCulturaBodyResult
    {

        public ReturnModelObtenerTiposDocumentosIndentidad() { }

        public ReturnModelObtenerTiposDocumentosIndentidad(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }
        public object Paises { get; set; }
        public object ZonasGeograficas { get; set; }
        public object ZonaGeografica { get; set; }
        public Tiposdocumento[] TiposDocumentos { get; set; }
        public object TiposEpocas { get; set; }
        public object TiposTecnicas { get; set; }
        public object TiposPersonas { get; set; }
        public object TiposMotivos { get; set; }
        public object TiposRespuestas { get; set; }
        public object TiposGenericos { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Tiposdocumento
    {
        public object[] PatSolicitudSalidaObras { get; set; }
        public object[] PatFichaTecnicaBienes { get; set; }
        public int DocId { get; set; }
        public string DocNombre { get; set; }
        public string DocActivo { get; set; }
        public string Codigo { get; set; }
    }

}
