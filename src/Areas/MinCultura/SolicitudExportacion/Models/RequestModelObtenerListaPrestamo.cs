namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class RequestModelObtenerListaPrestamo
    {
        public int SosId { get; set; }
        public int? IdFicha { get; set; }
        public int? IdPrestamo { get; set; }
    }

}
