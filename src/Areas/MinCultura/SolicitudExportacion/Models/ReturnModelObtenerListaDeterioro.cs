namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelObtenerListaDeterioro : MinCulturaBodyResult
    {

        public ReturnModelObtenerListaDeterioro() { }

        public ReturnModelObtenerListaDeterioro(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }

        public object Concepto { get; set; }
        public object Conceptos { get; set; }
        public object ConceptosObras { get; set; }
        public object Prestamos { get; set; }
        public Deterioro[] Deterioros { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }

    public class Deterioro
    {
        public int IdDeterioro { get; set; }
        public string Descripcion { get; set; }
        public int NoDeterioro { get; set; }
        public int SiDeterioro { get; set; }
        public string Letra { get; set; }
        public string Localizacion { get; set; }
        public int FicId { get; set; }
    }

}
