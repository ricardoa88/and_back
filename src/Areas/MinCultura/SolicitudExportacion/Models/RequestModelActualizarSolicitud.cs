namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class RequestModelActualizarSolicitud
    {
        public string SosId { get; set; }
        public string Ciudad { get; set; }
        public int DocIdSolicitante { get; set; }
        public int ZopId { get; set; }
        public string ZonId { get; set; }
        public string SosNombreSolicitante { get; set; }
        public string SosNroDocumentoSolicitante { get; set; }
        public int SosNroFoliosAnexos { get; set; }
        public string SosFechaParaDarConcepto { get; set; }
        public int SosCantidad { get; set; }
        public int TmsId { get; set; }
        public string SosLugarExpedicion { get; set; }
        public string SosDireccionSolicitante { get; set; }
        public string SosTelefonoSolicitante { get; set; }
        public string SosCorreoSolicitante { get; set; }
        public string SosNombreIntermediario { get; set; }
        public int DocIdIntermediario { get; set; }
        public string SosNroDocumentoIntermediario { get; set; }
        public string SosDireccionIntermediario { get; set; }
        public string SosTelefonoIntermediario { get; set; }
        public string SosSinoIntermediario { get; set; }
        public string SosSinoAnexos { get; set; }
        public string SosSinoProrroga { get; set; }
        public string ZopNombre { get; set; }
        public int SosTipoPersonaId { get; set; }
        public int SosZonPadreId { get; set; }
        public string SosZonId { get; set; }
        public int IntZopId { get; set; }
        public string IntCiudad { get; set; }
        public int? IntUbicacionZopId { get; set; }
        public string IntUbicacionCiudad { get; set; }
        public string IntUbicacionEmail { get; set; }
        public object ProrrogaFechaRegreso { get; set; }
        public string ProrrogaMotivo { get; set; }
        public int DestinoZopId { get; set; }
        public string DestinoCiudad { get; set; }
        public string DestinoDireccion { get; set; }
        public string DestinoFinExportacion { get; set; }
        public string DestinoEntidad { get; set; }
        public string DestinoTelefono { get; set; }
        public int DestinoTiempoPermanencia { get; set; }
        public int DestinoTipoTiempoPermanencia { get; set; }
        public string ReitegroObservaciones { get; set; }
        public string SosNombreRepresentante { get; set; }
        public bool AceptaHabeasdata { get; set; }
        public bool RequiereIntermediario { get; set; }
        public Anexo[] AnexoSolicitante { get; set; }
    }

   

}
