namespace tramites_servicios_webapi.MinCultura.SolicitudExportacion.Models
{
    public class ReturnModelActualizarObra : MinCulturaBodyResult
    {

        public ReturnModelActualizarObra() { }

        public ReturnModelActualizarObra(bool operacionExitosa, string mensaje)
        {
            Mensaje = mensaje;
            OperacionExitosa = operacionExitosa;
        }

        public Fichatecnicabienes FichaTecnicaBienes { get; set; }
        public bool Success { get; set; }
        public Error[] Errors { get; set; }
    }
}
