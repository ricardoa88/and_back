using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using tramites_servicios_webapi.sic.DenunciaInfraccion.Services;
using tramites_servicios_webapi.sic.DenunciaInfraccion.Models;

namespace tramites_servicios_webapi.sic.DenunciaInfraccion.Controllers
{
    /// <summary>
    /// Consulta Registro Sanitario del INVIMA
    /// </summary>
    [Route("api/sic/[controller]")]
    [ApiController]
    public class DenunciaInfraccionController : ControllerBase
    {

        /// <summary>
        ///  consultar un solo tipo de referencia, de los que se usan para los trámites de radicación de la SIC
        /// </summary>
        /// <param name="value">Indica el tipo de referencia que se quiere consultar</param>
        /// <returns>Contiene la lista de elementos consultados según el parámetro de entrada especificado</returns>
        [HttpGet("getListasGenericas/{value}", Name = "getListasGenericas")]
        public async Task<IActionResult> getListasGenericas(string value)
        {
            var respuesta = await DenunciaInfraccionService.getListasGenericas(value);
            if (respuesta == null)
                return NotFound();
            return respuesta;
        }

        /// <summary>
        ///  consultar un por region 
        /// </summary>
        /// <param name="value">códigos retornados por el webservice para la consulta de países</param>
        /// <returns>Contiene la lista de elementos consultados según el parámetro de entrada especificado</returns>
        [HttpGet("getListasRegion/{value}", Name = "getListasRegion")]
        public async Task<IActionResult> getListasRegion(string value)
        {
            var respuesta = await DenunciaInfraccionService.getListasGenericas("REGION/" + value);
            if (respuesta == null)
                return NotFound();
            return respuesta;
        }

        /// <summary>
        ///  consultar un por region 
        /// </summary>
        /// <param name="value">códigos retornados por el webservice para la consulta de países</param>
        /// <returns>Contiene la lista de elementos consultados según el parámetro de entrada especificado</returns>
        [HttpGet("getListasCiudad/{value}", Name = "getListasCiudad")]
        public async Task<IActionResult> getListasCiudad(string value)
        {
            var respuesta = await DenunciaInfraccionService.getListasGenericas("CIUDAD/" + value);
            if (respuesta == null)
                return NotFound();
            return respuesta;
        }

        // <summary>
        /// Obtiene el listado de estratos para realizar el registro de personas
        /// </summary>
        /// <returns>Listado de estratos</returns>
        [HttpPost("ConsultarPersona")]
        public async Task<IActionResult> ConsultarPersona([FromBody] requestPersona persona)
        {            
            var respuesta = await DenunciaInfraccionService.ConsultarPersona(persona);
            if (respuesta == null)
                return NotFound();
            return respuesta;
        }

        // <summary>
        /// Obtiene el listado de estratos para realizar el registro de personas
        /// </summary>
        /// <returns>Listado de estratos</returns>
        [HttpPost("AutenticarUsuario")]
        public async Task<IActionResult> AutenticarUsuario([FromBody] requestUsuario usuario)        
        {   
            var respuesta = await DenunciaInfraccionService.AutenticarUsuario(usuario);
            if (respuesta == null)
                return NotFound();
            return respuesta;
        }

        // <summary>
        /// Obtiene el listado de estratos para realizar el registro de personas
        /// </summary>
        /// <returns>Listado de estratos</returns>
        [HttpPost("RegistrarPersona")]
        public async Task<IActionResult> RegistrarPersona([FromBody] Persona persona)
        {            
            var respuesta = await DenunciaInfraccionService.RegistrarPersona(persona);
            if (respuesta == null)
                return NotFound();
            return respuesta;
        }

        // <summary>
        /// Obtiene el listado de estratos para realizar el registro de personas
        /// </summary>
        /// <returns>Listado de estratos</returns>
        [HttpPost("RegistrarUsuario")]
        public async Task<IActionResult> RegistrarUsuario([FromBody] requestUsuarioId usuario)        
        {   
            var respuesta = await DenunciaInfraccionService.RegistrarUsuario(usuario);
            if (respuesta == null)
                return NotFound();
            return respuesta;
        }        


       [HttpPost("RegistrarRadicacion")]
        public async Task<IActionResult> RegistrarRadicacion([FromBody] Radicacion radicacion)
        {
            var respuesta = await DenunciaInfraccionService.RegistrarRadicacion(radicacion);
            if (respuesta == null)
                return NotFound();
            return respuesta;
        }


       [HttpPost("ConsultarRadicacion")]
        public async Task<IActionResult> ConsultarRadicacion([FromBody] ConsultaRadicacion radicacion)
        {
            var respuesta = await DenunciaInfraccionService.ConsultarRadicacion(radicacion);
            if (respuesta == null)
                return NotFound();
            return respuesta;
        }

       [HttpPost("RegistrarDenuncia")]
        public async Task<IActionResult> RegistrarDenuncia([FromBody] Denuncia radicacion)
        {
            var respuesta = await DenunciaInfraccionService.RegistrarDenuncia(radicacion);
            if (respuesta == null)
                return NotFound();
            return respuesta;
        }


    }
}
