using System.Collections.Generic;

namespace tramites_servicios_webapi.sic.DenunciaInfraccion.Models
{
    /// <summary>
    /// Modelo para los selects genericos
    /// </summary>
    public class SelectGenerico
    {
        public string codigo { get; set; }
        public string mensaje { get; set; }
        public List<ResultadoTipo> resultado { get; set; }
    }

    public class ResultadoTipo
    {
        public string tipo { get; set; }
        public List<ElementoSelect> valores { get; set; }
    }

    public class ElementoSelect
    {
        public string codigo { get; set; }
        public string valor { get; set; }
    }

    public class responseElementoSelect
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class requestConsultaPersona
    {
        public requestAutorizacion autorizacion { get; set; }
        public requestPersona persona { get; set; }
    }

    public class requestRegistrarUsuario
    {
        public requestAutorizacion autorizacion { get; set; }
        public requestUsuarioId usuario { get; set; }
    }    

    public class RegistroUsuario
    {
        public requestAutorizacion autorizacion { get; set; }
        public requestUsuario usuario { get; set; }
    }

    public class requestAutorizacion
    {
        public string usuario { get; set; }
        public string password { get; set; }
    }

    public class requestPersona
    {
        public string tipoDocumento { get; set; }
        public string numeroDocumento { get; set; }
        public string id { get; set; }
    }

    public class requestUsuario
    {
        public string login { get; set; }
        public string password { get; set; }
        public string sistema { get; set; }
    }
    public class requestUsuarioId
    {
        public string id { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public string sistema { get; set; }
        public string codigoRol { get; set; }
    }

    public class responseConsultaPersona
    {
        public string codigo { get; set; }
        public string mensaje { get; set; }
        public List<Persona> persona { get; set; }
    }

    public class responseRegistroUsuario
    {
        public string codigo { get; set; }
        public string mensaje { get; set; }
    }

    public class responseAutenticarUsuario
    {
        public string codigo { get; set; }
        public string mensaje { get; set; }        
        public responseUsuario usuario { get; set; }
    }    

    public class Natural
    {
        public string primerNombre { get; set; }
        public string segundoNombre { get; set; }
        public string primerApellido { get; set; }
        public string segundoApellido { get; set; }
        public string escolaridad { get; set; }
        public string genero { get; set; }
        public string grupoEtnico { get; set; }
        public string condicionDiscapacidad { get; set; }
        public string atencionPreferencial { get; set; }
        public string grupoInteres { get; set; }
        public string tratamiento { get; set; }
        public string saludo { get; set; }
    }

    public class Telefonos
    {
        public int id { get; set; }
        public string extension { get; set; }
        public string numero { get; set; }
        public string tipo { get; set; }
    }

    public class Direcciones
    {
        public int id { get; set; }
        public string codigoContinente { get; set; }
        public string codigoPais { get; set; }
        public string tipo { get; set; }
        public List<Telefonos> telefonos { get; set; }
        public string descripcion { get; set; }
        public int codigoCiudad { get; set; }
        public int codigoRegion { get; set; }
    }

    public class Email
    {
        public int id { get; set; }
        public string tipo { get; set; }
        public string descripcion { get; set; }
    }

    public class Persona
    {
        public string tipoPersona { get; set; }
        public Natural natural { get; set; }
        public List<Direcciones> direcciones { get; set; }
        public int id { get; set; }
        public List<Email> emails { get; set; }
        public string tipoDocumento { get; set; }
        public long numeroDocumento { get; set; }
        public object empresa { get; set; }
    }

    public class responsePersona
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }
        public Persona persona { get; set; }
    }

    public class responseUsuario
    {
        public int id { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public string sistema { get; set; }
        public int consecutivo { get; set; }
        public string codigoRol { get; set; }
        public string nombreRol { get; set; }
    }

    public class responseAutenticacion
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }
        public responseUsuario usuario { get; set; }
    }

    public class requestAutenticacion
    {
        public requestAutorizacion autorizacion { get; set; }
        public requestUsuario usuario { get; set; }
    }    

    public class requestRegistrar
    {
        public requestAutorizacion autorizacion { get; set; }
        public Persona persona { get; set; }
    }

    public class Adjunto {
        public string nombreArchivo { get; set; }
        public int numeroAdjunto { get; set; }

        public string contenidoArchivo_BASE64 { get; set; }

    }
    public class Perfil
    {
        public int dependencia { get; set; }
        public int tramite { get; set; }
        public int evento { get; set; }
        public int actuacion { get; set; }
    }

    public class Radicacion
    {
        public int anio { get; set; }
        public int numero { get; set; }
        public int idFuncionario { get; set; }
        public string tipoRadicacion { get; set; }
        public string tipoDocumento { get; set; }
        public string medioEntrada { get; set; }
        public int dependenciaDestino { get; set; }
        public int totalFolios { get; set; }
        public int idTasa { get; set; }

        public Perfil perfil { get; set; }
        public Persona radicador { get; set; }
        public List<Adjunto> adjuntos { get; set; }
    }


    public class ConsultaRadicacion
    {
        public int anio { get; set; }
        public int numero { get; set; }
    }


    public class responseRegistroRadicacion
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }
        public Radicacion radicacion { get; set; }
    }


    public class responseConsultaRadicacion
    {
        public int codigo { get; set; }
        public string mensaje { get; set; }

        public List<infoConsultaRadicacion> radicaciones { get; set; }
    }



    public class infoConsultaRadicacionAdjuntos
    {
        public string path { get; set; }
        public string nombreArchivoRadicado { get; set; }
        public int numeroAdjunto { get; set; }
        public string fechaDigitalizacion { get; set; }

    }


    public class infoConsultaRadicacion
    {
        public string anio { get; set; }
        public int numero { get; set; }
        public string control { get; set; }
        public int consecutivo { get; set; }
        public List<infoConsultaRadicacionAdjuntos> adjuntos { get; set; }

        
    }


    public class requestRegistroRadicacion
    {
        public requestAutorizacion autorizacion { get; set; }
        public Radicacion radicacion { get; set; }
    }


    public class requestConsultaRadicacion
    {
        public requestAutorizacion autorizacion { get; set; }
        public ConsultaRadicacion radicacion { get; set; }
    }
    public class Radicador {
        public string id { get; set; }
    }

    public class Denuncia {
        public List<Adjunto> adjuntos { get; set; }
        public int[] codigosTipoAlerta { get; set; }
        public Persona apoderado { get; set; }
        public Persona denunciado { get; set; }
        public string hechos { get; set; }
        public string observaciones { get; set; }
        public Radicador radicador { get; set; }
    } 

    public class requestRegistroDenuncia {
        public requestAutorizacion autorizacion { get; set; }
        public Denuncia radicacion { get; set; }
    }
}
