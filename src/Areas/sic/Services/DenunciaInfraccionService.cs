using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using tramites_servicios_webapi.sic.DenunciaInfraccion.Models;
using System.Diagnostics;

namespace tramites_servicios_webapi.sic.DenunciaInfraccion.Services
{
    public class DenunciaInfraccionService
    {
        private static readonly HttpClient client;
        private const String urlBase = "http://gov.sic.gov.co:28080/sic.ws.interop/";
        private const string urlListasGenericas = "util/referencia/";
        private const string urlConsultaPersona = "persona/consultar";
        private const string urlRegistrarUsuario = "persona/usuario/registrar";
        private const string urlAutenticarUsuario = "persona/usuario/autenticar";
        private const string urlRegistrarPersona = "persona/registrar";
        private const string urlRegistrarRadicacion = "radicacion/registrar";
        private const string urlConsultarRadicacion = "radicacion/adjunto/consultar";
        private const string urlRegistrarDenuncia = "radicacion//registrar/denuncia";



        static DenunciaInfraccionService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri(urlBase);
        }

        /// <summary>
        /// Obtiene las listas genericas
        /// </summary>
        /// <param name="value">tipo referencia a consultar</param>
        /// <returns>Listado de registros</returns>
        public static async Task<JsonResult> getListasGenericas(string value)
        {
            //Console.WriteLine("entro al metodo con value " + value);
            JsonResult resultado = null;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            StringContent stringContent = new StringContent("", Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(new HttpMethod("GET"), urlBase + urlListasGenericas + value);
            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    var listaModel = JsonConvert.DeserializeObject<SelectGenerico>(response);
                    //Console.WriteLine(response);
                    var selectListItems = new List<responseElementoSelect> ();                                         
                    foreach(ElementoSelect dato in listaModel.resultado[0].valores)
                    {
                        responseElementoSelect selectListItem = new responseElementoSelect();
                        selectListItem.Value = dato.codigo;
                        selectListItem.Text = dato.valor;
                        selectListItems.Add(selectListItem);
                    }
                    resultado = new JsonResult(selectListItems);
                    //Console.WriteLine(resultado);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
            return resultado;
        }


        /// <summary>
        /// Permite consultar los datos de una persona específica si se encuentra  en el sistema
        /// </summary>
        /// <param name="persona">Datos del ciudadano</param>
        /// <returns>Estado del registro</returns>
        public static async Task<JsonResult> ConsultarPersona(requestPersona persona)
        {
            JsonResult result = null;        
            var consultar = new requestConsultaPersona();
            consultar.autorizacion = obtenerAutorizacion();
            consultar.persona = persona;  
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Content-Type", "application/json");            
            var request = new HttpRequestMessage(new HttpMethod("POST"), urlBase + urlConsultaPersona);
            request.Content = new StringContent(JsonConvert.SerializeObject(consultar), Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    //Console.WriteLine(response);
                    result = new JsonResult(JsonConvert.DeserializeObject<responsePersona>(response));
                }
                else                
                    Console.WriteLine("Error");
                    //result = new RegistroUsuarioRespuestaModel("Se presentó un error al registrar el ciudadano", false);                
            }
            catch (Exception e)
            {
                result = new JsonResult(e.Message);
            }
            return result;
        }

        /// <summary>
        /// Permite consultar los datos de una persona específica si se encuentra  en el sistema
        /// </summary>
        /// <param name="persona">Datos del ciudadano</param>
        /// <returns>Estado del registro</returns>
        public static async Task<JsonResult> RegistrarPersona(Persona persona)
        {
            Console.WriteLine("PERSONA ::::::::::::::::::::::::::::::");
            Console.WriteLine(JsonConvert.SerializeObject(persona));
            JsonResult result = null;        
            var registrar = new requestRegistrar();
            registrar.autorizacion = obtenerAutorizacion();
            registrar.persona = persona;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Content-Type", "application/json");            
            var request = new HttpRequestMessage(new HttpMethod("POST"), urlBase + urlRegistrarPersona);
            request.Content = new StringContent(JsonConvert.SerializeObject(registrar), Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    Console.WriteLine(response);
                    result = new JsonResult(JsonConvert.DeserializeObject<responsePersona>(response));
                }
                else                
                    Console.WriteLine("Error");
                    //result = new RegistroUsuarioRespuestaModel("Se presentó un error al registrar el ciudadano", false);                
            }
            catch (Exception e)
            {
                result = new JsonResult(e.Message);
            }
            return result;
        }        

        /// <summary>
        /// Permite consultar los datos de una persona específica si se encuentra  en el sistema
        /// </summary>
        /// <param name="data">Datos del ciudadano</param>
        /// <returns>Estado del registro</returns>
        public static async Task<JsonResult> RegistrarUsuario(requestUsuarioId usuario)
        {
            Console.WriteLine("en este punto");
            Console.WriteLine(usuario.codigoRol);
            Console.WriteLine(usuario.id);
            Console.WriteLine(usuario.login);
            Console.WriteLine(usuario.password);
            Console.WriteLine(usuario.sistema);


            JsonResult result = null;        
            var registrar = new requestRegistrarUsuario();
            registrar.autorizacion = obtenerAutorizacion();
            registrar.usuario = usuario;



            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Content-Type", "application/json");            
            var request = new HttpRequestMessage(new HttpMethod("POST"), urlBase + urlRegistrarUsuario);
            request.Content = new StringContent(JsonConvert.SerializeObject(registrar), Encoding.UTF8, "application/json");



            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    //Console.WriteLine(response);
                    result = new JsonResult(JsonConvert.DeserializeObject<responseAutenticarUsuario>(response));
                }
                else                
                    Console.WriteLine(resp);                    
            }
            catch (Exception e)
            {
                result = new JsonResult(e.Message);
            }
            return result;
        }

        private static requestAutorizacion obtenerAutorizacion()
        {
            var autorizacion = new requestAutorizacion();
            autorizacion.usuario = "e06a63e4d46846b747c05175465988dc";
            autorizacion.password= "39f58f78231cec4846c968469a4709cf";
            return autorizacion;
        }

        /// <summary>
        /// Permite autenticar a un usuario específico si se encuentra asociado al sistema indicado
        /// </summary>
        /// <param name="data">Datos del ciudadano</param>
        /// <returns>Estado del registro</returns>
        public static async Task<JsonResult> AutenticarUsuario(requestUsuario usuario)
        {
            JsonResult result = null;        
            var autenticar = new requestAutenticacion();
            autenticar.autorizacion = obtenerAutorizacion();
            autenticar.usuario = usuario;                        
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Content-Type", "application/json");            
            var request = new HttpRequestMessage(new HttpMethod("POST"), urlBase + urlAutenticarUsuario);
            request.Content = new StringContent(JsonConvert.SerializeObject(autenticar), Encoding.UTF8, "application/json");
            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;                    
                    result = new JsonResult(JsonConvert.DeserializeObject<responseAutenticacion>(response));
                }
                else                
                    Console.WriteLine("Error");
            }
            catch (Exception e)
            {
                result = new JsonResult(e.Message);
            }
            return result;
        }


       

        public static async Task<JsonResult> RegistrarRadicacion(Radicacion radicacion)
        {


            JsonResult result = null;
            var registrar = new requestRegistroRadicacion();
            registrar.autorizacion = obtenerAutorizacion();
            registrar.radicacion = radicacion;


            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Content-Type", "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), urlBase + urlRegistrarRadicacion);
            request.Content = new StringContent(JsonConvert.SerializeObject(registrar), Encoding.UTF8, "application/json");


            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    //Console.WriteLine(response);
                    result = new JsonResult(JsonConvert.DeserializeObject<responseRegistroRadicacion>(response));
                }
                else
                    Console.WriteLine(resp);
            }
            catch (Exception e)
            {
                result = new JsonResult(e.Message);
            }
            return result;
        }


        public static async Task<JsonResult> ConsultarRadicacion(ConsultaRadicacion radicacion)
        {


            JsonResult result = null;
            var consultar = new requestConsultaRadicacion();
            consultar.autorizacion = obtenerAutorizacion();
            consultar.radicacion = radicacion;


            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Content-Type", "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), urlBase + urlConsultarRadicacion);
            request.Content = new StringContent(JsonConvert.SerializeObject(consultar), Encoding.UTF8, "application/json");


            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    //Console.WriteLine(response);
                    result = new JsonResult(JsonConvert.DeserializeObject<responseConsultaRadicacion>(response));
                }
                else
                    Console.WriteLine(resp);
            }
            catch (Exception e)
            {
                result = new JsonResult(e.Message);
            }
            return result;
        }

        public static async Task<JsonResult> RegistrarDenuncia(Denuncia radicacion)
        {


            JsonResult result = null;
            var registrar = new requestRegistroDenuncia();
            registrar.autorizacion = obtenerAutorizacion();
            registrar.radicacion = radicacion;


            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Content-Type", "application/json");
            var request = new HttpRequestMessage(new HttpMethod("POST"), urlBase + urlRegistrarDenuncia) ;
            request.Content = new StringContent(JsonConvert.SerializeObject(registrar), Encoding.UTF8, "application/json");


            try
            {
                HttpResponseMessage resp = await client.SendAsync(request);
                if (resp.IsSuccessStatusCode)
                {
                    string response = resp.Content.ReadAsStringAsync().Result;
                    //Console.WriteLine(response);
                    result = new JsonResult(JsonConvert.DeserializeObject<responseRegistroRadicacion>(response));
                }
                else
                    Console.WriteLine(resp);
            }
            catch (Exception e)
            {
                result = new JsonResult(e.Message);
            }
            return result;
        }




    }




}
