using System;
using Xunit;
using tramites_servicios_webapi.Controllers;
using tramites_servicios_webapi.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;

namespace ServiciosTramitesXUnitTest
{
    public class RecursosTest
    {

        [Fact]
        public async System.Threading.Tasks.Task GetTestAsync()
        {
            string path = "https://localhost:44376/api/Recursos";
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos en la coleccion por el id del item ingresado.");
            }

            dynamic result = await response.Content.ReadAsStringAsync();

            Assert.NotEmpty(result);
           
        }

        [Theory]
        //Id si esta en mongo
        [InlineData("1040")]
        public async System.Threading.Tasks.Task TramiteGet(string id)
        {
            string path = "https://localhost:44376/api/Recursos/TramiteGet/" + id;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos en la coleccion por el id del item ingresado.");
            }

            List<RecursosList> model = new List<RecursosList>();
            dynamic result = await response.Content.ReadAsStringAsync();
            model = JsonConvert.DeserializeObject<List<RecursosList>>(result);
            
            Assert.NotNull(model);

        }

    }
}
