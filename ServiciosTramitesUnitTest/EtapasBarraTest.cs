using System;
using Xunit;
using tramites_servicios_webapi.Controllers;
using tramites_servicios_webapi.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;

namespace ServiciosTramitesXUnitTest
{
    public class EtapasBarraTest
    {

        [Fact]
        public async System.Threading.Tasks.Task GetAsync()
        {
            string path = "https://localhost:44376/api/Etapas-Barra";
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos en la coleccion por el id del item ingresado.");
            }

            dynamic result = await response.Content.ReadAsStringAsync();

            Assert.NotEmpty(result);
           
        }

        [Theory]
        [InlineData("1040")]
        public async System.Threading.Tasks.Task GetDataBarraTramite(string id)
        {
            string path = "https://localhost:44376/Api/Etapas-Barra/GetDataBarraTramite/" + id;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos en la coleccion por el id del item ingresado.");
            }

            List<EtapasBarrasList> model = new List<EtapasBarrasList>();
            dynamic result = await response.Content.ReadAsStringAsync();
            model = JsonConvert.DeserializeObject<List<EtapasBarrasList>>(result);
            
            Assert.NotNull(model);

        }

        [Theory]
        [InlineData("1033")]
        [InlineData("S001")]
        [InlineData("1040")]
        public async System.Threading.Tasks.Task GetUrlTramiteLinea(string idTramite)
        {
            string path = "https://localhost:44376/Api/Etapas-Barra/GetUrlTramiteLinea/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos en la coleccion por el id del item ingresado.");
            }

            UrlTramiteDesarrollado model = new UrlTramiteDesarrollado();
            dynamic result = await response.Content.ReadAsStringAsync();
            model = JsonConvert.DeserializeObject<UrlTramiteDesarrollado>(result);

            Assert.NotNull(model);

        }

    }
}
