using System;
using Xunit;
using tramites_servicios_webapi.Controllers;
using tramites_servicios_webapi.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;
using tramites_servicios_webapi.Areas.ServiciosTramites.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace ServiciosTramitesXUnitTest
{
    public class TramitesEmbebidos
    {

        [Fact]
        public async System.Threading.Tasks.Task GetTramitesEmbebidosTestAsync()
        {
            string path = "https://localhost:44376/api/TramitesEmbebidos";
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos de tramites embebidos.");
            }

            dynamic result = await response.Content.ReadAsStringAsync();

            List<TBL_INTEGRACION_TRAMITES> model = new List<TBL_INTEGRACION_TRAMITES>();
            dynamic resultado = await response.Content.ReadAsStringAsync();
            Assert.NotEmpty(resultado);
            model = JsonConvert.DeserializeObject<IEnumerable<TBL_INTEGRACION_TRAMITES>>(result);

            Assert.NotNull(model);

        }

        [Theory]
        [InlineData("1")]
        [InlineData("2")]
        public async System.Threading.Tasks.Task GetEmbebidosByIdTest(string idIntegrador)
        {
            string path = "https://localhost:44376/api/TramitesEmbebidos/" + idIntegrador;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos por el campo integrador id.");
            }

            TBL_INTEGRACION_TRAMITES model = new TBL_INTEGRACION_TRAMITES();
            dynamic result = await response.Content.ReadAsStringAsync();
            Assert.NotEmpty(result);

        }

        [Theory]
        [InlineData("1033")]
        [InlineData("21312200")]
        public async System.Threading.Tasks.Task GetEmbebidosByIdTramiteTest(string idTramite)
        {
            string path = "https://localhost:44376/api/TramitesEmbebidos/GetEmbebidosByIdTramite/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            Assert.True(response.IsSuccessStatusCode);

            TBL_INTEGRACION_TRAMITES model = new TBL_INTEGRACION_TRAMITES();
            dynamic result = await response.Content.ReadAsStringAsync();
            Assert.NotEmpty(result);

            model = JsonConvert.DeserializeObject<TBL_INTEGRACION_TRAMITES>(result);

            Assert.NotNull(model);

        }


        [Theory]
        [InlineData("1033")]
        public async void CreateTest(string idTramite)
        {
            string path = "https://localhost:44376/api/TramitesEmbebidos/GetEmbebidosByIdTramite/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            TBL_INTEGRACION_TRAMITES model = new TBL_INTEGRACION_TRAMITES();
            dynamic result = await response.Content.ReadAsStringAsync();
            model = JsonConvert.DeserializeObject<TBL_INTEGRACION_TRAMITES>(result);
            Random rnd = new Random();
            var idItem = Convert.ToString(Convert.ToInt32(idTramite) + rnd.Next(0, 1000));

            if (model.URL_TRAMITE != null)
            {
                model.URL_TRAMITE = "https://www.tutorialrepublic.com";
                model.NUMERO = idItem;
                model.Id = 0;
                model.INTEGRADOR_ID= 0;
            }

            string pathPost = "https://localhost:44376/api/TramitesEmbebidos/";

            var jsonString = JsonConvert.SerializeObject(model);
            var httpContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            response = await _httpClient.PostAsync(pathPost, httpContent);

            if (response.IsSuccessStatusCode)
            {
                response.EnsureSuccessStatusCode();
            }
            
            TBL_INTEGRACION_TRAMITES modelRespuesta = new TBL_INTEGRACION_TRAMITES();

            dynamic resultPost = await response.Content.ReadAsStringAsync();

            var resultDeserealizado = JsonConvert.DeserializeObject(resultPost);
            if (resultDeserealizado.ContainsKey("statusCode"))
            {
                Assert.NotNull(resultDeserealizado.message);
            }
            else {

            modelRespuesta = JsonConvert.DeserializeObject<TBL_INTEGRACION_TRAMITES>(resultPost);

            Assert.Equal(idItem, modelRespuesta.NUMERO);

            }

        }


        [Theory]
        [InlineData("2016", "https://www.tutorialrepublic.com")]
        [InlineData("2016", "https://fr.wikipedia.org/wiki/Main_Page")]
        public async void UpdateTest(string idTramite, string url)
        {
            string path = "https://localhost:44376/api/TramitesEmbebidos/GetEmbebidosByIdTramite/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            TBL_INTEGRACION_TRAMITES model = new TBL_INTEGRACION_TRAMITES();
            dynamic result = await response.Content.ReadAsStringAsync();
            model = JsonConvert.DeserializeObject<TBL_INTEGRACION_TRAMITES>(result);
            if (model.NUMERO != null)
            {
                model.URL_TRAMITE = url;
            }

            string pathPost = "https://localhost:44376/api/TramitesEmbebidos/" + model.INTEGRADOR_ID;

            var jsonString = JsonConvert.SerializeObject(model);
            var httpContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            response = await _httpClient.PutAsync(pathPost, httpContent);

            if (response.IsSuccessStatusCode)
            {
                response.EnsureSuccessStatusCode();
            }

            response = await _httpClient.GetAsync(path);

            TBL_INTEGRACION_TRAMITES modelVerifica = new TBL_INTEGRACION_TRAMITES();
            dynamic resultVerifica = await response.Content.ReadAsStringAsync();
            modelVerifica = JsonConvert.DeserializeObject<TBL_INTEGRACION_TRAMITES>(resultVerifica);
            Assert.Equal(url, modelVerifica.URL_TRAMITE);

        }

        [Theory]
        [InlineData("1040","fuerza",1,1)]
        public async void GetTramiteEmbebidoFiltersTest(string idTramite, string entidad, int estado, int numPage )
        {
            FiltrosTramitesEmbebidos filtros = new FiltrosTramitesEmbebidos();
            filtros.IdTramite = idTramite;
            filtros.Estado= estado;
            filtros.Entidad = entidad;

            string pathPost = "https://localhost:44376/api/TramitesEmbebidos/GetTramiteEmbebidoFilters/"+ numPage;
            HttpClient _httpClient = new HttpClient();

            var jsonString = JsonConvert.SerializeObject(filtros);
            var httpContent = new StringContent(jsonString, Encoding.UTF8, "application/json");

            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.PostAsync(pathPost, httpContent);

            if (response.IsSuccessStatusCode)
            {
                response.EnsureSuccessStatusCode();
            }

            List<TramitesEmbebidos> modelRespuesta = new List<TramitesEmbebidos>();

            dynamic resultPost = await response.Content.ReadAsStringAsync();

            modelRespuesta = JsonConvert.DeserializeObject<List<TramitesEmbebidos>>(resultPost);

            Assert.NotNull(modelRespuesta);


        }


    }
}
