using System;
using Xunit;
using tramites_servicios_webapi.Areas.ServiciosNoSuit.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;
using tramites_servicios_webapi.Areas.ServiciosNoSuit.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace ServiciosTramitesXUnitTest
{
    public class FichaNoSuit
    {

        [Theory]
        [InlineData("S001")]
        public async System.Threading.Tasks.Task GetNotSuiteTramiteByIdTest(string idTramite)
        {
            string path = "https://localhost:44376/api/FichaNoSuitTramite/GetNotSuiteTramiteById/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();
            List<FichaNoSuit> modelResult = new List<FichaNoSuit>();
            modelResult = JsonConvert.DeserializeObject<List<FichaNoSuit>>(result);
            Assert.NotNull(modelResult);
        }

        [Theory]
        [InlineData("S001")]
        public async System.Threading.Tasks.Task GetDocumentacionRequeridaByIdTest(string idTramite)
        {
            string path = "https://localhost:44376/api/FichaNoSuitTramite/GetDocumentacionRequeridaById/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();
            List<DocumentacioRequerida> modelResult = new List<DocumentacioRequerida>();
            modelResult = JsonConvert.DeserializeObject<List<DocumentacioRequerida>>(result);
            Assert.NotNull(modelResult);
        }


        [Theory]
        [InlineData("S001")]
        public async System.Threading.Tasks.Task GetConsideracionesAdicionalesByIdTest(string idTramite)
        {
            string path = "https://localhost:44376/api/FichaNoSuitTramite/GetConsideracionesAdicionalesById/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();
            List<ConsideracionesAdicionales> modelResult = new List<ConsideracionesAdicionales>();
            modelResult = JsonConvert.DeserializeObject<List<ConsideracionesAdicionales>>(result);
            Assert.NotNull(modelResult);
        }

        [Theory]
        [InlineData("S001")]
        public async System.Threading.Tasks.Task GetPuntosAtencionNoSuitByIdTest(string idTramite)
        {
            string path = "https://localhost:44376/api/FichaNoSuitTramite/GetPuntosAtencionNoSuitById/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();
            List<PuntosAtencionNosuit> modelResult = new List<PuntosAtencionNosuit>();
            modelResult = JsonConvert.DeserializeObject<List<PuntosAtencionNosuit>>(result);
            Assert.NotNull(modelResult);
        }

    }
}
