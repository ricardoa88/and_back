using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tramites_servicios_webapi.Minjusticia.AgendamientoConsultorios.Models;
using tramites_servicios_webapi.Mintransporte.Services;
using Xunit;

namespace ServiciosTramitesXUnitTest.minjusticia
{
    public class MinjusticiaUserServicesTest
    {
        [Fact]
        public async System.Threading.Tasks.Task WhenACorrectLoginIsProvided_ServiceShouldReturnSuccess()
        {
            // Arrange
            var login = new  LoginMinjusticiaModel
            {
               UserName = "5555555555",
               Password = "Clave123+."
            };

            // Act
            var result = await AgendamientoConsultorioService.Login(login);
            Assert.True(result.State, "Login correcto!!");
        }
    }
}
