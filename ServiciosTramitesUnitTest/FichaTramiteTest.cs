﻿using System;
using Xunit;
using tramites_servicios_webapi.Controllers;
using tramites_servicios_webapi.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;
using tramites_servicios_webapi.Areas.ServiciosTramites.Controllers;
using Microsoft.AspNetCore.Mvc;
using tramites_servicios_webapi.Areas.ServiciosSuit.Models;

namespace ServiciosTramitesXUnitTest
{
    public class FichaTramite
    {

        [Theory]
        [InlineData("asdasdas")]
        [InlineData("10188")]
        [InlineData("1033")]
        public async System.Threading.Tasks.Task GetTipoFichaTramiteByIdTestAsync(string idTramite)
        {
            string path = "https://localhost:44376/api/FichaTramite/GetTipoFichaTramiteById/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos de tramites embebidos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();

            var resultDeserealizado = JsonConvert.DeserializeObject(result);
            if (resultDeserealizado.ContainsKey("statusCode"))
            {
                Assert.NotNull(resultDeserealizado.message);
            }
           
        }

        [Theory]
        [InlineData("asdasdas")]
        [InlineData("10188")]
        [InlineData("1033")]
        public async System.Threading.Tasks.Task GetInfoFichaEstandarByIdTestAsync(string idTramite)
        {
            string path = "https://localhost:44376/api/FichaTramite/GetInfoFichaEstandarById/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos de tramites embebidos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();

            FichaEstandar modelRespuesta = new FichaEstandar();
            modelRespuesta = JsonConvert.DeserializeObject<FichaEstandar>(result);
            Assert.NotNull(modelRespuesta);

        }

        [Theory]
        [InlineData("10188")]
        [InlineData("1033")]
        public async System.Threading.Tasks.Task GetTipoTramiteFichaEspecificaByIdTestAsync(string idTramite)
        {
            string path = "https://localhost:44376/api/FichaTramite/GetTipoTramiteFichaEspecificaById/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos de tramites embebidos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();

            TipoTramiteEspecifico modelRespuesta = new TipoTramiteEspecifico();
            modelRespuesta = JsonConvert.DeserializeObject<TipoTramiteEspecifico>(result);
            Assert.NotNull(modelRespuesta);

        }

        [Theory]
        [InlineData("10188")]
        [InlineData("aDADS")]
        public async System.Threading.Tasks.Task GetTiposAudienciaByIdTestAsync(string idTramite)
        {
            string path = "https://localhost:44376/api/FichaTramite/GetTiposAudienciaById/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos de tramites embebidos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();
            var respuestaServicio = JsonConvert.DeserializeObject(result);
            Assert.NotNull(respuestaServicio);

        }

        [Theory]
        [InlineData("10188", "Ciudadano")]
        public async System.Threading.Tasks.Task GetMomentosByIdAudiencia(string idTramite, string audiencia)
        {
            string path = "https://localhost:44376/api/FichaTramite/GetMomentosByIdAudiencia/" + idTramite+"/"+ audiencia;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();
            var respuestaServicio = JsonConvert.DeserializeObject(result);
            Assert.NotNull(respuestaServicio);

        }

        [Theory]
        [InlineData("10188", "Ciudadano")]
        public async System.Threading.Tasks.Task GetDataFichaByIdAudienciaTest(string idTramite, string audiencia)
        {
            string path = "https://localhost:44376/api/FichaTramite/GetDataFichaByIdAudiencia/" + idTramite + "/" + audiencia;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();
            List<FichaTramiteEspecifica> modelResult = new List<FichaTramiteEspecifica>();
            modelResult = JsonConvert.DeserializeObject<List<FichaTramiteEspecifica>>(result);
            Assert.NotNull(modelResult);

        }

        [Theory]
        [InlineData("1040", "Ciudadano",9)]
        public async System.Threading.Tasks.Task GetCanalesByMomentoIdAudienciaTest(string idTramite, string audiencia, int momento)
        {
            string path = "https://localhost:44376/api/FichaTramite/GetCanalesByMomentoIdAudiencia/" + idTramite + "/" + audiencia+"/"+momento;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();
            List<Canales> modelResult = new List<Canales>();
            modelResult = JsonConvert.DeserializeObject<List<Canales>>(result);
            Assert.NotNull(modelResult);

        }

        [Theory]
        [InlineData("1040", "Ciudadano", 6)]
        public async System.Threading.Tasks.Task GetPagosByMomentoIdAudienciaTest(string idTramite, string audiencia, int momento)
        {
            string path = "https://localhost:44376/api/FichaTramite/GetPagosByMomentoIdAudiencia/" + idTramite + "/" + audiencia + "/" + momento;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();
            List<Pagos> modelResult = new List<Pagos>();
            modelResult = JsonConvert.DeserializeObject<List<Pagos>>(result);
            Assert.NotNull(modelResult);

        }

        [Theory]
        [InlineData("1040")]
        public async System.Threading.Tasks.Task GetPuntosAtencionByIdTest(string idTramite)
        {
            string path = "https://localhost:44376/api/FichaTramite/GetPuntosAtencionById/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();
            List<PuntosAtencion> modelResult = new List<PuntosAtencion>();
            modelResult = JsonConvert.DeserializeObject<List<PuntosAtencion>>(result);
            Assert.NotNull(modelResult);

        }

        [Theory]
        [InlineData("1040")]
        public async System.Threading.Tasks.Task GetNormatividadByIdTest(string idTramite)
        {
            string path = "https://localhost:44376/api/FichaTramite/GetNormatividadById/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos.");
                Assert.NotEqual("Not Found", response.ReasonPhrase);
            }

            dynamic result = await response.Content.ReadAsStringAsync();
            List<Normatividad> modelResult = new List<Normatividad>();
            modelResult = JsonConvert.DeserializeObject<List<Normatividad>>(result);
            Assert.NotNull(modelResult);
        }
    }
}
