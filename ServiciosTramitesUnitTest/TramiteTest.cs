using System;
using Xunit;
using tramites_servicios_webapi.Controllers;
using tramites_servicios_webapi.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;

namespace ServiciosTramitesXUnitTest
{
    public class TramiteTest
    {

        [Fact]
        public async System.Threading.Tasks.Task GetTestAsync()
        {
            string path = "https://localhost:44376/api/tramite";
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos en la coleccion por el id del item ingresado.");
            }

            dynamic result = await response.Content.ReadAsStringAsync();

            Assert.NotEmpty(result);
           
        }

        [Fact]
        public async System.Threading.Tasks.Task GetTramitesEmbebidosTest()
        {
            string path = "https://localhost:44376/api/tramite/GetTramitesEmbebidos";
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos en la coleccion por el id del item ingresado.");
            }

            


            List<TramitesEmbebidos> model = new List<TramitesEmbebidos>();
            dynamic result = await response.Content.ReadAsStringAsync();
            Assert.NotEmpty(result);
            model = JsonConvert.DeserializeObject<List<TramitesEmbebidos>>(result);

            Assert.NotNull(model);

        }

        [Theory]
        [InlineData("S001")]
        [InlineData("S002")]
        [InlineData("12312312")]
        public async System.Threading.Tasks.Task GetInfoTramitesByIdTest(string idTramite)
        {
            string path = "https://localhost:44376/api/tramite/GetInfoTramitesById/"+idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos en la coleccion por el id del item ingresado.");
            }

            List<TramitesEmbebidos> model = new List<TramitesEmbebidos>();
            dynamic result = await response.Content.ReadAsStringAsync();
            Assert.NotEmpty(result);

        }
    }
}
