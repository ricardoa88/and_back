using System;
using Xunit;
using tramites_servicios_webapi.Controllers;
using tramites_servicios_webapi.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Text;

namespace ServiciosTramitesXUnitTest
{
    public class EntidadSuitTest
    {

        [Theory]
        [InlineData("1040")]
        public async System.Threading.Tasks.Task GetTestAsync(string idTramite)
        {
            string path = "https://localhost:44376/api/EntidadSuit/GetUrlEntidadByItem/" + idTramite;
            HttpClient _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = await _httpClient.GetAsync(path);

            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine("No se encontraron datos en la coleccion por el id del item ingresado.");
            }

            EntidadUrl model = new EntidadUrl();

            dynamic result = await response.Content.ReadAsStringAsync();

            model = JsonConvert.DeserializeObject<EntidadUrl>(result);

            Assert.NotNull(model);
           
        }

    }
}
