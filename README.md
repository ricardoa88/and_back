# TRAMITES Y SERVICIOS - WEB API

Tramites y servicios webapi proyecto .net core 2.2.
- [https://dotnet.microsoft.com/download/dotnet-core/2.2](https://dotnet.microsoft.com/download/dotnet-core/2.2)

## Requerimientos
Requiere tener instalado en su maquina :
- NET Core SDK
- Docker
- Docker compose


## Installation

Use docker compose.

```bash
docker-compose up -d
```
abrir ejemplo [http://127.0.0.1:8081/api/values](http://localhost:8081/api/values) en el navegador.


# AMBIENTE QA / DEV

Ejecutar contenedores
```bash
docker-compose -f docker-compose-dev.yml up -d
```

Restaurar backups de base de datos MASTERGOVCO y SUIT3 (Despues validar la conexion a BD)
```bash
chmod +x ./sqlserver/init/init.sh
docker exec -it tramites_y_servicios_db /var/opt/mssql/backup/init.sh
```